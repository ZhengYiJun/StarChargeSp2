package com.h3bpm.starcharge.bean;

public enum  SealApply {
    /**
     * SealApply
     */
    SealDept,
    Operator,
    DateTime,
    SealFileName,
    FileCount,
    FileType,
    CappingSeal,
    Attachments,
    Memo,
    HigherAudit,
    HigherUpAudit,
    Signature
}
