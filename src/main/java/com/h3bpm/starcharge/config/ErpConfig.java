package com.h3bpm.starcharge.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ErpConfig {

    @Value("http://www.wbdhoa.com:8081/")
    private String url;

    public String getUrl() {
        return url;
    }
}
