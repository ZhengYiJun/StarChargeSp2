package com.h3bpm.starcharge.support.core.annontation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ClassName ESealRequestParam
 * @Desciption 上上签请求参数映射注解
 * @Author lvyz
 * @Date 2019/6/11 10:40
 **/
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ESealRequestParam {

	// 上上签字段名-中文
	String eSealField() default "";

	// BPM字段名-英文
	String bpmField() default "";

	boolean table() default false;

	// 子表名称
	String tableName() default "";

	// 上上签子表列属性名称
	String[] eSealColumnFields() default {};

	// 子表属性
	String[] bpmColumnFields() default {} ;


}
