package com.h3bpm.starcharge.controller.k3c;

import OThinker.H3.Controller.ControllerBase;
import com.h3bpm.starcharge.common.bean.RetResponse;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.ErrorUtil;
import com.h3bpm.starcharge.common.uitl.k3c.K3cUtil;
import com.h3bpm.starcharge.config.K3CloundConfig;
import com.h3bpm.starcharge.service.k3c.K3cService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author LLongAgo
 * @date 2019/4/25
 * @since 1.0.0
 */
@Controller
@RequestMapping("/k3c/general")
public class GeneralController extends ControllerBase {

    @Autowired
    private K3cUtil util;
    @Autowired
    private K3cService k3cService;
    @Autowired
    private K3CloundConfig k3CloundConfig;

    @Override
    public String getFunctionCode() {
        return null;
    }

    /**
     * 分配
     *
     * @param formId 表单id
     * @param orgId  分配组织id
     * @param id     id
     * @return RetResult
     */
    @RequestMapping(value = "/allocate", produces = "application/json;charset=utf-8")
    @ResponseBody
    public RetResult allocate(@RequestParam String formId, @RequestParam String orgId, @RequestParam String id) {
        try {
            String cookie = util.getLoginCookie();
            // 拼接分配参数
            String allocateParam = k3cService.buildAllocateParam(formId, orgId, id);
            return k3cService.save(k3CloundConfig.getUrl() + k3CloundConfig.getAllocate(), cookie, allocateParam);
        } catch (Exception e) {
            return ErrorUtil.generalException(e);
        }
    }

}