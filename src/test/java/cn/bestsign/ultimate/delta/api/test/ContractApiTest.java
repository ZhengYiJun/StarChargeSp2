package cn.bestsign.ultimate.delta.api.test;


import api.client.BestSignClient;
import api.domain.contract.create.*;
import api.domain.contract.sign.SignContractVO;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class ContractApiTest {
    final String privateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC1dJfnXe216cOBi9yY61iZXDouDgDMitwOM4gCUWPJ9KlXUsbOG0LnEt9jUxsBRMcvkfnFaVn2ZLC7KxieaIdnIIGw1rN2a0WFJIwBuFrH1yaUUfgMGNOUJjhiph0JLOhQroLntUh//WycHZy8Phw6tQ7S/5f5FKVQi6HCMtcHok70HwU72Axr+e56F4pVGAcaMp7YXjdOmpFPXx6j6GF8FYh2unXUYrBGRH/5rHqAo0Ag8A2zkL47aKCvkucfeZiu4tuehR3unrI4tdAm/CpntsJ2BYvmhRgqntBHK3CW6Gme88jjepbSEkhDjROzlK0QMJeqU+s5HUM1BtLFJtPtAgMBAAECggEACM6w9NT1Tcgb6jTMr0t1EHSOil+5oDP5PGM57crfihTrB0cISUa/d5HN7/c/r08UT/XI5tEXQcNfZKZR2pZ+Q/4q7VdufIf2ZuEAPrEhDuQdhkN4Q7YMxvsX74najPB2Ejx2NCLzcurtE6LIUca9/gd9wbYQPVHIOGGep1tqXn9oF4/QeuFsFZK9GTS5TLGCekREW5gH57Eo2DHSiDlKTgCqjir6+yF2mRPWGGiaLKtDIBsOM2qpxI3C8yjgD5DKy3SrZ/vBe9kBeJ6VZzJqNGYmC7A/FQhdTMD9mshZUPQD3g9z6kmLkxBs0Q3nSGN/j/QBkcuGCal5gO0na3BcbQKBgQD+/GGVBCsCNax/K7LL+MZsLDOrKW1jsWdO8X49Ep/CnF1Exuz38dvQiW+hFo2K3gRB5ZkENWWy+HL+Z0rnwSI2jbRttMxN9q9Sdb8dRG1TazZtLIsOvz1oMF9VZ5PXn68To3oKNVIm1TGh7TdhqMS0VXPvpBiRLsWHX28Xf/6tTwKBgQC2LViECiIjUlKxONb7P68bT1Bs+YMs9329ESEf2ohloW6pql4ZMro9nJr+QPt6mUAgUyWZWWaVLuC/54+a5Y5SGDYBYFVN3iSLVIsttjq1R/YJVD0u6BGo92hqEmCfimQqHaci5WjAGD1zRpVoI9vOSAKhR7u6MkyIstqUrG10AwKBgEK10Onlr0LivACBdEO9EFyYq+Pp8L6WWUrkD3z29Gk784Lc8H5l/nZuno/skJd2QnLjGMdrGPJb4eoBKC2976+KH1xcYt863N+cAqYrktayRAkIEFGJYw1xKl/zu1A8bNece39UN+wE9vlAUK7yMpRjjvNxYSQKso8aPrxNNlotAoGAU7FpZN+y5z8+tiRCv5J2Q7mgXTATz2iz31QrP5MJ7obHbDLUoAbqALwdiIkZ/yzAhRktwNGNiyPKJN+g3axwQc7VoLQ8/FT9vPTOK+X3+qhgo9CLey0qT9G5qmFe+mx9r8uHqURzZyy7rmXS5dDzfkUe0DFAUT4iYvqn6H1+mzkCgYBUGred7m5xXs96xaHHVM5Y2mqpLszHndSMqDMMKUKIWSFBuOsGIqHfua/f+ZWHX4IbvveG5sEtYdBy4xReZRrW/yYcHL+/dIkP4DwE9unlwFnpKQ8F7Ynv7sWx+nilI0RuveSqt3M6HwgpQwaE9WCbhMvr6Gai9yASJuDQYwSttQ==";

    private final BestSignClient bestSignClient = new BestSignClient(
            "https://api.bestsign.info",
            "l2ZxwSA6OBs6IgsQ6og9f7ixuPMR7hMQ",
            "3seTeUqUyhGlYZfD3CbGVXPDrcu1ieG4",
            privateKey
    );


    private CreateLabel getLable(SignFlowConstants.SignLabelType labelType, float shift){
        CreateLabel slabelVO1 = new CreateLabel();
        slabelVO1.setDocumentOrder(0);
        slabelVO1.setHeight(50f);
        slabelVO1.setWidth(50f);
        slabelVO1.setPageNumber(1);
        slabelVO1.setX(200f+shift);
        slabelVO1.setY(200f+shift);
        slabelVO1.setType(labelType);
        return slabelVO1;
    }

    private String getFileBase64Content(String filePath) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(filePath);
        byte[] bytes = new byte[fileInputStream.available()];
        fileInputStream.read(bytes);
        return Base64.getEncoder().encodeToString(bytes);
    }

    @Test
    public void testSignContract(){
        SignContractVO signContractVO = new SignContractVO();
        signContractVO.setSealName("测试 是是");
        List<Long> contractIds = Arrays.asList(2042451531952816137L,2042451531952816136L,2042451531952816135L);
        signContractVO.setContractIds(contractIds);
        String string = bestSignClient.executeRequest("/api/contracts/sign","POST",signContractVO);

        System.out.println( string);
    }


    /**
     * 合同下载接口，返回的是zip
     * @throws IOException
     */
    @Test
    public void contractsDownload() throws IOException {
        SignContractVO signContractVO = new SignContractVO();
        List<Long> contractIds = Arrays.asList(2074548388677812233L);
        signContractVO.setContractIds(contractIds);
        String base64 = bestSignClient.executeRequest("/api/contracts/download-file", "POST", signContractVO);

        byte[] bytes = Base64.getDecoder().decode(base64);

        Files.write(Paths.get("C:\\Users\\leon\\Desktop\\2074548388677812233测试.zip"),bytes);

        System.out.println(base64);
    }
}
