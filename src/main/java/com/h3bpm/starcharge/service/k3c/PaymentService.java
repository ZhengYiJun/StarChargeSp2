package com.h3bpm.starcharge.service.k3c;

import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.starcharge.common.bean.RetResult;

/**
 * @author LLongAgo
 * @date 2019/4/22
 * @since 1.0.0
 */
public interface PaymentService {
    /**
     * 电费预付
     * @param instanceData InstanceData
     * @return RetResult
     */
    RetResult electricChargePre(InstanceData instanceData) throws Exception;
}