package cn.bestsign.ultimate.delta.api.test;

import api.APITemplateInvoke;
import api.domain.template.create.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author whthomas
 * @date 2018/7/4
 */
public class TemplateAPITest extends BestSignClientTest {


    private APITemplateInvoke invoke;

    @Before
    public void init() {
        invoke = new APITemplateInvoke(bestSignClient);
    }

    @Test
    public void testSendTemplates() {
        SendContractsFromTemplateVO sendContractsFromTemplateVO = new SendContractsFromTemplateVO();
        sendContractsFromTemplateVO.setTemplateId(2018846479799549958L);
        DefaultConfig defaultConfig = new DefaultConfig();
        defaultConfig.setContractTitle("小兔兔测试api使用模板发合同");
        defaultConfig.setExpireDays(30);
        defaultConfig.setSignOrdered(false);

        sendContractsFromTemplateVO.setDefaultConfig(defaultConfig);
        List<Role> roleList = new ArrayList<>();

        //如果是可变签署方(使用place holder的notification )
        //该id需要和模板中的对应，可通过查询模板获取
        //这里的值可以覆盖模板已经设置好的值
        Role signerRole = new Role();
        signerRole.setFaceVerify(true);
        signerRole.setRoleId(2018846539702599687L);
        roleList.add(signerRole);

//        *
//         *这个notification只对固定签署方(贵公司)生效，如果不设置，会取模板中设置的，可变签署方取placeHolder中的
//         Role fixRole = new Role();
//         fixRole.setUserAccount("15757181054");
//         fixRole.setUserName("舒敬潋");
//         fixRole.setEnterpriseName("宝鸡有一群怀揣着梦想的少年相信在牛大叔的带领下会创造生命的奇迹网络科技有限公司");
//         fixRole.setNotification("15700080775");
//         fixRole.setFaceVerify(true);
//         //fixRole.setRoleId()
//         roleList.add(fixRole);

        sendContractsFromTemplateVO.setRoles(roleList);
        List<PlaceHolder> placeHolderList = new ArrayList<>();
        PlaceHolder placeHolder1 = new PlaceHolder();
        placeHolder1.setNotification("15700080775");
        placeHolder1.setUserAccount("15700080775");
        placeHolder1.setUserName("郑小雪");
        //这个字段必填
        placeHolder1.setEnterpriseName("公司y");
        placeHolder1.setContractTitle("小兔兔测试api发送模板合同1");
        List<LabelVO> labelVOS = new ArrayList<>();
        //对每个合同接收方的定制信息，比如说性别，身份证
        labelVOS.add(getLabel("性别(合同字段,必填)", "女"));
        labelVOS.add(getLabel("收件人填写(合同字段,必填)", "女"));
        placeHolder1.setTextLabels(labelVOS);

        PlaceHolder placeHolder2 = new PlaceHolder();
        placeHolder2.setNotification("15700000006");
        placeHolder2.setUserAccount("15700000006");
        placeHolder2.setUserName("小兔兔");
        placeHolder2.setEnterpriseName("公司x");
        placeHolder2.setContractTitle("小兔兔测试api发送模板合同2");
        //对每个合同接收方的定制信息，比如说性别，身份证
        List<LabelVO> labelVOS2 = new ArrayList<>();
        labelVOS2.add(getLabel("性别(合同字段,必填)", "男"));
        labelVOS2.add(getLabel("收件人填写(合同字段,必填)", "男"));

        placeHolder2.setTextLabels(labelVOS2);

        placeHolderList.add(placeHolder1);
        placeHolderList.add(placeHolder2);
        sendContractsFromTemplateVO.setPlaceHolders(placeHolderList);

        final String result = bestSignClient.executeRequest("/api/templates/send-contracts-sync", "post", sendContractsFromTemplateVO);
        System.out.println(result);
    }

    private LabelVO getLabel(String name, String value) {
        LabelVO labelVO = new LabelVO();
        labelVO.setName(name);
        labelVO.setValue(value);
        return labelVO;
    }

    @Test
    public void testQueryTemplate() {

        invoke.queryTemplate(2018846479799549958L);

    }

}
