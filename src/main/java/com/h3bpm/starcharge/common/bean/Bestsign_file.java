package com.h3bpm.starcharge.common.bean;

public class Bestsign_file {

    /*
    * 	CREATE TABLE `Bestsign_file` (
  `id` char(36) NOT NULL,
  `instanceId` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `fileName` varchar(200) DEFAULT NULL,
  `fileType` varchar(200) DEFAULT NULL,
  `Content` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    *
    *
    * */
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String contractId;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    /**
     * 流程id。
     */
    private String instanceId;

    /**
     *  preview 提交前预览
     *
     *  previewAfter 提交后预览
     *
     *  download  下载
     *
     */
    private String type;

    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件类型
     */
    private String fileType;

    /**
     * 文件流
     */
    private byte[] content;


    public Bestsign_file() {
    }

    public Bestsign_file(String instanceId, String type, String fileName, String fileType, byte[] content) {
        this.instanceId = instanceId;
        this.type = type;
        this.fileName = fileName;
        this.fileType = fileType;
        this.content = content;
    }

    public Bestsign_file(String id, String contractId, String instanceId, String type, String fileName, String fileType, byte[] content) {
        this.id = id;
        this.contractId = contractId;
        this.instanceId = instanceId;
        this.type = type;
        this.fileName = fileName;
        this.fileType = fileType;
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
