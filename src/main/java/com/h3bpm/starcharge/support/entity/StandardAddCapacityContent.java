package com.h3bpm.starcharge.support.entity;

import com.h3bpm.starcharge.support.core.BasicContractsField;
import com.h3bpm.starcharge.support.core.annontation.ESealRequestParam;

import java.util.List;
import java.util.Map;

/**
 * @ClassName StandardAddCapacityContent
 * @Desciption
 * @Author lvyz
 * @Date 2019/6/11 9:26
 **/
public class StandardAddCapacityContent extends BasicContractsField {

	@ESealRequestParam(eSealField = "报装容量", bpmField = "addPower")
	private String addPower;

	@ESealRequestParam(eSealField = "增容方式", bpmField = "addPowerMode")
	private String addPowerMode;

	@ESealRequestParam(eSealField = "充电设施确认期限", bpmField = "lifeTime")
	private String lifeTime;

	@ESealRequestParam(eSealField = "结算方式", bpmField = "settlementCycle")
	private String settlementCycle;

	@ESealRequestParam(eSealField = "桩群名称", bpmField = "pileGroupName")
	private String pileGroupName;

	@ESealRequestParam(eSealField = "开放时间段", bpmField = "openTime")
	private String openTime;

	@ESealRequestParam(eSealField = "充电桩详细位置", bpmField = "pileGroupAddr")
	private String pileGroupAddr;

	@ESealRequestParam(eSealField = "收费类型", bpmField = "parkRateType")
	private String parkRateType;

	@ESealRequestParam(eSealField = "收费标准", bpmField = "parkRateStandard")
	private String parkRateStandard;

	@ESealRequestParam(eSealField = "电价约定", bpmField = "powerRatePlan")
	private String powerRatePlan;

	@ESealRequestParam(eSealField = "充电服务费", bpmField = "serviceFee")
	private String serviceFee;

	@ESealRequestParam(eSealField = "分享服务费（%）", bpmField = "ratio")
	private String ratio;

	//规格型号( 无(KW) )
	@ESealRequestParam(
			table = true,
			tableName = "StandardAddCapacityPile",
			eSealColumnFields = {"产品名称", "规格型号", "数量", "单位"},
			bpmColumnFields = {"pileName", "pileSpec", "pileQty", "pileGunQty"})
	private List<Map<String, String>> standardAddCapacityPile;

	@ESealRequestParam(eSealField = "其他", bpmField = "specialAgreement")
	private String specialAgreement;

	@ESealRequestParam(eSealField = "合同有效期", bpmField = "contractTermOfValidity")
	private String contractTermOfValidity;

	@ESealRequestParam(eSealField = "协议顺延年限", bpmField = "postponedYear")
	private String postponedYear;

	@ESealRequestParam(eSealField = "合同份数", bpmField = "contactQty")
	private String contactQty;

	@ESealRequestParam(eSealField = "乙方执合同数", bpmField = "pBContactQty")
	private String pBContactQty;

}
