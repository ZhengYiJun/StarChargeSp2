package com.h3bpm.starcharge.support.eseal;

import OThinker.Common.Organization.Models.User;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import api.APIController;
import api.domain.contract.create.CreateContractVO;
import api.domain.contract.sign.SignContractVO;
import api.domain.contract.sign.Signer;
import api.domain.template.create.SendContractsSyncVO;
import com.alibaba.fastjson.JSONObject;
import com.h3bpm.base.util.AppUtility;
import com.h3bpm.starcharge.common.uitl.BestsignPropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @ClassName ESealService
 * @Desciption 上上签（电子章）抽象类
 * @Author lvyz
 * @Date 2019/6/10 17:25
 **/
public abstract class AbstractESeal implements ESealHandler {

	private static final Logger log = LoggerFactory.getLogger(AbstractESeal.class);

	static final String ACCOUNT = BestsignPropertiesUtil.getProperty("account");

	static final String USERNAME = BestsignPropertiesUtil.getProperty("account");

	static final APIController API_HANDLER = new APIController();

	static SignContractVO mySignContract = new SignContractVO();

	private static final String MY_ACCOUNT = "starcharge.dehe@wanbangauto.com";

	private static final String MY_COMPANY_NAME = "万帮充电设备有限公司";

	private static final String MY_SEAL_NAME = "万帮充电合同章";

	// 初始化本公司为发件人
	static {
		mySignContract.setSigner(new Signer(MY_ACCOUNT, MY_COMPANY_NAME));
		mySignContract.setSealName(MY_SEAL_NAME);
	}

	/**
	 * 同步合同到上上签
	 * @param instanceId
	 * @return
	 */
	@Override
	public String synContract(String instanceId){

		try {

			InstanceData instanceData = new InstanceData(AppUtility.getEngine(), instanceId, User.AdministratorID);

			// 取审批流数据
			Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();

			// 设置请求参数
			SendContractsSyncVO sendParam = this.createParams(valueTable);

			log.info("同步标准合同到上上签 | {}", JSONObject.toJSONString(sendParam));

			return API_HANDLER.sendContractsSync(sendParam);


		} catch (Exception e) {

			log.error("同步标准合同到上上签异常 MSG {}", e.getMessage());
			e.printStackTrace();

		}

		return null;
	}

	/**
	 * 上传合同到上上签
	 * @param instanceId
	 * @return
	 */
	@Override
	public String uploadContract(String instanceId){

		try {

			InstanceData instanceData = new InstanceData(AppUtility.getEngine(), instanceId, User.AdministratorID);

			CreateContractVO sendParam = this.createParams(instanceData);

			log.info("同步非标准合同到上上签 | {}", JSONObject.toJSONString(sendParam));

			return API_HANDLER.createContract(sendParam);


		} catch (Exception e) {

			log.error("同步标准合同到上上签异常 MSG {}", e.getMessage());
			e.printStackTrace();

		}

		return null;
	}

	/**
	 * 盖章
	 * @param contractId
	 * @param sealName 章名
	 * @param companyName 盖章的公司名称
	 */
	@Override
	public void signature(String contractId, String sealName, String companyName) {

		SignContractVO signContractVO = new SignContractVO();

		// 设置发件人
		signContractVO.setSigner(new Signer(ACCOUNT, companyName));
		signContractVO.setSealName(sealName);

		// 设置需要盖章的合同ID
		List<Long> contractIds = Collections.singletonList(Long.parseLong(contractId));
		signContractVO.setContractIds(contractIds);

		// 执行盖章
		API_HANDLER.sign(signContractVO);

	}

	/**
	 * 盖章
	 * @param contractId
	 */
	@Override
	public void signature(String contractId) {

		// 设置需要盖章的合同ID
		List<Long> contractIds = Collections.singletonList(Long.parseLong(contractId));
		mySignContract.setContractIds(contractIds);

		// 执行盖章
		API_HANDLER.sign(mySignContract);
	}

	/**
	 * 标准合同上传参数
	 * @param valueTable
	 * @return
	 * @throws Exception
	 */
	protected abstract SendContractsSyncVO createParams(Map<String, Object> valueTable) throws Exception;

	/**
	 * 非标准合同上传参数
	 * @param instanceData
	 * @return
	 * @throws Exception
	 */
	protected abstract CreateContractVO createParams(InstanceData instanceData) throws Exception;
}
