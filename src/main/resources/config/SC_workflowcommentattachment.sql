CREATE TABLE [dbo].[SC_workflowcommentattachment] (
  [id] char(36) COLLATE Chinese_PRC_CI_AS NOT NULL,
  [commentId] char(36) COLLATE Chinese_PRC_CI_AS NOT NULL,
  [content] varbinary(max)  NOT NULL,
  [contentType] varchar(200) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [contentLength] int  NULL,
  [fileName] nvarchar(400) COLLATE Chinese_PRC_CI_AS  NULL,
  [description] nvarchar(1000) COLLATE Chinese_PRC_CI_AS  NULL,
  [display] bit DEFAULT ((0)) NOT NULL,
  [extension] nvarchar(10) COLLATE Chinese_PRC_CI_AS  NULL,
  [url] nvarchar(200) NULL,
  [gmtCreate] datetime DEFAULT (getdate()) NOT NULL,
  [gmtModified] datetime DEFAULT (getdate()) NOT NULL,
  CONSTRAINT [pk_workflowcommentattach] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
)  
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[SC_workflowcommentattachment] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcommentattachment',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'评论id',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcommentattachment',
'COLUMN', N'commentId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'内容',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcommentattachment',
'COLUMN', N'content'
GO

EXEC sp_addextendedproperty
'MS_Description', N'附件类型',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcommentattachment',
'COLUMN', N'contentType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'附件大小',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcommentattachment',
'COLUMN', N'contentLength'
GO

EXEC sp_addextendedproperty
'MS_Description', N'附件名',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcommentattachment',
'COLUMN', N'fileName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'附件描述',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcommentattachment',
'COLUMN', N'description'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否需要在页面直接显示（仅对图片起作用，如果用附件形式上传的话，一律为0）',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcommentattachment',
'COLUMN', N'display'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcommentattachment',
'COLUMN', N'gmtCreate'
GO

EXEC sp_addextendedproperty
'MS_Description', N'修改时间',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcommentattachment',
'COLUMN', N'gmtModified'