package com.h3bpm.starcharge.controller.erp;

import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.Instance.Data.IInstanceDataItem;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.ErrorUtil;
import com.h3bpm.starcharge.ret.k3c.K3cBase;
import com.h3bpm.starcharge.service.erp.ErpGuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/erp/ErpGuest")
public class ErpGuestController  extends ControllerBase {

    @Autowired
    private  ErpGuestService erpGuestService;

    @Override
    public String getFunctionCode() {
        return null;
    }

    /**
     * 新增客户
     * @param instanceId    instanceId
     * @return RetResult
     */
    @RequestMapping(value = "/save", produces = "application/json;charset=utf-8")
    @ResponseBody
    public RetResult save(@RequestParam String instanceId) {

        try {
           InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            RetResult result=erpGuestService.save(instanceData);
            // 将编码写回表单
            IInstanceDataItem item = instanceData.getItem("guestCode");
            if (item != null) {
                K3cBase data = (K3cBase) result.getData();
                item.setValue(data.getNumber());
                instanceData.Submit();
            }
            return result;
        } catch (Exception e) {
            return ErrorUtil.generalException(e);
        }
    }
}
