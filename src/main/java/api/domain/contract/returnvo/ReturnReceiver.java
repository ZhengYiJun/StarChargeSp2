package api.domain.contract.returnvo;

public class ReturnReceiver {

    private String receiveId;

    public String getReceiveId() {
        return receiveId;
    }

    public void setReceiveId(String receiveId) {
        this.receiveId = receiveId;
    }

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private String enterpriseId;

    public String getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(String enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    /**
     * 参与人标识
     */
    private String receiverId;
    /**
     * 账号
     */
    private String userAccount;
    /**
     * 企业名称
     */
    private String enterpriseName;
    /**
     * 姓名
     */
    private String userName;
    /**
     * 用户类型
     */
    private String userType;
    /**
     * 参与人种类
     */
    private String receiverType;
    /**
     * 签署链接
     */
    private String operateUrl;
    /**
     * 操作短链接
     */
    private String shortOperateUrl;


    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    public String getOperateUrl() {
        return operateUrl;
    }

    public void setOperateUrl(String operateUrl) {
        this.operateUrl = operateUrl;
    }

    public String getShortOperateUrl() {
        return shortOperateUrl;
    }

    public void setShortOperateUrl(String shortOperateUrl) {
        this.shortOperateUrl = shortOperateUrl;
    }
}
