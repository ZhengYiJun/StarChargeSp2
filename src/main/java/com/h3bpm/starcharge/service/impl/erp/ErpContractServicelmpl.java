package com.h3bpm.starcharge.service.impl.erp;

import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.alibaba.fastjson.JSONObject;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.OkHttpUtil;
import com.h3bpm.starcharge.common.uitl.SqlUtils;
import com.h3bpm.starcharge.config.ErpConfig;
import com.h3bpm.starcharge.service.erp.ErpContractService;
import data.DataRow;
import data.DataTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ErpContractServicelmpl implements ErpContractService {

    @Autowired
    private ErpConfig erpConfig;
    /**
     * 合同通过驳回
     * @param instanceData  InstanceData
     * @param flag  flag
     * @return RetResult
     */
    @Override
    public RetResult pass(String instanceId, String flag, String type) throws Exception {
//        HashMap<String, Object> valueTable = instanceData.getBizObject().getValueTable();
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("id", valueTable.get("customerId"));
//        jsonObject.put("name", valueTable.get("customername"));
//        System.out.println(jsonObject.toJSONString());
        Map<String, String> map = new HashMap<>(4);
//        map.put("contractID", valueTable.get("contractID").toString())
        map.put("instanceId", instanceId);
        map.put("flag", flag);

        String url=erpConfig.getUrl();
//判断type的值1为合约审批，2为调试审批，3为上线审批
        if(type.equals("1"))
        {
            url =url+ "Customer/IBPMContractToERP";
        } else if(type.equals("2"))
        {
            //从数据库中得到调试意见
            String sql = "select Text from OT_comment where BizObjectSchemaCode='erpPileOnline' and DataField='debugInfo' " +
                    "and InstanceId='860cb1a1-5960-43f9-a81a-829f8dffe13d' order by CreatedTime desc";

            DataTable dataTable = SqlUtils.doQuery(sql);
            String debupInfo="";
            if (null != dataTable) {

                String debupText = "";

                if (dataTable.getRows().size() > 0) {
                    DataRow dataRow = dataTable.getRows().get(0);
                    debupText = dataRow.getString(0);
                }
                debupInfo=debupText;
            }


            map.put("debupInfo", debupInfo);
            url =url+ "Stub/IBPMContractDebugToERP";
        }
        else if(type.equals("3"))
        {
            url =url+ "Stub/IBPMContractOnlineToERP";
        }
        else
        {
            url =url;
        }



//        String url ="http://localhost:8089/Customer/IBPMContractToERP";
        String dwe;
        dwe= OkHttpUtil.post(url , map);
        JSONObject retData = JSONObject.parseObject(dwe);

        RetResult result= new RetResult();
        result.setCode("200");
        result.setMsg("secuss");
        result.setData(retData);

        return result;
    }
}
