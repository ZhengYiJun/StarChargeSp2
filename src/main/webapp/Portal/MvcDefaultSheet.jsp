<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" session="false" %>
<%@ page import="OThinker.Common.DotNetToJavaStringHelper" %>
<%@ page import="OThinker.H3.Controller.MvcSheet.MvcDefaultSheet" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="com.h3bpm.base.util.SqlUtils" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="gt-ie8 gt-ie9 not-ie">

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <%
        boolean IsMobile = false;
        if ("true".equals(request.getParameter("IsMobile"))) {
            IsMobile = true;
        }
        boolean IsPrint= false;
        if ("print".equals(request.getParameter("IsMobile"))) {
            IsPrint = true;
        }
        String PortalRoot = request.getContextPath() + "/Portal";
        String Mode = request.getParameter("Mode");
        String Command = request.getParameter("Command");
        String htmlContent = "";
        String submit_content = "";
        String localLan = request.getParameter("localLan");
        //add by linjh@Future 2018.9.25 sql 注入
        localLan = StringEscapeUtils.escapeSql(localLan);
        if (Command == null || Command.isEmpty()) {
            MvcDefaultSheet mdc = new MvcDefaultSheet(request, response);
            //用户信息失效跳转至登录页面 update by zhangj
            if (null == mdc.getActionContext().getUser()) {
                request.getRequestDispatcher(PortalRoot + "/Portal/index.html").forward(request, response);
                return;
            }
            request.setAttribute("Mode", Mode);
            request.setAttribute("Command", Command);
            boolean isEdit = mdc.IsEditInstanceData();
            if (isEdit) {
                if (!mdc.getActionContext().getUser().ValidateBizObjectAdmin(
                        mdc.getActionContext().getSchemaCode(),
                        "",
                        mdc.getActionContext().getBizObject().getOwnerId())) {
                    response.flushBuffer();
                } else {
                    htmlContent = mdc.getActionContext().getSheet().getRuntimeContentStr();
                    if (htmlContent.isEmpty()) {
                        String bizObjectID = request.getParameter("BizObjectID");
                        String schemaCode = request.getParameter("SchemaCode");
                        //add by linjh@Future 2018.9.25 sql 注入
                        bizObjectID = SqlUtils.escapeSql(bizObjectID);
                        schemaCode = StringEscapeUtils.escapeSql(schemaCode);
                        htmlContent = mdc.OnInitByInstanceID(bizObjectID, schemaCode);
                    }
                }
            } else {
                htmlContent = mdc.getActionContext().getBizSheetOnly().getRuntimeContentStr();
//                    System.out.println(htmlContent);
                if (htmlContent.isEmpty()) {
                    if ("Originate".equals(Mode)) {
                        //发起
                        String WorkflowCode = request.getParameter("WorkflowCode");
                        int WorkflowVersion = -1;
                        if (!DotNetToJavaStringHelper.isNullOrEmpty(request.getParameter("WorkflowVersion"))) {
                            WorkflowVersion = Integer.valueOf(request.getParameter("WorkflowVersion"));
                        }
                        //Update by linjh:如果WorkflowVersion=-1，则默认设置为1。
                        if (WorkflowVersion == -1) {
                            WorkflowVersion = 1;
                        }
                        if (!DotNetToJavaStringHelper.isNullOrEmpty(request.getParameter("SchemaCode"))) {
                            WorkflowCode = request.getParameter("SchemaCode");
                        }
                        htmlContent = mdc.OnInitByWorkflow(WorkflowCode, WorkflowVersion);
                    } else {
                        String WorkItemID = request.getParameter("WorkItemID");
                        //add by linjh@Future 2018.9.25 sql 注入
                        if (WorkItemID != null && SqlUtils.sql_inj(WorkItemID)) {
                            return;
                        }
                        WorkItemID = SqlUtils.escapeSql(WorkItemID);
                        //若WorkItemID为空，则根据InstanceID和SheetCode查询表单信息
                        if (DotNetToJavaStringHelper.isNullOrEmpty(WorkItemID)) {
                            String InstanceID = request.getParameter("InstanceId");
                            String SheetCode = request.getParameter("SchemaCode");

                            htmlContent = mdc.OnInitByInstanceID(InstanceID, SheetCode);
                        } else {
                            //根据WorkItemID查询表单信息
                            htmlContent = mdc.OnInitByWorkItemID(WorkItemID);
                        }
                    }
                }
            }
            request.setAttribute("Command", "load");
        } else {
            request.setAttribute("Command", Command);
            if ("Submit".equals(Command)) {
                if (!submit_content.isEmpty()) {
                    response.getWriter().write(submit_content);
                }
            }
        }
    %>
    <script type="text/javascript">
        var IsMobile = "<%=IsMobile%>" == "true";
        <%--var _localLan = "<%=localLan%>";--%>
        var local = window.localStorage.getItem("H3.Language"); // 本地语言
        var _localLan = local || 'zh_cn';

        window.localStorage.setItem("H3.Language", _localLan);

        var _PORTALROOT_GLOBAL = "<%=PortalRoot%>";
        if (typeof (pageInfo) != "undefined") {
            pageInfo.LockImage = "WFRes/images/WaitProcess.gif";
        }

        var OnSubmitForm = function () {
            if (IsMobile) {
                return false;
            }
            return true;
        }
    </script>

    <%
        if (IsMobile) {
    %>
    <%--移动端--%>

    <%--<link href="Mobile/lib/ionic/css/ionic.min.css" rel="stylesheet"/>--%>
    <link href="https://cdn.bootcss.com/ionic/1.3.2/css/ionic.min.css" rel="stylesheet">
    <link href="Mobile/css/formMobile/detail.min.css" rel="stylesheet"/>
    <link href="Mobile/css/fonts.min.css?v=201802081023" rel="stylesheet"/>
    <link href="WFRes/css/MvcSheetMobileNew.min.css?v=20190616" rel="stylesheet" type="text/css"/>
    <link href="Mobile/lib/ion-datetime-picker/release/ion-datetime-picker.min.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="Mobile/js/dingTalkPC.min.js?v=201901171113"></script>
    <script type="text/javascript" charset="utf-8"
            src="Mobile/lib/ionic/js/ionic.bundle.min.js?v=201802081023"></script>
    <%--<script src="https://cdn.bootcss.com/ionic/1.3.2/js/ionic.bundle.min.js"></script>--%>
    <script type="text/javascript" charset="utf-8" src="Mobile/js/ngIOS9UIWebViewPatch.js?v=201802081023"></script>
    <%--<script type="text/javascript" charset="utf-8" src="Mobile/form/jquery-2.1.3.min.js?v=201802081023"></script>--%>
    <script src="https://cdn.bootcss.com/jquery/2.1.3/jquery.min.js"></script>
    <%--<script type="text/javascript" charset="utf-8" src="Mobile/lib/ngCordova/ng-cordova.js?v=201802081023"></script>--%>
    <script src="https://cdn.bootcss.com/ng-cordova/0.1.23-alpha/ng-cordova.js"></script>
    <%--<script type="text/javascript" charset="utf-8" src="Mobile/lib/oclazyload/ocLazyLoad.js?v=201802081023"></script>--%>
    <script src="https://cdn.bootcss.com/oclazyload/0.5.0/ocLazyLoad.js"></script>
    <script type="text/javascript" charset="utf-8" src="Mobile/js/dingTalk.min.js?v=201802081023"></script>
    <script type="text/javascript" charset="utf-8" src="Mobile/form/formApp.js?v=201905232320"></script>
    <script type="text/javascript" charset="utf-8" src="Mobile/form/formservices.js?v=201905232320"></script>
    <script type="text/javascript" charset="utf-8" src="Mobile/form/formDirectives.js?v=201802081023"></script>
    <script type="text/javascript" charset="utf-8" src="Mobile/form/formControllers.js?v=201906030956"></script>
    <script type="text/javascript" charset="utf-8" src="Mobile/lib/ion-datetime-picker/release/ion-datetime-picker.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="Mobile/form/services/sheetQuery.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="Mobile/form/services/httpService.js?v=201802081023"></script>
    <script type="text/javascript" charset="utf-8" src="Mobile/form/services/sheetUserService.js?v=201802081020"></script>
    <script type="text/javascript" charset="utf-8" src="Mobile/form/filters/highlightFilter.min.js?v=201802081023"></script>

    <script type="text/javascript" charset="utf-8" src="vendor/angular/angular-file/ng-file-upload-shim.min.js?v=201802081023"></script>
    <script type="text/javascript" charset="utf-8" src="vendor/angular/angular-file/ng-file-upload.min.js?v=201802081023"></script>

    <%
    } else {
    %>
    <link rel="stylesheet" href="WFRes/editor/themes/default/default.css"/>
    <link rel="stylesheet" href="WFRes/editor/plugins/code/prettify.css"/>

    <link href="https://cdn.bootcss.com/twitter-bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="WFRes/assets/stylesheets/pixel-admin.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="WFRes/assets/stylesheets/themes.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="WFRes/_Content/themes/ligerUI/Aqua/css/ligerui-all.css"
          rel="stylesheet" type="text/css"/>
    <link href="WFRes/css/MvcSheet.css?v=20180306123" rel="stylesheet" type="text/css"/>
    <link href="WFRes/css/MvcSheetPrint.css" rel="stylesheet"
          type="text/css" media="print"/>
    <link rel="shortcut icon" type="image/x-icon"
          href="WFRes/images/favicon.ico" media="screen"/>

    <%--<script type="text/javascript">--%>
    <%--document.write("<script src=\"WFRes/_Scripts/jquery/jquery.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")--%>
    <%--</script>--%>
    <script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.js"></script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/jquery/ajaxfileupload.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/jquery/jquery.lang.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/ligerUI/ligerui.all.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/Calendar/WdatePicker.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/editor/kindeditor-all.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/editor/lang/zh_CN.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <%
        }
    %>

    <%--<script type="text/javascript">
        document.write("<script src=\"js/aes.js?v=20180608134659\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>--%>

    <script type="text/javascript">
        document.write("<script src=\"starCharge/js/MoneyChange.min.js?v=201803211727\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>

    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/SheetControls.min.js?v=201905232320\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/MvcSheetUI.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetQuery.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetAttachment.min.js?v=20190618\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetCheckbox.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetCheckboxList.min.js?v=20180103\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetComment.min.js?v=201905232320\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetDropDownList.min.js?v=20180107\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetGridView.min.js?v=20180103\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetHiddenField.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetHyperLink.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetInstancePrioritySelector.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetLabel.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetOffice.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetRadioButtonList.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetRichTextBox.min.js?v=201803201735\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetTextBox.min.js?v=201800329797\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetTime.min.js?v=20171223\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetToolbar.min.js?v=201905232320\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetUser.min.js?v=201803161103\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetTimeSpan.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetCountLabel.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetOriginatorUnit.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/Controls/SheetRelationInstance.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MvcSheet/MvcSheet.js?v=201906051604\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"js/jQuery.md5.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MVCRuntime/Sheet.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MVCRuntime/Sheet.Computation.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MVCRuntime/Sheet.Display.min.js\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/_Scripts/MVCRuntime/Sheet.Validate.min.js?v=201803290978\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"js/sweetalert.min.js?v=20181009\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>

    <style type="text/css">
        .item {
            border-bottom: 0px;
            padding: 6px;
        }

        .item-checkbox {
            padding-left: 60px;
        }

        .list {
            margin-bottom: 0px;
        }
    </style>
    <%
        if (IsMobile) {
    %>
    <script type="text/javascript">

    </script>
    <%
        }
    %>
</head>
<body class="theme-default main-menu-animated"
      style="background-color: rgb(204, 204, 204);">
<!--onsubmit="return false":避免ENTER键回传页面-->
<form id="form1" name="form1" onsubmit="return OnSubmitForm();" class="form1 test">
    <%--PC端框架总是加载--%>
    <div class="main-container container sheetContent" id="sheetContent" style="display: none">
        <div class="panel">
            <div id="main-navbar"
                 class="navbar navbar-inverse toolBar mainnavbar" role="navigation">
                <div class="navbar-inner">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed"
                                data-toggle="collapse" data-target="#main-navbar-collapse">
                            <i class="navbar-icon fa fa-bars"></i>
                        </button>
                    </div>
                    <div id="main-navbar-collapse"
                         class="collapse navbar-collapse main-navbar-collapse">
                        <ul class="nav navbar-nav SheetToolBar test" id="divTopBars">
                            <!-- <div id="cphMenu" >
                                    <li data-action="Submit"><a href="javascript:void(0);">
                                                    <i class="panel-title-icon fa fa-check toolImage"></i> <span
                                                    class="toolText" data-en_us="Submit">提交</span>
                                    </a></li>
                            </div> -->
                        </ul>
                    </div>
                </div>
            </div>
            <div id="content-wrapper">
                <%=htmlContent%>
            </div>
        </div>
    </div>

    <%
        if (IsMobile) {
    %>
    <div id="ionicForm" ng-app="formApp" ng-controller="mainCtrl">
        <ion-nav-view></ion-nav-view>
    </div>

    <%
    } else {
    %>
    <script>
        var init = [];
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/assets/javascripts/bootstrap.min.js?201412041112\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        document.write("<script src=\"WFRes/assets/javascripts/pixel-admin.min.js?201412041112\" language=\"JavaScript\" charset=\"UTF-8\"><\/script>")
    </script>
    <script type="text/javascript">
        init.push(function () {
            var w = 12;//$("textarea[data-richtextbox]").length > 0 ? 12 : 0;
            $(window).resize(
                function () {
                    $("#main-navbar").css("width",
                        $("#main-navbar").parent().width() - w);
                    //表单按钮展示超出一行的加下拉按钮并隐藏
                    var topBarWidth = 0;
                    var topBarFlag = true;
                    //获取所有按钮的总宽
                    $("#divTopBars").children("li").each(function () {
                        topBarWidth += ($(this).width() + 1);
                    })

                    if (topBarWidth > ($("#main-navbar").width() - 20) && ($("#divTopBars").children("li").width() + 1) != ($("#main-navbar").width() - 20)) {
                        $("#divTopBars").css({
                            "height": $("#divTopBars").children("li").height(),
                            "overflow": "hidden",
                            "padding-right": "20px"
                        });
                        $("#divTopBars").after("<i class='glyphicon glyphicon-chevron-down' id='dropTopBars' style='position:absolute;right:3px;top:15px;color:dodgerblue;font-size:16px;'></i>");

                        $("#divTopBars").on("click", function (event) {
                            // alert('111')
                            if (event.target.nodeName == "UL") {
                                if (topBarFlag) {
                                    topBarFlag = false;
                                    $("#divTopBars").css({"height": "inherit", "overflow": "inherit"});
                                    $("i#dropTopBars").addClass("glyphicon-chevron-up").removeClass("glyphicon-chevron-down");
                                } else {
                                    topBarFlag = true;
                                    $("#divTopBars").css({
                                        "height": $("#divTopBars").children("li").height(),
                                        "overflow": "hidden"
                                    });
                                    $("i#dropTopBars").addClass("glyphicon-chevron-down").removeClass("glyphicon-chevron-up");
                                }
                            }
                        });
                    } else {
                        $("i#dropTopBars").hide();
                    }
                });
            $("#main-navbar").css("width", $("#main-navbar").parent().width() - w);
        })
        window.PixelAdmin.start(init);

        $(function () {
            $("[id*=sheetContent]").show();
            //执行入口
            // console.log("start init : " + new Date());
            $.MvcSheet.Init();
            $("#lblSequenceNoTitle").text("BPM单号");


            // console.log("end init : " + new Date());
            <%
                if (IsPrint) {
            %>
            //实现表单打印
            $.ajax({
                url : "/Portal/print/parseHtml",// 后台方法名称 作用于主表
                type : "post",
                cache : false,
                //contentType: "application/json;charsetset=UTF-8",
                data : {
                    printContent:$("#content-wrapper").html()
                },  // 输入参数 JOSN格式
                dataType : "json",
                success : function(e) {

                    window.open(e.url);
                    //var url = window.location.href.replace('&IsMobile=print', '&IsMobile=true');
                    //window.location.href = url;
                    window.history.back(-1);
                }
            });
            <%
                }
            %>
        })
    </script>
    <%
        }
    %>
</form>
</body>

<!-- add by luwei -->
<script type="text/javascript">
    $(function () {
        /* 绑定click方法 TODO 可能其他方法也需要绑定 */
        $("[data-onclick]").each(function () {
            var functionString = $(this).data("onclick");
            $(this).bind('click', function () {
                eval(functionString);
            })
        });

        /* OnlyData 不显示 label */
        $("#divSheet").find("[data-bindtype]").each(
            function () {
                //系统字段
                var sysKeyWords = ["ActivityCode", "ActivityName",
                    "InstanceId", "InstanceName", "InstancePriority",
                    "OriginateTime", "SequenceNo", "Originator",
                    "Originator.FullName", "Originator.LoginName",
                    "Originator.UserName", "Originator.UserID",
                    "Originator.Email", "Originator.EmployeeNumber",
                    "Originator.EmployeeRank",
                    "Originator.Appellation", "Originator.OfficePhone",
                    "Originator.Mobile", "Originator.OUName",
                    "Originator.OUFullName", "Originator.OU"
                ];
                var datafield = $(this).data("datafield");
                if (sysKeyWords.indexOf(datafield)) {
                    return;
                }
                if ($(this).data("bindtype") == "OnlyData") {
                    $(this).hide();
                }
            })
        var speed = 200;
        $('body,html').animate({scrollTop: 0}, speed);
    })

</script>
</html>