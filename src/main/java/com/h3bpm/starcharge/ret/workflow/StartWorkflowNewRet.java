package com.h3bpm.starcharge.ret.workflow;

import lombok.Data;

/**
 * StartWorkflowNewRet class
 *
 * @author llongago
 * @date 2019/2/18
 */
@Data
public class StartWorkflowNewRet {
    /**
     * 是否成功
     */
    private boolean success;
    /**
     * 系统返回消息
     */
    private String message;
    /**
     * 启动的流程实例ID
     */
    private String instanceID;
    /**
     * 第一个节点的ItemID
     */
    private String workItemID;
    /**
     * 第一个节点的url
     */
    private String workItemUrl;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(String instanceID) {
        this.instanceID = instanceID;
    }

    public String getWorkItemID() {
        return workItemID;
    }

    public void setWorkItemID(String workItemID) {
        this.workItemID = workItemID;
    }

    public String getWorkItemUrl() {
        return workItemUrl;
    }

    public void setWorkItemUrl(String workItemUrl) {
        this.workItemUrl = workItemUrl;
    }
}