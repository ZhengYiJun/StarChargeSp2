package com.h3bpm.starcharge.bean;

public enum  ChargePolicy {

    /**
     * 星星充电大客户充电政策合作协议
     *
     * select name+',' as Nname from syscolumns where id=(select max(id) from sysobjects where xtype='u' and name='I_InvestmentContract')
     */

    ObjectID,
    CreatedTime,
    contractNo,
    ParentPropertyName,
    ModifiedBy,
    subAccount,
    Name,
    chargingCapacity,
    OwnerId,
    ModifiedTime,
    cityName,
    addressB,
    CappingSeal,
    ParentIndex,
    RunningInstanceId,
    partyBBank,
    ParentObjectID,
    CreatedBy,
    partyBOpen,
    partyBTax,
    telB,
    partyB,
    CreatedByParentId,
    OwnerParentId,
    contractId,
    effectiveDate,
    partyBAccount,
    OurUnitName,
    effectiveDate1,
    startDate,
    endDate,
    totalPrice,
    serviceCost,
    otherPolicies,
    /**
     * 子表
     */
    chargePolicyDetail,

    monthlyCharge,
    monthlyDiscount,
}
