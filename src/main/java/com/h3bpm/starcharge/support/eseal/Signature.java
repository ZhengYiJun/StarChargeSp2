package com.h3bpm.starcharge.support.eseal;

import OThinker.H3.Entity.Instance.Data.InstanceData;
import api.domain.contract.create.CreateContractVO;
import api.domain.template.create.SendContractsSyncVO;

import java.util.Map;

/**
 * @ClassName Signature
 * @Desciption 盖章
 * @Author lvyz
 * @Date 2019/6/18 10:51
 **/
public class Signature extends AbstractESeal {

	@Override
	@Deprecated
	protected SendContractsSyncVO createParams(Map<String, Object> valueTable) throws Exception {
		return null;
	}

	@Override
	@Deprecated
	protected CreateContractVO createParams(InstanceData instanceData) throws Exception {
		return null;
	}



}
