package com.h3bpm.starcharge.common.bean;

/**
 * BaseResult bean
 *
 * @author LLongAgo
 * @date 2019/3/4
 * @since 1.0.0
 */
public class BaseResult {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public BaseResult() {
    }

    public BaseResult(String name, String value) {
        this.name = name;
        this.value = value;
    }
}