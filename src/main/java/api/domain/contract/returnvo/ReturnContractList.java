package api.domain.contract.returnvo;

import java.util.List;

public class ReturnContractList {

    private List<ReturnContract> contracts;

    public List<ReturnContract> getContracts() {
        return contracts;
    }

    public void setContracts(List<ReturnContract> contracts) {
        this.contracts = contracts;
    }
}
