package com.h3bpm.starcharge.support.entity;

import com.h3bpm.starcharge.support.core.BasicContractsField;
import com.h3bpm.starcharge.support.core.annontation.ESealRequestParam;

import java.util.List;
import java.util.Map;

public class StandardCommodityContent extends BasicContractsField {

    @ESealRequestParam(eSealField = "质保期", bpmField = "WarrantyPeriod")
    private String warrantyPeriod;

    @ESealRequestParam(eSealField = "交货期", bpmField = "DeliveryDate")
    private String deliveryDate;

    @ESealRequestParam(eSealField = "交货地点", bpmField = "DeliveryAdd")
    private String deliveryAdd;

    @ESealRequestParam(eSealField = "交付期限", bpmField = "DeliveryPeriod")
    private String deliveryPeriod;

    @ESealRequestParam(eSealField = "设备总价款", bpmField = "EquipmentTotal")
    private String equipmentTotal;

    @ESealRequestParam(eSealField = "设备预付款", bpmField = "FirstPayment")
    private String firstPayment;

    @ESealRequestParam(eSealField = "托管费支付方式", bpmField = "PayWay")
    private String payWay;

    @ESealRequestParam(eSealField = "付款周期", bpmField = "PayPeriod")
    private String payPeriod;

    @ESealRequestParam(eSealField = "月支付", bpmField = "PeriodicPay")
    private String periodicPay;

    @ESealRequestParam(eSealField = "其他付款方式", bpmField = "Others")
    private String others;

    @ESealRequestParam(eSealField = "委托开始时间", bpmField = "OCStart")
    private String oCStart;

    @ESealRequestParam(eSealField = "委托结束时间", bpmField = "OCEnd")
    private String oCEnd;

    @ESealRequestParam(eSealField = "交流终端运营费", bpmField = "ACOC")
    private String aCOC;

    @ESealRequestParam(eSealField = "直流终端运营费", bpmField = "DCOC")
    private String dCOC;

    @ESealRequestParam(eSealField = "客户选项", bpmField = "OSProject")
    private String oSProject;

    @ESealRequestParam(eSealField = "单价", bpmField = "CuxSUP")
    private String cuxSUP;

    @ESealRequestParam(
            table = true,
            tableName = "StandardCommodityPile",
            eSealColumnFields = {"产品名称", "规格型号", "数量", "单价"},
            bpmColumnFields = {"pileName", "pileSpec", "pileQty", "pileGunQty"})
    private List<Map<String, String>> standardCommodityPile;



}
