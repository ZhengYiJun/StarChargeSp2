package com.h3bpm.starcharge.common.uitl;

import com.h3bpm.starcharge.common.bean.JsonResult;
import com.h3bpm.starcharge.common.bean.RestfulErrorCode;
import com.h3bpm.starcharge.common.bean.RetResponse;
import com.h3bpm.starcharge.common.bean.RetResult;
import org.apache.poi.ss.formula.functions.T;

/**
 * 错误工具
 *
 * @author LLongAgo
 * @date 2019/3/9
 * @since 1.0.0
 */
public class ErrorUtil {

    /**
     * 封装服务器错误
     * @param e Exception
     * @return JsonResult
     */
    public static JsonResult packageServerError(Exception e) {
        e.printStackTrace();
        return new JsonResult<>(RestfulErrorCode.ERROR.getCode(), e.getMessage());
    }

    public static RetResult generalException(Exception e) {
        e.printStackTrace();
        return RetResponse.makeErrRsp(e.getMessage());
    }

}