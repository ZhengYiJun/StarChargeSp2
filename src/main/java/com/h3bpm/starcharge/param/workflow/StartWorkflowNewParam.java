package com.h3bpm.starcharge.param.workflow;

import lombok.Data;

/**
 * StartWorkflowNewParam class
 *
 * @author llongago
 * @date 2019/2/18
 */
@Data
public class StartWorkflowNewParam {
    /**
     * 流程模板编码
     */
    private String workflowCode;
    /**
     * 用户编码
     */
    private String userCode;
    /**
     * 是否结束第一个活动
     */
    private String finishStart;
    /**
     * 流程实例启动初始化数据项集合（JSON格式）
     */
    private String paramValues;

    public StartWorkflowNewParam () {
    }

    public StartWorkflowNewParam(String workflowCode, String userCode, String finishStart, String paramValues) {
        this.workflowCode = workflowCode;
        this.userCode = userCode;
        this.finishStart = finishStart;
        this.paramValues = paramValues;
    }

    public String getWorkflowCode() {
        return workflowCode;
    }

    public void setWorkflowCode(String workflowCode) {
        this.workflowCode = workflowCode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getFinishStart() {
        return finishStart;
    }

    public void setFinishStart(String finishStart) {
        this.finishStart = finishStart;
    }

    public String getParamValues() {
        return paramValues;
    }

    public void setParamValues(String paramValues) {
        this.paramValues = paramValues;
    }
}