package com.h3bpm.starcharge.bean.k3c;

import java.util.List;

/**
 * @author LLongAgo
 * @date 2019/4/10
 * @since 1.0.0
 */
public class CustomerInfo {
    private String id;
    private String customerName;
    private String customerGroup;
    private String cognateType;
    private String useOrganization;
    private String customerCode;
    private String country;
    private String province;
    private String city;
    private String address;
    private String relationOrg;
    private String currency;
    private String clearType;
    private String collection;
    private String taxType;
    private String invoiceType;
    private String defaultTax;
    private String FPTT;
    private String NSDJH;
    private String KHYH;
    private String YHZH;
    private String KPTXDZ;
    private String KPLXDH;
    private String customerGroupId;
    private String relationOrgId;

    private List<Linkman> linkman;
    private List<BankInfo> bankInfo;

    public static class Linkman {
        private String linkName;
        private String mobile;
        private String type;

        public String getLinkName() {
            return linkName;
        }

        public void setLinkName(String linkName) {
            this.linkName = linkName;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class BankInfo {
        private String id;
        private String bank;
        private String account;
        private String accountName;
        private String address;
        private String currency;

        public String getBank() {
            return bank;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setBank(String bank) {
            this.bank = bank;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getAccountName() {
            return accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public void setCustomerGroup(String customerGroup) {
        this.customerGroup = customerGroup;
    }

    public String getCognateType() {
        return cognateType;
    }

    public void setCognateType(String cognateType) {
        this.cognateType = cognateType;
    }

    public String getUseOrganization() {
        return useOrganization;
    }

    public void setUseOrganization(String useOrganization) {
        this.useOrganization = useOrganization;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRelationOrg() {
        return relationOrg;
    }

    public void setRelationOrg(String relationOrg) {
        this.relationOrg = relationOrg;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getClearType() {
        return clearType;
    }

    public void setClearType(String clearType) {
        this.clearType = clearType;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getDefaultTax() {
        return defaultTax;
    }

    public void setDefaultTax(String defaultTax) {
        this.defaultTax = defaultTax;
    }

    public String getFPTT() {
        return FPTT;
    }

    public void setFPTT(String FPTT) {
        this.FPTT = FPTT;
    }

    public String getNSDJH() {
        return NSDJH;
    }

    public void setNSDJH(String NSDJH) {
        this.NSDJH = NSDJH;
    }

    public String getKHYH() {
        return KHYH;
    }

    public void setKHYH(String KHYH) {
        this.KHYH = KHYH;
    }

    public String getYHZH() {
        return YHZH;
    }

    public void setYHZH(String YHZH) {
        this.YHZH = YHZH;
    }

    public String getKPTXDZ() {
        return KPTXDZ;
    }

    public void setKPTXDZ(String KPTXDZ) {
        this.KPTXDZ = KPTXDZ;
    }

    public String getKPLXDH() {
        return KPLXDH;
    }

    public void setKPLXDH(String KPLXDH) {
        this.KPLXDH = KPLXDH;
    }

    public String getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(String customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public String getRelationOrgId() {
        return relationOrgId;
    }

    public void setRelationOrgId(String relationOrgId) {
        this.relationOrgId = relationOrgId;
    }

    public List<Linkman> getLinkman() {
        return linkman;
    }

    public void setLinkman(List<Linkman> linkman) {
        this.linkman = linkman;
    }

    public List<BankInfo> getBankInfo() {
        return bankInfo;
    }

    public void setBankInfo(List<BankInfo> bankInfo) {
        this.bankInfo = bankInfo;
    }
}