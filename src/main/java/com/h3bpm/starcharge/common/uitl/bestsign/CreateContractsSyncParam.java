package com.h3bpm.starcharge.common.uitl.bestsign;


import OThinker.Common.DateTimeUtil;
import OThinker.H3.Entity.DataModel.BizObject;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import api.domain.template.create.*;
import com.h3bpm.starcharge.bean.*;
import com.h3bpm.starcharge.common.bean.CommonSales;
import com.h3bpm.starcharge.common.uitl.BestsignPropertiesUtil;
import com.h3bpm.starcharge.pojo.WorkFlowCode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class CreateContractsSyncParam {

    private static final String ACCOUNT = BestsignPropertiesUtil.getProperty("account");

    public static SendContractsSyncVO createParam(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {

            switch (WorkFlowCode.valueOf(instanceData.getSchemaCode())) {
                case sales:
                    sendContractsSyncVO = createSales(instanceData);
                    break;
                case OEMStandardEngineering:
                    sendContractsSyncVO = createOEMStandardEngineering(instanceData);
                    break;
                case hostFactorySale:
                    sendContractsSyncVO = createHostFactorySale(instanceData);
                    break;
                case ChargePolicy:
                    sendContractsSyncVO = CreateInvestmentContract.chargePolicy(instanceData);
                    break;
                case NonVehicleCharging:
                    sendContractsSyncVO = CreateInvestmentContract.nonVehicleCharging(instanceData);
                    break;
                case AgentSigning:
                    sendContractsSyncVO = CreateInvestmentContract.agentSigning(instanceData);
                    break;
                case SupplementaryAgreement:
                    sendContractsSyncVO = CreateInvestmentContract.supplementaryAgreement(instanceData);
                    break;
                case OperatingContract:
                    sendContractsSyncVO = CreateInvestmentContract.operatingContract(instanceData);
                    break;
                case AgreementNoPartyC:
                    sendContractsSyncVO = CreateInvestmentContract.agreementNoPartyC(instanceData);
                    break;
                case CooperationAgreement:
                    sendContractsSyncVO = CreateInvestmentContract.cooperationAgreement(instanceData);
                    break;
                case CooperationAgree:
                    sendContractsSyncVO = CreateInvestmentContract.cooperationAgree(instanceData);
                    break;
                case EnergyVehicles:
                    sendContractsSyncVO = CreateInvestmentContract.energyVehicles(instanceData);
                    break;
                case SupplyContract:
                    sendContractsSyncVO = CreateInvestmentContract.supplyContract(instanceData);
                    break;
                case ChargingFacilities:
                    sendContractsSyncVO = CreateInvestmentContract.chargingFacilities(instanceData);
                    break;
                case SiteLeaseContract:
                    sendContractsSyncVO = CreateInvestmentContract.siteLeaseContract(instanceData);
                    break;
                case GeneralProject:
                    sendContractsSyncVO = CreateProductContract.generalProject(instanceData);
                    break;
                case LowProcurement:
                    sendContractsSyncVO = CreateProductContract.lowProcurement(instanceData);
                    break;
                case projectDesign:
                    sendContractsSyncVO = CreateProductContract.createProjectDesign(instanceData);
                    break;
                case zeroStarSupport:
                    sendContractsSyncVO = CreateProductContract.createZeroStarSupport(instanceData);
                    break;
                case newspaperService:
                    sendContractsSyncVO = CreateProductContract.createNewspaperService(instanceData);
                    break;
                case MembraneStructure:
                    sendContractsSyncVO = CreateProductContract.createMembraneStructure(instanceData);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }

    /**
     * 主机厂标准工程合同
     * @param instanceData
     * @return
     * @throws Exception
     */
    public static SendContractsSyncVO createOEMStandardEngineering(InstanceData instanceData) throws Exception {
        SendContractsSyncVO sendContractsSyncVO = new SendContractsSyncVO();
        sendContractsSyncVO.setSender(new Sender("",""));
        Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
        String contractType = String.valueOf(valueTable.get(OEMStandardEngineering.contractType.name()));

        List<PlaceHolder> placeHolderList = new ArrayList<>();
        List<LabelVO> labelVOList = new ArrayList<>();
        labelVOList.add(new LabelVO("合同编号",String.valueOf(valueTable.get(OEMStandardEngineering.contractNo.name()))));
        String templateId = "";
        List<Role> roles = new ArrayList<>();
        if("新能源汽车充电设施安装施工协议".equals(contractType)) {
            templateId = "2156330220678810631";
            placeHolderList.add(new PlaceHolder(ACCOUNT,ACCOUNT,(String)valueTable.get(OEMStandardEngineering.partyB.name()),labelVOList));
            roles.add(new Role(2156331064186904577L,String.valueOf(valueTable.get(OEMStandardEngineering.telA.name())),
                    String.valueOf(valueTable.get(OEMStandardEngineering.partyA1.name())),String.valueOf(valueTable.get(OEMStandardEngineering.partyA1.name()))));
            addOEMConstructionLabelVO(labelVOList, valueTable);
        } else {
            templateId = "2156442849300712451";
            placeHolderList.add(new PlaceHolder(ACCOUNT,ACCOUNT,(String)valueTable.get(OEMStandardEngineering.partyA1.name()),labelVOList));
            roles.add(new Role(2156443560814059521L,String.valueOf(valueTable.get(OEMStandardEngineering.telB.name())),
                    String.valueOf(valueTable.get(OEMStandardEngineering.partyB.name())),String.valueOf(valueTable.get(OEMStandardEngineering.partyB.name()))));
            addOMECooperationLabelVO(labelVOList, valueTable);
        }
        sendContractsSyncVO.setTemplateId(templateId);
        sendContractsSyncVO.setRoles(roles);
        sendContractsSyncVO.setPlaceHolders(placeHolderList);

        return sendContractsSyncVO;
    }

    /**
     * 标准销售
     * @param instanceData
     * @return
     * @throws Exception
     */
    public static SendContractsSyncVO createSales(InstanceData instanceData) throws Exception {
        SendContractsSyncVO sendContractsSyncVO = new SendContractsSyncVO();
        sendContractsSyncVO.setSender(new Sender("",""));
        Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
        String contractType = String.valueOf(valueTable.get(Sales.contractType.name()));
        List<PlaceHolder> placeHolderList = new ArrayList<>();
        List<LabelVO> labelVOList = new ArrayList<>();
        labelVOList.add(new LabelVO("电子邮箱",String.valueOf(valueTable.get(Sales.emailA.name()))));
        labelVOList.add(new LabelVO("乙方电子邮箱",String.valueOf(valueTable.get(Sales.emailB.name()))));
        labelVOList.add(new LabelVO("甲方",String.valueOf(valueTable.get(Sales.partyA.name()))));
        labelVOList.add(new LabelVO("合同类型",String.valueOf(valueTable.get(Sales.contractType.name()))));
        labelVOList.add(new LabelVO("甲方地址",String.valueOf(valueTable.get(Sales.addressA.name()))));
        labelVOList.add(new LabelVO("甲方法定代表人",String.valueOf(valueTable.get(Sales.legalRepresentA.name()))));
        labelVOList.add(new LabelVO("甲方法定代表人职务",String.valueOf(valueTable.get(Sales.legalRepresentPostA.name()))));
        labelVOList.add(new LabelVO("甲方授权代表人",String.valueOf(valueTable.get(Sales.authRepresentA.name()))));
        labelVOList.add(new LabelVO("甲方授权代表人职务",String.valueOf(valueTable.get(Sales.authRepresentPostA.name()))));
        labelVOList.add(new LabelVO("甲方传真",String.valueOf(valueTable.get(Sales.faxA.name()))));
        labelVOList.add(new LabelVO("甲方开户行",String.valueOf(valueTable.get(Sales.openBankA.name()))));
        labelVOList.add(new LabelVO("甲方税务登记号",String.valueOf(valueTable.get(Sales.taxRegistNoA.name()))));
        labelVOList.add(new LabelVO("甲方账号",String.valueOf(valueTable.get(Sales.bankAccountA.name()))));
        labelVOList.add(new LabelVO("乙方法定代表人",String.valueOf(valueTable.get(Sales.legalRepresentB.name()))));
        labelVOList.add(new LabelVO("乙方法定代表人职务",String.valueOf(valueTable.get(Sales.legalRepresentPostB.name()))));
        labelVOList.add(new LabelVO("乙方授权代表人",String.valueOf(valueTable.get(Sales.authRepresentB.name()))));
        labelVOList.add(new LabelVO("乙方授权代表人职务",String.valueOf(valueTable.get(Sales.authRepresentPostB.name()))));
        labelVOList.add(new LabelVO("乙方传真",String.valueOf(valueTable.get(Sales.faxB.name()))));
        labelVOList.add(new LabelVO("其他条约",String.valueOf(valueTable.get(Sales.otherContent.name()))));
        List<Role> roles = new ArrayList<>();
        // 合同模板id
        String templateId = null;
        if (contractType != null) {
            String[] contractTypeArr = contractType.split("\\+");
            if (contractTypeArr.length == 1) {
                if (CommonSales.PURCHASECONTRACTNAME.equals(contractTypeArr[0])) {
                    templateId = CommonSales.PURCHASE_ID;
                    roles.add(new Role(2156411330364440584L,String.valueOf(valueTable.get(Sales.telA.name())),
                            String.valueOf(valueTable.get(Sales.partyA.name())),String.valueOf(valueTable.get(Sales.partyA.name()))));
                    addPurchaseLabelVO(labelVOList, valueTable);
                } else {
                    templateId = CommonSales.CONSTRUCTION_ID;
                    roles.add(new Role(2156331064186904577L,String.valueOf(valueTable.get(Sales.telA.name())),
                            String.valueOf(valueTable.get(Sales.partyA.name())),String.valueOf(valueTable.get(Sales.partyA.name()))));
                    addConstructionLabelVO(labelVOList, valueTable);
                }
            } else if (contractTypeArr.length == 2) {
                if (contractType.indexOf(CommonSales.CONSTRUCTIONCONTRACTNAME) != -1) {
                    templateId = CommonSales.PURCHASE_CONSTRUCTION_ID;
                    roles.add(new Role(2156451702075560968L,String.valueOf(valueTable.get(Sales.telA.name())),
                            String.valueOf(valueTable.get(Sales.partyA.name())),String.valueOf(valueTable.get(Sales.partyA.name()))));
                    addPurchaseLabelVO(labelVOList, valueTable);
                    addConstructionLabelVO(labelVOList, valueTable);
                } else {
                    addPurchaseLabelVO(labelVOList, valueTable);
                    addEntrustLabelVO(labelVOList,valueTable);
                    templateId = CommonSales.PURCHASE_ENTRUST_ID;
                    roles.add(new Role(2157130999606021122L,String.valueOf(valueTable.get(Sales.telA.name())),
                            String.valueOf(valueTable.get(Sales.partyA.name())),String.valueOf(valueTable.get(Sales.partyA.name()))));
                }
            } else if (contractTypeArr.length == 3) {
                templateId = CommonSales.PURCHASE_CONSTRUCTION_ENTRUST_ID;
                roles.add(new Role(2156408362609150984L,String.valueOf(valueTable.get(Sales.telA.name())),
                        String.valueOf(valueTable.get(Sales.partyA.name())),String.valueOf(valueTable.get(Sales.partyA.name()))));
                addPurchaseLabelVO(labelVOList, valueTable);
                addConstructionLabelVO(labelVOList, valueTable);
                addEntrustLabelVO(labelVOList,valueTable);
            } else {
                templateId = "";
            }
        }
        sendContractsSyncVO.setTemplateId(templateId);
        placeHolderList.add(new PlaceHolder(ACCOUNT,ACCOUNT,String.valueOf(valueTable.get(Sales.OurUnitName.name())),labelVOList));
        sendContractsSyncVO.setRoles(roles);
        sendContractsSyncVO.setPlaceHolders(placeHolderList);
        return sendContractsSyncVO;
    }

    private static void addPurchaseLabelVO(List<LabelVO> labelVOList, Map<String, Object> valueTable) {
        labelVOList.add(new LabelVO("采购合同编号", String.valueOf(valueTable.get(Sales.purchaseContractNo.name()))));
        labelVOList.add(new LabelVO("合计总价（小写）",String.valueOf(valueTable.get(Sales.totalAmountLower.name()))));
        labelVOList.add(new LabelVO("合计总价（大写）",String.valueOf(valueTable.get(Sales.totalAmountUpper.name()))));
        labelVOList.add(new LabelVO("质保开始时间",String.valueOf(valueTable.get(Sales.qualityStartTime.name()))));
        labelVOList.add(new LabelVO("调试时间",String.valueOf(valueTable.get(Sales.debuggerTime.name()))));
        labelVOList.add(new LabelVO("质保期",String.valueOf(valueTable.get(Sales.warrantyPeriod.name()))));
        labelVOList.add(new LabelVO("交货开始时间",String.valueOf(valueTable.get(Sales.deliveryTime.name()))));
        labelVOList.add(new LabelVO("交货期",String.valueOf(valueTable.get(Sales.deliveryPeriod.name()))));
        labelVOList.add(new LabelVO("交货地点",String.valueOf(valueTable.get(Sales.deliveryAddress.name()))));
        labelVOList.add(new LabelVO("交货联系人",String.valueOf(valueTable.get(Sales.deliveryContacts.name()))));
        labelVOList.add(new LabelVO("发货联系电话",String.valueOf(valueTable.get(Sales.deliveryTel.name()))));
        labelVOList.add(new LabelVO("设备总价款",String.valueOf(valueTable.get(Sales.equipTotalAmount.name()))));
        labelVOList.add(new LabelVO("其他",String.valueOf(valueTable.get(Sales.otherContent.name()))));
        if("其他".equals(valueTable.get(Sales.equipPaymentMethod.name()))) {
            labelVOList.add(new LabelVO("设备付款方式", String.valueOf(valueTable.get(Sales.equipPaymentMdOther.name()))));
        } else {
            labelVOList.add(new LabelVO("设备付款方式", String.valueOf(valueTable.get(Sales.equipPaymentMethod.name()))));
        }
        labelVOList.add(new LabelVO("设备预付款",String.valueOf(valueTable.get(Sales.equipAdvancePay.name()))));
        labelVOList.add(new LabelVO("甲方电话",String.valueOf(valueTable.get(Sales.telA.name()))));
        // 子表
        BizObject[] zbBizObjects = (BizObject[]) valueTable.get(Sales.equipmentInfo.name());
        for (int i = 1;i <= zbBizObjects.length;i++) {
            Map<String, Object> zbValueTable = null;
            try {
                zbValueTable = zbBizObjects[i-1].getValueTable();
                String str = "";
                if (i!=1){
                    str = String.valueOf(i);
                }
                labelVOList.add(new LabelVO("产品名称"+str,String.valueOf(zbValueTable.get(Sales.productName.name()))));
                labelVOList.add(new LabelVO("规格型号"+str,String.valueOf(zbValueTable.get(Sales.specificationModels.name()))));
                labelVOList.add(new LabelVO("单位"+str,String.valueOf(zbValueTable.get(Sales.unit.name()))));
                labelVOList.add(new LabelVO("数量"+str,String.valueOf(zbValueTable.get(Sales.qty.name()))));
                labelVOList.add(new LabelVO("单价"+str,String.valueOf(zbValueTable.get(Sales.price.name()))));
                labelVOList.add(new LabelVO("总价"+str,String.valueOf(zbValueTable.get(Sales.amount.name()))));
                labelVOList.add(new LabelVO("备注"+str,String.valueOf(zbValueTable.get(Sales.equipRemark.name()))));
            } catch (Exception e) {
            }
        }
    }
    private static void addOEMConstructionLabelVO(List<LabelVO> labelVOList, Map<String, Object> valueTable) {
        labelVOList.add(new LabelVO("甲方",String.valueOf(valueTable.get(OEMStandardEngineering.partyA1.name()))));
        labelVOList.add(new LabelVO("施工总价款",String.valueOf(valueTable.get(OEMStandardEngineering.constructionPrice.name()))));
        labelVOList.add(new LabelVO("施工总价大写",String.valueOf(valueTable.get(OEMStandardEngineering.constructionPriceUpper.name()))));
        labelVOList.add(new LabelVO("施工地点",String.valueOf(valueTable.get(OEMStandardEngineering.constructionSite.name()))));
        labelVOList.add(new LabelVO("施工结算价格",String.valueOf(valueTable.get(OEMStandardEngineering.constructionSettlement.name()))));
        labelVOList.add(new LabelVO("施工工期",String.valueOf(valueTable.get(OEMStandardEngineering.constructionTime.name()))));
        labelVOList.add(new LabelVO("施工预付款",String.valueOf(valueTable.get(OEMStandardEngineering.constructionPayment.name()))));
        labelVOList.add(new LabelVO("施工尾款",String.valueOf(valueTable.get(OEMStandardEngineering.constructionPaymentFinal.name()))));
        labelVOList.add(new LabelVO("甲方地址",String.valueOf(valueTable.get(OEMStandardEngineering.addressA.name()))));
        labelVOList.add(new LabelVO("甲方法定代表人",String.valueOf(valueTable.get(OEMStandardEngineering.legalRepresentA.name()))));
        labelVOList.add(new LabelVO("甲方法定代表人职务",String.valueOf(valueTable.get(OEMStandardEngineering.legalRepresentPostA.name()))));
        labelVOList.add(new LabelVO("甲方授权代表人",String.valueOf(valueTable.get(OEMStandardEngineering.authRepresentA.name()))));
        labelVOList.add(new LabelVO("甲方授权代表人职务",String.valueOf(valueTable.get(OEMStandardEngineering.authRepresentPostA.name()))));
        labelVOList.add(new LabelVO("甲方电话",String.valueOf(valueTable.get(OEMStandardEngineering.telA.name()))));
        labelVOList.add(new LabelVO("甲方传真",String.valueOf(valueTable.get(OEMStandardEngineering.faxA.name()))));
        labelVOList.add(new LabelVO("甲方开户行",String.valueOf(valueTable.get(OEMStandardEngineering.openBankA.name()))));
        labelVOList.add(new LabelVO("甲方税务登记号",String.valueOf(valueTable.get(OEMStandardEngineering.taxRegistNoA.name()))));
        labelVOList.add(new LabelVO("甲方账号",String.valueOf(valueTable.get(OEMStandardEngineering.bankAccountA.name()))));
        labelVOList.add(new LabelVO("乙方法定代表人",String.valueOf(valueTable.get(OEMStandardEngineering.legalRepresentB.name()))));
        labelVOList.add(new LabelVO("乙方法定代表人职务",String.valueOf(valueTable.get(OEMStandardEngineering.legalRepresentPostB.name()))));
        labelVOList.add(new LabelVO("乙方授权代表人",String.valueOf(valueTable.get(OEMStandardEngineering.authRepresentB.name()))));
        labelVOList.add(new LabelVO("乙方授权代表人职务",String.valueOf(valueTable.get(OEMStandardEngineering.authRepresentPostB.name()))));
        labelVOList.add(new LabelVO("乙方传真",String.valueOf(valueTable.get(OEMStandardEngineering.faxB.name()))));
    }
    private static void addOMECooperationLabelVO(List<LabelVO> labelVOList, Map<String, Object> valueTable) {
        labelVOList.add(new LabelVO("乙方",String.valueOf(valueTable.get(OEMStandardEngineering.partyB.name()))));
        labelVOList.add(new LabelVO("注册地址",String.valueOf(valueTable.get(OEMStandardEngineering.registeredAddress.name()))));
        labelVOList.add(new LabelVO("服务区域",String.valueOf(valueTable.get(OEMStandardEngineering.installationArea.name()))));
        labelVOList.add(new LabelVO("合同服务内容类型",String.valueOf(valueTable.get(OEMStandardEngineering.contractServiceType.name()))));
        labelVOList.add(new LabelVO("安装费",String.valueOf(valueTable.get(OEMStandardEngineering.basicInstallationFee.name()))));
        labelVOList.add(new LabelVO("大写",String.valueOf(valueTable.get(OEMStandardEngineering.basicInstallationUpper.name()))));
        labelVOList.add(new LabelVO("电缆长度",String.valueOf(valueTable.get(OEMStandardEngineering.cableLength.name()))));
        labelVOList.add(new LabelVO("电缆超出长度",String.valueOf(valueTable.get(OEMStandardEngineering.excessCableLength.name()))));
        labelVOList.add(new LabelVO("电缆单价",String.valueOf(valueTable.get(OEMStandardEngineering.cablePrice.name()))));
        labelVOList.add(new LabelVO("含不含税",String.valueOf(valueTable.get(OEMStandardEngineering.whetherTax.name()))));
        labelVOList.add(new LabelVO("税率",String.valueOf(valueTable.get(OEMStandardEngineering.rate.name()))));
        labelVOList.add(new LabelVO("勘测服务费",String.valueOf(valueTable.get(OEMStandardEngineering.surveyFee.name()))));
        labelVOList.add(new LabelVO("报装服务费（小写）",String.valueOf(valueTable.get(OEMStandardEngineering.surveyInstallationFee.name()))));
        labelVOList.add(new LabelVO("费用结算标准",String.valueOf(valueTable.get(OEMStandardEngineering.settlementStandard.name()))));
        labelVOList.add(new LabelVO("合同开始时间",DateTimeUtil.getDateToString((Date)valueTable.get(OEMStandardEngineering.serviceStartDate.name()),"yyyy-MM-dd")));
        labelVOList.add(new LabelVO("合同结束时间",DateTimeUtil.getDateToString((Date)valueTable.get(OEMStandardEngineering.serviceEndDate.name()),"yyyy-MM-dd")));
        labelVOList.add(new LabelVO("乙方法定代表人",String.valueOf(valueTable.get(OEMStandardEngineering.legalRepresentB.name()))));
        labelVOList.add(new LabelVO("乙方电话",String.valueOf(valueTable.get(OEMStandardEngineering.telB.name()))));
        labelVOList.add(new LabelVO("乙方电子邮箱",String.valueOf(valueTable.get(OEMStandardEngineering.emailB.name()))));
        labelVOList.add(new LabelVO("乙方开户银行",String.valueOf(valueTable.get(OEMStandardEngineering.openBankB.name()))));
        labelVOList.add(new LabelVO("乙方账号",String.valueOf(valueTable.get(OEMStandardEngineering.bankAccountB.name()))));
        labelVOList.add(new LabelVO("乙方税号",String.valueOf(valueTable.get(OEMStandardEngineering.taxRegistNoB.name()))));
        labelVOList.add(new LabelVO("乙方公司地址",String.valueOf(valueTable.get(OEMStandardEngineering.addressCompanyB.name()))));
        labelVOList.add(new LabelVO("乙方委托代理人",String.valueOf(valueTable.get(OEMStandardEngineering.proxyB.name()))));
        labelVOList.add(new LabelVO("甲方委托代理人",String.valueOf(valueTable.get(OEMStandardEngineering.proxyA.name()))));
        labelVOList.add(new LabelVO("电子邮箱",String.valueOf(valueTable.get(OEMStandardEngineering.emailA.name()))));
    }
    private static void addConstructionLabelVO(List<LabelVO> labelVOList, Map<String, Object> valueTable) {
        labelVOList.add(new LabelVO("施工合同编号", String.valueOf(valueTable.get(Sales.constructionContractNo.name()))));
        labelVOList.add(new LabelVO("施工总价款", String.valueOf(valueTable.get(Sales.constructTotalAmount.name()))));
        labelVOList.add(new LabelVO("施工工期", String.valueOf(valueTable.get(Sales.constructPeriod.name()))));
        labelVOList.add(new LabelVO("施工预付款", String.valueOf(valueTable.get(Sales.constructAdvancePay.name()))));
        labelVOList.add(new LabelVO("施工尾款", String.valueOf(valueTable.get(Sales.constructFinalPay.name()))));
        labelVOList.add(new LabelVO("电话",String.valueOf(valueTable.get(Sales.telA.name()))));
    }
    private static void addEntrustLabelVO(List<LabelVO> labelVOList, Map<String, Object> valueTable) {
        labelVOList.add(new LabelVO("委托合同编号", String.valueOf(valueTable.get(Sales.entrustContractNo.name()))));
        labelVOList.add(new LabelVO("甲方证件号", String.valueOf(valueTable.get(Sales.certificateNoA.name()))));
        labelVOList.add(new LabelVO("甲方联系人", String.valueOf(valueTable.get(Sales.contactsA.name()))));
        labelVOList.add(new LabelVO("甲方联系电话", String.valueOf(valueTable.get(Sales.telA.name()))));
        labelVOList.add(new LabelVO("充电桩位置", String.valueOf(valueTable.get(Sales.stubAddress.name()))));
        labelVOList.add(new LabelVO("对外公开使用", String.valueOf(valueTable.get(Sales.openUse.name()))));
        labelVOList.add(new LabelVO("使用时间段", String.valueOf(valueTable.get(Sales.usePeriod.name()))));
        labelVOList.add(new LabelVO("停车费", String.valueOf(valueTable.get(Sales.parkingRate.name()))));
        labelVOList.add(new LabelVO("委托期限", String.valueOf(valueTable.get(Sales.entrustPeriod.name()))));
        labelVOList.add(new LabelVO("委托开始时间", DateTimeUtil.getDateToString((Date)valueTable.get(Sales.entrustStartTime.name()),"yyyy-MM-dd")));
        labelVOList.add(new LabelVO("委托结束时间", DateTimeUtil.getDateToString((Date)valueTable.get(Sales.entrustEndTime.name()),"yyyy-MM-dd")));
        labelVOList.add(new LabelVO("A类收费标准", String.valueOf(valueTable.get(Sales.priceStandardA.name()))));
        labelVOList.add(new LabelVO("B类收费标准", String.valueOf(valueTable.get(Sales.priceStandardB.name()))));
        labelVOList.add(new LabelVO("C类收费标准", String.valueOf(valueTable.get(Sales.priceStandardC.name()))));
        labelVOList.add(new LabelVO("A类桩数量", String.valueOf(valueTable.get(Sales.stubQtyA.name()))));
        labelVOList.add(new LabelVO("B类桩数量", String.valueOf(valueTable.get(Sales.stubQtyB.name()))));
        labelVOList.add(new LabelVO("C类桩数量", String.valueOf(valueTable.get(Sales.stubQtyC.name()))));
        labelVOList.add(new LabelVO("一年合计价格", String.valueOf(valueTable.get(Sales.stubQtyTotal.name()))));
        if("其他".equals(valueTable.get(Sales.trustFeePayMethod.name()))) {
            labelVOList.add(new LabelVO("托管费支付方式", String.valueOf(valueTable.get(Sales.others.name()))));
        } else {
            labelVOList.add(new LabelVO("托管费支付方式", String.valueOf(valueTable.get(Sales.trustFeePayMethod.name()))));
        }
        labelVOList.add(new LabelVO("运营付款期限", String.valueOf(valueTable.get(Sales.operatePayPeriod.name()))));
        labelVOList.add(new LabelVO("定制服务费单价", String.valueOf(valueTable.get(Sales.servicePrice.name()))));
        labelVOList.add(new LabelVO("客诉处理", String.valueOf(valueTable.get(Sales.customerComplaint.name()))));
        labelVOList.add(new LabelVO("满意度调查", String.valueOf(valueTable.get(Sales.customerSatisfate.name()))));
        labelVOList.add(new LabelVO("用户关怀", String.valueOf(valueTable.get(Sales.customerUserCare.name()))));
        labelVOList.add(new LabelVO("录音存贮", String.valueOf(valueTable.get(Sales.customerRecordStorage.name()))));
        labelVOList.add(new LabelVO("延保", String.valueOf(valueTable.get(Sales.equipmentExtendInsurance.name()))));
        labelVOList.add(new LabelVO("设备检修", String.valueOf(valueTable.get(Sales.equipmentRepair.name()))));
        labelVOList.add(new LabelVO("派单系统（手机端）", String.valueOf(valueTable.get(Sales.equipmentDispatch.name()))));
        labelVOList.add(new LabelVO("最新协议升级", String.valueOf(valueTable.get(Sales.platformProtocolUpgrade.name()))));
        labelVOList.add(new LabelVO("新功能升级", String.valueOf(valueTable.get(Sales.platformNewFunction.name()))));
        labelVOList.add(new LabelVO("24小时响应", String.valueOf(valueTable.get(Sales.platformResponse.name()))));
        labelVOList.add(new LabelVO("数据提取", String.valueOf(valueTable.get(Sales.dataFetch.name()))));
        labelVOList.add(new LabelVO("定制报表", String.valueOf(valueTable.get(Sales.dataReport.name()))));
        labelVOList.add(new LabelVO("数据存贮", String.valueOf(valueTable.get(Sales.dataStore.name()))));
        labelVOList.add(new LabelVO("离线报告", String.valueOf(valueTable.get(Sales.safetyOfflineReport.name()))));
        labelVOList.add(new LabelVO("故障代码报告", String.valueOf(valueTable.get(Sales.safetyFaultReport.name()))));
        labelVOList.add(new LabelVO("充电桩运营方式", String.valueOf(valueTable.get(Sales.stubOperationMode.name()))));
        labelVOList.add(new LabelVO("同意/不同意", String.valueOf(valueTable.get(Sales.isAgree.name()))));
        labelVOList.add(new LabelVO("丙方授权代表人", String.valueOf(valueTable.get(Sales.authRepresentC.name()))));
    }


    public static SendContractsSyncVO createHostFactorySale(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            //构造主表数据
            labelVOList.add(new LabelVO("采购合同编号",String.valueOf(valueTable.get(HostFactorySale.purchaseContractNo.name()))));
            labelVOList.add(new LabelVO("甲方",String.valueOf(valueTable.get(HostFactorySale.partyA.name()))));
            labelVOList.add(new LabelVO("合计总价（小写）",String.valueOf(valueTable.get(HostFactorySale.totalAmountLower.name()))));
            labelVOList.add(new LabelVO("合计总价（大写）",String.valueOf(valueTable.get(HostFactorySale.totalAmountUpper.name()))));
            labelVOList.add(new LabelVO("质保期",String.valueOf(valueTable.get(HostFactorySale.warrantyPeriod.name()))));
            labelVOList.add(new LabelVO("交货期",String.valueOf(valueTable.get(HostFactorySale.deliveryPeriod.name()))));
            labelVOList.add(new LabelVO("设备总价款",String.valueOf(valueTable.get(HostFactorySale.equipTotalAmount.name()))));
            labelVOList.add(new LabelVO("设备付款方式",String.valueOf(valueTable.get(HostFactorySale.equipPaymentMethod.name()))));
            labelVOList.add(new LabelVO("设备预付款",String.valueOf(valueTable.get(HostFactorySale.equipAdvancePay.name()))));
            labelVOList.add(new LabelVO("甲方地址",String.valueOf(valueTable.get(HostFactorySale.addressA.name()))));
            labelVOList.add(new LabelVO("甲方法定代表人",String.valueOf(valueTable.get(HostFactorySale.legalRepresentA.name()))));
            labelVOList.add(new LabelVO("甲方法定代表人职务",String.valueOf(valueTable.get(HostFactorySale.legalRepresentPostA.name()))));
            labelVOList.add(new LabelVO("甲方授权代表人",String.valueOf(valueTable.get(HostFactorySale.authRepresentA.name()))));
            labelVOList.add(new LabelVO("甲方授权代表人职务",String.valueOf(valueTable.get(HostFactorySale.authRepresentPostA.name()))));
            labelVOList.add(new LabelVO("甲方电话",String.valueOf(valueTable.get(HostFactorySale.telA.name()))));
            labelVOList.add(new LabelVO("甲方传真",String.valueOf(valueTable.get(HostFactorySale.faxA.name()))));
            labelVOList.add(new LabelVO("甲方开户行",String.valueOf(valueTable.get(HostFactorySale.openBankA.name()))));
            labelVOList.add(new LabelVO("甲方税务登记号",String.valueOf(valueTable.get(HostFactorySale.taxRegistNoA.name()))));
            labelVOList.add(new LabelVO("甲方账号",String.valueOf(valueTable.get(HostFactorySale.bankAccountA.name()))));
            labelVOList.add(new LabelVO("乙方授权代表人",String.valueOf(valueTable.get(HostFactorySale.authRepresentB.name()))));
            labelVOList.add(new LabelVO("乙方授权代表人职务",String.valueOf(valueTable.get(HostFactorySale.authRepresentPostB.name()))));
            labelVOList.add(new LabelVO("乙方法定代表人",String.valueOf(valueTable.get(HostFactorySale.legalRepresentB.name()))));
            labelVOList.add(new LabelVO("乙方法定代表人职务",String.valueOf(valueTable.get(HostFactorySale.legalRepresentPostB.name()))));
            labelVOList.add(new LabelVO("乙方传真",String.valueOf(valueTable.get(HostFactorySale.telB.name()))));
            //构造子表
            BizObject[] zbBizObjects = (BizObject[])valueTable.get(HostFactorySale.hostFactorySaleEquipmentInfo.name());
            for (int i = 1;i <= zbBizObjects.length;i++) {
                Map<String, Object> zbValueTable = zbBizObjects[i-1].getValueTable();
                String str = "";
                if (i!=1){
                    str = String.valueOf(i);
                }
                labelVOList.add(new LabelVO("产品名称"+str,String.valueOf(zbValueTable.get(HostFactorySale.productName.name()))));
                labelVOList.add(new LabelVO("规格型号"+str,String.valueOf(zbValueTable.get(HostFactorySale.specificationModels.name()))));
                labelVOList.add(new LabelVO("单位"+str,String.valueOf(zbValueTable.get(HostFactorySale.unit.name()))));
                labelVOList.add(new LabelVO("数量"+str,String.valueOf(zbValueTable.get(HostFactorySale.qty.name()))));
                labelVOList.add(new LabelVO("单价"+str,String.valueOf(zbValueTable.get(HostFactorySale.price.name()))));
                labelVOList.add(new LabelVO("总价"+str,String.valueOf(zbValueTable.get(HostFactorySale.amount.name()))));
                labelVOList.add(new LabelVO("备注"+str,String.valueOf(zbValueTable.get(HostFactorySale.equipRemark.name()))));
            }
            List<PlaceHolder> placeHolders = new ArrayList<PlaceHolder>();
            placeHolders.add(new PlaceHolder(ACCOUNT,ACCOUNT,String.valueOf(valueTable.get(HostFactorySale.OurUnitName.name())),labelVOList));
            String templateId = "2156410776833753089";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2156411330364440584L,String.valueOf(valueTable.get(HostFactorySale.telA.name())),
                    String.valueOf(valueTable.get(HostFactorySale.partyA.name())),String.valueOf(valueTable.get(HostFactorySale.partyA.name()))));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
        }
        return sendContractsSyncVO;
    }

}
