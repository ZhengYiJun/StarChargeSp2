package com.h3bpm.starcharge.common.bean;

import java.util.List;

/**
 * 工作流子表参数
 *
 * @author LLongAgo
 * @date 2019/3/27
 * @since 1.0.0
 */
public class WorkFlowZbParam {

    /**
     * name : amount
     * value : [[{"name":"name","value":"zhangsan"},{"name":"age","value":"12"}],[{"name":"name","value":"lisi"},{"name":"age","value":"12"}]]
     */

    private String name;
    private List<List<ValueBean>> value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<List<ValueBean>> getValue() {
        return value;
    }

    public void setValue(List<List<ValueBean>> value) {
        this.value = value;
    }

    public static class ValueBean {
        /**
         * name : name
         * value : zhangsan
         */

        private String name;
        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}