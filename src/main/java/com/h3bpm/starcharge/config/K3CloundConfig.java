package com.h3bpm.starcharge.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * K3C配置文件
 *
 * @author LLongAgo
 * @date 2019/3/22
 * @since 1.0.0
 */
@Component
public class K3CloundConfig {

    /**
     * 金蝶url前缀
     */
    @Value("${k3c_API_URL}")
    private String url;

    /**
     * 登录接口
     */
    @Value("${k3c_login}")
    private String login;

    /**
     * 查看接口
     */
    @Value("${k3c_view}")
    private String view;

    /**
     * 保存接口
     */
    @Value("${k3c_save}")
    private String save;

    /**
     * 批量保存
     */
    @Value("${k3c_batchSave}")
    private String batchSave;

    /**
     * 提交接口
     */
    @Value("${k3c_submit}")
    private String submit;

    /**
     * 审核接口
     */
    @Value("${k3c_audit}")
    private String audit;

    /**
     * 反审核接口
     */
    @Value("${k3c_unAudit}")
    private String unAudit;

    /**
     * 状态改变接口
     */
    @Value("${k3c_statusConver}")
    private String statusConver;

    /**
     * 分配接口
     */
    @Value("${k3c_allocate}")
    private String allocate;

    /**
     * 单据查询接口
     */
    @Value("${k3c_executeBillQuery}")
    private String executeBillQuery;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getSave() {
        return save;
    }

    public void setSave(String save) {
        this.save = save;
    }

    public String getSubmit() {
        return submit;
    }

    public void setSubmit(String submit) {
        this.submit = submit;
    }

    public String getAudit() {
        return audit;
    }

    public void setAudit(String audit) {
        this.audit = audit;
    }

    public String getUnAudit() {
        return unAudit;
    }

    public void setUnAudit(String unAudit) {
        this.unAudit = unAudit;
    }

    public String getStatusConver() {
        return statusConver;
    }

    public void setStatusConver(String statusConver) {
        this.statusConver = statusConver;
    }

    public String getAllocate() {
        return allocate;
    }

    public void setAllocate(String allocate) {
        this.allocate = allocate;
    }

    public String getExecuteBillQuery() {
        return executeBillQuery;
    }

    public void setExecuteBillQuery(String executeBillQuery) {
        this.executeBillQuery = executeBillQuery;
    }

    public String getBatchSave() {
        return batchSave;
    }

    public void setBatchSave(String batchSave) {
        this.batchSave = batchSave;
    }
}