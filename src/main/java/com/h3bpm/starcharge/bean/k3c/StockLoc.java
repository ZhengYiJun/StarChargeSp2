package com.h3bpm.starcharge.bean.k3c;

import java.util.List;

/**
 * 仓位
 *
 * @author LLongAgo
 * @date 2019/3/15
 * @since 1.0.0
 */
public class StockLoc {

    /**
     * code : 0
     * msg : ok
     * data : [{"name":"106724","number":"GY"},{"name":"106931","number":"GZSF0001"},{"name":"106932","number":"GZSF0002"},{"name":"106933","number":"GZSF0003"},{"name":"106934","number":"GZSF0004"},{"name":"106935","number":"GZSF0005"},{"name":"106936","number":"GZSF0006"},{"name":"106937","number":"GZSF0007"},{"name":"106938","number":"GZSF0008"},{"name":"106939","number":"GZSF0009"},{"name":"106947","number":"GZSF0010"},{"name":"106949","number":"GZSF0011"},{"name":"106950","number":"GZSF0012"},{"name":"106951","number":"GZSF0013"},{"name":"106952","number":"GZSF0014"},{"name":"106953","number":"GZSF0015"},{"name":"106954","number":"GZSF0016"},{"name":"106955","number":"GZSF0017"},{"name":"106956","number":"GZSF0018"},{"name":"106958","number":"GZSF0019"},{"name":"106959","number":"GZSF0020"},{"name":"106966","number":"GZSF0022"},{"name":"106998","number":"Y001"},{"name":"106999","number":"Y002"}]
     */

    private String code;
    private String msg;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : 106724
         * number : GY
         */

        private String name;
        private String number;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }
    }
}