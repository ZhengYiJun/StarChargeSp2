package com.h3bpm.starcharge.common.uitl;

import OThinker.Common.Data.Database.Parameter;
import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.base.util.AppUtility;
import data.DataTable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class SqlUtils extends ControllerBase {

    public static String genInsertSql(String tableName, Object javaBean) throws Exception {
        Class bean = javaBean.getClass();
        Field[] fields = bean.getDeclaredFields();

        String tableColunStr="";
        String tableValueStr="";

        for (Field field : fields) {
            field.setAccessible(true);
            Object fieldValue = field.get(javaBean);

            //忽略时间
            if("class java.util.Date".equals(field.getGenericType().toString())){
                // tableValueStr+="to_date('"+fieldValue+"','yyyy-mm-dd hh24:mi:ss')";
                continue;
            }

            if(fieldValue != null){
                tableValueStr+="'"+fieldValue+"'"+",";
            }else {
                tableValueStr+="''"+",";
            }
            tableColunStr += field.getName()+",";
        }

        tableColunStr = tableColunStr.substring(0,tableColunStr.length()-1);
        tableValueStr = tableValueStr.substring(0,tableValueStr.length()-1);

        String sql = "INSERT INTO "+tableName + "("+tableColunStr+")"+" VALUES("+tableValueStr+")";

        return sql;
    }
//    public static Parameter[] genInsertSql(Object javaBean) throws Exception {
//        List<Parameter> parameters = new ArrayList<>();
//        Class bean = javaBean.getClass();
//        Field[] fields = bean.getDeclaredFields();
//        for (Field field : fields) {
//            field.setAccessible(true);
//            Object fieldValue = field.get(javaBean);
//            parameters.add(new Parameter(field.getName(),field.getType(),fieldValue,1));
//        }
//    }
    
    /**
     * 更新SQL拼接
     * @param tableName
     * @param javaBean
     * @return SQL语句
     */
    public static String genUpdateSql(String tableName, Object javaBean) throws Exception {
        Class bean = javaBean.getClass();
        Field[] fields = bean.getDeclaredFields();
 
        
        String tableUpdate = "";
        String tableId = "";

        for (Field field : fields) {
            field.setAccessible(true);
            Object fieldValue = field.get(javaBean);

            //忽略时间
            if("class java.util.Date".equals(field.getGenericType().toString())){
                // tableValueStr+="to_date('"+fieldValue+"','yyyy-mm-dd hh24:mi:ss')";
                continue;
            }
            String tableValueStr="";
            if(fieldValue != null){
                tableValueStr ="'"+fieldValue+"'";
            }else {
                tableValueStr ="''";
            }
            String tableColunStr = toUnderScore(field.getName());
            tableUpdate += tableColunStr + "=" + tableValueStr + ",";
             
            if("CONTRACTANDOUTSIDEUSERID".equals(tableColunStr)){
            	tableId = ""+fieldValue;//主键
            }
            	
        }

        tableUpdate = tableUpdate.substring(0,tableUpdate.length()-1);

        String sql = "update "+tableName + " set "+tableUpdate+""+" where contractandoutsideuserid = '"+tableId+"'";

        return sql;
    }
    /**
     * 更新SQL拼接
     * @param tableName
     * @param javaBean
     * @param 主键名称
     * @return SQL语句
     */
    public static String genUpdateSql(String tableName, Object javaBean,String id) throws Exception {
    	id = id.toUpperCase();
        Class bean = javaBean.getClass();
        Field[] fields = bean.getDeclaredFields();
 
        
        String tableUpdate = "";
        String tableId = "";

        for (Field field : fields) {
            field.setAccessible(true);
            Object fieldValue = field.get(javaBean);

            //忽略时间
            if("class java.util.Date".equals(field.getGenericType().toString())){
                // tableValueStr+="to_date('"+fieldValue+"','yyyy-mm-dd hh24:mi:ss')";
                continue;
            }
            String tableValueStr="";
            if(fieldValue != null){
                tableValueStr ="'"+fieldValue+"'";
            }else {
                tableValueStr ="''";
            }
            String tableColunStr = field.getName().toUpperCase();
            tableUpdate += tableColunStr + "=" + tableValueStr + ",";
             
            if(id.equals(tableColunStr)){
                //主键
            	tableId = ""+fieldValue;
            }
            	
        }

        tableUpdate = tableUpdate.substring(0,tableUpdate.length()-1);

        String sql = "update "+tableName + " set "+tableUpdate+""+" where "+id+" = '"+tableId+"'";

        return sql;
    }
    
    public static String toUnderScore(String str){
        StringBuilder sb=new StringBuilder(str);
        int temp=0;//定位
        for(int i=0;i<str.length();i++){
            if(Character.isUpperCase(str.charAt(i))){
                sb.insert(i+temp, "_");
                temp+=1;
            }
        }
        return sb.toString().toUpperCase();
    }

    @Override
    public String getFunctionCode() {
        return null;
    }

    public static int executeInsert(String sql){
        int i = AppUtility.getEngine().getEngineConfig().getCommandFactory().CreateCommand().ExecuteNonQuery(sql);
        return i;
    }

    public static int doNoQuery(String sql){
        int i = AppUtility.getEngine().getEngineConfig().getCommandFactory().CreateCommand().ExecuteNonQuery(sql);
        return i;
    }

    public static int doNoQuery(String sql,Parameter[] parameters){
        int i = AppUtility.getEngine().getEngineConfig().getCommandFactory().CreateCommand().ExecuteNonQuery(sql,parameters);
        return i;
    }

    public static DataTable doQuery(String sql) throws Exception {
        DataTable dataTable = AppUtility.getEngine().getQuery().QueryTable(sql);
        return dataTable;
    }

    public static String getInstanceNameById(String instanceId) throws Exception {
        InstanceData instanceData = null;
        try {
             instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instanceData.getInstanceContext().getInstanceName();
    }

    public static InstanceData getInstanceDataById(String instanceId) throws Exception {
        InstanceData instanceData = null;
        try {
            instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instanceData;
    }

}
