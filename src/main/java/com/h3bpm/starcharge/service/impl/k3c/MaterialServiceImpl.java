package com.h3bpm.starcharge.service.impl.k3c;

import OThinker.H3.Entity.DataModel.BizObject;
import OThinker.H3.Entity.Instance.Data.InstanceData;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.h3bpm.starcharge.bean.k3c.MaterialBase;
import com.h3bpm.starcharge.bean.k3c.MaterialInfo;
import com.h3bpm.starcharge.bean.k3c.MaterialView;
import com.h3bpm.starcharge.common.bean.BaseResult;
import com.h3bpm.starcharge.common.bean.JsonResult;
import com.h3bpm.starcharge.common.bean.RestfulErrorCode;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.FileUtil;
import com.h3bpm.starcharge.common.uitl.k3c.K3cUtil;
import com.h3bpm.starcharge.config.K3CloundConfig;
import com.h3bpm.starcharge.ret.k3c.K3cBase;
import com.h3bpm.starcharge.service.k3c.BaseDataService;
import com.h3bpm.starcharge.service.k3c.K3cMaterialService;
import com.h3bpm.starcharge.service.k3c.K3cService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * K3cMaterialService实现类
 *
 * @author LLongAgo
 * @since 1.0.0
 */
@Service
public class MaterialServiceImpl implements K3cMaterialService {

    @Autowired
    private K3cUtil util;
    @Autowired
    private BaseDataService baseDataService;
    @Autowired
    private K3cService k3cService;
    @Autowired
    private K3CloundConfig k3CloundConfig;

    @Value("${k3c_dbId}")
    private String dbId;
    @Value("${k3c_uid}")
    private String uid;
    @Value("${k3c_pwd}")
    private String pwd;
    @Value("${k3c_lang}")
    private int lang;

    @Value("${materialFormId}")
    private String materialFormId;
    @Value("${allocateParam}")
    private String allocateParam;
    @Value("${unAuditMaterialJson}")
    private String unAuditMaterialJson;
    @Value("${searchListJson}")
    private String searchListJson;
    @Value("${searchNormalListJson}")
    private String searchNormalListJson;
    @Value("${viewJson}")
    private String viewJson;

    private static final String STAR_CHARGE = "星充";
    private static final String OUT_SOURCING = "1";


    @Override
    @SuppressWarnings("unchecked")
    public RetResult save(InstanceData instanceData) throws Exception {
        HashMap<String, Object> valueTable = instanceData.getBizObject().getValueTable();
        // 获取申请类型
        String applyType = (String) valueTable.get("applyType");
        BizObject[] zbBizObjects = (BizObject[]) valueTable.get(applyType);
        JSONObject basic = setSaveParam(zbBizObjects);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("formid", materialFormId);
        jsonObject.put("data", basic);
        // 禁止循环引用
        String param = JSON.toJSONString(jsonObject, SerializerFeature.DisableCircularReferenceDetect);
        String cookie = util.getLoginCookie();
        // 保存
        RetResult result = k3cService.save(k3CloundConfig.getUrl() + k3CloundConfig.getBatchSave(), cookie, param);
        // K3C接口偶尔异常，再调一次
        if (!RestfulErrorCode.SUCCESS.getCode().equals(result.getCode())) {
            result = k3cService.save(k3CloundConfig.getUrl() + k3CloundConfig.getBatchSave(), cookie, param);
        }
        if (!RestfulErrorCode.SUCCESS.getCode().equals(result.getCode())) {
            return result;
        }
        K3cBase k3cBase = (K3cBase) result.getData();
        String submitParam = k3cService.buildParam(materialFormId, k3cBase.getId(), null, null);
        // 提交
        k3cService.save(k3CloundConfig.getUrl() + k3CloundConfig.getSubmit(), cookie, submitParam);
        // 审核
        k3cService.save(k3CloundConfig.getUrl() + k3CloundConfig.getAudit(), cookie, submitParam);
        // 根据使用组织分配
        String useOrganization = (String) valueTable.get("useOrganization");
        JsonResult<List<BaseResult>> organizationList;
        StringBuilder sb = new StringBuilder();
        organizationList = baseDataService.getOrganizationBySql(STAR_CHARGE.equals(useOrganization) ? "FNUMBER like '102%'" : "FNUMBER = 101");
        List<BaseResult> data = organizationList.getData();
        for (BaseResult d : data) {
            sb.append(d.getValue()).append(",");
        }
        String orgStr = sb.toString();
        orgStr = orgStr.substring(0, orgStr.length() - 1);
        // 拼接分配参数
        String allocateParam = k3cService.buildAllocateParam(materialFormId, orgStr, k3cBase.getId());
        k3cService.save(k3CloundConfig.getUrl() + k3CloundConfig.getAllocate(), cookie, allocateParam);
        return result;
    }

    @Override
    public void delete(String formId, String param) throws Exception {
        util.delete(formId, param);
    }

    @Override
    public JsonResult<K3cBase> allSave(String formId, String param) throws Exception {
        return util.save(formId, param);
    }

    @Override
    public JsonResult<K3cBase> push(String formId, String param) throws Exception {
        return util.push(formId, param);
    }

    @Override
    public void submit(String formId, String param) throws Exception {
        JsonResult<K3cBase> result = util.submit(formId, param);
        if ("500".equals(result.getCode().toString())) {
            throw new Exception("请求异常");
        }
    }

    @Override
    public JsonResult<K3cBase> audit(String formId, String param) throws Exception {
        JsonResult<K3cBase> result = util.audit(formId, param);
        if ("500".equals(result.getCode().toString())) {
            throw new Exception("请求异常");
        }
        return result;
    }

    /**
     * 反审核
     *
     * @param formId 表单id
     * @param param  参数
     * @return JsonResult
     * @throws Exception
     */
    @Override
    public JsonResult<K3cBase> unAudit(String formId, String param) throws Exception {
        return util.unAudit(formId, param);
    }

    /**
     * 物料分配
     *
     * @param ids    String
     * @param orgIds String
     * @return JsonResult
     */
    @Override
    public JsonResult<K3cBase> allocate(String ids, String orgIds) throws Exception {
        return util.allocate(materialFormId, String.format(allocateParam, ids, orgIds));
    }

    /**
     * 物料列表
     *
     * @param materialCode String
     * @throws Exception
     */
    @Override
    public JsonResult<List<MaterialBase>> getList(String materialCode, String useOrgId) throws Exception {
        String searchStr = "FNumber like '" + materialCode + "%'";
        if (null != useOrgId && !"".equals(useOrgId)) {
            searchStr += "AND FUSEORGID = " + useOrgId;
        }
        String formatStr = String.format(searchListJson, materialFormId, "FMATERIALID,FNumber,FName", searchStr);
        String result = util.getList(formatStr);
        List<String[]> resultList = new Gson().fromJson(result, new TypeToken<List<String[]>>() {
        }.getType());
        List<MaterialBase> materialBaseList = new ArrayList<>();
        for (String[] list : resultList) {
            MaterialBase base = new MaterialBase();
            base.setId(list[0]);
            base.setCode(list[1]);
            base.setName(list[2]);
            materialBaseList.add(base);
        }
        JsonResult<List<MaterialBase>> jsonResult = new JsonResult<>();
        jsonResult.setData(materialBaseList);
        return jsonResult;
    }

    @Override
    public JSONArray getOrderIdList(String formId, String serchKeys, String filterString) throws Exception {
        String formatStr = String.format(searchNormalListJson, formId, serchKeys, filterString);
        String result = util.getList(formatStr);
        JSONArray array = JSONArray.parseArray(result);
        return array;
    }

    /**
     * 获取普通列表
     *
     * @param formId String,SerchKeys String
     * @throws Exception
     */
    @Override
    public JsonResult<List<MaterialBase>> getNormalList(String formId, String serchKeys, String filterString) throws Exception {
        String formatStr = String.format(searchNormalListJson, formId, serchKeys, filterString);
        String result = util.getList(formatStr);
        List<String[]> resultList = new Gson().fromJson(result, new TypeToken<List<String[]>>() {
        }.getType());
        List<MaterialBase> materialBaseList = new ArrayList<>();
        for (String[] list : resultList) {
            MaterialBase base = new MaterialBase();
            base.setCode(list[0]);
            base.setName(list[1]);
            if (list.length > 2) {
                base.setId(list[2]);
            } else {
                base.setId("1");
            }

            materialBaseList.add(base);
        }
        JsonResult<List<MaterialBase>> jsonResult = new JsonResult<>();
        jsonResult.setData(materialBaseList);
        return jsonResult;
    }

    /**
     * 物料详情
     *
     * @param id getInfo
     * @return JsonResult<MaterialInfo>
     * @throws Exception
     */
    @Override
    public JsonResult<MaterialInfo> getInfo(String id) throws Exception {
        String result = util.view(materialFormId, String.format(viewJson, id));
        JsonResult<MaterialInfo> jsonResult = new JsonResult<>();
        MaterialView materialView = new Gson().fromJson(result, MaterialView.class);
        if (null != materialView.getResult().getResponseStatus()) {
            jsonResult.setCode(RestfulErrorCode.ERROR.getCode());
            jsonResult.setMsg("该物料信息不存在");
            return jsonResult;
        }

        MaterialInfo materialInfo = new MaterialInfo();
        MaterialView.ResultBeanX.ResultBean viewResult = materialView.getResult().getResult();

        materialInfo.setUseOrgId(viewResult.getUseOrgId().getNumber());
        materialInfo.setMaterialId(String.valueOf(viewResult.getId()));
        materialInfo.setMaterialCode(viewResult.getNumber());
        materialInfo.setMaterialName(viewResult.getName().get(0).getValue());
        materialInfo.setSpecification(viewResult.getDescription().get(0).getValue());
        materialInfo.setBrand(viewResult.getF_pp());
        materialInfo.setMaterialGroup(viewResult.getMaterialGroup() != null ? viewResult.getMaterialGroup().getNumber() : "");
        materialInfo.setTallyType(viewResult.getF_WB_CWJZLB().getNumber());
        materialInfo.setRemark(viewResult.getDescription().get(0).getValue());
        materialInfo.setProducingArea(viewResult.getF_WB_CD());
        materialInfo.setCertificate(viewResult.getF_WB_DSFRZ());
        materialInfo.setMachineNo(viewResult.getF_WB_JZH());
        materialInfo.setColor(viewResult.getF_ztys());
        materialInfo.setSpearBrand(viewResult.getF_qpp());
        materialInfo.setCardType(viewResult.getF_klx());
        materialInfo.setSpearLength(viewResult.getF_qcd());
        materialInfo.setMaterialProperty(viewResult.getMaterialBase().get(0).getErpClsID());
        materialInfo.setStockType(viewResult.getMaterialBase().get(0).getCategoryID().getNumber());
        materialInfo.setUnit(viewResult.getMaterialBase().get(0).getBaseUnitId().getNumber());
        materialInfo.setSafeStock(String.valueOf(viewResult.getMaterialStock().get(0).getSafeStock()));
        materialInfo.setStock(String.valueOf(viewResult.getMaterialStock().get(0).getStockId()));
        materialInfo.setFixedLeadTime(String.valueOf(viewResult.getMaterialPlan().get(0).getFixLeadTime()));
        materialInfo.setMinNum(String.valueOf(viewResult.getMaterialPlan().get(0).getMinPOQty()));
        materialInfo.setBatchIncrement(String.valueOf(viewResult.getMaterialPlan().get(0).getIncreaseQty()));
        materialInfo.setPlanner(String.valueOf(viewResult.getMaterialPlan().get(0).getPlanerID()));
        materialInfo.setSendType(viewResult.getMaterialProduce().get(0).getIssueType());
        materialInfo.setControType(viewResult.getMaterialProduce().get(0).getOverControlMode());
        materialInfo.setSendStock(String.valueOf(viewResult.getMaterialProduce().get(0).getPickStockId()));

        jsonResult.setData(materialInfo);
        return jsonResult;
    }

    /**
     * 反审核物料
     *
     * @param ids String
     * @return JsonResult<K3cBase>
     */
    @Override
    public JsonResult<K3cBase> unAuditMaterial(String ids) throws Exception {
        String param = String.format(unAuditMaterialJson, ids);
        return util.unAudit(materialFormId, param);
    }

    /**
     * 物料维护
     *
     * @param instanceData InstanceData
     * @param tableId      子表id
     * @return RetResult
     * @throws Exception
     */
    @Override
    @SuppressWarnings("unchecked")
    public RetResult maintenance(InstanceData instanceData, String tableId) throws Exception {
        Map<String, String> map = getParam(instanceData, tableId);
        String ids = map.get("ids");
        String param = map.get("param");
        String cookie = util.getLoginCookie();
        // 反审核
        String unAuditParam = k3cService.buildParam(materialFormId, ids, null, null);
        RetResult unAuditResult = k3cService.save(k3CloundConfig.getUrl() + k3CloundConfig.getUnAudit(), cookie, unAuditParam);
        if (!unAuditResult.getCode().equals(RestfulErrorCode.SUCCESS.getCode())) {
            return unAuditResult;
        }
        // 保存
        RetResult result = k3cService.save(k3CloundConfig.getUrl() + k3CloundConfig.getBatchSave(), cookie, param);
        // K3C接口偶尔异常，再调一次
        if (!RestfulErrorCode.SUCCESS.getCode().equals(result.getCode())) {
            result = k3cService.save(k3CloundConfig.getUrl() + k3CloundConfig.getBatchSave(), cookie, param);
        }
        if (!RestfulErrorCode.SUCCESS.getCode().equals(result.getCode())) {
            return result;
        }
        // 提交
        k3cService.save(k3CloundConfig.getUrl() + k3CloundConfig.getSubmit(), cookie, unAuditParam);
        // 审核
        k3cService.save(k3CloundConfig.getUrl() + k3CloundConfig.getAudit(), cookie, unAuditParam);
        return result;
    }

    /**
     * 获取修改参数
     *
     * @param instanceData InstanceData
     * @param tableId      子表id
     * @return Map
     * @throws Exception
     */
    private Map<String, String> getParam(InstanceData instanceData, String tableId) throws Exception {
        HashMap<String, Object> valueTable = instanceData.getBizObject().getValueTable();
        BizObject[] zbBizObjects = (BizObject[]) valueTable.get(tableId);
        String pactFile;
        switch (tableId) {
            case "materialJH":
                pactFile = "k3c/material-edit-JH.json";
                break;
            case "materialCK":
                pactFile = "k3c/material-edit-CK.json";
                break;
            case "materialCG":
                pactFile = "k3c/material-edit-CG.json";
                break;
            case "materialCW":
                pactFile = "k3c/material-edit-CW.json";
                break;
            default:
                pactFile = "k3c/material-edit-BG.json";
                break;
        }
        // 获取json模板
        String template = FileUtil.readJsonData(pactFile);
        JSONObject basic = JSON.parseObject(template);
        List<Map<String, Object>> modelArr = new ArrayList<>();
        // 获取id物料id
        StringBuilder sb = new StringBuilder();
        for (BizObject bizObj : zbBizObjects) {
            Map<String, Object> zbValueTable = bizObj.getValueTable();
            sb.append(zbValueTable.get("materialId")).append(",");
            Map<String, Object> model = new HashMap<>(4);
            model.put("FMATERIALID", Integer.valueOf((String) zbValueTable.get("materialId")));
            Map<String, String> useOrgId = new HashMap<>(4);
            useOrgId.put("FNumber", (String) zbValueTable.get("useOrgId"));
            model.put("FUseOrgId", useOrgId);
            // 设置参数
            setEditParam(model, zbValueTable, tableId);
            modelArr.add(model);
        }
        String ids = sb.toString();
        ids = ids.substring(0, ids.length() - 1);
        basic.put("Model", modelArr);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("formid", materialFormId);
        jsonObject.put("data", basic);
        String param = JSON.toJSONString(jsonObject);
        Map<String, String> map = new HashMap<>(4);
        map.put("ids", ids);
        map.put("param", param);
        return map;
    }

    /**
     * 设置修改参数
     */
    private void setEditParam(Map<String, Object> model, Map<String, Object> zbValueTable, String tableId) {
        switch (tableId) {
            case "materialJH":
                setJHParam(model, zbValueTable);
                break;
            case "materialCK":
                setCKParam(model, zbValueTable);
                break;
            case "materialCG":
                setCGParam(model, zbValueTable);
                break;
            case "materialCW":
                setCWParam(model, zbValueTable);
                break;
            default:
                setBGParam(model, zbValueTable);
                break;
        }
    }

    /**
     * 设置保存参数
     */
    @SuppressWarnings("unchecked")
    private JSONObject setSaveParam(BizObject[] zbBizObjects) throws Exception {
        // 解析json模板
        String template = FileUtil.readJsonData("k3c/material-save-batch.json");
        JSONObject basic = JSON.parseObject(template);
        // 获取model数组
        JSONArray jsonArray = basic.getJSONArray("Model");
        Map<String, Object> modelTemplate = (Map<String, Object>) jsonArray.get(0);
        jsonArray.clear();
        for (BizObject zbBizObject : zbBizObjects) {
            Map<String, Object> model = new HashMap<>(32);
            model.putAll(modelTemplate);
            Map<String, Object> zbValueTable = zbBizObject.getValueTable();
            setBGParam(model, zbValueTable);
            jsonArray.add(model);
        }
        return basic;
    }

    /**
     * 设置计划参数
     */
    private void setJHParam(Map<String, Object> model, Map<String, Object> zbValueTable) {
        // 安全库存
        Map<String, Integer> entity = new HashMap<>(2);
        entity.put("FSafeStock", Integer.valueOf((String) zbValueTable.get("safeStock")));
        model.put("SubHeadEntity1", entity);
    }

    /**
     * 设置财务参数
     */
    private void setCWParam(Map<String, Object> model, Map<String, Object> zbValueTable) {
        // 财务记账类别
        Map<String, Object> cw = new HashMap<>(2);
        cw.put("FNumber", zbValueTable.get("tallyType"));
        model.put("F_WB_CWJZLB", cw);
        // 存货类别
        Map<String, Object> subEntity = new HashMap<>(2);
        Map<String, Object> category = new HashMap<>(2);
        category.put("FNumber", zbValueTable.get("stockType"));
        subEntity.put("FCategoryID", category);
        model.put("SubHeadEntity", subEntity);
    }

    /**
     * 设置仓库参数
     */
    private void setCKParam(Map<String, Object> model, Map<String, Object> zbValueTable) {
        Map<String, Object> entity1 = new HashMap<>(4);
        // 仓库
        Map<String, Object> stock = new HashMap<>(2);
        stock.put("FNumber", zbValueTable.get("stock"));
        entity1.put("FStockId", stock);
        // 仓位
        Map<String, Object> stockPosition = new HashMap<>(2);
        Map<String, Object> place = new HashMap<>(2);
        place.put("FNumber", zbValueTable.get("stockPosition"));
        stockPosition.put("FSTOCKPLACEID__FF100001", place);
        entity1.put("FStockPlaceId", stockPosition);
        model.put("SubHeadEntity1", entity1);
        Map<String, Object> entity5 = new HashMap<>(4);
        // 发料方式
        entity5.put("FIssueType", zbValueTable.get("sendType"));
        // 超发控制方式
        entity5.put("FOverControlMode", zbValueTable.get("controType"));
        Map<String, Object> pickStock = new HashMap<>(2);
        pickStock.put("FNumber", zbValueTable.get("sendStock"));
        entity5.put("FPickStockId", pickStock);
        model.put("SubHeadEntity5", entity5);
    }

    /**
     * 设置采购参数
     */
    private void setCGParam(Map<String, Object> model, Map<String, Object> zbValueTable) {
        // 产地
        model.put("F_WB_CD", zbValueTable.get("producingArea"));
        // 第三方证书
        model.put("F_WB_DSFRZ", zbValueTable.get("certificate"));
        Map<String, Object> cg = new HashMap<>(8);
        // 固定提前期
        cg.put("FFixLeadTime", zbValueTable.get("fixedLeadTime"));
        // 最小起订量
        cg.put("FMinPOQty", zbValueTable.get("minNum"));
        // 批量增量
        cg.put("FIncreaseQty", zbValueTable.get("batchIncrement"));
        model.put("SubHeadEntity4", cg);
    }

    /**
     * 设置变更参数
     */
    private void setBGParam(Map<String, Object> model, Map<String, Object> zbValueTable) {
        setBGParamBase(model, zbValueTable);
        // 基本单位
        Map<String, Object> unit = new HashMap<>(2);
        unit.put("FNumber", zbValueTable.get("unit"));
        Map<String, Object> subEntity1 = new HashMap<>(4);
        subEntity1.put("FStoreUnitID", unit);
        model.put("SubHeadEntity1", subEntity1);
        Map<String, Object> subEntity2 = new HashMap<>(4);
        subEntity2.put("FSaleUnitId", unit);
        subEntity2.put("FSalePriceUnitId", unit);
        model.put("SubHeadEntity2", subEntity2);
        Map<String, Object> subEntity3 = new HashMap<>(4);
        subEntity3.put("FPurchaseUnitId", unit);
        subEntity3.put("FPurchasePriceUnitId", unit);
        model.put("SubHeadEntity3", subEntity3);
        // 存货类别
        Map<String, Object> subEntity = new HashMap<>(4);
        Map<String, Object> category = new HashMap<>(2);
        category.put("FNumber", zbValueTable.get("stockType"));
        subEntity.put("FCategoryID", category);
        subEntity.put("FBaseUnitId", unit);
        //物料属性
        String materialProperty = (String) zbValueTable.get("materialProperty");
        subEntity.put("FErpClsID", materialProperty);
        // 外购->允许采购
        if (OUT_SOURCING.equals(materialProperty)) {
            subEntity.put("FIsPurchase", true);
        }
        model.put("SubHeadEntity", subEntity);
        Map<String, Object> cg = new HashMap<>(8);
        // 固定提前期
        cg.put("FFixLeadTime", zbValueTable.get("fixedLeadTime"));
        // 最小起订量
        cg.put("FMinPOQty", zbValueTable.get("minNum"));
        // 批量增量
        cg.put("FIncreaseQty", zbValueTable.get("batchIncrement"));
        model.put("SubHeadEntity4", cg);
        Map<String, Object> entity5 = new HashMap<>(8);
        // 发料方式
        entity5.put("FIssueType", zbValueTable.get("sendType"));
        // 超发控制方式
        entity5.put("FOverControlMode", zbValueTable.get("controType"));
        entity5.put("FProduceUnitId", unit);
        entity5.put("FBOMUnitId", unit);
        entity5.put("FMinIssueUnitId", unit);
        Map<String, Object> pickStock = new HashMap<>(2);
        pickStock.put("FNumber", zbValueTable.get("sendStock"));
        entity5.put("FPickStockId", pickStock);
        model.put("SubHeadEntity5", entity5);
        Map<String, Object> entity7 = new HashMap<>(4);
        entity7.put("FSubconUnitId", unit);
        entity7.put("FSubconPriceUnitId", unit);
        model.put("SubHeadEntity7", entity7);
    }

    /**
     * 设备变更基本参数
     */
    private void setBGParamBase(Map<String, Object> model, Map<String, Object> zbValueTable) {
        // 编码
        model.put("FNumber", zbValueTable.get("materialCode"));
        // 名称
        model.put("FName", zbValueTable.get("materialName"));
        // 规格型号
        model.put("FSpecification", zbValueTable.get("specification"));
        // 品牌
        model.put("F_pp", zbValueTable.get("brand"));
        // 物料分组
        Map<String, Object> wlfz = new HashMap<>(2);
        wlfz.put("FNumber", zbValueTable.get("materialGroup"));
        model.put("FMaterialGroup", wlfz);
        // 财务记账类别
        Map<String, Object> cw = new HashMap<>(2);
        cw.put("FNumber", zbValueTable.get("tallyType"));
        model.put("F_WB_CWJZLB", cw);
        // 描述
        model.put("FDescription", zbValueTable.get("remark"));
        // 产地
        model.put("F_WB_CD", zbValueTable.get("producingArea"));
        // 第三方证书
        model.put("F_WB_DSFRZ", zbValueTable.get("certificate"));
        // 机种号
        model.put("F_WB_JZH", zbValueTable.get("machineNo"));
        // 桩体颜色
        model.put("F_ztys", zbValueTable.get("color"));
        // 枪品牌
        model.put("F_qpp", zbValueTable.get("spearBrand"));
        // 卡类型
        model.put("F_klx", zbValueTable.get("cardType"));
        // 枪长度
        model.put("F_qcd", zbValueTable.get("spearLength"));
    }
}