package com.h3bpm.starcharge.common.bean;

/**
 * restfulCode
 *
 * @author LLongAgo
 * @date 2019/3/9
 * @since 1.0.0
 */
public enum RestfulErrorCode {
    /**
     * 成功
     */
    SUCCESS("0", "ok"),
    LOGINERROR("300", "登录失败，请检查!"),
    ERROR("500", "失败!");

    private String code;
    private String msg;

    private RestfulErrorCode (String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
}