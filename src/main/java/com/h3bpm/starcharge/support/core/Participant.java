package com.h3bpm.starcharge.support.core;

/**
 * 参与方
 */
public enum Participant {

	PA("partyA","甲方"),

	PB("partyB", "乙方"),

	PA_TEL("PATel","甲方电话"),

	PB_TEL("PBTel", "乙方电话"),

	;

	// 私有化构造函数
	private Participant(String name, String displayName) {
		this.name = name;
		this.displayName = displayName;
	}

	// 字段名称
	private String name;

	// 展示名称
	private String displayName;

	public String getName() {
		return name;
	}

	public String getDisplayName() {
		return displayName;
	}
}
