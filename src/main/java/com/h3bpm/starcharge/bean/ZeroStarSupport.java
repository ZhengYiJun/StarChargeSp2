package com.h3bpm.starcharge.bean;

/**
 * @Author : Coco
 * @Description :
 * @Date : Create in 18:35 2019/1/10
 * @Modified :
 */
public enum ZeroStarSupport {
    /**
     * ZeroStarSupport
     */
    contractNo,
    partyA,
    partyB,
    otherContent,
    projectAddress,
    projectCost,
    projectCostTotal,
    stubAmountBao,
    stubPriceAC,
    stubQtyAC,
    stubAmountAC,
    stubPriceDC,
    stubQtyDC,
    stubAmountDC,
    completeDays,
    warrantyYears,
    payMethodAmt100,
    payMethodAmt90,
    legalRepresentA,
    agentA,
    addressA,
    contactsA,
    telA,
    openBankA,
    bankAccountA,
    taxRegistNoA,
    legalRepresentB,
    agentB,
    addressB,
    contactsB,
    telB,
    openBankB,
    bankAccountB,
    taxRegistNoB,
    projectContent,
    otherCost,
    otherty,
    productModle,
    priceA,
    numA,
    priceB,
    priceBUp,
    costTotalUp,
    PriceUp100,
    priceUp90,
    rate
}
