package api.domain.contract.returnvo;

import java.util.List;

public class ReturnContract {

    private String contractId;

    private List<ReturnReceiver> receivers;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public List<ReturnReceiver> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<ReturnReceiver> receivers) {
        this.receivers = receivers;
    }
}
