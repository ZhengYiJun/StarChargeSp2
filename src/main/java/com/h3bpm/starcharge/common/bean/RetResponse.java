package com.h3bpm.starcharge.common.bean;

/**
 * RetResponse
 *
 * @author LLongAgo
 * @date 2019/3/22
 * @since 1.0.0
 */
public class RetResponse {
    private final static String SUCCESS = "ok";

    public static RetResult makeOKRsp() {
        return new RetResult().setCode(RestfulErrorCode.SUCCESS).setMsg(SUCCESS);
    }

    public static RetResult makeOKRsp(Object data) {
        return new RetResult().setCode(RestfulErrorCode.SUCCESS).setMsg(SUCCESS).setData(data);
    }

    public static RetResult makeErrRsp(String message) {
        return new RetResult().setCode(RestfulErrorCode.ERROR).setMsg(message);
    }

    public static RetResult makeRsp(String code, String msg) {
        return new RetResult().setCode(code).setMsg(msg);
    }

    public static RetResult makeRsp(String code, String msg, Object data) {
        return new RetResult().setCode(code).setMsg(msg).setData(data);
    }
}