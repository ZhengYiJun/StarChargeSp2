package api.domain.contract.returnvo;

public enum ContractMessageType {
    /**
     * 合同发送成功
     */
    CONTRACT_SEND_RESULT,
    /**
     * 合同被收件人签署
     */
    OPERATION_COMPLETE,
    /**
     * 合同签署完成（合同已结束）
     */
    CONTRACT_COMPLETE,
    /**
     *  企业实名认证通过
     */
    ENTERPRISE_AUTH,


}
