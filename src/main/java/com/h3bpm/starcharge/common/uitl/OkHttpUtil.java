package com.h3bpm.starcharge.common.uitl;

import okhttp3.*;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * OkHttp封装工具类
 *
 * @author LLongAgo
 * @date 2019/2/20
 * @since 1.0.0
 */
public class OkHttpUtil {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static OkHttpClient client = new OkHttpClient().newBuilder().connectTimeout(3 * 1000, TimeUnit.MILLISECONDS).build();

    /**
     * POST提交Json数据
     *
     * @param url  url地址
     * @param json json字符串
     * @return String
     * @throws IOException IOException
     */
    public static String postByJson(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("连接超时");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });

        Response response = call.execute();
        if (response.isSuccessful()) {
            return null == response.body() ? "" : response.body().string();
        } else {
            throw new IOException("Unexpected code " + response);
        }
    }

    /**
     * post提交表单数据
     * @param url       url地址
     * @param paramsMap 参数
     * @return String
     * @throws IOException IOException
     */
    public static String post(String url, Map<String, String> paramsMap) throws IOException {
        FormBody.Builder builder = new FormBody.Builder();
        for (String key : paramsMap.keySet()) {
            //追加表单信息
            builder.add(key, paramsMap.get(key));
        }
        RequestBody formBody = builder.build();
        Request request = new Request.Builder().url(url).post(formBody).build();
        Call call = client.newCall(request);
        Response response = call.execute();
        if (response.isSuccessful()) {
            return null == response.body() ? "" : response.body().string();
        } else {
            throw new IOException("Unexpected code " + response);
        }
    }

    /**
     * post提交表单数据
     * @param url       url地址
     * @return String
     * @throws IOException IOException
     */
    public static String postERP(String url) throws IOException {
        FormBody.Builder builder = new FormBody.Builder();

        RequestBody formBody = builder.build();
        Request request = new Request.Builder().url(url).post(formBody).build();
        Call call = client.newCall(request);
        Response response = call.execute();
        if (response.isSuccessful()) {
            return null == response.body() ? "" : response.body().string();
        } else {
            throw new IOException("Unexpected code " + response);
        }
    }

}