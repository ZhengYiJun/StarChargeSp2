package com.h3bpm.starcharge.support.core;

import com.h3bpm.starcharge.support.core.annontation.ESealRequestParam;

/**
 * @ClassName Contracts
 * @Desciption 合同内容接口，所有定义上上签需要上传字段的Class都必须实现该接口
 * @Author lvyz
 * @Date 2019/6/10 20:23
 **/
public class BasicContractsField {

	@ESealRequestParam(eSealField = "合同编号", bpmField = "contractCode")
	private String contractCode;

	@ESealRequestParam(eSealField = "签订地", bpmField = "signAddr")
	private String signAddress;

	@ESealRequestParam(eSealField = "甲方", bpmField = "partyA")
	private String partyA;

	@ESealRequestParam(eSealField = "甲方地址", bpmField = "PAAdd")
	private String partAAddress;

	@ESealRequestParam(eSealField = "乙方", bpmField = "partyB")
	private String partyB;

	@ESealRequestParam(eSealField = "乙方地址", bpmField = "PBAdd")
	private String partBAddress;

	@ESealRequestParam(eSealField = "合同份数", bpmField = "contactQty")
	private String contactQty;

	@ESealRequestParam(eSealField = "乙方执合同数", bpmField = "pBContactQty")
	private String partBContactQty;

	@ESealRequestParam(eSealField = "电子邮箱", bpmField = "PAEmail")
	private String partAEmail;

	@ESealRequestParam(eSealField = "乙方电子邮箱", bpmField = "PBEmail")
	private String partBEmail;

	@ESealRequestParam(eSealField = "甲方法定代表人", bpmField = "PALegal")
	private String partALegal;

	@ESealRequestParam(eSealField = "甲方电话", bpmField = "PATel")
	private String partATel;

	@ESealRequestParam(eSealField = "甲方开户银行", bpmField = "PABank")
	private String partABank;

	@ESealRequestParam(eSealField = "甲方账号", bpmField = "PAAcc")
	private String partAAcc;

	@ESealRequestParam(eSealField = "甲方税号", bpmField = "PATax")
	private String partATax;

	@ESealRequestParam(eSealField = "甲方联系人", bpmField = "PAContacts")
	private String partAContacts;

	@ESealRequestParam(eSealField = "乙方法定代表人", bpmField = "PBLegal")
	private String partBLegal;

	@ESealRequestParam(eSealField = "乙方电话", bpmField = "PBTel")
	private String partBTel;

	@ESealRequestParam(eSealField = "乙方开户银行", bpmField = "PBBank")
	private String partBBank;

	@ESealRequestParam(eSealField = "乙方账号", bpmField = "PBAcc")
	private String partBAcc;

	@ESealRequestParam(eSealField = "乙方税号", bpmField = "PBTax")
	private String partBTax;

	@ESealRequestParam(eSealField = "乙方联系人", bpmField = "PBContacts")
	private String partBContacts;

	public String getContractCode() {
		return contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public String getSignAddress() {
		return signAddress;
	}

	public void setSignAddress(String signAddress) {
		this.signAddress = signAddress;
	}

	public String getPartyA() {
		return partyA;
	}

	public void setPartyA(String partyA) {
		this.partyA = partyA;
	}

	public String getPartAAddress() {
		return partAAddress;
	}

	public void setPartAAddress(String partAAddress) {
		this.partAAddress = partAAddress;
	}

	public String getPartyB() {
		return partyB;
	}

	public void setPartyB(String partyB) {
		this.partyB = partyB;
	}

	public String getPartBAddress() {
		return partBAddress;
	}

	public void setPartBAddress(String partBAddress) {
		this.partBAddress = partBAddress;
	}

	public String getContactQty() {
		return contactQty;
	}

	public void setContactQty(String contactQty) {
		this.contactQty = contactQty;
	}

	public String getPartBContactQty() {
		return partBContactQty;
	}

	public void setPartBContactQty(String partBContactQty) {
		this.partBContactQty = partBContactQty;
	}

	public String getPartAEmail() {
		return partAEmail;
	}

	public void setPartAEmail(String partAEmail) {
		this.partAEmail = partAEmail;
	}

	public String getPartBEmail() {
		return partBEmail;
	}

	public void setPartBEmail(String partBEmail) {
		this.partBEmail = partBEmail;
	}

	public String getPartALegal() {
		return partALegal;
	}

	public void setPartALegal(String partALegal) {
		this.partALegal = partALegal;
	}

	public String getPartATel() {
		return partATel;
	}

	public void setPartATel(String partATel) {
		this.partATel = partATel;
	}

	public String getPartABank() {
		return partABank;
	}

	public void setPartABank(String partABank) {
		this.partABank = partABank;
	}

	public String getPartAAcc() {
		return partAAcc;
	}

	public void setPartAAcc(String partAAcc) {
		this.partAAcc = partAAcc;
	}

	public String getPartATax() {
		return partATax;
	}

	public void setPartATax(String partATax) {
		this.partATax = partATax;
	}

	public String getPartAContacts() {
		return partAContacts;
	}

	public void setPartAContacts(String partAContacts) {
		this.partAContacts = partAContacts;
	}

	public String getPartBLegal() {
		return partBLegal;
	}

	public void setPartBLegal(String partBLegal) {
		this.partBLegal = partBLegal;
	}

	public String getPartBTel() {
		return partBTel;
	}

	public void setPartBTel(String partBTel) {
		this.partBTel = partBTel;
	}

	public String getPartBBank() {
		return partBBank;
	}

	public void setPartBBank(String partBBank) {
		this.partBBank = partBBank;
	}

	public String getPartBAcc() {
		return partBAcc;
	}

	public void setPartBAcc(String partBAcc) {
		this.partBAcc = partBAcc;
	}

	public String getPartBTax() {
		return partBTax;
	}

	public void setPartBTax(String partBTax) {
		this.partBTax = partBTax;
	}

	public String getPartBContacts() {
		return partBContacts;
	}

	public void setPartBContacts(String partBContacts) {
		this.partBContacts = partBContacts;
	}
}
