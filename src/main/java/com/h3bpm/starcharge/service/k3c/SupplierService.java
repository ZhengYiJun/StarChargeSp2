package com.h3bpm.starcharge.service.k3c;

import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.starcharge.common.bean.RetResult;

public interface SupplierService {

    RetResult save(InstanceData instanceData) throws Exception;

    RetResult info(String number) throws Exception;

    RetResult edit(InstanceData instanceData) throws Exception;
}
