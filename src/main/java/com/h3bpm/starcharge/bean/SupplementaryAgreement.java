package com.h3bpm.starcharge.bean;

public enum SupplementaryAgreement {

    /**
     * 补充协议
     */
    ObjectID,
    contractTerms,
    CreatedTime,
    partyA4,
    originalContents,
    ParentPropertyName,
    ModifiedBy,
    newContents,
    Name,
    partyBRepresentative,
    OwnerId,
    ModifiedTime,
    partyBHolds,
    CappingSeal,
    contractName,
    ParentIndex,
    RunningInstanceId,
    ParentObjectID,
    CreatedBy,
    serialNumber,
    partyARepresentative,
    partyB4,
    contractNumber,
    CreatedByParentId,
    OwnerParentId,
    contractId,
    contractNo,
    phoneB,
    signDate,
}
