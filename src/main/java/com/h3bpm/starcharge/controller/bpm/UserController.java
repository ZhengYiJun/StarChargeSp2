package com.h3bpm.starcharge.controller.bpm;

import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import cn.hutool.core.io.IoUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.ErrorUtil;
import com.h3bpm.starcharge.service.bpm.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author LLongAgo
 * @date 2019/5/8
 * @since 1.0.0
 */
@Controller
@RequestMapping("/starCharge/user")
public class UserController extends ControllerBase {

    @Autowired
    private UserService userService;

    @Override
    public String getFunctionCode() {
        return null;
    }

    /**
     * Excel导入新员工
     *
     * @param instanceId    流程id
     * @return RetResult
     */
    @ResponseBody
    @RequestMapping("/addByExcel")
    public RetResult addByExcel(@RequestParam String instanceId) {
        try {
            InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            return userService.addByExcel(instanceData);
        } catch (Exception e) {
            return ErrorUtil.generalException(e);
        }
    }

    /**
     * 员工调岗
     *
     * @param instanceId    流程id
     * @return RetResult
     */
    @ResponseBody
    @RequestMapping("/changeDeptByExcel")
    public RetResult changeDeptByExcel(@RequestParam String instanceId) {
        try {
            InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            return userService.changeDeptByExcel(instanceData);
        } catch (Exception e) {
            return ErrorUtil.generalException(e);
        }
    }

    /**
     * 下载所有用户
     *
     * @param response HttpServletResponse
     */
    @RequestMapping(value = "/allUsers")
    public void getAllUsers(HttpServletResponse response) {
        try {
            List<List<String>> rows = userService.getAllUsers();
            outPut(rows, response, "users.xls");
        } catch (Exception e) {
            ErrorUtil.generalException(e);
        }
    }

    /**
     * 下载所有组织负责人
     *
     * @param response HttpServletResponse
     */
    @RequestMapping(value = "/allLeaders")
    public void getDeptLeaders(HttpServletResponse response) {
        try {
            List<List<String>> rows = userService.getDeptLeaders();
            outPut(rows, response, "leaders.xls");
        } catch (Exception e) {
            ErrorUtil.generalException(e);
        }
    }

    /**
     * 下载所有部门ID
     *
     * @param response HttpServletResponse
     */
    @RequestMapping(value = "/deptObjects")
    public void getDeptObjects(HttpServletResponse response) {
        try {
            List<List<String>> rows = userService.getDeptObjects();
            outPut(rows, response, "deptObjects.xls");
        } catch (Exception e) {
            ErrorUtil.generalException(e);
        }
    }

    /**
     * 获取员工角色
     *
     * @param objectId 员工id
     * @return RetResult
     */
    @ResponseBody
    @RequestMapping("/getUserPost")
    public RetResult getUserPost(@RequestParam String objectId) {
        try {
            return userService.getUserPost(objectId);
        } catch (Exception e) {
            return ErrorUtil.generalException(e);
        }
    }

    /**
     * 输出Excel文件流
     */
    private void outPut(List<List<String>> rows, HttpServletResponse response, String fileName) throws Exception {
        // 通过工具类创建writer，默认创建xls格式
        ExcelWriter writer = ExcelUtil.getWriter();
        writer.write(rows);
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
        ServletOutputStream out = response.getOutputStream();
        writer.flush(out, true);
        // 关闭writer，释放内存
        writer.close();
        // 记得关闭输出Servlet流
        IoUtil.close(out);
    }
}