package com.h3bpm.starcharge.bean.k3c;

import java.util.List;

/**
 * 供应商详情
 * @author LLongAgo
 * @date 2019/4/11
 * @since 1.0.0
 */
public class SupplierInfo {

    private String customerName;
    private String customerCode;
    private String country;
    private String address;
    private String qualityPeriod;
    private String YFKTJ;
    private String companyTel;
    private String TYSHXYDM;
    private String registeredAddress;
    private String supplierType;
    private String supplierCategory;
    private String supplierLevel;
    private String customerGroup;
    private String cognateType;
    private String DLSTXRZ;
    private String GYSZSYXQ;
    private String YCTXRZ;
    private String YCZSYXQ;
    private String JGTXRZ;
    private String JGCZSYXQ;
    private String currency;
    private String clearType;
    private String collection;
    private String taxType;
    private String NSDJH;
    private String invoiceType;
    private String defaultTax;
    private String customerGroupId;

    private List<SupplierInfo.Linkman> linkman;
    private List<SupplierInfo.BankInfo> bankInfo;

    public static class Linkman {
        private String id;
        private String linkName;
        private String mobile;
        private String type;
        private String duty;
        private String email;
        private String addressName;
        private String address;
        private String locId;
        private String number;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getId() {
            return id;
        }

        public String getLocId() {
            return locId;
        }

        public void setLocId(String locId) {
            this.locId = locId;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDuty() {
            return duty;
        }

        public void setDuty(String duty) {
            this.duty = duty;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAddressName() {
            return addressName;
        }

        public void setAddressName(String addressName) {
            this.addressName = addressName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLinkName() {
            return linkName;
        }

        public void setLinkName(String linkName) {
            this.linkName = linkName;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class BankInfo {
        private String id;
        private String bank;
        private String account;
        private String accountName;
        private String address;
        private String currency;

        public String getBank() {
            return bank;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setBank(String bank) {
            this.bank = bank;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getAccountName() {
            return accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getQualityPeriod() {
        return qualityPeriod;
    }

    public void setQualityPeriod(String qualityPeriod) {
        this.qualityPeriod = qualityPeriod;
    }

    public String getYFKTJ() {
        return YFKTJ;
    }

    public void setYFKTJ(String YFKTJ) {
        this.YFKTJ = YFKTJ;
    }

    public String getCompanyTel() {
        return companyTel;
    }

    public void setCompanyTel(String companyTel) {
        this.companyTel = companyTel;
    }

    public String getTYSHXYDM() {
        return TYSHXYDM;
    }

    public void setTYSHXYDM(String TYSHXYDM) {
        this.TYSHXYDM = TYSHXYDM;
    }

    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    public String getSupplierType() {
        return supplierType;
    }

    public void setSupplierType(String supplierType) {
        this.supplierType = supplierType;
    }

    public String getSupplierCategory() {
        return supplierCategory;
    }

    public void setSupplierCategory(String supplierCategory) {
        this.supplierCategory = supplierCategory;
    }

    public String getSupplierLevel() {
        return supplierLevel;
    }

    public void setSupplierLevel(String supplierLevel) {
        this.supplierLevel = supplierLevel;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public void setCustomerGroup(String customerGroup) {
        this.customerGroup = customerGroup;
    }

    public String getCognateType() {
        return cognateType;
    }

    public void setCognateType(String cognateType) {
        this.cognateType = cognateType;
    }

    public String getDLSTXRZ() {
        return DLSTXRZ;
    }

    public void setDLSTXRZ(String DLSTXRZ) {
        this.DLSTXRZ = DLSTXRZ;
    }

    public String getGYSZSYXQ() {
        return GYSZSYXQ;
    }

    public void setGYSZSYXQ(String GYSZSYXQ) {
        this.GYSZSYXQ = GYSZSYXQ;
    }

    public String getYCTXRZ() {
        return YCTXRZ;
    }

    public void setYCTXRZ(String YCTXRZ) {
        this.YCTXRZ = YCTXRZ;
    }

    public String getYCZSYXQ() {
        return YCZSYXQ;
    }

    public void setYCZSYXQ(String YCZSYXQ) {
        this.YCZSYXQ = YCZSYXQ;
    }

    public String getJGTXRZ() {
        return JGTXRZ;
    }

    public void setJGTXRZ(String JGTXRZ) {
        this.JGTXRZ = JGTXRZ;
    }

    public String getJGCZSYXQ() {
        return JGCZSYXQ;
    }

    public void setJGCZSYXQ(String JGCZSYXQ) {
        this.JGCZSYXQ = JGCZSYXQ;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getClearType() {
        return clearType;
    }

    public void setClearType(String clearType) {
        this.clearType = clearType;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getNSDJH() {
        return NSDJH;
    }

    public void setNSDJH(String NSDJH) {
        this.NSDJH = NSDJH;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getDefaultTax() {
        return defaultTax;
    }

    public void setDefaultTax(String defaultTax) {
        this.defaultTax = defaultTax;
    }

    public String getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(String customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public List<SupplierInfo.Linkman> getLinkman() {
        return linkman;
    }

    public void setLinkman(List<SupplierInfo.Linkman> linkman) {
        this.linkman = linkman;
    }

    public List<SupplierInfo.BankInfo> getBankInfo() {
        return bankInfo;
    }

    public void setBankInfo(List<SupplierInfo.BankInfo> bankInfo) {
        this.bankInfo = bankInfo;
    }
}