package com.h3bpm.starcharge.service;

import com.h3bpm.base.res.ResBody;
import com.h3bpm.starcharge.common.bean.WorkflowComment;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author qinyt
 * @className WorkflowService
 * @description workflow服务类接口
 * @date 2019/3/1 13:21
 **/
public interface WorkflowCommentService {

    /**
     *  根据instanceId得到相应的评论
     * @param instanceId
     * @return ResBody 包含成功/失败信息的ResBody
     */
    ResBody getComments(String instanceId);


    /**
     * 新增评论
     * @param comment 评论具体信息
     * @return ResBody 包含成功失败信息的ResBody
     */
    ResBody addComment(WorkflowComment comment);

    /**
     * 上传文件到默认的临时目录
     * @param file 要上传的文件
     * @param uploadByImage 是否是按图片方式上传
     * @return 失败则返回失败信息，成功则返回文件路径
     */
    ResBody uploadAttachment(MultipartFile file, boolean uploadByImage);

    /**
     * 推送流程结束的消息给发起人
     * @param instanceId
     * @return
     */
    ResBody sendEndMessage(String instanceId);
}
