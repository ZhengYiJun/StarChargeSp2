package com.h3bpm.starcharge.service.bpm;

import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.param.workflow.StartWorkflowNewParam;
import com.h3bpm.starcharge.ret.workflow.StartWorkflowNewRet;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * StarchargeBpmService class
 *
 * @author llongago
 * @date 2019/2/18
 */
public interface StarChargeBpmService {
    /**
     * 发起流程
     * @param param StartWorkflowNewParam
     * @return StartWorkflowNewRet
     */
    StartWorkflowNewRet startWorkflowNew(StartWorkflowNewParam param);

    /**
     * 获取流程明细
     *
     * @param instanceId 流程id
     * @return RetResult
     */
    String getWorkItemObject(String instanceId) throws Exception;

    /**
     * 获取流程明细
     *
     * @param instanceId 流程id
     * @return List<Map < String ,   String>>
     */
    List<Map<String, String>> getParticipants(String instanceId) throws Exception;

    /**
     * 获取当前审批人协办是否在进行中
     * @param instanceId
     * @param creator
     * @return
     */
    Boolean isWorkItemAssist(String instanceId,String creator);

}