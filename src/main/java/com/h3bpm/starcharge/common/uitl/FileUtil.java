package com.h3bpm.starcharge.common.uitl;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * 文件工具
 *
 * @author LLongAgo
 * @date 2019/3/22
 * @since 1.0.0
 */
public class FileUtil {

    public static String readJsonData(String pactFile) throws IOException {
        // 读取文件数据
        StringBuilder sb = new StringBuilder();
        Resource resource = new ClassPathResource(pactFile);

        File myFile = resource.getFile();
        if (!myFile.exists()) {
            System.err.println("Can't Find " + pactFile);
        }
        try {
            FileInputStream fis = new FileInputStream(myFile);
            InputStreamReader inputStreamReader = new InputStreamReader(fis, StandardCharsets.UTF_8.name());
            BufferedReader in  = new BufferedReader(inputStreamReader);

            String str;
            while ((str = in.readLine()) != null) {
                sb.append(str);
            }
            in.close();
        } catch (IOException e) {
            e.getStackTrace();
        }
        return sb.toString();
    }

    public static ArrayList<Object> fileToArray(File file) {
        ArrayList<Object> arrayList = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String s = null;
            while ((s = br.readLine()) != null) {
                arrayList.add(s);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return arrayList;
    }
}