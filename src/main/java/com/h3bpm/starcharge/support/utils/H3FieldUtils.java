package com.h3bpm.starcharge.support.utils;

import OThinker.H3.Entity.DataModel.BizObjectSchema;
import OThinker.H3.Entity.DataModel.PropertySchema;
import com.h3bpm.base.util.AppUtility;
import com.h3bpm.starcharge.support.core.H3FieldEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName H3FieldUtils
 * @Desciption 获取H3流程模型数据列表字段工具类
 * @Author lvyz
 * @Date 2019/6/10 18:17
 **/
public class H3FieldUtils {

	// 系统定义的属性
	private final static List<String> SYS_FIELDS = Arrays.asList("CreatedBy", "CreatedTime", "ObjectID", "ModifiedBy", "Name", "OwnerId", "ModifiedTime", "CreatedByParentId", "OwnerParentId", "RunningInstanceId","ContractId","contractId");

	/**
	 * 获取属性
	 * @param workflowCode
	 * @return
	 */
	public static List<H3FieldEntity> getPropertySchemaList(String workflowCode) throws Exception {

		BizObjectSchema schema = AppUtility.getEngine().getBizObjectManager().GetDraftSchema(workflowCode);

		return getH3FieldChildren(schema.getProperties());
	}

	/**
	 * 获取子表的属性
	 * @param properties
	 * @return
	 */
	private static List<H3FieldEntity> getH3FieldChildren(PropertySchema[] properties){

		List<H3FieldEntity> list = new ArrayList<>();

		if (properties == null){
			return list;
		}

		for (PropertySchema schema : properties) {

			// 去掉系统定义的属性
			if(SYS_FIELDS.contains(schema.getName())){
				continue;
			}

			H3FieldEntity entity = new H3FieldEntity();
			entity.setDisplayName(schema.getDisplayName());
			entity.setName(schema.getName());

			// 包含子表-递归调用本方法
			if(schema.getChildSchema() != null && schema.getChildSchema().getProperties() != null){

				List<H3FieldEntity> childList = getH3FieldChildren(schema.getChildSchema().getProperties());
				entity.setChildList(childList);
			}

			list.add(entity);
		}

		return list;
	}
}
