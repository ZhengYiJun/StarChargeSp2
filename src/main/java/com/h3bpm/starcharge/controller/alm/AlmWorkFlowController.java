package com.h3bpm.starcharge.controller.alm;

import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.alibaba.fastjson.JSONObject;
import com.h3bpm.starcharge.bean.alm.AlmCreateWorkflow;
import com.h3bpm.starcharge.common.bean.RetResponse;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.ErrorUtil;
import com.h3bpm.starcharge.service.alm.AlmWorkflowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @author lvvyz
 */
@Controller
@RequestMapping("/alm/workflow")
public class AlmWorkFlowController extends ControllerBase {

    private static final Logger log = LoggerFactory.getLogger(AlmWorkFlowController.class);

    @Override
    public String getFunctionCode() {
        return null;
    }

    @Autowired
    private AlmWorkflowService almWorkflowService;

    /**
     * @param workflowCode  流程code
     * @param accountMobile 传递用户手机号
     * @param finishStart   是否跳过第一个流程
     * @param formArrayStr  表单参数JSON
     * @return
     */
    @ResponseBody
    @PostMapping("/create")
    public RetResult create(@RequestParam String workflowCode,
                            @RequestParam String accountMobile,
                            @RequestParam boolean finishStart,
                            @RequestParam(value = "formParam") String formArrayStr) {
        try {

            log.error("[ALM]创建流程实例 | {}", formArrayStr);

            // 创建流程参数
            AlmCreateWorkflow entity = new AlmCreateWorkflow();
            entity.setWorkflowCode(workflowCode);
            entity.setAccountMobile(accountMobile);
            entity.setFinishStart(finishStart);
            entity.setFormParam(formArrayStr);

            List<JSONObject> resultList = this.almWorkflowService.createFlows(entity);

            return RetResponse.makeOKRsp(resultList);

        } catch (Exception e) {

            log.error("[ALM]创建流程实例异常, MSG = {}", e.getMessage());
            e.printStackTrace();

            return ErrorUtil.generalException(e);
        }
    }

    /**
     * 审批通过的时候调用
     *
     * @param instanceId
     * @param workflowCode
     */
    @ResponseBody
    @PostMapping("/success")
    public RetResult success(@RequestParam String instanceId, @RequestParam String workflowCode) {

        try {

            this.almWorkflowService.success(instanceId, workflowCode);

            return RetResponse.makeOKRsp();

        } catch (Exception e) {

            log.error("[ALM]审批通过,回调ALM异常, msg = {}", e.getMessage());
            e.printStackTrace();

            return RetResponse.makeErrRsp("系统异常");
        }
    }

    /**
     * 合同审批通过的时候调用
     *
     * @param instanceId
     * @param workflowCode
     */
    @ResponseBody
    @PostMapping("/contractsSuccess")
    public RetResult contractsSuccess(@RequestParam String instanceId, @RequestParam String workflowCode) {

        try {

            this.almWorkflowService.contractsSuccess(instanceId, workflowCode);

            return RetResponse.makeOKRsp();

        } catch (Exception e) {

            log.error("[ALM]审批通过,回调ALM异常, msg = {}", e.getMessage());
            e.printStackTrace();

            return RetResponse.makeErrRsp("系统异常");
        }
    }

    /**
     * 审批拒绝的时候调用
     *
     * @param instanceId
     * @param workflowCode
     */
    @ResponseBody
    @PostMapping("/refuse")
    public RetResult refuse(@RequestParam String instanceId, @RequestParam String workflowCode) {

        try {

            this.almWorkflowService.refuse(instanceId, workflowCode);

            return RetResponse.makeOKRsp();

        } catch (Exception e) {

            log.error("[ALM]审批驳回,回调ALM异常, msg = {}", e.getMessage());
            e.printStackTrace();

            return RetResponse.makeErrRsp("系统异常");
        }
    }

    /**
     * 删除一个流程实例-测试用
     *
     * @param instanceId
     * @return
     */
    @ResponseBody
    @PostMapping("/remove")
    public RetResult remove(String instanceId) {

        try {

            getEngine().getInstanceManager().RemoveInstance(instanceId, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return RetResponse.makeOKRsp();
    }

    /**
     * 查询流程实例详情-测试用
     *
     * @param instanceId
     * @return
     */
    @ResponseBody
    @PostMapping("/findDetail")
    public RetResult findDetail(String instanceId, String workflowCode) {

        try {

            InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);

            Map<String, Object> map = instanceData.getBizObject().getValueTable();

            return RetResponse.makeOKRsp(map.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return RetResponse.makeOKRsp();
    }
}