package com.h3bpm.starcharge.pojo;

public enum WorkFlowCode {

    /**
     * 非标
     */
    SealApply,
    OtherContract,

    /**
     * 工程类合同
     */
    OEMStandardEngineering,


    /**
     * 标准
     */
    sales,
    hostFactorySale,

    /**
     * 标准 投资类
     */
    InvestmentContract,
    ChargePolicy,
    NonVehicleCharging,
    AgentSigning,
    SupplementaryAgreement,
    OperatingContract,
    AgreementNoPartyC,
    CooperationAgreement,
    CooperationAgree,
    EnergyVehicles,
    SupplyContract,
    ChargingFacilities,
    SiteLeaseContract,


    /**
     *  标准 工程类
     */
    GeneralProject,
    LowProcurement,
    projectDesign,
    zeroStarSupport,
    newspaperService,
    MembraneStructure
}
