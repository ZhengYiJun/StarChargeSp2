package api.domain.contract.create;

public class CreateDocumentVO {
    /**
     * 文档的顺序
     */
    private Integer order ;
    /**
     * 文件的内容，BASE64编码
     */
    private String content;
    /**
     * 文件名称
     */
    private String  fileName;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return "CreateDocumentVO{" +
                "order=" + order +
                ", content='" + content + '\'' +
                ", fileName='" + fileName + '\'' +
                '}';
    }
}
