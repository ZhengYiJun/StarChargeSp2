package com.h3bpm.starcharge.bean;

/**
 * @Author : Coco
 * @Description :
 * @Date : Create in 18:36 2019/1/10
 * @Modified :
 */
public enum NewspaperService {
    /**
     * NewspaperService
     */
    contractNo,
    partyA,
    partyB,
    entryName,
    newspaperCapacity,
    timeLimit,
    serviceFeeUpper,
    serviceFeeLower,
    accountPercent,
    representA,
    representIdNoA,
    representB,
    representIdNoB,
    legalRepresentA,
    agentA,
    addressA,
    openBankA,
    bankAccountA,
    postalCodeA,
    telA,
    legalRepresentB,
    agentB,
    addressB,
    openBankB,
    bankAccountB,
    postalCodeB,
    telB,
    otherContent
}
