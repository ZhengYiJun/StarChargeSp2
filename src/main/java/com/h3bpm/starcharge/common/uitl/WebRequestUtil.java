package com.h3bpm.starcharge.common.uitl;

import OThinker.Common.DotNetToJavaStringHelper;
import org.apache.cxf.helpers.IOUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class WebRequestUtil {

    static Logger logger = Logger.getLogger(WebRequestUtil.class);

    public static String LOGINURL = "http://trddev.jingruiauto.com/v1/sys/security/trdsys/login";


    public static String getWebRequest(String getUrl, String dataEncode,Map<String,String> headerMap)
            throws Exception {
        String ret = "";
        HttpURLConnection con = null;
        InputStream is = null;
        BufferedReader reader = null;

        try {
            StringBuffer sbf = new StringBuffer();
            getUrl = getUrl.replaceAll("\r|\n", "");
            URL url = new URL(getUrl);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestProperty("Content-Type", "application/json");
            if (headerMap != null && headerMap.size() > 0 ) {
                for (String key : headerMap.keySet()) {
                    con.setRequestProperty(key, headerMap.get(key));
                }
            }
            logger.info("=======普通连接========="+getUrl);
            con.connect();
            logger.info("========连接成功======");
            is = con.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is, dataEncode));
            String line = null;
            while ((line = reader.readLine()) != null) {
                sbf.append(line);
                sbf.append("\r\n");
            }
            ret = sbf.toString();
        } catch (Exception ex) {
            logger.error("=====error====="+ex);
        } finally {
            // 关闭流
            reader.close();
            is.close();
            con.disconnect();
        }
        return ret;

    }

    //带token的post请求
    public static String postWebRequest(String postUrl, String paramData,
            String dataEncode,Map<String,String> headerMap) throws Exception {
			String ret = "";
			InputStream in = null;
			HttpURLConnection con = null;
			DataOutputStream out = null;
			try {
				URL url = new URL(postUrl);
				con = (HttpURLConnection) url.openConnection();
				byte[] data = paramData.getBytes(dataEncode);
				con.setDoOutput(true);
				con.setDoInput(true);
				con.setUseCaches(false);
				con.setRequestMethod("POST");
				con.setRequestProperty("Charset", dataEncode);
				con.setRequestProperty("Content-Length",
				String.valueOf(data.length));
				con.setRequestProperty("Content-Type", "application/json");

				 if (headerMap != null && headerMap.size() > 0 ) {
                     for (String key : headerMap.keySet()) {
                         con.setRequestProperty(key, headerMap.get(key));
                     }
                 }
				con.connect();
				out = new DataOutputStream(con.getOutputStream());
				out.write(data);
				out.flush();
				if (con.getResponseCode() == 200) {
					logger.info("==success==");
					in = con.getInputStream();
					ret = IOUtils.toString(in);
				}else {
					return "连接失败";
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				// 关闭流
				if(null!=in) {
					in.close();
				}
				if(null!=con) {
					con.disconnect();
				}
				if(null!=out) {
					out.close();
				}
			}
			return ret;
		
		}
    
    

}
