package com.h3bpm.starcharge.controller.erp;

import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.starcharge.common.bean.JsonResult;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.ErrorUtil;
import com.h3bpm.starcharge.service.erp.ErpCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/erp/ErpCustomer")
public class ErpCustomerController extends ControllerBase {

    @Autowired
    private ErpCustomerService erpCustomerService;

    @Override
    public String getFunctionCode() {
        return null;
    }


    /**
     * 新增商户
     * @param instanceId    instanceId
     * @return RetResult
     */
    @RequestMapping(value = "/save", produces = "application/json;charset=utf-8")
    @ResponseBody
    public RetResult save(@RequestParam String instanceId) {
        try {
            InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            RetResult SDFS=erpCustomerService.save(instanceData);
            return SDFS;
        } catch (Exception e) {
            return ErrorUtil.generalException(e);
        }
    }

    /**
     * 场站定位
     *
     * @return JsonResult
     */
    @RequestMapping(value = "/classList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public JsonResult getClassList() {
        try {
            return erpCustomerService.getClassList();
        } catch (Exception e) {
            return ErrorUtil.packageServerError(e);
        }
    }



}
