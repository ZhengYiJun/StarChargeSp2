package com.h3bpm.starcharge.controller.bpm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.h3bpm.base.util.AppUtility;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.Controllers.Designer.WorkflowHanderController;
import OThinker.H3.Entity.Instance.InstanceContext;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import OThinker.H3.Entity.WorkflowTemplate.Activity.Activity;
import OThinker.H3.Entity.WorkflowTemplate.WorkflowDocument.WorkflowDocument;

@Controller
@RequestMapping("/Portal/Mobile")
public class StarInstanceController extends WorkflowHanderController{

	@Override
	public String getFunctionCode() {
		
		return null;
	}
	@SuppressWarnings("static-access")
	@GetMapping("/SearchApprover")
	@ResponseBody
	public JSONObject getInstanceManager(String instanceId) {
		
		JSONObject json=new JSONObject();
		json.put("errorNo", 1);
		json.put("errorMessage","失败"); 
	try {
			InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            InstanceContext context = instanceData.getInstanceContext();
            WorkflowDocument workflowTemplate = AppUtility.getEngine().getWorkflowManager().GetPublishedTemplate(context.getWorkflowCode(), context.getWorkflowVersion());

			List<Map<String,Object>> approverList=new ArrayList<>();
			for(Activity act :workflowTemplate.getActivities()) {
				if(act.getIsApproveActivity()) {
					Map<String,Object> map=new HashMap<>(2);
					 List<Object> list=GetParticipants(act.getActivityCode(),instanceId);
					 StringBuffer name=new StringBuffer();
					 for(Object a:list) {
						 @SuppressWarnings("unchecked")
						Map<String,Object> approve=(Map<String, Object>) a;
						   name.append(approve.get("Name")+";");
					 }
					 map.put("name", name.toString());
					 map.put("activityName", act.getDisplayName());
					 approverList.add(map);
				}
				
				
			}
			//成功
			json.put("errorNo", 0);
			json.put("data",approverList); 
			json.put("errorMessage","成功"); 
		return	json;
		} catch (Exception e) {
			json.put("errorNo", 1);//失败
			json.put("errorMessage","失败"); 
			e.printStackTrace();
		}
		return json;
	}

}
