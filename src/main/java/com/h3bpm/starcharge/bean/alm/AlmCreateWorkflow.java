package com.h3bpm.starcharge.bean.alm;

/***
 * create by lvyz on 2019/5/24
 */
public class AlmCreateWorkflow {

    public static final String FILE_URL = "fileUrls";

    // 流程编号
    private String workflowCode;

    // 账户手机号
    private String accountMobile;

    // 是否完成第一个流程
    private boolean finishStart;

    // 提交参数
    private String formParam;

    public String getWorkflowCode() {
        return workflowCode;
    }

    public void setWorkflowCode(String workflowCode) {
        this.workflowCode = workflowCode;
    }

    public String getAccountMobile() {
        return accountMobile;
    }

    public void setAccountMobile(String accountMobile) {
        this.accountMobile = accountMobile;
    }

    public String isFinishStart() {
        return finishStart ? "TRUE" : "FALSE";
    }

    public void setFinishStart(boolean finishStart) {
        this.finishStart = finishStart;
    }

    public String getFormParam() {
        return formParam;
    }

    public void setFormParam(String formParam) {
        this.formParam = formParam;
    }
}
