package com.h3bpm.starcharge.support.eseal;

import com.h3bpm.starcharge.support.core.ContractsType;

/**
 * @ClassName ContractFactory
 * @Desciption 上上签（电子章）处理器工厂
 * @Author lvyz
 * @Date 2019/6/10 20:36
 **/
public class ContractsFactory {

	/**
	 * 获取合同处理器
	 * @param type
	 * @return
	 */
	public static AbstractESeal create(ContractsType type){

		if(ContractsType.STANDARD_ADD_CAPACITY.equals(type)){
			return new StandardAddCapacity();
		}

		if(ContractsType.STANDARD_BOT.equals(type)){
			return new StandardBOT();
		}

		if(ContractsType.STANDARD_COMMODITY.equals(type)){
			return new StandardCommodity();
		}

		if(ContractsType.STANDARD_NO_ADD_CAPACITY.equals(type)){
			return new StandardNoAddCapacity();
		}

		if(ContractsType.STANDARD_VENUE_RENTAL.equals(type)){
			return new StandardRentalSale();
		}
		return null;
	}
}
