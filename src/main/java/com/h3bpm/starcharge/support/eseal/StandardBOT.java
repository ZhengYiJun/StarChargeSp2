package com.h3bpm.starcharge.support.eseal;

import OThinker.H3.Entity.Instance.Data.InstanceData;
import api.domain.contract.create.CreateContractVO;
import api.domain.template.create.*;
import com.h3bpm.starcharge.support.entity.StandardAddCapacityContent;
import com.h3bpm.starcharge.support.entity.StandardBOTContent;
import com.h3bpm.starcharge.support.utils.ESealUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StandardBOT extends AbstractESeal {

    private static final String TEMP_ID = "2157081475873246216";

    private static final Long ROLE_ID = 2157081776512568323L;

    @Override
    public SendContractsSyncVO createParams(Map<String, Object> valueTable) throws Exception {
        // 转换自定义合同对象
        StandardBOTContent content = ESealUtils.toCustomEntity(StandardBOTContent.class, valueTable);

        // 甲方名称
        String PAName = content.getPartyA();
        // 乙方名称
        String PBName = content.getPartyB();
        // 乙方电话
        String PBTel = content.getPartBTel();

        // 获取模版参数
        List<LabelVO> labelList = ESealUtils.getLabelList(content);

        List<PlaceHolder> placeHolders = new ArrayList<>();
        placeHolders.add(new PlaceHolder(ACCOUNT, USERNAME, PAName, labelList));
        List<Role> roles = new ArrayList<>();

        roles.add(new Role(ROLE_ID, PBTel, PBName, PBName));

        return new SendContractsSyncVO(new Sender("", ""), TEMP_ID, placeHolders, roles);
    }

    @Override
    @Deprecated
    protected CreateContractVO createParams(InstanceData instanceData) throws Exception {
        return null;
    }


}
