package api.domain.contract.download;

import java.util.List;

public class DownloadFileVo {

    /**
     * 操作人。非必填。未填写则默认操作人为“申请了开发者的企业的主管理员”。如果填写了账号、企业名称则以填写的为准
     */
    private Operator operator;
    /**
     * 需要下载的合同编号数组 必填
     */
    private List<String> contractIds;

    public DownloadFileVo(List<String> contractIds) {
        this.contractIds = contractIds;
    }

    public DownloadFileVo(Operator operator, List<String> contractIds) {
        this.operator = operator;
        this.contractIds = contractIds;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public List<String> getContractIds() {
        return contractIds;
    }

    public void setContractIds(List<String> contractIds) {
        this.contractIds = contractIds;
    }
}
