package com.h3bpm.starcharge.controller.scit;

import OThinker.H3.Controller.ControllerBase;
import com.google.gson.Gson;
import com.h3bpm.starcharge.common.uitl.OkHttpUtil;
import com.h3bpm.starcharge.param.scit.SendReceiveParam;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * Scit控制器
 *
 * @author LLongAgo
 * @date 2019/2/20
 * @since 1.0.0
 */
@Controller
@RequestMapping("/starCharge/scit")
public class ScitController extends ControllerBase {

    @Override
    public String getFunctionCode() {
        return null;
    }

    @Value("${bpmReceiveUrl}")
    private String bpmReceiveUrl;

    /**
     * 发送通知
     * @param instanceId instanceId
     * @param remark     备注
     * @return String
     * @throws IOException
     */
    @RequestMapping(value = "/sendReceive", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public String receive(@RequestParam String instanceId, @RequestParam(required = false) String remark) throws IOException {
        SendReceiveParam param = new SendReceiveParam();
        String timeStamp = String.valueOf(System.currentTimeMillis() / 1000);
        param.setProcessInstanceId(instanceId);
        param.setRemark(remark);
        param.setResult("AGREE");
        return OkHttpUtil.postByJson(bpmReceiveUrl + "?timestamp=" + timeStamp, new Gson().toJson(param));
    }
}