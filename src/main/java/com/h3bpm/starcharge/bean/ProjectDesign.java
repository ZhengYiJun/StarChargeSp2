package com.h3bpm.starcharge.bean;

/**
 * @Author : Coco
 * @Description :
 * @Date : Create in 18:33 2019/1/10
 * @Modified :
 */
public enum ProjectDesign {
    /**
     * ProjectDesign
     */
    ContractNo,
    contractSignAddress,
    projectName,
    projectNo,
    projectAddress,
    designCertificateLevel,
    partyA,
    partyB,
    entryName,
    designContent,
    deliveryAddress,
    contractDesignFee,
    telA,
    contactsA,
    openBankA,
    bankAccountA,
    telB,
    contactsB,
    openBankB,
    bankAccountB,
    emailA,
    emailB,
    otherContract
}
