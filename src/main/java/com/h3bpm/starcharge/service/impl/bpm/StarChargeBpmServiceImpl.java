package com.h3bpm.starcharge.service.impl.bpm;

import OThinker.H3.Portal.webservices.Entity.BPMServiceResult;
import OThinker.H3.Portal.webservices.impl.BPMServiceImpl;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.SqlUtils;
import com.h3bpm.starcharge.param.workflow.StartWorkflowNewParam;
import com.h3bpm.starcharge.ret.workflow.StartWorkflowNewRet;
import com.h3bpm.starcharge.service.bpm.StarChargeBpmService;
import data.DataRow;
import data.DataTable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * StarchargeBpmServiceImpl class
 *
 * @author llongago
 * @date 2019/2/18
 */
@Service
public class StarChargeBpmServiceImpl implements StarChargeBpmService {

    @Value("${workflowSystemCode}")
    private String systemCode;

    @Value("${workflowSecret}")
    private String secret;

    private static final BPMServiceImpl bpmService = new BPMServiceImpl();

    /**
     * 发起流程
     * @param param StartWorkflowNewParam
     * @return StartWorkflowNewRet
     */
    @Override
    public StartWorkflowNewRet startWorkflowNew(StartWorkflowNewParam param) {
        try {
            BPMServiceResult result = bpmService.StartWorkflowNew(systemCode, secret, param.getWorkflowCode(), param.getUserCode(), param.getFinishStart(), param.getParamValues());
            return setRet(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取流程明细
     *
     * @param instanceId 流程id
     * @return RetResult
     */
    @Override
    public String getWorkItemObject(String instanceId) throws Exception {
        String sql = "SELECT ObjectID FROM OT_WorkItem WHERE InstanceId = '" + instanceId + "'";
        DataTable dataTable = SqlUtils.doQuery(sql);
        if (dataTable.getRows().size() > 0) {
            return dataTable.getRows().get(0).getString(0);
        }
        return "";
    }

    /**
     * 设置返回值
     * @param result BPMServiceResult
     * @return StartWorkflowNewRet
     */
    private StartWorkflowNewRet setRet(BPMServiceResult result) {
        StartWorkflowNewRet ret = new StartWorkflowNewRet();
        ret.setSuccess(result.isSuccess());
        ret.setMessage(result.getMessage());
        ret.setInstanceID(result.getInstanceId());
        ret.setWorkItemID(result.getWorkItemId());
        ret.setWorkItemUrl(result.getWorkItemUrl());
        return ret;
    }

    /**
     * 获取流程明细
     *
     * @param instanceId 流程id
     * @return List<Map < String ,   String>>
     */
    @Override
    public List<Map<String, String>> getParticipants(String instanceId) throws Exception {
        DataTable dataTable = SqlUtils.doQuery("SELECT DISTINCT(Participant), ParticipantName, Code FROM OT_WorkItemFinished as wf INNER JOIN OT_User as u ON wf.Participant = u.ObjectID WHERE InstanceId = '" + instanceId + "'");
        List<Map<String, String>> list = new ArrayList<>();
        if (dataTable.getRows().size() > 0) {
            for (int i = 0; i < dataTable.getRows().size(); i++) {
                DataRow dataRow = dataTable.getRows().get(i);
                Map<String, String> map = new HashMap<>(8);
                map.put("objectID", dataRow.getString(0));
                map.put("name", dataRow.getString(1));
                map.put("tel", dataRow.getString(2));
                list.add(map);
            }
        }
        return list;
    }

    @Override
    public Boolean isWorkItemAssist(String instanceId, String creator) {
        String querySql = "select ObjectId from OT_workitem where instanceid = '%s' AND ItemType = 5 AND Creator = '%s'";
        DataTable dataTable = null;
        try {
            dataTable = SqlUtils.doQuery(String.format(querySql,instanceId,creator));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (dataTable.getRows().size() > 0) {
            return true;
        } else {
            return false;
        }
    }
}