package com.h3bpm.starcharge.controller.erp;


import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.starcharge.common.bean.RetResponse;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.ErrorUtil;
import com.h3bpm.starcharge.common.uitl.ScitParamUtil;
import com.h3bpm.starcharge.controller.alm.AlmWorkFlowController;
import com.h3bpm.starcharge.param.workflow.StartWorkflowNewParam;
import com.h3bpm.starcharge.ret.workflow.StartWorkflowNewRet;
import com.h3bpm.starcharge.service.bpm.StarChargeBpmService;
import com.h3bpm.starcharge.service.erp.ErpContractService;
import com.h3bpm.starcharge.service.k3c.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;
import java.net.URLDecoder;
import java.net.URLEncoder;
/**
 * @author Star
 */

@Controller
@RequestMapping("/erp/ErpContract")
public class ErpContractController extends ControllerBase {
    private static final Logger log = LoggerFactory.getLogger(AlmWorkFlowController.class);
    @Autowired
    private ErpContractService contractService;

    @Override
    public String getFunctionCode() {
        return null;
    }
    @Autowired
    private StarChargeBpmService starChargeBpmService;

    /**
     * 合同通过驳回
     * @param instanceId    instanceId
     * @param flag    flag
     * @return RetResult
     */
    @RequestMapping(value = "/pass", method = RequestMethod.POST,produces = "application/json;charset=utf-8")
    @ResponseBody
    public RetResult pass(@RequestParam String instanceId,@RequestParam String flag,@RequestParam String type) {
        try {
//            InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            RetResult SDFS= contractService.pass(instanceId,flag,type);
            return SDFS;
        } catch (Exception e) {
            return ErrorUtil.generalException(e);
        }
    }

    /**
     * 发起流程
     *
     * @param workflowCode 流程模板编码
     * @param userCode     用户编码
     * @param finishStart  是否结束第一个活动
     * @param paramValues  普通参数（JSON格式）
     * @return StartWorkflowNewRet
     */
    @RequestMapping(value = "/start", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public RetResult startWorkFlow(@RequestParam String workflowCode,
                                   @RequestParam String userCode,
                                   @RequestParam String finishStart,
                                   @RequestParam(required = false) String paramValues) {
        String paramValue = "";
        String param;
        try {
            if (null != paramValues && !"".equals(paramValues)) {
                paramValue = ScitParamUtil.paramToJson(paramValues);
            }


            StringBuilder sb = new StringBuilder();
            sb.append(paramValue);

            param = sb.toString() ;
            StartWorkflowNewParam workflowParam = new StartWorkflowNewParam(workflowCode, userCode, finishStart, param);
            System.out.println(workflowParam.getParamValues());
            StartWorkflowNewRet result = starChargeBpmService.startWorkflowNew(workflowParam);
            if (result.isSuccess()) {
                String instanceId = result.getInstanceID();
                InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            }
            if (!result.isSuccess()) {
                return RetResponse.makeErrRsp(result.getMessage());
            }
            Map<String, Object> map = new HashMap<>(2);
            map.put("instanceId", result.getInstanceID());
            return RetResponse.makeOKRsp(map);
        } catch (Exception e) {
            return ErrorUtil.generalException(e);
        }
    }

}
