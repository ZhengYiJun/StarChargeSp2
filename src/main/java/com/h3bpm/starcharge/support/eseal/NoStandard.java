package com.h3bpm.starcharge.support.eseal;

import OThinker.Common.Data.BoolMatchValue;
import OThinker.Common.DotNetToJavaStringHelper;
import OThinker.Common.Organization.Models.User;
import OThinker.H3.Entity.Data.Attachment.Attachment;
import OThinker.H3.Entity.Data.Attachment.AttachmentHeader;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import OThinker.H3.Entity.Instance.InstanceContext;
import api.APIController;
import api.domain.contract.create.*;
import api.domain.template.create.SendContractsSyncVO;
import com.h3bpm.base.util.AppUtility;
import org.apache.commons.collections.CollectionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName NoStandard
 * @Desciption 非标准合同上传
 * @Author lvyz
 * @Date 2019/6/17 17:22
 **/
public class NoStandard extends AbstractESeal {

	private static final String ATTACHMENTS = "contract";

	private static final String KEY_WORK_ONE = "盖章处";

	private static final String KEY_WORK_TWO = "签章处";

	@Override
	@Deprecated
	public SendContractsSyncVO createParams(Map<String, Object> valueTable) throws Exception {
		return null;
	}

	@Override
	public CreateContractVO createParams(InstanceData instanceData) throws Exception {

		CreateContractVO createContractVO = new CreateContractVO();

		InstanceContext instanceContext = instanceData.getInstanceContext();

		// 获取附件处理器
		List<AttachmentHeader> headers = AppUtility.getEngine().getBizObjectManager().QueryAttachment(
				instanceData.getSchemaCode(), instanceData.getBizObject().getObjectID(), this.ATTACHMENTS, BoolMatchValue.True, "");


		List<CreateDocumentVO> documents = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(documents)) {

			// 取第一个附件处理器
			AttachmentHeader header = headers.get(0);

			// 取附件内容
			Attachment attachment = AppUtility.getEngine().getBizObjectManager().GetAttachment(User.AdministratorID,
					instanceData.getSchemaCode(),
					instanceData.getBizObject().getObjectID(),
					header.getObjectID());

			// 初始化文档
			CreateDocumentVO documentVO = new CreateDocumentVO();

			// 设置文档名称
			documentVO.setFileName(header.getFileName());

			// 二进制文件 base64转码
			documentVO.setContent(APIController.getBase64Content(attachment.getContent()));

			// 文档顺序默认第一个
			documentVO.setOrder(0);

			// 设置上传的文档
			documents.add(documentVO);
		}

		createContractVO.setDocuments(documents);

		// 构造基本信息
		createContractVO.setContractDescription(instanceContext.getInstanceName());
		createContractVO.setContractTitle(instanceContext.getInstanceName());
		createContractVO.setSignDeadline(new Date(System.currentTimeMillis() + 864000000));
		createContractVO.setSignOrdered(false);

		// 设置盖章人
		createContractVO.setReceivers(this.createReceiver(instanceData.getBizObject().getValueTable(), documents));

		return createContractVO;

	}

	/**
	 * 创建盖章人-可多个
	 * @param paramTables 表单数据
	 * @param documents 需要上传的文档集合
	 * @return
	 * @throws IOException
	 */
	private List<CreateReceiverVO> createReceiver(Map<String, Object> paramTables, List<CreateDocumentVO> documents) throws IOException {


		List<CreateReceiverVO> receivers = new ArrayList<>();

		//构造 盖章人one
		CreateReceiverVO receiverVOOne = new CreateReceiverVO();
		receiverVOOne.setUserAccount(ACCOUNT);
		receiverVOOne.setEnterpriseName((String) paramTables.get("OurUnitName"));
		receiverVOOne.setUserType(SignFlowConstants.UserType.ENTERPRISE);
		receiverVOOne.setReceiverType(SignFlowConstants.ReceiverType.SIGNER);
		receiverVOOne.setRouteOrder(1);

		//构造 盖章位置
		CalculatePosition calculatePositionOne = new CalculatePosition(this.KEY_WORK_ONE, documents);
		receiverVOOne.setLabels(APIController.calculatePositions(calculatePositionOne));
		receivers.add(receiverVOOne);

		//构造 盖章人two
		if (!DotNetToJavaStringHelper.isNullOrEmpty((String) paramTables.get("OurUnitNameTwo"))) {
			CreateReceiverVO receiverVOTwo = new CreateReceiverVO();
			receiverVOTwo.setUserAccount(ACCOUNT);
			receiverVOTwo.setEnterpriseName((String) paramTables.get("OurUnitNameTwo"));
			receiverVOTwo.setUserType(SignFlowConstants.UserType.ENTERPRISE);
			receiverVOTwo.setReceiverType(SignFlowConstants.ReceiverType.SIGNER);
			receiverVOTwo.setRouteOrder(1);
			//构造 盖章位置
			CalculatePosition calculatePositionTwo = new CalculatePosition(this.KEY_WORK_TWO, documents);
			receiverVOTwo.setLabels(APIController.calculatePositions(calculatePositionTwo));
			receivers.add(receiverVOTwo);
		}

		return receivers;
	}
}
