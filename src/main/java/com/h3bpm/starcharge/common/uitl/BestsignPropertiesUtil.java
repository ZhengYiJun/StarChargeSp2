package com.h3bpm.starcharge.common.uitl;

import api.client.BestSignClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Properties;

/**
 * 所有系统的配置都从这获取
 * @author luwei
 *
 */
public class BestsignPropertiesUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BestsignPropertiesUtil.class);

    private static final String PROPERTIES_FILE = "config/bestsign.properties";

    private static Properties properties = null;

    public static String getProperty(String key){
    	if (properties == null) {
    		Properties prop = new Properties();
        	try {
        		InputStream in = BestsignPropertiesUtil.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE);
        		prop.load(in);
                properties = prop;
    		} catch (Exception e) {
    			LOGGER.error("获取配置文件失败", e);
    			throw new RuntimeException(e);
    		}
		}
    	
    	return properties.getProperty(key);
    }

    public static String getPropertyNoCache(String key) {
        Properties prop = new Properties();
        try {
            InputStream in = BestsignPropertiesUtil.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE);
            prop.load(in);
            return prop.getProperty(key);
        } catch (Exception e) {
            LOGGER.error("获取配置文件失败", e);
            throw new RuntimeException(e);
        }
    }

    public static String getConfigFileName() {
        return PROPERTIES_FILE;
    }

    public static BestSignClient getBestSignClient() {

        return new BestSignClient(
                getProperty("server-host"),
                getProperty("clientId"),
                getProperty("clientSecret"),
                getProperty("private_key")
        );
    }

}
