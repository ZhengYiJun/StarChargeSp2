package com.h3bpm.starcharge.service.impl.erp;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.alibaba.fastjson.JSONObject;
import com.h3bpm.starcharge.common.bean.BaseResult;
import com.h3bpm.starcharge.common.bean.JsonResult;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.OkHttpUtil;
import com.h3bpm.starcharge.config.ErpConfig;
import com.h3bpm.starcharge.service.erp.ErpCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.h3bpm.starcharge.common.uitl.HttpClientUtil.doPost;


@Service
public class ErpCustomerServiceImpl implements ErpCustomerService {

    @Autowired
    private ErpConfig erpConfig;
    /**
     * 保存
     * @param instanceData  InstanceData
     * @return RetResult
     */
    @Override
    public RetResult save(InstanceData instanceData) throws Exception {
        HashMap<String, Object> valueTable = instanceData.getBizObject().getValueTable();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", valueTable.get("customerId"));
        jsonObject.put("name", valueTable.get("customername"));
        System.out.println(jsonObject.toJSONString());
        Map<String, String> map = new HashMap<>(4);
        map.put("id", valueTable.get("customerId").toString());
        map.put("name", valueTable.get("customername").toString());

        String url =erpConfig.getUrl()+ "Customer/testCustomer";

        String dwe;
        dwe=OkHttpUtil.post(url , map);
        JSONObject retData = JSONObject.parseObject(dwe);

        RetResult result= new RetResult();
        result.setCode("200");
        result.setMsg("secuss");
        result.setData(retData);

        return result;
    }


    @Override
    public JsonResult<List<BaseResult>> getClassList() throws Exception {
        String url = erpConfig.getUrl()+ "Customer/getClassList";
       // String url ="http://localhost:8089/Customer/getClassList";

        String dwe;
        //dwe=OkHttpUtil.post(url,map);
        dwe=doPost(url);
        JSONObject retData = JSONObject.parseObject(dwe);
        String result=retData.get("data").toString();
        List<String[]> resultList = new Gson().fromJson(result, new TypeToken<List<String[]>>(){}.getType());

        List<BaseResult> baseResultList = new ArrayList<>();
        if (null != resultList && resultList.size() > 0) {
            for (String[] list : resultList) {
                BaseResult base = new BaseResult();
                base.setName(list[0]);
                base.setValue(list[1]);
                baseResultList.add(base);

            }
        }
        JsonResult<List<BaseResult>> jsonResult = new JsonResult<>();
        jsonResult.setData(baseResultList);
        return jsonResult;
    }



}
