package com.h3bpm.starcharge.common.bean;

/**
 * 通用JsonResult
 *
 * @author LLongAgo
 * @date 2019/3/4
 * @since 1.0.0
 */
public class JsonResult<T> {

    private String code;
    private String msg;
    private T data;

    /**
     * 若没有数据返回，默认状态码为0，提示信息为：操作成功！
     */
    public JsonResult() {
        this.code = RestfulErrorCode.SUCCESS.getCode();
        this.msg = RestfulErrorCode.SUCCESS.getMsg();
    }

    /**
     * 若没有数据返回，可以人为指定状态码和提示信息
     * @param code
     * @param msg
     */
    public JsonResult(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 有数据返回时，状态码为0，默认提示信息为：操作成功！
     * @param data
     */
    public JsonResult(T data) {
        this.code = RestfulErrorCode.SUCCESS.getCode();
        this.msg = RestfulErrorCode.SUCCESS.getMsg();
        this.data = data;
    }

    /**
     * 有数据返回，状态码为0，人为指定提示信息
     * @param data
     * @param msg
     */
    public JsonResult(T data, String msg) {
        this.code = RestfulErrorCode.SUCCESS.getCode();
        this.msg = msg;
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}