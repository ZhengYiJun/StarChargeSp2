package com.h3bpm.starcharge.ret.k3c;

/**
 * 物料基本
 *
 * @author LLongAgo
 * @date 2019/3/4
 * @since 1.0.0
 */
public class K3cBase {
    private String id;

    private String number;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public K3cBase(String id) {
        this.id = id;
    }

    public K3cBase() {
    }

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
}