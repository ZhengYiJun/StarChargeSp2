package com.h3bpm.starcharge.bean;

public enum AgentSigning {

    /**
     * select name+',' as Nname from syscolumns where id=(select max(id) from sysobjects where xtype='u' and name='I_AgentSigning')
     * 代理商签约商户确认函
     */

    ObjectID,
    CreatedBy,
    CreatedTime,
    contractNo,
    communicateNumber,
    partyA3,
    partyB3,
    ParentPropertyName,
    ModifiedBy,
    merchantService,
    Name,
    merchantName,
    starService,
    OwnerId,
    ModifiedTime,
    cooperationAgreementNo,
    CreatedByParentId,
    OwnerParentId,
    contractId,
    quantityNumber,
    agencyService,
    CappingSeal,
    ParentIndex,
    RunningInstanceId,
    ParentObjectID,
    phoneB,
    contractNum,

}
