package com.h3bpm.starcharge.bean.k3c;

/**
 * MaterialBase class
 *
 * @author llongago
 * @date 2019/3/12
 */
public class MaterialBase {

    private String id;
    private String code;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}