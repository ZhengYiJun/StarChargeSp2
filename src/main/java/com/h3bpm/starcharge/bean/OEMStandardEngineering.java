package com.h3bpm.starcharge.bean;

import org.springframework.data.annotation.CreatedBy;

/**
 * @Author : Coco
 * @Description :
 * @Date : Create in 17:25 2019/1/3
 * @Modified :
 */
public enum OEMStandardEngineering {
    /**
     * OEMStandardEngineering
     */
    ObjectID,
    legalRepresentPostA,
    CreatedTime,
    constructionPayment,
    contractType,
    ParentPropertyName,
    ModifiedBy,
    Name,
    constructionSite,
    registeredAddress,
    cablePrice,
    serviceStartDate,
    addressA,
    constructionTime,
    ParentIndex,
    surveyFee,
    ParentObjectID,
    authRepresentA,
    emailB,
    authRepresentB,
    basicInstallationFee,
    CreatedBy,
    constructionPaymentFinal,
    surveyInstallationFee,
    telA,
    telB,
    legalRepresentB,
    legalRepresentA,
    partyB,
    CreatedByParentId,
    OwnerParentId,
    installationArea,
    constructionPrice,
    cableLength,
    whetherTax,
    contractNo,
    partyA1,
    taxRegistNoB,
    openBankA,
    OwnerId,
    ModifiedTime,
    rate,
    openBankB,
    authRepresentPostB,
    bankAccountA,
    RunningInstanceId,
    authRepresentPostA,
    bankAccountB,
    settlementStandard,
    excessCableLength,
    serviceEndDate,
    faxB,
    faxA,
    taxRegistNoA,
    constructionSettlement,
    basicInstallationUpper,
    constructionPriceUpper,
    addressCompanyB,
    proxyB,
    emailA,
    contractServiceType,
    proxyA,
    legalRepresentPostB
}
