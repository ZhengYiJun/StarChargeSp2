package com.h3bpm.starcharge.bean;

public enum CooperationAgreement {
    /**
     * 星星充电充电桩群/站投资业务代理合作协议
     */

    ObjectID,
    partyA7,
    CreatedTime,
    authRepresentB7,
    addressB7,
    openBankA7,
    ParentPropertyName,
    ModifiedBy,
    Name,
    OwnerId,
    ModifiedTime,
    CappingSeal,
    cooperateAreaScope,
    ParentIndex,
    RunningInstanceId,
    override,
    bankAccountA7,
    ParentObjectID,
    CreatedBy,
    signAddress7,
    partyB7,
    authRepresentA7,
    addressA7,
    openBankB7,
    bond,
    CreatedByParentId,
    OwnerParentId,
    contractId,
    bankAccountB7,
    effectiveDate1,
    contractNo,
    signingTime1,
    phoneB,
    other,

    /**
     * 子表
     */
    sublist,
    contribution,
    unitPrice,
}
