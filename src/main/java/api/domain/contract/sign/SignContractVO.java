package api.domain.contract.sign;

import java.util.List;

public class SignContractVO {

    private Signer signer;
    private String sealName;
    private List<Long> contractIds;

    public SignContractVO(Signer signer, String sealName, List<Long> contractIds) {
        this.signer = signer;
        this.sealName = sealName;
        this.contractIds = contractIds;
    }

    public SignContractVO() {
    }

    public Signer getSigner() {
        return signer;
    }

    public void setSigner(Signer signer) {
        this.signer = signer;
    }

    public String getSealName() {
        return sealName;
    }

    public void setSealName(String sealName) {
        this.sealName = sealName;
    }

    public List<Long> getContractIds() {
        return contractIds;
    }

    public void setContractIds(List<Long> contractIds) {
        this.contractIds = contractIds;
    }

    @Override
    public String toString() {
        return "SignContractVO{" +
                "signer=" + signer +
                ", sealName='" + sealName + '\'' +
                ", contractIds=" + contractIds +
                '}';
    }
}
