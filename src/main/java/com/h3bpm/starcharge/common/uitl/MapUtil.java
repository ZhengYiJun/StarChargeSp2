package com.h3bpm.starcharge.common.uitl;

import java.util.Map;

/**
 * @author LLongAgo
 * @date 2019/4/18
 * @since 1.0.0
 */
public class MapUtil {

    public static void batchSet(Map<String, Object> map, Map<String, Object> vb, String[] keys, String[] values) {
        if (keys.length == values.length) {
            for (int i = 0; i < keys.length; i++) {
                map.put(keys[i], vb.get(values[i]));
            }
        }
    }

    public static void batchSetString(Map<String, String> map, Map<String, Object> vb, String[] keys, String[] values) {
        if (keys.length == values.length) {
            for (int i = 0; i < keys.length; i++) {
                map.put(keys[i], (String) vb.get(values[i]));
            }
        }
    }

    public static void batchSetString(Map<String, String> map, String[] keys, String[] values) {
        if (keys.length == values.length) {
            for (int i = 0; i < keys.length; i++) {
                map.put(keys[i], values[i]);
            }
        }
    }
}