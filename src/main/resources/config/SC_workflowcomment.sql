﻿CREATE TABLE [dbo].[SC_workflowcomment] (
  [id] char(36) COLLATE Chinese_PRC_CI_AS NOT NULL,
  [workitemId] char(36) COLLATE Chinese_PRC_CI_AS  NULL,
  [sheetCode] varchar(200) COLLATE Chinese_PRC_CI_AS  NULL,
  [instanceId] char(36) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [content] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [userId] char(36) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [userName] nvarchar(200) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [userAvator] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [notifyUserIds] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [gmtCreate] datetime DEFAULT (getdate()) NOT NULL,
  [gmtModified] datetime DEFAULT (getdate()) NOT NULL,
  CONSTRAINT [pk_workflowcomment] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
)  
ON [PRIMARY]
GO

ALTER TABLE [dbo].[SC_workflowcomment] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcomment',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'workitem id',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcomment',
'COLUMN', N'workitemId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'表单编码',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcomment',
'COLUMN', N'sheetCode'
GO

EXEC sp_addextendedproperty
'MS_Description', N'流程实例ID',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcomment',
'COLUMN', N'instanceId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'评论内容',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcomment',
'COLUMN', N'content'
GO

EXEC sp_addextendedproperty
'MS_Description', N'评论用户id',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcomment',
'COLUMN', N'userId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'评论用户名',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcomment',
'COLUMN', N'userName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'评论用户头像',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcomment',
'COLUMN', N'userAvator'
GO

EXEC sp_addextendedproperty
'MS_Description', N'评论时间',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcomment',
'COLUMN', N'gmtCreate'
GO

EXEC sp_addextendedproperty
'MS_Description', N'评论更新时间',
'SCHEMA', N'dbo',
'TABLE', N'SC_workflowcomment',
'COLUMN', N'gmtModified'