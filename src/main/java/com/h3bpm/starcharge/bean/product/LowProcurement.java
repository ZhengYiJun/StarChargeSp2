package com.h3bpm.starcharge.bean.product;

public enum LowProcurement {

    /**
     * 采购合同（低压）
     */

    advancePayRate,
    agentA,
    agentB,
    CappingSeal,
    contractId,
    contractNo,
    CreatedBy,
    CreatedByParentId,
    CreatedTime,
    deliveryAddress,
    finalPayRate,
    legalRepresentA,
    legalRepresentB,
    ModifiedBy,
    ModifiedTime,
    Name,
    ObjectID,
    otherRemark,
    OwnerId,
    OwnerParentId,
    ParentIndex,
    ParentObjectID,
    ParentPropertyName,
    partyA,
    partyB,
    phoneB,
    premiun,
    receiver,
    RunningInstanceId,
    totalPrice,
    warrantyPeriod,

    /**
     * 子表
     */
    produnctDetail,

    productName,
    productType,
    productSit,
    productNum,
    productPrice,
    productSum,
    sendTime,
    productRemark
}
