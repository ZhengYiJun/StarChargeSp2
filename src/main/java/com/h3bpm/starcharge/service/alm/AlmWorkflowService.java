package com.h3bpm.starcharge.service.alm;

import com.alibaba.fastjson.JSONObject;
import com.h3bpm.starcharge.bean.alm.AlmCreateWorkflow;

import java.util.List;

/***
 * create by lvyz on 2019/5/24
 */
public interface AlmWorkflowService {

    /**
     * 创建流程
     * @param flowEntity
     */
    List<JSONObject> createFlows(AlmCreateWorkflow flowEntity);

    /**
     * 审批同意
     * -- 通知alm审批状态
     * @param instanceId
     * @param workflowCode
     */
    void success(String instanceId, String workflowCode);


    /**
     * 合同审批通过
     * @param instanceId
     * @param workflowCode
     */
    void contractsSuccess(String instanceId, String workflowCode) throws Exception;

    /**
     * 审批驳回
     * -- 删除实例
     * -- 删除alm与实例绑定关系
     * -- 通知alm审批状态
     * @param instanceId
     * @param workflowCode
     */
    void refuse(String instanceId, String workflowCode);

}
