package com.h3bpm.starcharge.support.eseal;

public interface ESealHandler {


	/**
	 * 同步合同到上上签
	 * @param instanceId
	 * @return
	 */
	String synContract(String instanceId);

	/**
	 * 上传合同到上上签
	 * @param instanceId
	 * @return
	 */
	String uploadContract(String instanceId);

	/**
	 * 合同盖章
	 * @param contractId
	 * @param sealName 章名
	 * @param companyName 盖章的公司名称
	 */
	void signature(String contractId, String sealName, String companyName);

	/**
	 * 合同盖章
	 * @param contractId
	 */
	void signature(String contractId);



}
