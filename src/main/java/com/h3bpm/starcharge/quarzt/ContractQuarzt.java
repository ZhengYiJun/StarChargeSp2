package com.h3bpm.starcharge.quarzt;

import OThinker.Common.DotNetToJavaStringHelper;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.h3bpm.starcharge.common.uitl.SqlUtils;
import com.h3bpm.starcharge.controller.BestsignController;
import data.DataTable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * @author zhengyj
 * @date 2019/5/20
 * 合同类  定时器
 */
@Component(value = "ContractQuarzt")
public class ContractQuarzt {

    /**
     * 查询 处于 等待中 的非标合同
     *
     * 非标合同 等待中是由于 合同id 没有生成，可能是由于 没按规范上传，也可能 是因为  提交后事件 调用 有问题
     *  固 需要查询 出 ，此类情况，再次调用 生成合同接口生成合同
     *
     * 查询逻辑
     *
     * 1 合同未生存 的  流程 一定 是  停在  等待节点， 此时 OT_InstanceContext 流程实列表中 state = 2 进行中
        2 OT_Token 对应该流程到此的节点，State = 0 进行中 ，Approval = -1 非审批节点
         3 由于是等待节点，此流程对应的 OT_WorkItem 中应该没有数据
            4 由于非合同类流程中 也存在等待节点，所有 再系统管理 建立 一个  数据字典  类型为  监测非标合同流程编码   。 来对应监测，
                5 时间从 什么时候 开始
     *      共五个条件  产生 如下 sql
     */
    private static String QUERYWAITCONTRACT = "SELECT a.Objectid from OT_InstanceContext a LEFT  JOIN OT_Token b on a.ObjectID = b.ParentObjectID WHERE a.state = 2 and b.State = 0 and b.Approval = -1 " +
            "and (select count(*) from OT_WorkItem c WHERE a.ObjectID = c.InstanceId) = 0 " +
            "AND a.WorkflowCode in (SELECT Code from OT_EnumerableMetadata WHERE  Category = '监测非标合同流程编码' ) " +
            "and a.CreatedTime >  convert(datetime,'%s', 20)";

    static Logger LOGGER = Logger.getLogger(ControllerBase.class);

    private static BestsignController bestsignController = new BestsignController();

    private static ObjectMapper mapper = new ObjectMapper();

    public void checkContractId(){
            // 查询 符合条件的 流程
        LOGGER.info("===ContractQuarzt=====checkContractId========Start======");
        try {
            Date date = new Date();
            DateTime dateTime = DateUtil.offsetHour(date, -2);
            QUERYWAITCONTRACT = String.format(QUERYWAITCONTRACT, DateUtil.format(dateTime, "yyyy-MM-dd HH:mm:ss"));
            DataTable dataTable = SqlUtils.doQuery(QUERYWAITCONTRACT);
            for(int i = 0; i < dataTable.getRows().size(); i++) {
                String instanceId = dataTable.getRows().get(i).getString(0);
                InstanceData instanceData = SqlUtils.getInstanceDataById(instanceId);
                if (DotNetToJavaStringHelper.isNullOrEmpty(instanceData.getItem("ContractId").getValue().toString())) {
                    LOGGER.info("====checkInstanceId======="+ instanceId);
                    Object resultMap  =  bestsignController.createContract(instanceId);
                    LOGGER.info("====checkContractId======="+ mapper.writeValueAsString(resultMap));
                }
            }
            LOGGER.info("===ContractQuarzt=====checkContractId========End======");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
