package api.domain.contract.sign;

public class Signer {
    /**
     * 发件人账号 非必填
     */
    private String account;
    /**
     * 发件人所属企业 非必填
     */
    private String enterpriseName;

    public Signer(String account, String enterpriseName) {
        this.account = account;
        this.enterpriseName = enterpriseName;
    }

    public Signer() {
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }
}
