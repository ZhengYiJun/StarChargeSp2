package api.domain.contract.create;

import java.util.List;

public class CalculatePosition {
    /**
     * 上传的附件中 ，根据值检索需要签名的位置
     */
    private String keyword;
    /**
     * 上传的附件
     */
    private List<CreateDocumentVO> documents;

    public CalculatePosition(String keyword, List<CreateDocumentVO> documents) {
        this.keyword = keyword;
        this.documents = documents;
    }

    public CalculatePosition() {
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<CreateDocumentVO> getDocuments() {
        return documents;
    }

    public void setDocuments(List<CreateDocumentVO> documents) {
        this.documents = documents;
    }
}
