package com.h3bpm.starcharge.support.entity;

import com.h3bpm.starcharge.support.core.BasicContractsField;
import com.h3bpm.starcharge.support.core.annontation.ESealRequestParam;

import java.util.List;
import java.util.Map;

public class StandardBOTContent extends BasicContractsField {

    @ESealRequestParam(eSealField = "设备投资额", bpmField = "PAEquipmentCost")
    private String partAEquipmentCost;

    @ESealRequestParam(eSealField = "辅材投资额", bpmField = "PASecCost")
    private String partASecCost;

    @ESealRequestParam(eSealField = "安装费", bpmField = "PAIF")
    private String partAIF;

    @ESealRequestParam(eSealField = "广告费", bpmField = "PAAF")
    private String partAAF;

    @ESealRequestParam(eSealField = "总投资", bpmField = "PATC")
    private String partATC;

    @ESealRequestParam(eSealField = "合同开始时间", bpmField = "contractStartDate")
    private String contractStartDate;

    @ESealRequestParam(eSealField = "合同结束时间", bpmField = "contractEndDate")
    private String contractEndDate;

    @ESealRequestParam(eSealField = "基础电价", bpmField = "price")
    private String price;

    @ESealRequestParam(eSealField = "充电服务费", bpmField = "serviceFee")
    private String serviceFee;

    @ESealRequestParam(
            table = true,
            tableName = "StandardBOTPile",
            eSealColumnFields = {"产品名称", "规格型号", "数量", "单位"},
            bpmColumnFields = {"pileName", "pileSpec", "pileQty", "pileGunQty"})
    private List<Map<String, String>> standardBOTPile;

}
