package com.h3bpm.starcharge.service.erp;

import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.starcharge.common.bean.RetResult;

public interface ErpContractService {
    RetResult pass(String instanceId ,String flag ,String type) throws Exception;
}
