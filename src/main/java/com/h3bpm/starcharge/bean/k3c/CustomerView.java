package com.h3bpm.starcharge.bean.k3c;

import java.util.List;

/**
 * @author LLongAgo
 * @date 2019/4/10
 * @since 1.0.0
 */
public class CustomerView {

    /**
     * Result : {"ResponseStatus":null,"Result":{"Id":644074,"msterID":644074,"DocumentStatus":"C","ForbidStatus":"A","MultiLanguageText":[{"PkId":122323,"LocaleId":2052,"Name":"吞吞吐吐吞吞吐吐","Description":" ","ShortName":" "}],"Name":[{"Key":2052,"Value":"吞吞吐吐吞吞吐吐"}],"Number":"tttttt000121","Description":[{"Key":2052,"Value":" "}],"CreateOrgId_Id":1,"CreateOrgId":{"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"},"UseOrgId_Id":1,"UseOrgId":{"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"},"CreatorId_Id":642651,"CreatorId":{"Id":642651,"Name":"张敏","UserAccount":" "},"ModifierId_Id":642651,"ModifierId":{"Id":642651,"Name":"张敏","UserAccount":" "},"CreateDate":"2019-04-10T09:20:03.793","FModifyDate":"2019-04-10T13:27:52.16","ShortName":[{"Key":2052,"Value":" "}],"COUNTRY_Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","COUNTRY":{"Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","FNumber":"China","MultiLanguageText":[{"PkId":"ae6420c4-0e15-4303-aa64-8297b511860c","LocaleId":1033,"FDataValue":"China"},{"PkId":"1001","LocaleId":2052,"FDataValue":"中国"}],"FDataValue":[{"Key":1033,"Value":"China"},{"Key":2052,"Value":"中国"}]},"PROVINCIAL_Id":" ","PROVINCIAL":null,"FZIP":" ","TEL":" ","FTAXREGISTERCODE":"88888888","FAX":" ","FGROUPCUSTID_Id":0,"FGROUPCUSTID":null,"FSUPPLIERID_Id":0,"FSUPPLIERID":null,"TRADINGCURRID_Id":1,"TRADINGCURRID":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"SALDEPTID_Id":0,"SALDEPTID":null,"SALGROUPID_Id":0,"SALGROUPID":null,"SELLER_Id":0,"SELLER":null,"TRANSLEADTIME":0,"PRICELISTID_Id":0,"PRICELISTID":null,"DISCOUNTLISTID_Id":0,"DISCOUNTLISTID":null,"SETTLETYPEID_Id":30609,"SETTLETYPEID":{"Id":30609,"msterID":30609,"MultiLanguageText":[{"PkId":15,"LocaleId":2052,"Name":"支付宝"}],"Name":[{"Key":2052,"Value":"支付宝"}],"Number":"JSFS32_SYS","TYPE":"2"},"RECEIVECURRID_Id":0,"RECEIVECURRID":null,"RECCONDITIONID_Id":20003,"RECCONDITIONID":{"Id":20003,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"货到收款"},{"PkId":5,"LocaleId":1033,"Name":"Collect on Delivery"}],"Name":[{"Key":2052,"Value":"货到收款"},{"Key":1033,"Value":"Collect on Delivery"}],"Number":"SKTJ01_SYS"},"FISCREDITCHECK":false,"APPROVERID_Id":642651,"APPROVERID":{"Id":642651,"Name":"张敏","UserAccount":" "},"APPROVEDATE":"2019-04-10T13:27:53.997","FORBIDDERID_Id":0,"FORBIDDERID":null,"FORBIDDATE":null,"TaxType_Id":"9e855eb97bec43e7b50c3e0e0bf51210","TaxType":{"Id":"9e855eb97bec43e7b50c3e0e0bf51210","FNumber":"SFL02_SYS","MultiLanguageText":[{"PkId":"6de0d14c-e106-4986-a732-5bbf127ec7dc","LocaleId":1033,"FDataValue":"General Taxpayer"},{"PkId":"7841e26e9e5a4fe6a83e7cdbe0c24175","LocaleId":2052,"FDataValue":"一般纳税人"},{"PkId":"2942b840514242b299738f8b9bbe2095","LocaleId":3076,"FDataValue":"一般納稅人"}],"FDataValue":[{"Key":1033,"Value":"General Taxpayer"},{"Key":2052,"Value":"一般纳税人"},{"Key":3076,"Value":"一般納稅人"}]},"CustTypeId_Id":" ","CustTypeId":null,"ADDRESS":"吞吞吐吐吞吞吐吐","WEBSITE":" ","FGroup_Id":114343,"FGroup":{"Id":114343,"Number":"B13","MultiLanguageText":[{"PkId":100380,"LocaleId":2052,"Name":"河北"}],"Name":[{"Key":2052,"Value":"河北"}]},"CompanyScale_Id":" ","CompanyScale":null,"CompanyType_Id":" ","CompanyType":null,"CompanyNature_Id":" ","CompanyNature":null,"CorrespondOrgId_Id":0,"CorrespondOrgId":null,"Priority":1,"InvoiceType":"1","TaxRate_Id":233,"TaxRate":{"Id":233,"msterID":233,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"17%增值税"},{"PkId":11,"LocaleId":1033,"Name":"17% VAT"}],"Name":[{"Key":2052,"Value":"17%增值税"},{"Key":1033,"Value":"17% VAT"}],"Number":"SL01_SYS"},"IsDefPayer":false,"CPAdminCode":" ","IsGroup":false,"IsTrade":true,"INVOICETITLE":"吞吞吐吐吞吞吐吐","INVOICEBANKNAME":"88888888","INVOICEBANKACCOUNT":"666666","INVOICETEL":"88888888","INVOICEADDRESS":"吞吞吐吐吞吞吐吐","F_WB_KHGLLX_Id":"5c136999714553","F_WB_KHGLLX":{"Id":"5c136999714553","FNumber":"1001","MultiLanguageText":[{"PkId":"5c136999714554","LocaleId":2052,"FDataValue":"关联"}],"FDataValue":[{"Key":2052,"Value":"关联"}]},"F_dbjkh":false,"BD_CUSTLOCATION":[],"BD_CUSTBANK":[{"Id":100680,"BANKCODE":"88888888","CURRENCYID_Id":1,"CURRENCYID":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"ACCOUNTNAME":"吞吞吐吐吞吞吐吐","ISDEFAULT":false,"COUNTRY_Id":" ","COUNTRY":null,"MultiLanguageText":[{"PkId":100680,"LocaleId":2052,"OPENBANKNAME":"吞吞吐吐吞吞吐吐"}],"OPENBANKNAME":[{"Key":2052,"Value":"吞吞吐吐吞吞吐吐"}],"BankTypeRec_Id":0,"BankTypeRec":null,"OpenAddressRec":"吞吞吐吐吞吞吐吐","CNAPS":" ","FTextBankDetail":" "}],"BD_CUSTCONTACT":[],"BD_CUSTORDERORG":[{"Id":0,"OrderOrgId_Id":0,"OrderOrgId":null,"ISDEFAULT":true}],"BD_CUSTOMEREXT":[{"Id":122322,"EnableSL":false,"FreezeStatus":"A","FreezeLimit":" ","FreezeOperator_Id":0,"FreezeOperator":null,"FreezeDate":null,"PROVINCE_Id":"5c145b495faf80","PROVINCE":{"Id":"5c145b495faf80","FNumber":"14","MultiLanguageText":[{"PkId":"5c145b495faf9f","LocaleId":2052,"FDataValue":"山西"}],"FDataValue":[{"Key":2052,"Value":"山西"}]},"CITY_Id":"5c2e1882e0a572","CITY":{"Id":"5c2e1882e0a572","FNumber":"1406","MultiLanguageText":[{"PkId":"5c2e1882e0a6c2","LocaleId":2052,"FDataValue":"朔州"}],"FDataValue":[{"Key":2052,"Value":"朔州"}]},"DefaultConsiLoc_Id":0,"DefaultConsiLoc":null,"DefaultSettleLoc_Id":0,"DefaultSettleLoc":null,"DefaultPayerLoc_Id":0,"DefaultPayerLoc":null,"DefaultContact_Id":0,"DefaultContact":null,"MarginLevel":0,"FDebitCard":" ","SettleId_Id":0,"SettleId":null,"ChargeId_Id":0,"ChargeId":null}]}}
     */

    private ResultBeanX Result;

    public ResultBeanX getResult() {
        return Result;
    }

    public void setResult(ResultBeanX Result) {
        this.Result = Result;
    }

    public static class ResultBeanX {
        /**
         * ResponseStatus : null
         * Result : {"Id":644074,"msterID":644074,"DocumentStatus":"C","ForbidStatus":"A","MultiLanguageText":[{"PkId":122323,"LocaleId":2052,"Name":"吞吞吐吐吞吞吐吐","Description":" ","ShortName":" "}],"Name":[{"Key":2052,"Value":"吞吞吐吐吞吞吐吐"}],"Number":"tttttt000121","Description":[{"Key":2052,"Value":" "}],"CreateOrgId_Id":1,"CreateOrgId":{"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"},"UseOrgId_Id":1,"UseOrgId":{"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"},"CreatorId_Id":642651,"CreatorId":{"Id":642651,"Name":"张敏","UserAccount":" "},"ModifierId_Id":642651,"ModifierId":{"Id":642651,"Name":"张敏","UserAccount":" "},"CreateDate":"2019-04-10T09:20:03.793","FModifyDate":"2019-04-10T13:27:52.16","ShortName":[{"Key":2052,"Value":" "}],"COUNTRY_Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","COUNTRY":{"Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","FNumber":"China","MultiLanguageText":[{"PkId":"ae6420c4-0e15-4303-aa64-8297b511860c","LocaleId":1033,"FDataValue":"China"},{"PkId":"1001","LocaleId":2052,"FDataValue":"中国"}],"FDataValue":[{"Key":1033,"Value":"China"},{"Key":2052,"Value":"中国"}]},"PROVINCIAL_Id":" ","PROVINCIAL":null,"FZIP":" ","TEL":" ","FTAXREGISTERCODE":"88888888","FAX":" ","FGROUPCUSTID_Id":0,"FGROUPCUSTID":null,"FSUPPLIERID_Id":0,"FSUPPLIERID":null,"TRADINGCURRID_Id":1,"TRADINGCURRID":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"SALDEPTID_Id":0,"SALDEPTID":null,"SALGROUPID_Id":0,"SALGROUPID":null,"SELLER_Id":0,"SELLER":null,"TRANSLEADTIME":0,"PRICELISTID_Id":0,"PRICELISTID":null,"DISCOUNTLISTID_Id":0,"DISCOUNTLISTID":null,"SETTLETYPEID_Id":30609,"SETTLETYPEID":{"Id":30609,"msterID":30609,"MultiLanguageText":[{"PkId":15,"LocaleId":2052,"Name":"支付宝"}],"Name":[{"Key":2052,"Value":"支付宝"}],"Number":"JSFS32_SYS","TYPE":"2"},"RECEIVECURRID_Id":0,"RECEIVECURRID":null,"RECCONDITIONID_Id":20003,"RECCONDITIONID":{"Id":20003,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"货到收款"},{"PkId":5,"LocaleId":1033,"Name":"Collect on Delivery"}],"Name":[{"Key":2052,"Value":"货到收款"},{"Key":1033,"Value":"Collect on Delivery"}],"Number":"SKTJ01_SYS"},"FISCREDITCHECK":false,"APPROVERID_Id":642651,"APPROVERID":{"Id":642651,"Name":"张敏","UserAccount":" "},"APPROVEDATE":"2019-04-10T13:27:53.997","FORBIDDERID_Id":0,"FORBIDDERID":null,"FORBIDDATE":null,"TaxType_Id":"9e855eb97bec43e7b50c3e0e0bf51210","TaxType":{"Id":"9e855eb97bec43e7b50c3e0e0bf51210","FNumber":"SFL02_SYS","MultiLanguageText":[{"PkId":"6de0d14c-e106-4986-a732-5bbf127ec7dc","LocaleId":1033,"FDataValue":"General Taxpayer"},{"PkId":"7841e26e9e5a4fe6a83e7cdbe0c24175","LocaleId":2052,"FDataValue":"一般纳税人"},{"PkId":"2942b840514242b299738f8b9bbe2095","LocaleId":3076,"FDataValue":"一般納稅人"}],"FDataValue":[{"Key":1033,"Value":"General Taxpayer"},{"Key":2052,"Value":"一般纳税人"},{"Key":3076,"Value":"一般納稅人"}]},"CustTypeId_Id":" ","CustTypeId":null,"ADDRESS":"吞吞吐吐吞吞吐吐","WEBSITE":" ","FGroup_Id":114343,"FGroup":{"Id":114343,"Number":"B13","MultiLanguageText":[{"PkId":100380,"LocaleId":2052,"Name":"河北"}],"Name":[{"Key":2052,"Value":"河北"}]},"CompanyScale_Id":" ","CompanyScale":null,"CompanyType_Id":" ","CompanyType":null,"CompanyNature_Id":" ","CompanyNature":null,"CorrespondOrgId_Id":0,"CorrespondOrgId":null,"Priority":1,"InvoiceType":"1","TaxRate_Id":233,"TaxRate":{"Id":233,"msterID":233,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"17%增值税"},{"PkId":11,"LocaleId":1033,"Name":"17% VAT"}],"Name":[{"Key":2052,"Value":"17%增值税"},{"Key":1033,"Value":"17% VAT"}],"Number":"SL01_SYS"},"IsDefPayer":false,"CPAdminCode":" ","IsGroup":false,"IsTrade":true,"INVOICETITLE":"吞吞吐吐吞吞吐吐","INVOICEBANKNAME":"88888888","INVOICEBANKACCOUNT":"666666","INVOICETEL":"88888888","INVOICEADDRESS":"吞吞吐吐吞吞吐吐","F_WB_KHGLLX_Id":"5c136999714553","F_WB_KHGLLX":{"Id":"5c136999714553","FNumber":"1001","MultiLanguageText":[{"PkId":"5c136999714554","LocaleId":2052,"FDataValue":"关联"}],"FDataValue":[{"Key":2052,"Value":"关联"}]},"F_dbjkh":false,"BD_CUSTLOCATION":[],"BD_CUSTBANK":[{"Id":100680,"BANKCODE":"88888888","CURRENCYID_Id":1,"CURRENCYID":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"ACCOUNTNAME":"吞吞吐吐吞吞吐吐","ISDEFAULT":false,"COUNTRY_Id":" ","COUNTRY":null,"MultiLanguageText":[{"PkId":100680,"LocaleId":2052,"OPENBANKNAME":"吞吞吐吐吞吞吐吐"}],"OPENBANKNAME":[{"Key":2052,"Value":"吞吞吐吐吞吞吐吐"}],"BankTypeRec_Id":0,"BankTypeRec":null,"OpenAddressRec":"吞吞吐吐吞吞吐吐","CNAPS":" ","FTextBankDetail":" "}],"BD_CUSTCONTACT":[],"BD_CUSTORDERORG":[{"Id":0,"OrderOrgId_Id":0,"OrderOrgId":null,"ISDEFAULT":true}],"BD_CUSTOMEREXT":[{"Id":122322,"EnableSL":false,"FreezeStatus":"A","FreezeLimit":" ","FreezeOperator_Id":0,"FreezeOperator":null,"FreezeDate":null,"PROVINCE_Id":"5c145b495faf80","PROVINCE":{"Id":"5c145b495faf80","FNumber":"14","MultiLanguageText":[{"PkId":"5c145b495faf9f","LocaleId":2052,"FDataValue":"山西"}],"FDataValue":[{"Key":2052,"Value":"山西"}]},"CITY_Id":"5c2e1882e0a572","CITY":{"Id":"5c2e1882e0a572","FNumber":"1406","MultiLanguageText":[{"PkId":"5c2e1882e0a6c2","LocaleId":2052,"FDataValue":"朔州"}],"FDataValue":[{"Key":2052,"Value":"朔州"}]},"DefaultConsiLoc_Id":0,"DefaultConsiLoc":null,"DefaultSettleLoc_Id":0,"DefaultSettleLoc":null,"DefaultPayerLoc_Id":0,"DefaultPayerLoc":null,"DefaultContact_Id":0,"DefaultContact":null,"MarginLevel":0,"FDebitCard":" ","SettleId_Id":0,"SettleId":null,"ChargeId_Id":0,"ChargeId":null}]}
         */

        private Object ResponseStatus;
        private ResultBean Result;

        public Object getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(Object ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public ResultBean getResult() {
            return Result;
        }

        public void setResult(ResultBean Result) {
            this.Result = Result;
        }

        public static class ResultBean {
            /**
             * Id : 644074
             * msterID : 644074
             * DocumentStatus : C
             * ForbidStatus : A
             * MultiLanguageText : [{"PkId":122323,"LocaleId":2052,"Name":"吞吞吐吐吞吞吐吐","Description":" ","ShortName":" "}]
             * Name : [{"Key":2052,"Value":"吞吞吐吐吞吞吐吐"}]
             * Number : tttttt000121
             * Description : [{"Key":2052,"Value":" "}]
             * CreateOrgId_Id : 1
             * CreateOrgId : {"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"}
             * UseOrgId_Id : 1
             * UseOrgId : {"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"}
             * CreatorId_Id : 642651
             * CreatorId : {"Id":642651,"Name":"张敏","UserAccount":" "}
             * ModifierId_Id : 642651
             * ModifierId : {"Id":642651,"Name":"张敏","UserAccount":" "}
             * CreateDate : 2019-04-10T09:20:03.793
             * FModifyDate : 2019-04-10T13:27:52.16
             * ShortName : [{"Key":2052,"Value":" "}]
             * COUNTRY_Id : 46a524cf-5797-4e46-bd0a-7203fc426d9c
             * COUNTRY : {"Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","FNumber":"China","MultiLanguageText":[{"PkId":"ae6420c4-0e15-4303-aa64-8297b511860c","LocaleId":1033,"FDataValue":"China"},{"PkId":"1001","LocaleId":2052,"FDataValue":"中国"}],"FDataValue":[{"Key":1033,"Value":"China"},{"Key":2052,"Value":"中国"}]}
             * PROVINCIAL_Id :
             * PROVINCIAL : null
             * FZIP :
             * TEL :
             * FTAXREGISTERCODE : 88888888
             * FAX :
             * FGROUPCUSTID_Id : 0
             * FGROUPCUSTID : null
             * FSUPPLIERID_Id : 0
             * FSUPPLIERID : null
             * TRADINGCURRID_Id : 1
             * TRADINGCURRID : {"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"}
             * SALDEPTID_Id : 0
             * SALDEPTID : null
             * SALGROUPID_Id : 0
             * SALGROUPID : null
             * SELLER_Id : 0
             * SELLER : null
             * TRANSLEADTIME : 0
             * PRICELISTID_Id : 0
             * PRICELISTID : null
             * DISCOUNTLISTID_Id : 0
             * DISCOUNTLISTID : null
             * SETTLETYPEID_Id : 30609
             * SETTLETYPEID : {"Id":30609,"msterID":30609,"MultiLanguageText":[{"PkId":15,"LocaleId":2052,"Name":"支付宝"}],"Name":[{"Key":2052,"Value":"支付宝"}],"Number":"JSFS32_SYS","TYPE":"2"}
             * RECEIVECURRID_Id : 0
             * RECEIVECURRID : null
             * RECCONDITIONID_Id : 20003
             * RECCONDITIONID : {"Id":20003,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"货到收款"},{"PkId":5,"LocaleId":1033,"Name":"Collect on Delivery"}],"Name":[{"Key":2052,"Value":"货到收款"},{"Key":1033,"Value":"Collect on Delivery"}],"Number":"SKTJ01_SYS"}
             * FISCREDITCHECK : false
             * APPROVERID_Id : 642651
             * APPROVERID : {"Id":642651,"Name":"张敏","UserAccount":" "}
             * APPROVEDATE : 2019-04-10T13:27:53.997
             * FORBIDDERID_Id : 0
             * FORBIDDERID : null
             * FORBIDDATE : null
             * TaxType_Id : 9e855eb97bec43e7b50c3e0e0bf51210
             * TaxType : {"Id":"9e855eb97bec43e7b50c3e0e0bf51210","FNumber":"SFL02_SYS","MultiLanguageText":[{"PkId":"6de0d14c-e106-4986-a732-5bbf127ec7dc","LocaleId":1033,"FDataValue":"General Taxpayer"},{"PkId":"7841e26e9e5a4fe6a83e7cdbe0c24175","LocaleId":2052,"FDataValue":"一般纳税人"},{"PkId":"2942b840514242b299738f8b9bbe2095","LocaleId":3076,"FDataValue":"一般納稅人"}],"FDataValue":[{"Key":1033,"Value":"General Taxpayer"},{"Key":2052,"Value":"一般纳税人"},{"Key":3076,"Value":"一般納稅人"}]}
             * CustTypeId_Id :
             * CustTypeId : null
             * ADDRESS : 吞吞吐吐吞吞吐吐
             * WEBSITE :
             * FGroup_Id : 114343
             * FGroup : {"Id":114343,"Number":"B13","MultiLanguageText":[{"PkId":100380,"LocaleId":2052,"Name":"河北"}],"Name":[{"Key":2052,"Value":"河北"}]}
             * CompanyScale_Id :
             * CompanyScale : null
             * CompanyType_Id :
             * CompanyType : null
             * CompanyNature_Id :
             * CompanyNature : null
             * CorrespondOrgId_Id : 0
             * CorrespondOrgId : null
             * Priority : 1
             * InvoiceType : 1
             * TaxRate_Id : 233
             * TaxRate : {"Id":233,"msterID":233,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"17%增值税"},{"PkId":11,"LocaleId":1033,"Name":"17% VAT"}],"Name":[{"Key":2052,"Value":"17%增值税"},{"Key":1033,"Value":"17% VAT"}],"Number":"SL01_SYS"}
             * IsDefPayer : false
             * CPAdminCode :
             * IsGroup : false
             * IsTrade : true
             * INVOICETITLE : 吞吞吐吐吞吞吐吐
             * INVOICEBANKNAME : 88888888
             * INVOICEBANKACCOUNT : 666666
             * INVOICETEL : 88888888
             * INVOICEADDRESS : 吞吞吐吐吞吞吐吐
             * F_WB_KHGLLX_Id : 5c136999714553
             * F_WB_KHGLLX : {"Id":"5c136999714553","FNumber":"1001","MultiLanguageText":[{"PkId":"5c136999714554","LocaleId":2052,"FDataValue":"关联"}],"FDataValue":[{"Key":2052,"Value":"关联"}]}
             * F_dbjkh : false
             * BD_CUSTLOCATION : []
             * BD_CUSTBANK : [{"Id":100680,"BANKCODE":"88888888","CURRENCYID_Id":1,"CURRENCYID":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"ACCOUNTNAME":"吞吞吐吐吞吞吐吐","ISDEFAULT":false,"COUNTRY_Id":" ","COUNTRY":null,"MultiLanguageText":[{"PkId":100680,"LocaleId":2052,"OPENBANKNAME":"吞吞吐吐吞吞吐吐"}],"OPENBANKNAME":[{"Key":2052,"Value":"吞吞吐吐吞吞吐吐"}],"BankTypeRec_Id":0,"BankTypeRec":null,"OpenAddressRec":"吞吞吐吐吞吞吐吐","CNAPS":" ","FTextBankDetail":" "}]
             * BD_CUSTCONTACT : []
             * BD_CUSTORDERORG : [{"Id":0,"OrderOrgId_Id":0,"OrderOrgId":null,"ISDEFAULT":true}]
             * BD_CUSTOMEREXT : [{"Id":122322,"EnableSL":false,"FreezeStatus":"A","FreezeLimit":" ","FreezeOperator_Id":0,"FreezeOperator":null,"FreezeDate":null,"PROVINCE_Id":"5c145b495faf80","PROVINCE":{"Id":"5c145b495faf80","FNumber":"14","MultiLanguageText":[{"PkId":"5c145b495faf9f","LocaleId":2052,"FDataValue":"山西"}],"FDataValue":[{"Key":2052,"Value":"山西"}]},"CITY_Id":"5c2e1882e0a572","CITY":{"Id":"5c2e1882e0a572","FNumber":"1406","MultiLanguageText":[{"PkId":"5c2e1882e0a6c2","LocaleId":2052,"FDataValue":"朔州"}],"FDataValue":[{"Key":2052,"Value":"朔州"}]},"DefaultConsiLoc_Id":0,"DefaultConsiLoc":null,"DefaultSettleLoc_Id":0,"DefaultSettleLoc":null,"DefaultPayerLoc_Id":0,"DefaultPayerLoc":null,"DefaultContact_Id":0,"DefaultContact":null,"MarginLevel":0,"FDebitCard":" ","SettleId_Id":0,"SettleId":null,"ChargeId_Id":0,"ChargeId":null}]
             */

            private int Id;
            private int msterID;
            private String DocumentStatus;
            private String ForbidStatus;
            private String Number;
            private int CreateOrgId_Id;
            private CreateOrgIdBean CreateOrgId;
            private int UseOrgId_Id;
            private UseOrgIdBean UseOrgId;
            private int CreatorId_Id;
            private CreatorIdBean CreatorId;
            private int ModifierId_Id;
            private ModifierIdBean ModifierId;
            private String CreateDate;
            private String FModifyDate;
            private String COUNTRY_Id;
            private COUNTRYBean COUNTRY;
            private String PROVINCIAL_Id;
            private Object PROVINCIAL;
            private String FZIP;
            private String TEL;
            private String FTAXREGISTERCODE;
            private String FAX;
            private int FGROUPCUSTID_Id;
            private Object FGROUPCUSTID;
            private int FSUPPLIERID_Id;
            private Object FSUPPLIERID;
            private int TRADINGCURRID_Id;
            private TRADINGCURRIDBean TRADINGCURRID;
            private int SALDEPTID_Id;
            private Object SALDEPTID;
            private int SALGROUPID_Id;
            private Object SALGROUPID;
            private int SELLER_Id;
            private Object SELLER;
            private int TRANSLEADTIME;
            private int PRICELISTID_Id;
            private Object PRICELISTID;
            private int DISCOUNTLISTID_Id;
            private Object DISCOUNTLISTID;
            private int SETTLETYPEID_Id;
            private SETTLETYPEIDBean SETTLETYPEID;
            private int RECEIVECURRID_Id;
            private Object RECEIVECURRID;
            private int RECCONDITIONID_Id;
            private RECCONDITIONIDBean RECCONDITIONID;
            private boolean FISCREDITCHECK;
            private int APPROVERID_Id;
            private APPROVERIDBean APPROVERID;
            private String APPROVEDATE;
            private int FORBIDDERID_Id;
            private Object FORBIDDERID;
            private Object FORBIDDATE;
            private String TaxType_Id;
            private TaxTypeBean TaxType;
            private String CustTypeId_Id;
            private Object CustTypeId;
            private String ADDRESS;
            private String WEBSITE;
            private int FGroup_Id;
            private FGroupBean FGroup;
            private String CompanyScale_Id;
            private Object CompanyScale;
            private String CompanyType_Id;
            private Object CompanyType;
            private String CompanyNature_Id;
            private Object CompanyNature;
            private int CorrespondOrgId_Id;
            private Object CorrespondOrgId;
            private int Priority;
            private String InvoiceType;
            private int TaxRate_Id;
            private TaxRateBean TaxRate;
            private boolean IsDefPayer;
            private String CPAdminCode;
            private boolean IsGroup;
            private boolean IsTrade;
            private String INVOICETITLE;
            private String INVOICEBANKNAME;
            private String INVOICEBANKACCOUNT;
            private String INVOICETEL;
            private String INVOICEADDRESS;
            private String F_WB_KHGLLX_Id;
            private FWBKHGLLXBean F_WB_KHGLLX;
            private boolean F_dbjkh;
            private List<MultiLanguageTextBeanXXXXXXXXXX> MultiLanguageText;
            private List<NameBeanXXXXXXX> Name;
            private List<DescriptionBean> Description;
            private List<ShortNameBean> ShortName;
            private List<?> BD_CUSTLOCATION;
            private List<BDCUSTBANKBean> BD_CUSTBANK;
            private List<?> BD_CUSTCONTACT;
            private List<BDCUSTORDERORGBean> BD_CUSTORDERORG;
            private List<BDCUSTOMEREXTBean> BD_CUSTOMEREXT;

            public int getId() {
                return Id;
            }

            public void setId(int Id) {
                this.Id = Id;
            }

            public int getMsterID() {
                return msterID;
            }

            public void setMsterID(int msterID) {
                this.msterID = msterID;
            }

            public String getDocumentStatus() {
                return DocumentStatus;
            }

            public void setDocumentStatus(String DocumentStatus) {
                this.DocumentStatus = DocumentStatus;
            }

            public String getForbidStatus() {
                return ForbidStatus;
            }

            public void setForbidStatus(String ForbidStatus) {
                this.ForbidStatus = ForbidStatus;
            }

            public String getNumber() {
                return Number;
            }

            public void setNumber(String Number) {
                this.Number = Number;
            }

            public int getCreateOrgId_Id() {
                return CreateOrgId_Id;
            }

            public void setCreateOrgId_Id(int CreateOrgId_Id) {
                this.CreateOrgId_Id = CreateOrgId_Id;
            }

            public CreateOrgIdBean getCreateOrgId() {
                return CreateOrgId;
            }

            public void setCreateOrgId(CreateOrgIdBean CreateOrgId) {
                this.CreateOrgId = CreateOrgId;
            }

            public int getUseOrgId_Id() {
                return UseOrgId_Id;
            }

            public void setUseOrgId_Id(int UseOrgId_Id) {
                this.UseOrgId_Id = UseOrgId_Id;
            }

            public UseOrgIdBean getUseOrgId() {
                return UseOrgId;
            }

            public void setUseOrgId(UseOrgIdBean UseOrgId) {
                this.UseOrgId = UseOrgId;
            }

            public int getCreatorId_Id() {
                return CreatorId_Id;
            }

            public void setCreatorId_Id(int CreatorId_Id) {
                this.CreatorId_Id = CreatorId_Id;
            }

            public CreatorIdBean getCreatorId() {
                return CreatorId;
            }

            public void setCreatorId(CreatorIdBean CreatorId) {
                this.CreatorId = CreatorId;
            }

            public int getModifierId_Id() {
                return ModifierId_Id;
            }

            public void setModifierId_Id(int ModifierId_Id) {
                this.ModifierId_Id = ModifierId_Id;
            }

            public ModifierIdBean getModifierId() {
                return ModifierId;
            }

            public void setModifierId(ModifierIdBean ModifierId) {
                this.ModifierId = ModifierId;
            }

            public String getCreateDate() {
                return CreateDate;
            }

            public void setCreateDate(String CreateDate) {
                this.CreateDate = CreateDate;
            }

            public String getFModifyDate() {
                return FModifyDate;
            }

            public void setFModifyDate(String FModifyDate) {
                this.FModifyDate = FModifyDate;
            }

            public String getCOUNTRY_Id() {
                return COUNTRY_Id;
            }

            public void setCOUNTRY_Id(String COUNTRY_Id) {
                this.COUNTRY_Id = COUNTRY_Id;
            }

            public COUNTRYBean getCOUNTRY() {
                return COUNTRY;
            }

            public void setCOUNTRY(COUNTRYBean COUNTRY) {
                this.COUNTRY = COUNTRY;
            }

            public String getPROVINCIAL_Id() {
                return PROVINCIAL_Id;
            }

            public void setPROVINCIAL_Id(String PROVINCIAL_Id) {
                this.PROVINCIAL_Id = PROVINCIAL_Id;
            }

            public Object getPROVINCIAL() {
                return PROVINCIAL;
            }

            public void setPROVINCIAL(Object PROVINCIAL) {
                this.PROVINCIAL = PROVINCIAL;
            }

            public String getFZIP() {
                return FZIP;
            }

            public void setFZIP(String FZIP) {
                this.FZIP = FZIP;
            }

            public String getTEL() {
                return TEL;
            }

            public void setTEL(String TEL) {
                this.TEL = TEL;
            }

            public String getFTAXREGISTERCODE() {
                return FTAXREGISTERCODE;
            }

            public void setFTAXREGISTERCODE(String FTAXREGISTERCODE) {
                this.FTAXREGISTERCODE = FTAXREGISTERCODE;
            }

            public String getFAX() {
                return FAX;
            }

            public void setFAX(String FAX) {
                this.FAX = FAX;
            }

            public int getFGROUPCUSTID_Id() {
                return FGROUPCUSTID_Id;
            }

            public void setFGROUPCUSTID_Id(int FGROUPCUSTID_Id) {
                this.FGROUPCUSTID_Id = FGROUPCUSTID_Id;
            }

            public Object getFGROUPCUSTID() {
                return FGROUPCUSTID;
            }

            public void setFGROUPCUSTID(Object FGROUPCUSTID) {
                this.FGROUPCUSTID = FGROUPCUSTID;
            }

            public int getFSUPPLIERID_Id() {
                return FSUPPLIERID_Id;
            }

            public void setFSUPPLIERID_Id(int FSUPPLIERID_Id) {
                this.FSUPPLIERID_Id = FSUPPLIERID_Id;
            }

            public Object getFSUPPLIERID() {
                return FSUPPLIERID;
            }

            public void setFSUPPLIERID(Object FSUPPLIERID) {
                this.FSUPPLIERID = FSUPPLIERID;
            }

            public int getTRADINGCURRID_Id() {
                return TRADINGCURRID_Id;
            }

            public void setTRADINGCURRID_Id(int TRADINGCURRID_Id) {
                this.TRADINGCURRID_Id = TRADINGCURRID_Id;
            }

            public TRADINGCURRIDBean getTRADINGCURRID() {
                return TRADINGCURRID;
            }

            public void setTRADINGCURRID(TRADINGCURRIDBean TRADINGCURRID) {
                this.TRADINGCURRID = TRADINGCURRID;
            }

            public int getSALDEPTID_Id() {
                return SALDEPTID_Id;
            }

            public void setSALDEPTID_Id(int SALDEPTID_Id) {
                this.SALDEPTID_Id = SALDEPTID_Id;
            }

            public Object getSALDEPTID() {
                return SALDEPTID;
            }

            public void setSALDEPTID(Object SALDEPTID) {
                this.SALDEPTID = SALDEPTID;
            }

            public int getSALGROUPID_Id() {
                return SALGROUPID_Id;
            }

            public void setSALGROUPID_Id(int SALGROUPID_Id) {
                this.SALGROUPID_Id = SALGROUPID_Id;
            }

            public Object getSALGROUPID() {
                return SALGROUPID;
            }

            public void setSALGROUPID(Object SALGROUPID) {
                this.SALGROUPID = SALGROUPID;
            }

            public int getSELLER_Id() {
                return SELLER_Id;
            }

            public void setSELLER_Id(int SELLER_Id) {
                this.SELLER_Id = SELLER_Id;
            }

            public Object getSELLER() {
                return SELLER;
            }

            public void setSELLER(Object SELLER) {
                this.SELLER = SELLER;
            }

            public int getTRANSLEADTIME() {
                return TRANSLEADTIME;
            }

            public void setTRANSLEADTIME(int TRANSLEADTIME) {
                this.TRANSLEADTIME = TRANSLEADTIME;
            }

            public int getPRICELISTID_Id() {
                return PRICELISTID_Id;
            }

            public void setPRICELISTID_Id(int PRICELISTID_Id) {
                this.PRICELISTID_Id = PRICELISTID_Id;
            }

            public Object getPRICELISTID() {
                return PRICELISTID;
            }

            public void setPRICELISTID(Object PRICELISTID) {
                this.PRICELISTID = PRICELISTID;
            }

            public int getDISCOUNTLISTID_Id() {
                return DISCOUNTLISTID_Id;
            }

            public void setDISCOUNTLISTID_Id(int DISCOUNTLISTID_Id) {
                this.DISCOUNTLISTID_Id = DISCOUNTLISTID_Id;
            }

            public Object getDISCOUNTLISTID() {
                return DISCOUNTLISTID;
            }

            public void setDISCOUNTLISTID(Object DISCOUNTLISTID) {
                this.DISCOUNTLISTID = DISCOUNTLISTID;
            }

            public int getSETTLETYPEID_Id() {
                return SETTLETYPEID_Id;
            }

            public void setSETTLETYPEID_Id(int SETTLETYPEID_Id) {
                this.SETTLETYPEID_Id = SETTLETYPEID_Id;
            }

            public SETTLETYPEIDBean getSETTLETYPEID() {
                return SETTLETYPEID;
            }

            public void setSETTLETYPEID(SETTLETYPEIDBean SETTLETYPEID) {
                this.SETTLETYPEID = SETTLETYPEID;
            }

            public int getRECEIVECURRID_Id() {
                return RECEIVECURRID_Id;
            }

            public void setRECEIVECURRID_Id(int RECEIVECURRID_Id) {
                this.RECEIVECURRID_Id = RECEIVECURRID_Id;
            }

            public Object getRECEIVECURRID() {
                return RECEIVECURRID;
            }

            public void setRECEIVECURRID(Object RECEIVECURRID) {
                this.RECEIVECURRID = RECEIVECURRID;
            }

            public int getRECCONDITIONID_Id() {
                return RECCONDITIONID_Id;
            }

            public void setRECCONDITIONID_Id(int RECCONDITIONID_Id) {
                this.RECCONDITIONID_Id = RECCONDITIONID_Id;
            }

            public RECCONDITIONIDBean getRECCONDITIONID() {
                return RECCONDITIONID;
            }

            public void setRECCONDITIONID(RECCONDITIONIDBean RECCONDITIONID) {
                this.RECCONDITIONID = RECCONDITIONID;
            }

            public boolean isFISCREDITCHECK() {
                return FISCREDITCHECK;
            }

            public void setFISCREDITCHECK(boolean FISCREDITCHECK) {
                this.FISCREDITCHECK = FISCREDITCHECK;
            }

            public int getAPPROVERID_Id() {
                return APPROVERID_Id;
            }

            public void setAPPROVERID_Id(int APPROVERID_Id) {
                this.APPROVERID_Id = APPROVERID_Id;
            }

            public APPROVERIDBean getAPPROVERID() {
                return APPROVERID;
            }

            public void setAPPROVERID(APPROVERIDBean APPROVERID) {
                this.APPROVERID = APPROVERID;
            }

            public String getAPPROVEDATE() {
                return APPROVEDATE;
            }

            public void setAPPROVEDATE(String APPROVEDATE) {
                this.APPROVEDATE = APPROVEDATE;
            }

            public int getFORBIDDERID_Id() {
                return FORBIDDERID_Id;
            }

            public void setFORBIDDERID_Id(int FORBIDDERID_Id) {
                this.FORBIDDERID_Id = FORBIDDERID_Id;
            }

            public Object getFORBIDDERID() {
                return FORBIDDERID;
            }

            public void setFORBIDDERID(Object FORBIDDERID) {
                this.FORBIDDERID = FORBIDDERID;
            }

            public Object getFORBIDDATE() {
                return FORBIDDATE;
            }

            public void setFORBIDDATE(Object FORBIDDATE) {
                this.FORBIDDATE = FORBIDDATE;
            }

            public String getTaxType_Id() {
                return TaxType_Id;
            }

            public void setTaxType_Id(String TaxType_Id) {
                this.TaxType_Id = TaxType_Id;
            }

            public TaxTypeBean getTaxType() {
                return TaxType;
            }

            public void setTaxType(TaxTypeBean TaxType) {
                this.TaxType = TaxType;
            }

            public String getCustTypeId_Id() {
                return CustTypeId_Id;
            }

            public void setCustTypeId_Id(String CustTypeId_Id) {
                this.CustTypeId_Id = CustTypeId_Id;
            }

            public Object getCustTypeId() {
                return CustTypeId;
            }

            public void setCustTypeId(Object CustTypeId) {
                this.CustTypeId = CustTypeId;
            }

            public String getADDRESS() {
                return ADDRESS;
            }

            public void setADDRESS(String ADDRESS) {
                this.ADDRESS = ADDRESS;
            }

            public String getWEBSITE() {
                return WEBSITE;
            }

            public void setWEBSITE(String WEBSITE) {
                this.WEBSITE = WEBSITE;
            }

            public int getFGroup_Id() {
                return FGroup_Id;
            }

            public void setFGroup_Id(int FGroup_Id) {
                this.FGroup_Id = FGroup_Id;
            }

            public FGroupBean getFGroup() {
                return FGroup;
            }

            public void setFGroup(FGroupBean FGroup) {
                this.FGroup = FGroup;
            }

            public String getCompanyScale_Id() {
                return CompanyScale_Id;
            }

            public void setCompanyScale_Id(String CompanyScale_Id) {
                this.CompanyScale_Id = CompanyScale_Id;
            }

            public Object getCompanyScale() {
                return CompanyScale;
            }

            public void setCompanyScale(Object CompanyScale) {
                this.CompanyScale = CompanyScale;
            }

            public String getCompanyType_Id() {
                return CompanyType_Id;
            }

            public void setCompanyType_Id(String CompanyType_Id) {
                this.CompanyType_Id = CompanyType_Id;
            }

            public Object getCompanyType() {
                return CompanyType;
            }

            public void setCompanyType(Object CompanyType) {
                this.CompanyType = CompanyType;
            }

            public String getCompanyNature_Id() {
                return CompanyNature_Id;
            }

            public void setCompanyNature_Id(String CompanyNature_Id) {
                this.CompanyNature_Id = CompanyNature_Id;
            }

            public Object getCompanyNature() {
                return CompanyNature;
            }

            public void setCompanyNature(Object CompanyNature) {
                this.CompanyNature = CompanyNature;
            }

            public int getCorrespondOrgId_Id() {
                return CorrespondOrgId_Id;
            }

            public void setCorrespondOrgId_Id(int CorrespondOrgId_Id) {
                this.CorrespondOrgId_Id = CorrespondOrgId_Id;
            }

            public Object getCorrespondOrgId() {
                return CorrespondOrgId;
            }

            public void setCorrespondOrgId(Object CorrespondOrgId) {
                this.CorrespondOrgId = CorrespondOrgId;
            }

            public int getPriority() {
                return Priority;
            }

            public void setPriority(int Priority) {
                this.Priority = Priority;
            }

            public String getInvoiceType() {
                return InvoiceType;
            }

            public void setInvoiceType(String InvoiceType) {
                this.InvoiceType = InvoiceType;
            }

            public int getTaxRate_Id() {
                return TaxRate_Id;
            }

            public void setTaxRate_Id(int TaxRate_Id) {
                this.TaxRate_Id = TaxRate_Id;
            }

            public TaxRateBean getTaxRate() {
                return TaxRate;
            }

            public void setTaxRate(TaxRateBean TaxRate) {
                this.TaxRate = TaxRate;
            }

            public boolean isIsDefPayer() {
                return IsDefPayer;
            }

            public void setIsDefPayer(boolean IsDefPayer) {
                this.IsDefPayer = IsDefPayer;
            }

            public String getCPAdminCode() {
                return CPAdminCode;
            }

            public void setCPAdminCode(String CPAdminCode) {
                this.CPAdminCode = CPAdminCode;
            }

            public boolean isIsGroup() {
                return IsGroup;
            }

            public void setIsGroup(boolean IsGroup) {
                this.IsGroup = IsGroup;
            }

            public boolean isIsTrade() {
                return IsTrade;
            }

            public void setIsTrade(boolean IsTrade) {
                this.IsTrade = IsTrade;
            }

            public String getINVOICETITLE() {
                return INVOICETITLE;
            }

            public void setINVOICETITLE(String INVOICETITLE) {
                this.INVOICETITLE = INVOICETITLE;
            }

            public String getINVOICEBANKNAME() {
                return INVOICEBANKNAME;
            }

            public void setINVOICEBANKNAME(String INVOICEBANKNAME) {
                this.INVOICEBANKNAME = INVOICEBANKNAME;
            }

            public String getINVOICEBANKACCOUNT() {
                return INVOICEBANKACCOUNT;
            }

            public void setINVOICEBANKACCOUNT(String INVOICEBANKACCOUNT) {
                this.INVOICEBANKACCOUNT = INVOICEBANKACCOUNT;
            }

            public String getINVOICETEL() {
                return INVOICETEL;
            }

            public void setINVOICETEL(String INVOICETEL) {
                this.INVOICETEL = INVOICETEL;
            }

            public String getINVOICEADDRESS() {
                return INVOICEADDRESS;
            }

            public void setINVOICEADDRESS(String INVOICEADDRESS) {
                this.INVOICEADDRESS = INVOICEADDRESS;
            }

            public String getF_WB_KHGLLX_Id() {
                return F_WB_KHGLLX_Id;
            }

            public void setF_WB_KHGLLX_Id(String F_WB_KHGLLX_Id) {
                this.F_WB_KHGLLX_Id = F_WB_KHGLLX_Id;
            }

            public FWBKHGLLXBean getF_WB_KHGLLX() {
                return F_WB_KHGLLX;
            }

            public void setF_WB_KHGLLX(FWBKHGLLXBean F_WB_KHGLLX) {
                this.F_WB_KHGLLX = F_WB_KHGLLX;
            }

            public boolean isF_dbjkh() {
                return F_dbjkh;
            }

            public void setF_dbjkh(boolean F_dbjkh) {
                this.F_dbjkh = F_dbjkh;
            }

            public List<MultiLanguageTextBeanXXXXXXXXXX> getMultiLanguageText() {
                return MultiLanguageText;
            }

            public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXX> MultiLanguageText) {
                this.MultiLanguageText = MultiLanguageText;
            }

            public List<NameBeanXXXXXXX> getName() {
                return Name;
            }

            public void setName(List<NameBeanXXXXXXX> Name) {
                this.Name = Name;
            }

            public List<DescriptionBean> getDescription() {
                return Description;
            }

            public void setDescription(List<DescriptionBean> Description) {
                this.Description = Description;
            }

            public List<ShortNameBean> getShortName() {
                return ShortName;
            }

            public void setShortName(List<ShortNameBean> ShortName) {
                this.ShortName = ShortName;
            }

            public List<?> getBD_CUSTLOCATION() {
                return BD_CUSTLOCATION;
            }

            public void setBD_CUSTLOCATION(List<?> BD_CUSTLOCATION) {
                this.BD_CUSTLOCATION = BD_CUSTLOCATION;
            }

            public List<BDCUSTBANKBean> getBD_CUSTBANK() {
                return BD_CUSTBANK;
            }

            public void setBD_CUSTBANK(List<BDCUSTBANKBean> BD_CUSTBANK) {
                this.BD_CUSTBANK = BD_CUSTBANK;
            }

            public List<?> getBD_CUSTCONTACT() {
                return BD_CUSTCONTACT;
            }

            public void setBD_CUSTCONTACT(List<?> BD_CUSTCONTACT) {
                this.BD_CUSTCONTACT = BD_CUSTCONTACT;
            }

            public List<BDCUSTORDERORGBean> getBD_CUSTORDERORG() {
                return BD_CUSTORDERORG;
            }

            public void setBD_CUSTORDERORG(List<BDCUSTORDERORGBean> BD_CUSTORDERORG) {
                this.BD_CUSTORDERORG = BD_CUSTORDERORG;
            }

            public List<BDCUSTOMEREXTBean> getBD_CUSTOMEREXT() {
                return BD_CUSTOMEREXT;
            }

            public void setBD_CUSTOMEREXT(List<BDCUSTOMEREXTBean> BD_CUSTOMEREXT) {
                this.BD_CUSTOMEREXT = BD_CUSTOMEREXT;
            }

            public static class CreateOrgIdBean {
                /**
                 * Id : 1
                 * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}]
                 * Name : [{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}]
                 * Number : 999
                 */

                private int Id;
                private String Number;
                private List<MultiLanguageTextBean> MultiLanguageText;
                private List<NameBean> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBean> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBean> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBean> getName() {
                    return Name;
                }

                public void setName(List<NameBean> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBean {
                    /**
                     * PkId : 1
                     * LocaleId : 2052
                     * Name : 万帮集团
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBean {
                    /**
                     * Key : 2052
                     * Value : 万帮集团
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class UseOrgIdBean {
                /**
                 * Id : 1
                 * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}]
                 * Name : [{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}]
                 * Number : 999
                 */

                private int Id;
                private String Number;
                private List<MultiLanguageTextBeanX> MultiLanguageText;
                private List<NameBeanX> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBeanX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBeanX> getName() {
                    return Name;
                }

                public void setName(List<NameBeanX> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBeanX {
                    /**
                     * PkId : 1
                     * LocaleId : 2052
                     * Name : 万帮集团
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBeanX {
                    /**
                     * Key : 2052
                     * Value : 万帮集团
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class CreatorIdBean {
                /**
                 * Id : 642651
                 * Name : 张敏
                 * UserAccount :
                 */

                private int Id;
                private String Name;
                private String UserAccount;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getUserAccount() {
                    return UserAccount;
                }

                public void setUserAccount(String UserAccount) {
                    this.UserAccount = UserAccount;
                }
            }

            public static class ModifierIdBean {
                /**
                 * Id : 642651
                 * Name : 张敏
                 * UserAccount :
                 */

                private int Id;
                private String Name;
                private String UserAccount;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getUserAccount() {
                    return UserAccount;
                }

                public void setUserAccount(String UserAccount) {
                    this.UserAccount = UserAccount;
                }
            }

            public static class COUNTRYBean {
                /**
                 * Id : 46a524cf-5797-4e46-bd0a-7203fc426d9c
                 * FNumber : China
                 * MultiLanguageText : [{"PkId":"ae6420c4-0e15-4303-aa64-8297b511860c","LocaleId":1033,"FDataValue":"China"},{"PkId":"1001","LocaleId":2052,"FDataValue":"中国"}]
                 * FDataValue : [{"Key":1033,"Value":"China"},{"Key":2052,"Value":"中国"}]
                 */

                private String Id;
                private String FNumber;
                private List<MultiLanguageTextBeanXX> MultiLanguageText;
                private List<FDataValueBean> FDataValue;

                public String getId() {
                    return Id;
                }

                public void setId(String Id) {
                    this.Id = Id;
                }

                public String getFNumber() {
                    return FNumber;
                }

                public void setFNumber(String FNumber) {
                    this.FNumber = FNumber;
                }

                public List<MultiLanguageTextBeanXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<FDataValueBean> getFDataValue() {
                    return FDataValue;
                }

                public void setFDataValue(List<FDataValueBean> FDataValue) {
                    this.FDataValue = FDataValue;
                }

                public static class MultiLanguageTextBeanXX {
                    /**
                     * PkId : ae6420c4-0e15-4303-aa64-8297b511860c
                     * LocaleId : 1033
                     * FDataValue : China
                     */

                    private String PkId;
                    private int LocaleId;
                    private String FDataValue;

                    public String getPkId() {
                        return PkId;
                    }

                    public void setPkId(String PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getFDataValue() {
                        return FDataValue;
                    }

                    public void setFDataValue(String FDataValue) {
                        this.FDataValue = FDataValue;
                    }
                }

                public static class FDataValueBean {
                    /**
                     * Key : 1033
                     * Value : China
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class TRADINGCURRIDBean {
                /**
                 * Id : 1
                 * msterID : 1
                 * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}]
                 * Name : [{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}]
                 * Number : PRE001
                 * Sysmbol : ¥
                 * PriceDigits : 6
                 * AmountDigits : 2
                 * IsShowCSymbol : true
                 * FormatOrder : 1
                 * RoundType : 1
                 */

                private int Id;
                private int msterID;
                private String Number;
                private String Sysmbol;
                private int PriceDigits;
                private int AmountDigits;
                private boolean IsShowCSymbol;
                private String FormatOrder;
                private String RoundType;
                private List<MultiLanguageTextBeanXXX> MultiLanguageText;
                private List<NameBeanXX> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getMsterID() {
                    return msterID;
                }

                public void setMsterID(int msterID) {
                    this.msterID = msterID;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public String getSysmbol() {
                    return Sysmbol;
                }

                public void setSysmbol(String Sysmbol) {
                    this.Sysmbol = Sysmbol;
                }

                public int getPriceDigits() {
                    return PriceDigits;
                }

                public void setPriceDigits(int PriceDigits) {
                    this.PriceDigits = PriceDigits;
                }

                public int getAmountDigits() {
                    return AmountDigits;
                }

                public void setAmountDigits(int AmountDigits) {
                    this.AmountDigits = AmountDigits;
                }

                public boolean isIsShowCSymbol() {
                    return IsShowCSymbol;
                }

                public void setIsShowCSymbol(boolean IsShowCSymbol) {
                    this.IsShowCSymbol = IsShowCSymbol;
                }

                public String getFormatOrder() {
                    return FormatOrder;
                }

                public void setFormatOrder(String FormatOrder) {
                    this.FormatOrder = FormatOrder;
                }

                public String getRoundType() {
                    return RoundType;
                }

                public void setRoundType(String RoundType) {
                    this.RoundType = RoundType;
                }

                public List<MultiLanguageTextBeanXXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBeanXX> getName() {
                    return Name;
                }

                public void setName(List<NameBeanXX> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBeanXXX {
                    /**
                     * PkId : 1
                     * LocaleId : 2052
                     * Name : 人民币
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBeanXX {
                    /**
                     * Key : 2052
                     * Value : 人民币
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class SETTLETYPEIDBean {
                /**
                 * Id : 30609
                 * msterID : 30609
                 * MultiLanguageText : [{"PkId":15,"LocaleId":2052,"Name":"支付宝"}]
                 * Name : [{"Key":2052,"Value":"支付宝"}]
                 * Number : JSFS32_SYS
                 * TYPE : 2
                 */

                private int Id;
                private int msterID;
                private String Number;
                private String TYPE;
                private List<MultiLanguageTextBeanXXXX> MultiLanguageText;
                private List<NameBeanXXX> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getMsterID() {
                    return msterID;
                }

                public void setMsterID(int msterID) {
                    this.msterID = msterID;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public String getTYPE() {
                    return TYPE;
                }

                public void setTYPE(String TYPE) {
                    this.TYPE = TYPE;
                }

                public List<MultiLanguageTextBeanXXXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXXXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBeanXXX> getName() {
                    return Name;
                }

                public void setName(List<NameBeanXXX> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBeanXXXX {
                    /**
                     * PkId : 15
                     * LocaleId : 2052
                     * Name : 支付宝
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBeanXXX {
                    /**
                     * Key : 2052
                     * Value : 支付宝
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class RECCONDITIONIDBean {
                /**
                 * Id : 20003
                 * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"货到收款"},{"PkId":5,"LocaleId":1033,"Name":"Collect on Delivery"}]
                 * Name : [{"Key":2052,"Value":"货到收款"},{"Key":1033,"Value":"Collect on Delivery"}]
                 * Number : SKTJ01_SYS
                 */

                private int Id;
                private String Number;
                private List<MultiLanguageTextBeanXXXXX> MultiLanguageText;
                private List<NameBeanXXXX> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBeanXXXXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBeanXXXX> getName() {
                    return Name;
                }

                public void setName(List<NameBeanXXXX> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBeanXXXXX {
                    /**
                     * PkId : 1
                     * LocaleId : 2052
                     * Name : 货到收款
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBeanXXXX {
                    /**
                     * Key : 2052
                     * Value : 货到收款
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class APPROVERIDBean {
                /**
                 * Id : 642651
                 * Name : 张敏
                 * UserAccount :
                 */

                private int Id;
                private String Name;
                private String UserAccount;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getUserAccount() {
                    return UserAccount;
                }

                public void setUserAccount(String UserAccount) {
                    this.UserAccount = UserAccount;
                }
            }

            public static class TaxTypeBean {
                /**
                 * Id : 9e855eb97bec43e7b50c3e0e0bf51210
                 * FNumber : SFL02_SYS
                 * MultiLanguageText : [{"PkId":"6de0d14c-e106-4986-a732-5bbf127ec7dc","LocaleId":1033,"FDataValue":"General Taxpayer"},{"PkId":"7841e26e9e5a4fe6a83e7cdbe0c24175","LocaleId":2052,"FDataValue":"一般纳税人"},{"PkId":"2942b840514242b299738f8b9bbe2095","LocaleId":3076,"FDataValue":"一般納稅人"}]
                 * FDataValue : [{"Key":1033,"Value":"General Taxpayer"},{"Key":2052,"Value":"一般纳税人"},{"Key":3076,"Value":"一般納稅人"}]
                 */

                private String Id;
                private String FNumber;
                private List<MultiLanguageTextBeanXXXXXX> MultiLanguageText;
                private List<FDataValueBeanX> FDataValue;

                public String getId() {
                    return Id;
                }

                public void setId(String Id) {
                    this.Id = Id;
                }

                public String getFNumber() {
                    return FNumber;
                }

                public void setFNumber(String FNumber) {
                    this.FNumber = FNumber;
                }

                public List<MultiLanguageTextBeanXXXXXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<FDataValueBeanX> getFDataValue() {
                    return FDataValue;
                }

                public void setFDataValue(List<FDataValueBeanX> FDataValue) {
                    this.FDataValue = FDataValue;
                }

                public static class MultiLanguageTextBeanXXXXXX {
                    /**
                     * PkId : 6de0d14c-e106-4986-a732-5bbf127ec7dc
                     * LocaleId : 1033
                     * FDataValue : General Taxpayer
                     */

                    private String PkId;
                    private int LocaleId;
                    private String FDataValue;

                    public String getPkId() {
                        return PkId;
                    }

                    public void setPkId(String PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getFDataValue() {
                        return FDataValue;
                    }

                    public void setFDataValue(String FDataValue) {
                        this.FDataValue = FDataValue;
                    }
                }

                public static class FDataValueBeanX {
                    /**
                     * Key : 1033
                     * Value : General Taxpayer
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class FGroupBean {
                /**
                 * Id : 114343
                 * Number : B13
                 * MultiLanguageText : [{"PkId":100380,"LocaleId":2052,"Name":"河北"}]
                 * Name : [{"Key":2052,"Value":"河北"}]
                 */

                private int Id;
                private String Number;
                private List<MultiLanguageTextBeanXXXXXXX> MultiLanguageText;
                private List<NameBeanXXXXX> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBeanXXXXXXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBeanXXXXX> getName() {
                    return Name;
                }

                public void setName(List<NameBeanXXXXX> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBeanXXXXXXX {
                    /**
                     * PkId : 100380
                     * LocaleId : 2052
                     * Name : 河北
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBeanXXXXX {
                    /**
                     * Key : 2052
                     * Value : 河北
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class TaxRateBean {
                /**
                 * Id : 233
                 * msterID : 233
                 * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"17%增值税"},{"PkId":11,"LocaleId":1033,"Name":"17% VAT"}]
                 * Name : [{"Key":2052,"Value":"17%增值税"},{"Key":1033,"Value":"17% VAT"}]
                 * Number : SL01_SYS
                 */

                private int Id;
                private int msterID;
                private String Number;
                private List<MultiLanguageTextBeanXXXXXXXX> MultiLanguageText;
                private List<NameBeanXXXXXX> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getMsterID() {
                    return msterID;
                }

                public void setMsterID(int msterID) {
                    this.msterID = msterID;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBeanXXXXXXXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBeanXXXXXX> getName() {
                    return Name;
                }

                public void setName(List<NameBeanXXXXXX> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBeanXXXXXXXX {
                    /**
                     * PkId : 1
                     * LocaleId : 2052
                     * Name : 17%增值税
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBeanXXXXXX {
                    /**
                     * Key : 2052
                     * Value : 17%增值税
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class FWBKHGLLXBean {
                /**
                 * Id : 5c136999714553
                 * FNumber : 1001
                 * MultiLanguageText : [{"PkId":"5c136999714554","LocaleId":2052,"FDataValue":"关联"}]
                 * FDataValue : [{"Key":2052,"Value":"关联"}]
                 */

                private String Id;
                private String FNumber;
                private List<MultiLanguageTextBeanXXXXXXXXX> MultiLanguageText;
                private List<FDataValueBeanXX> FDataValue;

                public String getId() {
                    return Id;
                }

                public void setId(String Id) {
                    this.Id = Id;
                }

                public String getFNumber() {
                    return FNumber;
                }

                public void setFNumber(String FNumber) {
                    this.FNumber = FNumber;
                }

                public List<MultiLanguageTextBeanXXXXXXXXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<FDataValueBeanXX> getFDataValue() {
                    return FDataValue;
                }

                public void setFDataValue(List<FDataValueBeanXX> FDataValue) {
                    this.FDataValue = FDataValue;
                }

                public static class MultiLanguageTextBeanXXXXXXXXX {
                    /**
                     * PkId : 5c136999714554
                     * LocaleId : 2052
                     * FDataValue : 关联
                     */

                    private String PkId;
                    private int LocaleId;
                    private String FDataValue;

                    public String getPkId() {
                        return PkId;
                    }

                    public void setPkId(String PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getFDataValue() {
                        return FDataValue;
                    }

                    public void setFDataValue(String FDataValue) {
                        this.FDataValue = FDataValue;
                    }
                }

                public static class FDataValueBeanXX {
                    /**
                     * Key : 2052
                     * Value : 关联
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class MultiLanguageTextBeanXXXXXXXXXX {
                /**
                 * PkId : 122323
                 * LocaleId : 2052
                 * Name : 吞吞吐吐吞吞吐吐
                 * Description :
                 * ShortName :
                 */

                private int PkId;
                private int LocaleId;
                private String Name;
                private String Description;
                private String ShortName;

                public int getPkId() {
                    return PkId;
                }

                public void setPkId(int PkId) {
                    this.PkId = PkId;
                }

                public int getLocaleId() {
                    return LocaleId;
                }

                public void setLocaleId(int LocaleId) {
                    this.LocaleId = LocaleId;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getDescription() {
                    return Description;
                }

                public void setDescription(String Description) {
                    this.Description = Description;
                }

                public String getShortName() {
                    return ShortName;
                }

                public void setShortName(String ShortName) {
                    this.ShortName = ShortName;
                }
            }

            public static class NameBeanXXXXXXX {
                /**
                 * Key : 2052
                 * Value : 吞吞吐吐吞吞吐吐
                 */

                private int Key;
                private String Value;

                public int getKey() {
                    return Key;
                }

                public void setKey(int Key) {
                    this.Key = Key;
                }

                public String getValue() {
                    return Value;
                }

                public void setValue(String Value) {
                    this.Value = Value;
                }
            }

            public static class DescriptionBean {
                /**
                 * Key : 2052
                 * Value :
                 */

                private int Key;
                private String Value;

                public int getKey() {
                    return Key;
                }

                public void setKey(int Key) {
                    this.Key = Key;
                }

                public String getValue() {
                    return Value;
                }

                public void setValue(String Value) {
                    this.Value = Value;
                }
            }

            public static class ShortNameBean {
                /**
                 * Key : 2052
                 * Value :
                 */

                private int Key;
                private String Value;

                public int getKey() {
                    return Key;
                }

                public void setKey(int Key) {
                    this.Key = Key;
                }

                public String getValue() {
                    return Value;
                }

                public void setValue(String Value) {
                    this.Value = Value;
                }
            }

            public static class BDCUSTBANKBean {
                /**
                 * Id : 100680
                 * BANKCODE : 88888888
                 * CURRENCYID_Id : 1
                 * CURRENCYID : {"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"}
                 * ACCOUNTNAME : 吞吞吐吐吞吞吐吐
                 * ISDEFAULT : false
                 * COUNTRY_Id :
                 * COUNTRY : null
                 * MultiLanguageText : [{"PkId":100680,"LocaleId":2052,"OPENBANKNAME":"吞吞吐吐吞吞吐吐"}]
                 * OPENBANKNAME : [{"Key":2052,"Value":"吞吞吐吐吞吞吐吐"}]
                 * BankTypeRec_Id : 0
                 * BankTypeRec : null
                 * OpenAddressRec : 吞吞吐吐吞吞吐吐
                 * CNAPS :
                 * FTextBankDetail :
                 */

                private int Id;
                private String BANKCODE;
                private int CURRENCYID_Id;
                private CURRENCYIDBean CURRENCYID;
                private String ACCOUNTNAME;
                private boolean ISDEFAULT;
                private String COUNTRY_Id;
                private Object COUNTRY;
                private int BankTypeRec_Id;
                private Object BankTypeRec;
                private String OpenAddressRec;
                private String CNAPS;
                private String FTextBankDetail;
                private List<MultiLanguageTextBeanXXXXXXXXXXXX> MultiLanguageText;
                private List<OPENBANKNAMEBean> OPENBANKNAME;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getBANKCODE() {
                    return BANKCODE;
                }

                public void setBANKCODE(String BANKCODE) {
                    this.BANKCODE = BANKCODE;
                }

                public int getCURRENCYID_Id() {
                    return CURRENCYID_Id;
                }

                public void setCURRENCYID_Id(int CURRENCYID_Id) {
                    this.CURRENCYID_Id = CURRENCYID_Id;
                }

                public CURRENCYIDBean getCURRENCYID() {
                    return CURRENCYID;
                }

                public void setCURRENCYID(CURRENCYIDBean CURRENCYID) {
                    this.CURRENCYID = CURRENCYID;
                }

                public String getACCOUNTNAME() {
                    return ACCOUNTNAME;
                }

                public void setACCOUNTNAME(String ACCOUNTNAME) {
                    this.ACCOUNTNAME = ACCOUNTNAME;
                }

                public boolean isISDEFAULT() {
                    return ISDEFAULT;
                }

                public void setISDEFAULT(boolean ISDEFAULT) {
                    this.ISDEFAULT = ISDEFAULT;
                }

                public String getCOUNTRY_Id() {
                    return COUNTRY_Id;
                }

                public void setCOUNTRY_Id(String COUNTRY_Id) {
                    this.COUNTRY_Id = COUNTRY_Id;
                }

                public Object getCOUNTRY() {
                    return COUNTRY;
                }

                public void setCOUNTRY(Object COUNTRY) {
                    this.COUNTRY = COUNTRY;
                }

                public int getBankTypeRec_Id() {
                    return BankTypeRec_Id;
                }

                public void setBankTypeRec_Id(int BankTypeRec_Id) {
                    this.BankTypeRec_Id = BankTypeRec_Id;
                }

                public Object getBankTypeRec() {
                    return BankTypeRec;
                }

                public void setBankTypeRec(Object BankTypeRec) {
                    this.BankTypeRec = BankTypeRec;
                }

                public String getOpenAddressRec() {
                    return OpenAddressRec;
                }

                public void setOpenAddressRec(String OpenAddressRec) {
                    this.OpenAddressRec = OpenAddressRec;
                }

                public String getCNAPS() {
                    return CNAPS;
                }

                public void setCNAPS(String CNAPS) {
                    this.CNAPS = CNAPS;
                }

                public String getFTextBankDetail() {
                    return FTextBankDetail;
                }

                public void setFTextBankDetail(String FTextBankDetail) {
                    this.FTextBankDetail = FTextBankDetail;
                }

                public List<MultiLanguageTextBeanXXXXXXXXXXXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<OPENBANKNAMEBean> getOPENBANKNAME() {
                    return OPENBANKNAME;
                }

                public void setOPENBANKNAME(List<OPENBANKNAMEBean> OPENBANKNAME) {
                    this.OPENBANKNAME = OPENBANKNAME;
                }

                public static class CURRENCYIDBean {
                    /**
                     * Id : 1
                     * msterID : 1
                     * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}]
                     * Name : [{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}]
                     * Number : PRE001
                     * Sysmbol : ¥
                     * PriceDigits : 6
                     * AmountDigits : 2
                     * IsShowCSymbol : true
                     * FormatOrder : 1
                     * RoundType : 1
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private String Sysmbol;
                    private int PriceDigits;
                    private int AmountDigits;
                    private boolean IsShowCSymbol;
                    private String FormatOrder;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public String getSysmbol() {
                        return Sysmbol;
                    }

                    public void setSysmbol(String Sysmbol) {
                        this.Sysmbol = Sysmbol;
                    }

                    public int getPriceDigits() {
                        return PriceDigits;
                    }

                    public void setPriceDigits(int PriceDigits) {
                        this.PriceDigits = PriceDigits;
                    }

                    public int getAmountDigits() {
                        return AmountDigits;
                    }

                    public void setAmountDigits(int AmountDigits) {
                        this.AmountDigits = AmountDigits;
                    }

                    public boolean isIsShowCSymbol() {
                        return IsShowCSymbol;
                    }

                    public void setIsShowCSymbol(boolean IsShowCSymbol) {
                        this.IsShowCSymbol = IsShowCSymbol;
                    }

                    public String getFormatOrder() {
                        return FormatOrder;
                    }

                    public void setFormatOrder(String FormatOrder) {
                        this.FormatOrder = FormatOrder;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXX {
                        /**
                         * PkId : 1
                         * LocaleId : 2052
                         * Name : 人民币
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 人民币
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class MultiLanguageTextBeanXXXXXXXXXXXX {
                    /**
                     * PkId : 100680
                     * LocaleId : 2052
                     * OPENBANKNAME : 吞吞吐吐吞吞吐吐
                     */

                    private int PkId;
                    private int LocaleId;
                    private String OPENBANKNAME;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getOPENBANKNAME() {
                        return OPENBANKNAME;
                    }

                    public void setOPENBANKNAME(String OPENBANKNAME) {
                        this.OPENBANKNAME = OPENBANKNAME;
                    }
                }

                public static class OPENBANKNAMEBean {
                    /**
                     * Key : 2052
                     * Value : 吞吞吐吐吞吞吐吐
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class BDCUSTORDERORGBean {
                /**
                 * Id : 0
                 * OrderOrgId_Id : 0
                 * OrderOrgId : null
                 * ISDEFAULT : true
                 */

                private int Id;
                private int OrderOrgId_Id;
                private Object OrderOrgId;
                private boolean ISDEFAULT;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getOrderOrgId_Id() {
                    return OrderOrgId_Id;
                }

                public void setOrderOrgId_Id(int OrderOrgId_Id) {
                    this.OrderOrgId_Id = OrderOrgId_Id;
                }

                public Object getOrderOrgId() {
                    return OrderOrgId;
                }

                public void setOrderOrgId(Object OrderOrgId) {
                    this.OrderOrgId = OrderOrgId;
                }

                public boolean isISDEFAULT() {
                    return ISDEFAULT;
                }

                public void setISDEFAULT(boolean ISDEFAULT) {
                    this.ISDEFAULT = ISDEFAULT;
                }
            }

            public static class BDCUSTOMEREXTBean {
                /**
                 * Id : 122322
                 * EnableSL : false
                 * FreezeStatus : A
                 * FreezeLimit :
                 * FreezeOperator_Id : 0
                 * FreezeOperator : null
                 * FreezeDate : null
                 * PROVINCE_Id : 5c145b495faf80
                 * PROVINCE : {"Id":"5c145b495faf80","FNumber":"14","MultiLanguageText":[{"PkId":"5c145b495faf9f","LocaleId":2052,"FDataValue":"山西"}],"FDataValue":[{"Key":2052,"Value":"山西"}]}
                 * CITY_Id : 5c2e1882e0a572
                 * CITY : {"Id":"5c2e1882e0a572","FNumber":"1406","MultiLanguageText":[{"PkId":"5c2e1882e0a6c2","LocaleId":2052,"FDataValue":"朔州"}],"FDataValue":[{"Key":2052,"Value":"朔州"}]}
                 * DefaultConsiLoc_Id : 0
                 * DefaultConsiLoc : null
                 * DefaultSettleLoc_Id : 0
                 * DefaultSettleLoc : null
                 * DefaultPayerLoc_Id : 0
                 * DefaultPayerLoc : null
                 * DefaultContact_Id : 0
                 * DefaultContact : null
                 * MarginLevel : 0
                 * FDebitCard :
                 * SettleId_Id : 0
                 * SettleId : null
                 * ChargeId_Id : 0
                 * ChargeId : null
                 */

                private int Id;
                private boolean EnableSL;
                private String FreezeStatus;
                private String FreezeLimit;
                private int FreezeOperator_Id;
                private Object FreezeOperator;
                private Object FreezeDate;
                private String PROVINCE_Id;
                private PROVINCEBean PROVINCE;
                private String CITY_Id;
                private CITYBean CITY;
                private int DefaultConsiLoc_Id;
                private Object DefaultConsiLoc;
                private int DefaultSettleLoc_Id;
                private Object DefaultSettleLoc;
                private int DefaultPayerLoc_Id;
                private Object DefaultPayerLoc;
                private int DefaultContact_Id;
                private Object DefaultContact;
                private int MarginLevel;
                private String FDebitCard;
                private int SettleId_Id;
                private Object SettleId;
                private int ChargeId_Id;
                private Object ChargeId;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public boolean isEnableSL() {
                    return EnableSL;
                }

                public void setEnableSL(boolean EnableSL) {
                    this.EnableSL = EnableSL;
                }

                public String getFreezeStatus() {
                    return FreezeStatus;
                }

                public void setFreezeStatus(String FreezeStatus) {
                    this.FreezeStatus = FreezeStatus;
                }

                public String getFreezeLimit() {
                    return FreezeLimit;
                }

                public void setFreezeLimit(String FreezeLimit) {
                    this.FreezeLimit = FreezeLimit;
                }

                public int getFreezeOperator_Id() {
                    return FreezeOperator_Id;
                }

                public void setFreezeOperator_Id(int FreezeOperator_Id) {
                    this.FreezeOperator_Id = FreezeOperator_Id;
                }

                public Object getFreezeOperator() {
                    return FreezeOperator;
                }

                public void setFreezeOperator(Object FreezeOperator) {
                    this.FreezeOperator = FreezeOperator;
                }

                public Object getFreezeDate() {
                    return FreezeDate;
                }

                public void setFreezeDate(Object FreezeDate) {
                    this.FreezeDate = FreezeDate;
                }

                public String getPROVINCE_Id() {
                    return PROVINCE_Id;
                }

                public void setPROVINCE_Id(String PROVINCE_Id) {
                    this.PROVINCE_Id = PROVINCE_Id;
                }

                public PROVINCEBean getPROVINCE() {
                    return PROVINCE;
                }

                public void setPROVINCE(PROVINCEBean PROVINCE) {
                    this.PROVINCE = PROVINCE;
                }

                public String getCITY_Id() {
                    return CITY_Id;
                }

                public void setCITY_Id(String CITY_Id) {
                    this.CITY_Id = CITY_Id;
                }

                public CITYBean getCITY() {
                    return CITY;
                }

                public void setCITY(CITYBean CITY) {
                    this.CITY = CITY;
                }

                public int getDefaultConsiLoc_Id() {
                    return DefaultConsiLoc_Id;
                }

                public void setDefaultConsiLoc_Id(int DefaultConsiLoc_Id) {
                    this.DefaultConsiLoc_Id = DefaultConsiLoc_Id;
                }

                public Object getDefaultConsiLoc() {
                    return DefaultConsiLoc;
                }

                public void setDefaultConsiLoc(Object DefaultConsiLoc) {
                    this.DefaultConsiLoc = DefaultConsiLoc;
                }

                public int getDefaultSettleLoc_Id() {
                    return DefaultSettleLoc_Id;
                }

                public void setDefaultSettleLoc_Id(int DefaultSettleLoc_Id) {
                    this.DefaultSettleLoc_Id = DefaultSettleLoc_Id;
                }

                public Object getDefaultSettleLoc() {
                    return DefaultSettleLoc;
                }

                public void setDefaultSettleLoc(Object DefaultSettleLoc) {
                    this.DefaultSettleLoc = DefaultSettleLoc;
                }

                public int getDefaultPayerLoc_Id() {
                    return DefaultPayerLoc_Id;
                }

                public void setDefaultPayerLoc_Id(int DefaultPayerLoc_Id) {
                    this.DefaultPayerLoc_Id = DefaultPayerLoc_Id;
                }

                public Object getDefaultPayerLoc() {
                    return DefaultPayerLoc;
                }

                public void setDefaultPayerLoc(Object DefaultPayerLoc) {
                    this.DefaultPayerLoc = DefaultPayerLoc;
                }

                public int getDefaultContact_Id() {
                    return DefaultContact_Id;
                }

                public void setDefaultContact_Id(int DefaultContact_Id) {
                    this.DefaultContact_Id = DefaultContact_Id;
                }

                public Object getDefaultContact() {
                    return DefaultContact;
                }

                public void setDefaultContact(Object DefaultContact) {
                    this.DefaultContact = DefaultContact;
                }

                public int getMarginLevel() {
                    return MarginLevel;
                }

                public void setMarginLevel(int MarginLevel) {
                    this.MarginLevel = MarginLevel;
                }

                public String getFDebitCard() {
                    return FDebitCard;
                }

                public void setFDebitCard(String FDebitCard) {
                    this.FDebitCard = FDebitCard;
                }

                public int getSettleId_Id() {
                    return SettleId_Id;
                }

                public void setSettleId_Id(int SettleId_Id) {
                    this.SettleId_Id = SettleId_Id;
                }

                public Object getSettleId() {
                    return SettleId;
                }

                public void setSettleId(Object SettleId) {
                    this.SettleId = SettleId;
                }

                public int getChargeId_Id() {
                    return ChargeId_Id;
                }

                public void setChargeId_Id(int ChargeId_Id) {
                    this.ChargeId_Id = ChargeId_Id;
                }

                public Object getChargeId() {
                    return ChargeId;
                }

                public void setChargeId(Object ChargeId) {
                    this.ChargeId = ChargeId;
                }

                public static class PROVINCEBean {
                    /**
                     * Id : 5c145b495faf80
                     * FNumber : 14
                     * MultiLanguageText : [{"PkId":"5c145b495faf9f","LocaleId":2052,"FDataValue":"山西"}]
                     * FDataValue : [{"Key":2052,"Value":"山西"}]
                     */

                    private String Id;
                    private String FNumber;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXX> MultiLanguageText;
                    private List<FDataValueBeanXXX> FDataValue;

                    public String getId() {
                        return Id;
                    }

                    public void setId(String Id) {
                        this.Id = Id;
                    }

                    public String getFNumber() {
                        return FNumber;
                    }

                    public void setFNumber(String FNumber) {
                        this.FNumber = FNumber;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<FDataValueBeanXXX> getFDataValue() {
                        return FDataValue;
                    }

                    public void setFDataValue(List<FDataValueBeanXXX> FDataValue) {
                        this.FDataValue = FDataValue;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXX {
                        /**
                         * PkId : 5c145b495faf9f
                         * LocaleId : 2052
                         * FDataValue : 山西
                         */

                        private String PkId;
                        private int LocaleId;
                        private String FDataValue;

                        public String getPkId() {
                            return PkId;
                        }

                        public void setPkId(String PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getFDataValue() {
                            return FDataValue;
                        }

                        public void setFDataValue(String FDataValue) {
                            this.FDataValue = FDataValue;
                        }
                    }

                    public static class FDataValueBeanXXX {
                        /**
                         * Key : 2052
                         * Value : 山西
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class CITYBean {
                    /**
                     * Id : 5c2e1882e0a572
                     * FNumber : 1406
                     * MultiLanguageText : [{"PkId":"5c2e1882e0a6c2","LocaleId":2052,"FDataValue":"朔州"}]
                     * FDataValue : [{"Key":2052,"Value":"朔州"}]
                     */

                    private String Id;
                    private String FNumber;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<FDataValueBeanXXXX> FDataValue;

                    public String getId() {
                        return Id;
                    }

                    public void setId(String Id) {
                        this.Id = Id;
                    }

                    public String getFNumber() {
                        return FNumber;
                    }

                    public void setFNumber(String FNumber) {
                        this.FNumber = FNumber;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<FDataValueBeanXXXX> getFDataValue() {
                        return FDataValue;
                    }

                    public void setFDataValue(List<FDataValueBeanXXXX> FDataValue) {
                        this.FDataValue = FDataValue;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXX {
                        /**
                         * PkId : 5c2e1882e0a6c2
                         * LocaleId : 2052
                         * FDataValue : 朔州
                         */

                        private String PkId;
                        private int LocaleId;
                        private String FDataValue;

                        public String getPkId() {
                            return PkId;
                        }

                        public void setPkId(String PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getFDataValue() {
                            return FDataValue;
                        }

                        public void setFDataValue(String FDataValue) {
                            this.FDataValue = FDataValue;
                        }
                    }

                    public static class FDataValueBeanXXXX {
                        /**
                         * Key : 2052
                         * Value : 朔州
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }
            }
        }
    }
}