package com.h3bpm.starcharge.bean.k3c;

/**
 * @author LLongAgo
 * @date 2019/4/18
 * @since 1.0.0
 */
public class LastNumber {

    /**
     * code : 0
     * data : {"fnumber":"9900010828",
     * "aaa":"{"bbb":}"}
     * msg : ok
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * fnumber : 9900010828
         */

        private String fnumber;

        public String getFnumber() {
            return fnumber;
        }

        public void setFnumber(String fnumber) {
            this.fnumber = fnumber;
        }
    }
}