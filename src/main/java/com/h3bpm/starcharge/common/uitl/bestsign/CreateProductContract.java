package com.h3bpm.starcharge.common.uitl.bestsign;

import OThinker.Common.DateTimeUtil;
import OThinker.H3.Entity.DataModel.BizObject;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import api.domain.template.create.*;
import com.h3bpm.starcharge.bean.NewspaperService;
import com.h3bpm.starcharge.bean.ProjectDesign;
import com.h3bpm.starcharge.bean.ZeroStarSupport;
import com.h3bpm.starcharge.bean.product.GeneralProject;
import com.h3bpm.starcharge.bean.product.LowProcurement;
import com.h3bpm.starcharge.bean.product.MembraneStructure;
import com.h3bpm.starcharge.common.uitl.BestsignPropertiesUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 工程类 构造方法
 */
public class CreateProductContract {


    private static final String ACCOUNT = BestsignPropertiesUtil.getProperty("account");

    /**
     * 项目工程总承包
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO generalProject(InstanceData instanceData){
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(GeneralProject.contractNo.name())));
            labelVOList.add(new LabelVO("项目名称",(String)valueTable.get(GeneralProject.entryName.name())));
            labelVOList.add(new LabelVO("建设单位",(String)valueTable.get(GeneralProject.constructionUnit.name())));
            labelVOList.add(new LabelVO("施工单位",(String)valueTable.get(GeneralProject.buildUnit.name())));
            labelVOList.add(new LabelVO("工程名称",(String)valueTable.get(GeneralProject.projectName.name())));
            labelVOList.add(new LabelVO("工程地点",(String)valueTable.get(GeneralProject.projectAddress.name())));
            labelVOList.add(new LabelVO("工程范围",(String)valueTable.get(GeneralProject.projectScope1.name())));
            labelVOList.add(new LabelVO("其他工程范围",(String)valueTable.get(GeneralProject.otherProjectScope.name())));
            labelVOList.add(new LabelVO("工程总价（大写）",(String)valueTable.get(GeneralProject.totalAmountUpper.name())));
            labelVOList.add(new LabelVO("工程总价（小写）",(String)valueTable.get(GeneralProject.totalAmountLower.name())));
            labelVOList.add(new LabelVO("施工开工日期",DateTimeUtil.getDateToString((Date)valueTable.get(GeneralProject.constructStartDate.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("工程竣工日期",DateTimeUtil.getDateToString((Date)valueTable.get(GeneralProject.projectCompleteDate.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("生效日期",(String)valueTable.get(GeneralProject.paymentCondition.name())));
            labelVOList.add(new LabelVO("工程履约保证金",(String)valueTable.get(GeneralProject.performanceBond.name())));
            labelVOList.add(new LabelVO("施工保证金",(String)valueTable.get(GeneralProject.constructMargin.name())));
            labelVOList.add(new LabelVO("工地负责人姓名",(String)valueTable.get(GeneralProject.chargeName.name())));
            labelVOList.add(new LabelVO("工地负责人电话",(String)valueTable.get(GeneralProject.chargeTel.name())));
            labelVOList.add(new LabelVO("其他",(String)valueTable.get(GeneralProject.other.name())));
            labelVOList.add(new LabelVO("甲方工商注册住所",(String)valueTable.get(GeneralProject.businessRegistAddressA.name())));
            labelVOList.add(new LabelVO("统一信用代码",(String)valueTable.get(GeneralProject.businessOrgCodeA.name())));
            labelVOList.add(new LabelVO("甲方法定代表人",(String)valueTable.get(GeneralProject.legalRepresentA.name())));
            labelVOList.add(new LabelVO("甲方授权代表人",(String)valueTable.get(GeneralProject.authRepresentA.name())));
            labelVOList.add(new LabelVO("甲方开户银行",(String)valueTable.get(GeneralProject.openBankA.name())));
            labelVOList.add(new LabelVO("甲方账号",(String)valueTable.get(GeneralProject.bankAccountA.name())));
            labelVOList.add(new LabelVO("乙方工商注册住所",(String)valueTable.get(GeneralProject.businessRegistAddressB.name())));
            labelVOList.add(new LabelVO("乙方统一社会信用代码",(String)valueTable.get(GeneralProject.businessOrgCodeB.name())));
            labelVOList.add(new LabelVO("乙方法定代表人",(String)valueTable.get(GeneralProject.legalRepresentB.name())));
            labelVOList.add(new LabelVO("乙方授权代表人",(String)valueTable.get(GeneralProject.authRepresentB.name())));
            labelVOList.add(new LabelVO("乙方开户银行",(String)valueTable.get(GeneralProject.openBankB.name())));
            labelVOList.add(new LabelVO("乙方账号",(String)valueTable.get(GeneralProject.bankAccountB.name())));
            labelVOList.add(new LabelVO("签订地",(String)valueTable.get(GeneralProject.contractSignAddress.name())));
            labelVOList.add(new LabelVO("现场责任人",(String)valueTable.get(GeneralProject.onSitePerson.name())));
            labelVOList.add(new LabelVO("现场安全员",(String)valueTable.get(GeneralProject.siteSafePerson.name())));
            labelVOList.add(new LabelVO("选填",(String)valueTable.get(GeneralProject.technical.name())));
            List<PlaceHolder> placeHolders = new ArrayList<>();
            placeHolders.add(new PlaceHolder(ACCOUNT,ACCOUNT,(String)valueTable.get(GeneralProject.constructionUnit.name()),labelVOList));
            String templateId = "2157108634033586183";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2157109061525443587L,(String)valueTable.get(GeneralProject.phoneB.name()),
                    (String)valueTable.get(GeneralProject.buildUnit.name()),(String)valueTable.get(GeneralProject.buildUnit.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }

    /**
     * 采购合同（低压）
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO lowProcurement(InstanceData instanceData){
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(LowProcurement.contractNo.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(LowProcurement.partyA.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(LowProcurement.partyB.name())));
            labelVOList.add(new LabelVO("总价",(String)valueTable.get(LowProcurement.totalPrice.name())));
            labelVOList.add(new LabelVO("交货地点",(String)valueTable.get(LowProcurement.deliveryAddress.name())));
            labelVOList.add(new LabelVO("收货人",(String)valueTable.get(LowProcurement.receiver.name())));
            labelVOList.add(new LabelVO("其他",(String)valueTable.get(LowProcurement.otherRemark.name())));
            labelVOList.add(new LabelVO("预付费",(String)valueTable.get(LowProcurement.advancePayRate.name())));
            labelVOList.add(new LabelVO("尾款",(String)valueTable.get(LowProcurement.finalPayRate.name())));
            labelVOList.add(new LabelVO("质保期",(String)valueTable.get(LowProcurement.warrantyPeriod.name())));
            labelVOList.add(new LabelVO("质保金",(String)valueTable.get(LowProcurement.premiun.name())));
            labelVOList.add(new LabelVO("甲方法定代表人",(String)valueTable.get(LowProcurement.legalRepresentA.name())));
            labelVOList.add(new LabelVO("甲方委托代理人",(String)valueTable.get(LowProcurement.agentA.name())));
            labelVOList.add(new LabelVO("乙方法定代表人",(String)valueTable.get(LowProcurement.legalRepresentB.name())));
            labelVOList.add(new LabelVO("乙方委托代理人",(String)valueTable.get(LowProcurement.agentB.name())));
            //构造子表
            try {
                BizObject[] zbBizObjects = (BizObject[]) valueTable.get(LowProcurement.produnctDetail.name());
                for (int i = 1; i <= zbBizObjects.length; i++) {
                    Map<String, Object> zbValueTable = zbBizObjects[i - 1].getValueTable();
                    String str = "";
                    if (i != 1) {
                        str = String.valueOf(i);
                    } else {
                        labelVOList.add(new LabelVO("交货期", DateTimeUtil.getDateToString((Date) valueTable.get(LowProcurement.sendTime.name()), "yyyy-MM-dd")));
                        labelVOList.add(new LabelVO("税率", "16%"));
                    }
                    labelVOList.add(new LabelVO("序号" + str, String.valueOf(i)));
                    labelVOList.add(new LabelVO("规格型号" + str, String.valueOf(zbValueTable.get(LowProcurement.productType.name()))));
                    labelVOList.add(new LabelVO("产品名称" + str, String.valueOf(zbValueTable.get(LowProcurement.productName.name()))));
                    labelVOList.add(new LabelVO("单位" + str, String.valueOf(zbValueTable.get(LowProcurement.productSit.name()))));
                    labelVOList.add(new LabelVO("数量" + str, String.valueOf(zbValueTable.get(LowProcurement.productNum.name()))));
                    labelVOList.add(new LabelVO("单价" + str, String.valueOf(zbValueTable.get(LowProcurement.productPrice.name()))));
                    labelVOList.add(new LabelVO("总价" + str, String.valueOf(zbValueTable.get(LowProcurement.productSum.name()))));
                    labelVOList.add(new LabelVO("备注" + str, String.valueOf(zbValueTable.get(LowProcurement.productRemark.name()))));
                }
            } catch (Exception e) {
            }
            List<PlaceHolder> placeHolders = new ArrayList<PlaceHolder>();
            placeHolders.add(new PlaceHolder(ACCOUNT,ACCOUNT,(String)valueTable.get(LowProcurement.partyA.name()),labelVOList));
            String templateId = "2156365194865942533";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2156365676489479176L,(String)valueTable.get(LowProcurement.phoneB.name()),
                    (String)valueTable.get(LowProcurement.partyB.name()),(String)valueTable.get(LowProcurement.partyB.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }


    public static SendContractsSyncVO createProjectDesign(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(ProjectDesign.ContractNo.name())));
            labelVOList.add(new LabelVO("合同签订地点",(String)valueTable.get(ProjectDesign.contractSignAddress.name())));
            labelVOList.add(new LabelVO("工程名称",(String)valueTable.get(ProjectDesign.projectName.name())));
            labelVOList.add(new LabelVO("工程编号",(String)valueTable.get(ProjectDesign.projectNo.name())));
            labelVOList.add(new LabelVO("工程地址",(String)valueTable.get(ProjectDesign.projectAddress.name())));
            labelVOList.add(new LabelVO("设计证书等级",(String)valueTable.get(ProjectDesign.designCertificateLevel.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(ProjectDesign.partyA.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(ProjectDesign.partyB.name())));
            labelVOList.add(new LabelVO("项目名称1",(String)valueTable.get(ProjectDesign.entryName.name())));
            labelVOList.add(new LabelVO("设计内容",(String)valueTable.get(ProjectDesign.designContent.name())));
            labelVOList.add(new LabelVO("交付地点",(String)valueTable.get(ProjectDesign.deliveryAddress.name())));
            labelVOList.add(new LabelVO("金额",(String)valueTable.get(ProjectDesign.contractDesignFee.name())));
            labelVOList.add(new LabelVO("甲方联系方式",(String)valueTable.get(ProjectDesign.telA.name())));
            labelVOList.add(new LabelVO("甲方联系人",(String)valueTable.get(ProjectDesign.contactsA.name())));
            labelVOList.add(new LabelVO("甲方开户银行",(String)valueTable.get(ProjectDesign.openBankA.name())));
            labelVOList.add(new LabelVO("甲方账号",(String)valueTable.get(ProjectDesign.bankAccountA.name())));
            labelVOList.add(new LabelVO("乙方联系方式",(String)valueTable.get(ProjectDesign.telB.name())));
            labelVOList.add(new LabelVO("乙方联系人",(String)valueTable.get(ProjectDesign.contactsB.name())));
            labelVOList.add(new LabelVO("乙方开户银行",(String)valueTable.get(ProjectDesign.openBankB.name())));
            labelVOList.add(new LabelVO("乙方账号",(String)valueTable.get(ProjectDesign.bankAccountB.name())));

            labelVOList.add(new LabelVO("甲方电子邮箱",(String)valueTable.get(ProjectDesign.emailA.name())));
            labelVOList.add(new LabelVO("乙方电子邮箱",(String)valueTable.get(ProjectDesign.emailB.name())));
            labelVOList.add(new LabelVO("其他条约",(String)valueTable.get(ProjectDesign.otherContract.name())));

            List<PlaceHolder> placeHolders = new ArrayList<>();
            placeHolders.add(new PlaceHolder(ACCOUNT,ACCOUNT,(String)valueTable.get(ProjectDesign.partyA.name()),labelVOList));
            String templateId = "2156394320599452676";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2156394713849007111L,(String)valueTable.get(ProjectDesign.telB.name()),
                    (String)valueTable.get(ProjectDesign.partyB.name()),(String)valueTable.get(ProjectDesign.partyB.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }

    public static SendContractsSyncVO createZeroStarSupport(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(ZeroStarSupport.contractNo.name())));
            labelVOList.add(new LabelVO("选填",(String)valueTable.get(ZeroStarSupport.projectContent.name())));
            labelVOList.add(new LabelVO("其他",(String)valueTable.get(ZeroStarSupport.otherContent.name())));
            labelVOList.add(new LabelVO("工程地点",(String)valueTable.get(ZeroStarSupport.projectAddress.name())));
            labelVOList.add(new LabelVO("工程造价",(String)valueTable.get(ZeroStarSupport.projectCost.name())));
            labelVOList.add(new LabelVO("其他造价",(String)valueTable.get(ZeroStarSupport.otherCost.name())));
            labelVOList.add(new LabelVO("其他条约",(String)valueTable.get(ZeroStarSupport.otherty.name())));
            labelVOList.add(new LabelVO("工程含税固定总价",(String)valueTable.get(ZeroStarSupport.projectCostTotal.name())));
            labelVOList.add(new LabelVO("桩群包干总价",(String)valueTable.get(ZeroStarSupport.stubAmountBao.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(ZeroStarSupport.partyA.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(ZeroStarSupport.partyB.name())));
            labelVOList.add(new LabelVO("交流充电桩单价",(String)valueTable.get(ZeroStarSupport.stubPriceAC.name())));
            labelVOList.add(new LabelVO("交流充电桩数量",(String)valueTable.get(ZeroStarSupport.stubQtyAC.name())));
            labelVOList.add(new LabelVO("交流充电桩总价",(String)valueTable.get(ZeroStarSupport.stubAmountAC.name())));
            labelVOList.add(new LabelVO("直流充电桩单价",(String)valueTable.get(ZeroStarSupport.stubPriceDC.name())));
            labelVOList.add(new LabelVO("直接充电桩数量",(String)valueTable.get(ZeroStarSupport.stubQtyDC.name())));
            labelVOList.add(new LabelVO("直流充电桩总价",(String)valueTable.get(ZeroStarSupport.stubAmountDC.name())));
            labelVOList.add(new LabelVO("工作日",(String)valueTable.get(ZeroStarSupport.completeDays.name())));
            labelVOList.add(new LabelVO("工程款支付100%",(String)valueTable.get(ZeroStarSupport.payMethodAmt100.name())));
            labelVOList.add(new LabelVO("工程款支付90%",(String)valueTable.get(ZeroStarSupport.payMethodAmt90.name())));
            labelVOList.add(new LabelVO("甲方法定代表人",(String)valueTable.get(ZeroStarSupport.legalRepresentA.name())));
            labelVOList.add(new LabelVO("甲方委托代理人",(String)valueTable.get(ZeroStarSupport.agentA.name())));
            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(ZeroStarSupport.addressA.name())));
            labelVOList.add(new LabelVO("甲方联系人",(String)valueTable.get(ZeroStarSupport.contactsA.name())));
            labelVOList.add(new LabelVO("甲方联系方式",(String)valueTable.get(ZeroStarSupport.telA.name())));
            labelVOList.add(new LabelVO("甲方开户银行",(String)valueTable.get(ZeroStarSupport.openBankA.name())));
            labelVOList.add(new LabelVO("甲方账号",(String)valueTable.get(ZeroStarSupport.bankAccountA.name())));
            labelVOList.add(new LabelVO("甲方税号",(String)valueTable.get(ZeroStarSupport.taxRegistNoA.name())));
            labelVOList.add(new LabelVO("乙方法定代表人",(String)valueTable.get(ZeroStarSupport.legalRepresentB.name())));
            labelVOList.add(new LabelVO("乙方委托代理人",(String)valueTable.get(ZeroStarSupport.agentB.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(ZeroStarSupport.addressB.name())));
            labelVOList.add(new LabelVO("乙方联系人",(String)valueTable.get(ZeroStarSupport.contactsB.name())));
            labelVOList.add(new LabelVO("乙方联系方式",(String)valueTable.get(ZeroStarSupport.telB.name())));
            labelVOList.add(new LabelVO("乙方开户银行",(String)valueTable.get(ZeroStarSupport.openBankB.name())));
            labelVOList.add(new LabelVO("乙方账号",(String)valueTable.get(ZeroStarSupport.bankAccountB.name())));
            labelVOList.add(new LabelVO("乙方税号",(String)valueTable.get(ZeroStarSupport.taxRegistNoB.name())));
            labelVOList.add(new LabelVO("配电箱包干单价",(String)valueTable.get(ZeroStarSupport.priceA.name())));
            labelVOList.add(new LabelVO("数量",(String)valueTable.get(ZeroStarSupport.numA.name())));
            labelVOList.add(new LabelVO("包干价外补贴及增加",(String)valueTable.get(ZeroStarSupport.priceB.name())));
            labelVOList.add(new LabelVO("大写2",(String)valueTable.get(ZeroStarSupport.priceBUp.name())));
            labelVOList.add(new LabelVO("大写",(String)valueTable.get(ZeroStarSupport.costTotalUp.name())));
            labelVOList.add(new LabelVO("大写3",(String)valueTable.get(ZeroStarSupport.PriceUp100.name())));
            labelVOList.add(new LabelVO("大写4",(String)valueTable.get(ZeroStarSupport.priceUp90.name())));
            labelVOList.add(new LabelVO("税率",(String)valueTable.get(ZeroStarSupport.rate.name())));
            List<PlaceHolder> placeHolders = new ArrayList<>();
            placeHolders.add(new PlaceHolder(ACCOUNT,ACCOUNT,(String)valueTable.get(ZeroStarSupport.partyA.name()),labelVOList));
            String templateId = "2156356083344479234";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2156356581183198211L,(String)valueTable.get(ZeroStarSupport.telB.name()),
                    (String)valueTable.get(ZeroStarSupport.partyB.name()),(String)valueTable.get(ZeroStarSupport.partyB.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }

    public static SendContractsSyncVO createNewspaperService(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(NewspaperService.contractNo.name())));
            labelVOList.add(new LabelVO("项目名称",(String)valueTable.get(NewspaperService.entryName.name())));
            labelVOList.add(new LabelVO("报装容量",(String)valueTable.get(NewspaperService.newspaperCapacity.name())));
            labelVOList.add(new LabelVO("时限规定",(String)valueTable.get(NewspaperService.timeLimit.name())));
            labelVOList.add(new LabelVO("报装服务费（大写）",(String)valueTable.get(NewspaperService.serviceFeeUpper.name())));
            labelVOList.add(new LabelVO("报装服务费（小写）",(String)valueTable.get(NewspaperService.serviceFeeLower.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(NewspaperService.partyA.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(NewspaperService.partyB.name())));
            labelVOList.add(new LabelVO("税率",(String)valueTable.get(NewspaperService.accountPercent.name())));
            labelVOList.add(new LabelVO("甲方代表姓名",(String)valueTable.get(NewspaperService.representA.name())));
            labelVOList.add(new LabelVO("甲方代表身份证号",(String)valueTable.get(NewspaperService.representIdNoA.name())));
            labelVOList.add(new LabelVO("乙方代表姓名",(String)valueTable.get(NewspaperService.representB.name())));
            labelVOList.add(new LabelVO("乙方代表身份证号",(String)valueTable.get(NewspaperService.representIdNoB.name())));
            labelVOList.add(new LabelVO("甲方法定代表人",(String)valueTable.get(NewspaperService.legalRepresentA.name())));
            labelVOList.add(new LabelVO("甲方委托代理人",(String)valueTable.get(NewspaperService.agentA.name())));
            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(NewspaperService.addressA.name())));
            labelVOList.add(new LabelVO("甲方开户银行",(String)valueTable.get(NewspaperService.openBankA.name())));
            labelVOList.add(new LabelVO("甲方账号",(String)valueTable.get(NewspaperService.bankAccountA.name())));
            labelVOList.add(new LabelVO("甲方邮政编码",(String)valueTable.get(NewspaperService.postalCodeA.name())));
            labelVOList.add(new LabelVO("甲方联系方式",(String)valueTable.get(NewspaperService.telA.name())));
            labelVOList.add(new LabelVO("乙方法定代表人",(String)valueTable.get(NewspaperService.legalRepresentB.name())));
            labelVOList.add(new LabelVO("乙方委托代理人",(String)valueTable.get(NewspaperService.agentB.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(NewspaperService.addressB.name())));
            labelVOList.add(new LabelVO("乙方开户银行",(String)valueTable.get(NewspaperService.openBankB.name())));
            labelVOList.add(new LabelVO("乙方账号",(String)valueTable.get(NewspaperService.bankAccountB.name())));
            labelVOList.add(new LabelVO("乙方邮政编码",(String)valueTable.get(NewspaperService.postalCodeB.name())));
            labelVOList.add(new LabelVO("乙方联系方式",(String)valueTable.get(NewspaperService.telB.name())));
            labelVOList.add(new LabelVO("其他",(String)valueTable.get(NewspaperService.otherContent.name())));
            List<PlaceHolder> placeHolders = new ArrayList<>();
            placeHolders.add(new PlaceHolder(ACCOUNT,ACCOUNT,(String)valueTable.get(NewspaperService.partyA.name()),labelVOList));
            String templateId = "2156410875055964165";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2156411343475834889L,(String)valueTable.get(NewspaperService.telB.name()),
                    (String)valueTable.get(NewspaperService.partyB.name()),(String)valueTable.get(NewspaperService.partyB.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }


    /**
     * 膜结构合同
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO createMembraneStructure(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(MembraneStructure.ContractNumber.name())));
            labelVOList.add(new LabelVO("建设单位",(String)valueTable.get(MembraneStructure.ConstructionUnit.name())));
            labelVOList.add(new LabelVO("甲方授权代表人",(String)valueTable.get(MembraneStructure.ConstructionUnitmag.name())));
            labelVOList.add(new LabelVO("施工单位",(String)valueTable.get(MembraneStructure.doUnit.name())));
            labelVOList.add(new LabelVO("乙方授权代表人",(String)valueTable.get(MembraneStructure.doUnitMag.name())));
            labelVOList.add(new LabelVO("工程名称",(String)valueTable.get(MembraneStructure.projectName.name())));
            labelVOList.add(new LabelVO("工程地址",(String)valueTable.get(MembraneStructure.projectAddress.name())));
            labelVOList.add(new LabelVO("膜结构型号",(String)valueTable.get(MembraneStructure.MoType.name())));
            labelVOList.add(new LabelVO("膜结构规格",(String)valueTable.get(MembraneStructure.MoSpaces.name())));
            labelVOList.add(new LabelVO("膜结构颜色",(String)valueTable.get(MembraneStructure.MoColor.name())));
            labelVOList.add(new LabelVO("雨棚前高",(String)valueTable.get(MembraneStructure.high.name())));
            labelVOList.add(new LabelVO("雨棚后高",(String)valueTable.get(MembraneStructure.sideHigh.name())));
            labelVOList.add(new LabelVO("雨棚投影长",(String)valueTable.get(MembraneStructure.Long.name())));
            labelVOList.add(new LabelVO("工程总价（小写）",(String)valueTable.get(MembraneStructure.projectAmountLow.name())));
            labelVOList.add(new LabelVO("工程总价（大写）",(String)valueTable.get(MembraneStructure.projectAmountUp.name())));
            labelVOList.add(new LabelVO("设计开工日期",DateTimeUtil.getDateToString((Date)valueTable.get(MembraneStructure.startDate.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("工程竣工日期",DateTimeUtil.getDateToString((Date)valueTable.get(MembraneStructure.endDate.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("其他条约",(String)valueTable.get(MembraneStructure.otherContent.name())));
            labelVOList.add(new LabelVO("甲方电子邮件",(String)valueTable.get(MembraneStructure.emailA.name())));
            labelVOList.add(new LabelVO("乙方电子邮件",(String)valueTable.get(MembraneStructure.emaliB.name())));
            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(MembraneStructure.addressA.name())));
            labelVOList.add(new LabelVO("甲方电话",(String)valueTable.get(MembraneStructure.phoneA.name())));
            labelVOList.add(new LabelVO("甲方开户行",(String)valueTable.get(MembraneStructure.openBackA.name())));
            labelVOList.add(new LabelVO("甲方开户银行",(String)valueTable.get(MembraneStructure.backNumA.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(MembraneStructure.addressB.name())));
            labelVOList.add(new LabelVO("乙方联系方式",(String)valueTable.get(MembraneStructure.phoneB.name())));
            labelVOList.add(new LabelVO("乙方开户银行",(String)valueTable.get(MembraneStructure.openBackB.name())));
            labelVOList.add(new LabelVO("乙方账号",(String)valueTable.get(MembraneStructure.backNumB.name())));
            List<PlaceHolder> placeHolders = new ArrayList<>();
            placeHolders.add(new PlaceHolder(ACCOUNT,ACCOUNT,(String)valueTable.get(MembraneStructure.ConstructionUnit.name()),labelVOList));
            String templateId = "2216517843371953154";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2216518320264314884L,(String)valueTable.get(MembraneStructure.phoneB.name()),
                    (String)valueTable.get(MembraneStructure.doUnit.name()),(String)valueTable.get(MembraneStructure.doUnit.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }


}
