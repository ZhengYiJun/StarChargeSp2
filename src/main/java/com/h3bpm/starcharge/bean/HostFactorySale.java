package com.h3bpm.starcharge.bean;

public enum  HostFactorySale {
    /**
     * HostFactorySale
     */
    purchaseContractNo,
            partyA,
    contractType,
            approvalPrice,
    remark,
            equipmentInfo,
    totalAmountLower,
            totalAmountUpper,
    warrantyPeriod,
            deliveryPeriod,
    deliveryAddress,
            deliveryContacts,
    deliveryTel,
            equipTotalAmount,
    equipPaymentMethod,
            equipAdvancePay,
    addressA,
            legalRepresentA,
    legalRepresentPostA,
            authRepresentA,
    authRepresentPostA,
            telA,
    faxA,
            openBankA,
    taxRegistNoA,
            bankAccountA,
    authRepresentB,
            authRepresentPostB,
    telB,
    legalRepresentB,
    legalRepresentPostB,
    OurUnitName,


    /**
     * 子表
     */
    hostFactorySaleEquipmentInfo,
    /**
     * 子表字段
     */
    productName,
    specificationModels,
            unit,
    qty,
            price,
    amount,
            equipRemark,

}
