package com.h3bpm.starcharge.ret.k3c;

import java.util.List;

/**
 * K3C新增物料返回参数
 *
 * @author LLongAgo
 * @date 2019/3/4
 * @since 1.0.0
 */
public class K3cBaseRet {

    /**
     * Result : {"ResponseStatus":{"ErrorCode":500,"IsSuccess":true,"MsgCode":0,"Errors":[{"FieldName":"","Message":"创建组织不能为空","DIndex":0}],"SuccessEntitys":[{"Id":642745,"Number":"hello","DIndex":0},{"Id":642746,"Number":"world","DIndex":1}]}}
     */

    private ResultBean Result;

    public ResultBean getResult() {
        return Result;
    }

    public void setResult(ResultBean Result) {
        this.Result = Result;
    }

    public static class ResultBean {
        /**
         * ResponseStatus : {"ErrorCode":500,"IsSuccess":true,"MsgCode":0,"Errors":[{"FieldName":"","Message":"创建组织不能为空","DIndex":0}],"SuccessEntitys":[{"Id":642745,"Number":"hello","DIndex":0},{"Id":642746,"Number":"world","DIndex":1}]}
         */

        private ResponseStatusBean ResponseStatus;

        public ResponseStatusBean getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(ResponseStatusBean ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public static class ResponseStatusBean {
            /**
             * ErrorCode : 500
             * IsSuccess : true
             * MsgCode : 0
             * Errors : [{"FieldName":"","Message":"创建组织不能为空","DIndex":0}]
             * SuccessEntitys : [{"Id":642745,"Number":"hello","DIndex":0},{"Id":642746,"Number":"world","DIndex":1}]
             */

            private int ErrorCode;
            private boolean IsSuccess;
            private int MsgCode;
            private List<ErrorsBean> Errors;
            private List<SuccessEntitysBean> SuccessEntitys;

            public int getErrorCode() {
                return ErrorCode;
            }

            public void setErrorCode(int ErrorCode) {
                this.ErrorCode = ErrorCode;
            }

            public boolean isIsSuccess() {
                return IsSuccess;
            }

            public void setIsSuccess(boolean IsSuccess) {
                this.IsSuccess = IsSuccess;
            }

            public int getMsgCode() {
                return MsgCode;
            }

            public void setMsgCode(int MsgCode) {
                this.MsgCode = MsgCode;
            }

            public List<ErrorsBean> getErrors() {
                return Errors;
            }

            public void setErrors(List<ErrorsBean> Errors) {
                this.Errors = Errors;
            }

            public List<SuccessEntitysBean> getSuccessEntitys() {
                return SuccessEntitys;
            }

            public void setSuccessEntitys(List<SuccessEntitysBean> SuccessEntitys) {
                this.SuccessEntitys = SuccessEntitys;
            }

            public static class ErrorsBean {
                /**
                 * FieldName :
                 * Message : 创建组织不能为空
                 * DIndex : 0
                 */

                private String FieldName;
                private String Message;
                private int DIndex;

                public String getFieldName() {
                    return FieldName;
                }

                public void setFieldName(String FieldName) {
                    this.FieldName = FieldName;
                }

                public String getMessage() {
                    return Message;
                }

                public void setMessage(String Message) {
                    this.Message = Message;
                }

                public int getDIndex() {
                    return DIndex;
                }

                public void setDIndex(int DIndex) {
                    this.DIndex = DIndex;
                }
            }

            public static class SuccessEntitysBean {
                /**
                 * Id : 642745
                 * Number : hello
                 * DIndex : 0
                 */

                private int Id;
                private String Number;
                private int DIndex;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public int getDIndex() {
                    return DIndex;
                }

                public void setDIndex(int DIndex) {
                    this.DIndex = DIndex;
                }
            }
        }
    }
}