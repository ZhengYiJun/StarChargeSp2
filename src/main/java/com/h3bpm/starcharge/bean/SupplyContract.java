package com.h3bpm.starcharge.bean;

public enum SupplyContract {

    /**
     * 供货合同
     */
    ObjectID,
    CreatedTime,
    signAddress10,
    ParentPropertyName,
    ModifiedBy,
    Name,
    dataFetch10,
    monthlyPayment,
    telA10,
    price,
    downPayment,
    openBankA10,
    partyA10,
    ParentIndex,
    authRepresentPostA10,
    ParentObjectID,
    customerComplaint10,
    equipmentRepair10,
    platformResponse10,
    CreatedBy,
    authRepresentA10,
    acceptancePeriod,
    customerChoose,
    customerRecordStorage10,
    methodPayment,
    delegateStartDate1,
    equipmentPrice,
    operationFee,
    CreatedByParentId,
    OwnerParentId,
    legalRepresentA10,
    dataReport10,
    safetyFaultReport10,
    paymentPeriod,
    legalRepresentPostA10,
    totalCost,
    fixedYear,
    other,
    addressA10,
    safetyOfflineReport10,
    bankAccountA10,
    delegateEndDate1,
    dataStore10,
    OwnerId,
    ModifiedTime,
    platformNewFunction10,
    partyB10,
    customerUserCare10,
    platformProtocolUpgrade10,
    CappingSeal,
    authRepresentPostB10,
    RunningInstanceId,
    authRepresentB10,
    customerSatisfate10,
    taxRegistNoA,
    operatingCost,
    equipmentDispatch10,
    equipmentExtendInsurance10,
    contractId,
    contractNo,
    totalAmountLower,
    totalAmountUpper,
    warrantyPeriod,
    deliveryDate,
    deliveryPlace,
    operatingExpenses,
    operationYears,
    otherExpenses,

    productSumLower,
    productSumUpper,
    emailA,
    emailB,



    /**
     * 子表
     */
    equipmentInventory,
    /**
     * 子表字段
     */
    productName,
    productType,
    productCount,
    productPrice,
    productTotal,
    productMemo,

    }
