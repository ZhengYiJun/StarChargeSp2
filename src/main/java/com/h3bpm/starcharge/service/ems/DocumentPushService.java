package com.h3bpm.starcharge.service.ems;

import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.base.res.ResBody;
import com.h3bpm.starcharge.common.bean.Bestsign_file;
import org.springframework.stereotype.Service;

@Service
public interface DocumentPushService {

    /**
     * 根据流程数据向ems推文件
     * @param instanceData
     * @return
     * @throws Exception
     */
    ResBody pushAttachment(InstanceData instanceData) throws Exception;

    ResBody postFileToEMS (Bestsign_file bestsignFile) throws Exception;

}

