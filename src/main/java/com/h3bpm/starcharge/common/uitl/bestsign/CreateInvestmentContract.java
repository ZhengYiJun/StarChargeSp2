package com.h3bpm.starcharge.common.uitl.bestsign;

import OThinker.Common.DateTimeUtil;
import OThinker.H3.Entity.DataModel.BizObject;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import api.domain.template.create.*;
import com.h3bpm.starcharge.bean.*;
import com.h3bpm.starcharge.common.uitl.BestsignPropertiesUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 投资类
 */
public class CreateInvestmentContract {

    private static final String ACCOUNT = BestsignPropertiesUtil.getProperty("account");

    private static final String USERNAME = BestsignPropertiesUtil.getProperty("account");



    //TODO roles 都需要更改

    /**
     * 星星充电大客户充电政策合作协议
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO chargePolicy(InstanceData instanceData){
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(ChargePolicy.contractNo.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(ChargePolicy.partyB.name())));

            labelVOList.add(new LabelVO("总价",(String)valueTable.get(ChargePolicy.totalPrice.name())));
            labelVOList.add(new LabelVO("充电服务费",(String)valueTable.get(ChargePolicy.serviceCost.name())));
            labelVOList.add(new LabelVO("其他优惠政策",(String)valueTable.get(ChargePolicy.otherPolicies.name())));

            labelVOList.add(new LabelVO("城市名",(String)valueTable.get(ChargePolicy.cityName.name())));
            labelVOList.add(new LabelVO("保底充电量",(String)valueTable.get(ChargePolicy.chargingCapacity.name())));
            labelVOList.add(new LabelVO("乙方开户名称",(String)valueTable.get(ChargePolicy.partyBOpen.name())));
            labelVOList.add(new LabelVO("乙方税号",(String)valueTable.get(ChargePolicy.partyBTax.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(ChargePolicy.addressB.name())));
            labelVOList.add(new LabelVO("电话",(String)valueTable.get(ChargePolicy.telB.name())));
            labelVOList.add(new LabelVO("乙方开户银行",(String)valueTable.get(ChargePolicy.partyBBank.name())));
            labelVOList.add(new LabelVO("乙方账号",(String)valueTable.get(ChargePolicy.partyBAccount.name())));
            labelVOList.add(new LabelVO("子账号",(String)valueTable.get(ChargePolicy.subAccount.name())));
            labelVOList.add(new LabelVO("合同开始时间",DateTimeUtil.getDateToString((Date)valueTable.get(ChargePolicy.startDate.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("合同结束时间",DateTimeUtil.getDateToString((Date)valueTable.get(ChargePolicy.endDate.name()),"yyyy-MM-dd")));
            //构造子表
            try {
                BizObject[] zbBizObjects = (BizObject[]) valueTable.get(ChargePolicy.chargePolicyDetail.name());
                for (int i = 1; i <= zbBizObjects.length; i++) {
                    Map<String, Object> zbValueTable = zbBizObjects[i - 1].getValueTable();
                    labelVOList.add(new LabelVO("月充电量" + i, String.valueOf(zbValueTable.get(ChargePolicy.monthlyCharge.name()))));
                    labelVOList.add(new LabelVO("月折扣" + i, String.valueOf(zbValueTable.get(ChargePolicy.monthlyDiscount.name()))));
                }
            } catch (Exception e) {

            }

            List<PlaceHolder> placeHolders = new ArrayList<PlaceHolder>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,(String)valueTable.get(ChargePolicy.OurUnitName.name()),labelVOList));
            String templateId = "2156968855631563776";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2156969547356179457L,(String)valueTable.get(ChargePolicy.telB.name()),
                    (String)valueTable.get(ChargePolicy.partyB.name()),(String)valueTable.get(ChargePolicy.partyB.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }

    /**
     * 新能源汽车充电设施推广合作协议（非增容）
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO nonVehicleCharging(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(NonVehicleCharging.ContractNo.name())));
            labelVOList.add(new LabelVO("签署地址",(String)valueTable.get(NonVehicleCharging.signAddress2.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(NonVehicleCharging.partyA2.name())));
            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(NonVehicleCharging.partyADomicile2.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(NonVehicleCharging.partyB2.name())));
            labelVOList.add(new LabelVO("桩群名称",(String)valueTable.get(NonVehicleCharging.partyBGroupName2.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(NonVehicleCharging.partyBDomicile2.name())));
            labelVOList.add(new LabelVO("充电桩详细位置",(String)valueTable.get(NonVehicleCharging.chargingPosition2.name())));
            labelVOList.add(new LabelVO("收费类型",(String)valueTable.get(NonVehicleCharging.chargesType2.name())));
            labelVOList.add(new LabelVO("收费标准",(String)valueTable.get(NonVehicleCharging.chargingStandard2.name())));
            labelVOList.add(new LabelVO("开放时间段",(String)valueTable.get(NonVehicleCharging.openingPeriod2.name())));
            labelVOList.add(new LabelVO("配电容量",(String)valueTable.get(NonVehicleCharging.distributionCapacity.name())));
            labelVOList.add(new LabelVO("租期",(String)valueTable.get(NonVehicleCharging.lease.name())));
            labelVOList.add(new LabelVO("租金 ",(String)valueTable.get(NonVehicleCharging.rent.name())));
            labelVOList.add(new LabelVO("结算方式",(String)valueTable.get(NonVehicleCharging.payment2.name())));
            labelVOList.add(new LabelVO("电价约定",(String)valueTable.get(NonVehicleCharging.electricityPrice.name())));
            labelVOList.add(new LabelVO("峰时电价",(String)valueTable.get(NonVehicleCharging.peakElectricity.name())));
            labelVOList.add(new LabelVO("峰时时间段",(String)valueTable.get(NonVehicleCharging.peakTime.name())));
            labelVOList.add(new LabelVO("谷时电价",(String)valueTable.get(NonVehicleCharging.valleyElectricity.name())));
            labelVOList.add(new LabelVO("谷时时间段",(String)valueTable.get(NonVehicleCharging.troughTime.name())));
            labelVOList.add(new LabelVO("平时电价",(String)valueTable.get(NonVehicleCharging.ordinaryElectricity.name())));
            labelVOList.add(new LabelVO("平时时间段",(String)valueTable.get(NonVehicleCharging.ordinaryTime.name())));
            labelVOList.add(new LabelVO("充电服务费",(String)valueTable.get(NonVehicleCharging.chargingCharge2.name())));
            labelVOList.add(new LabelVO("分享服务费（%）",(String)valueTable.get(NonVehicleCharging.sharingService2.name())));
            labelVOList.add(new LabelVO("协议顺延年限",(String)valueTable.get(NonVehicleCharging.informDeadline2.name())));
            labelVOList.add(new LabelVO("变更通知时限",(String)valueTable.get(NonVehicleCharging.changeNoticeLimit.name())));
            labelVOList.add(new LabelVO("变更确认时限",(String)valueTable.get(NonVehicleCharging.changeConfirmLimit.name())));
            labelVOList.add(new LabelVO("合同有效期",(String)valueTable.get(NonVehicleCharging.confirmDeadline2.name())));
            labelVOList.add(new LabelVO("质保期",(String)valueTable.get(NonVehicleCharging.warrantyPeriod.name())));
            labelVOList.add(new LabelVO("特别约定",(String)valueTable.get(NonVehicleCharging.specificAgreement2.name())));
            labelVOList.add(new LabelVO("合同份数",(String)valueTable.get(NonVehicleCharging.contractNumber.name())));
            labelVOList.add(new LabelVO("乙方执合同数",(String)valueTable.get(NonVehicleCharging.partyBConNum.name())));
            labelVOList.add(new LabelVO("甲方法定代表人",(String)valueTable.get(NonVehicleCharging.legalRepresentA2.name())));
            labelVOList.add(new LabelVO("甲方电话",(String)valueTable.get(NonVehicleCharging.telA2.name())));
            labelVOList.add(new LabelVO("电子邮箱",(String)valueTable.get(NonVehicleCharging.partyAEmail2.name())));
            labelVOList.add(new LabelVO("甲方开户行",(String)valueTable.get(NonVehicleCharging.openBankA2.name())));
            labelVOList.add(new LabelVO("甲方账号",(String)valueTable.get(NonVehicleCharging.bankAccountA2.name())));
            labelVOList.add(new LabelVO("甲方税务登记号",(String)valueTable.get(NonVehicleCharging.partyAein2.name())));
            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(NonVehicleCharging.addressA2.name())));
            labelVOList.add(new LabelVO("甲方联系人",(String)valueTable.get(NonVehicleCharging.contactsA2.name())));
            labelVOList.add(new LabelVO("法定代表人",(String)valueTable.get(NonVehicleCharging.legalRepresentB2.name())));
            labelVOList.add(new LabelVO("乙方电话",(String)valueTable.get(NonVehicleCharging.telB2.name())));
            labelVOList.add(new LabelVO("乙方电子邮箱",(String)valueTable.get(NonVehicleCharging.partyBEmail2.name())));
            labelVOList.add(new LabelVO("乙方开户银行",(String)valueTable.get(NonVehicleCharging.openBankB2.name())));
            labelVOList.add(new LabelVO("乙方账号",(String)valueTable.get(NonVehicleCharging.bankAccountB2.name())));
            labelVOList.add(new LabelVO("乙方税号",(String)valueTable.get(NonVehicleCharging.partyBein2.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(NonVehicleCharging.addressB2.name())));
            labelVOList.add(new LabelVO("乙方联系人",(String)valueTable.get(NonVehicleCharging.contactsB2.name())));


            //构造子表
            try {
                BizObject[] zbBizObjects = (BizObject[]) valueTable.get(NonVehicleCharging.nonVehicleChargingDetail.name());
                for (int i = 1; i <= zbBizObjects.length; i++) {
                    Map<String, Object> zbValueTable = zbBizObjects[i - 1].getValueTable();
                    String str = "";
                    if (i != 1) {
                        str = String.valueOf(i);
                    }
                    labelVOList.add(new LabelVO("产品名称" + str, String.valueOf(zbValueTable.get(NonVehicleCharging.productName.name()))));
                    labelVOList.add(new LabelVO("规格型号" + str, String.valueOf(zbValueTable.get(NonVehicleCharging.specificationType.name()))));
                    labelVOList.add(new LabelVO("数量" + str, String.valueOf(zbValueTable.get(NonVehicleCharging.theNumber.name()))));
                    labelVOList.add(new LabelVO("单位" + str, String.valueOf(zbValueTable.get(NonVehicleCharging.gun.name()))));
                }
            } catch (Exception e) {

            }

            List<PlaceHolder> placeHolders = new ArrayList<PlaceHolder>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,(String)valueTable.get(NonVehicleCharging.partyA2.name()),labelVOList));
            String templateId = "2156942625611975684";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2156943168749174786L,(String)valueTable.get(NonVehicleCharging.telB2.name()),
                    (String)valueTable.get(NonVehicleCharging.partyB2.name()),(String)valueTable.get(NonVehicleCharging.partyB2.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }

    /**
     * 代理商签约商户确认函
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO agentSigning(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(AgentSigning.contractNo.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(AgentSigning.partyA3.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(AgentSigning.partyB3.name())));
            labelVOList.add(new LabelVO("商户名称",(String)valueTable.get(AgentSigning.merchantName.name())));
            labelVOList.add(new LabelVO("合同份数",(String)valueTable.get(AgentSigning.contractNum.name())));
            labelVOList.add(new LabelVO("交流充电桩数量",(String)valueTable.get(AgentSigning.communicateNumber.name())));
            labelVOList.add(new LabelVO("直接充电桩数量",(String)valueTable.get(AgentSigning.quantityNumber.name())));
            labelVOList.add(new LabelVO("星充服务费分成比例",(String)valueTable.get(AgentSigning.starService.name())));
            labelVOList.add(new LabelVO("商户服务费分成比例",(String)valueTable.get(AgentSigning.merchantService.name())));
            labelVOList.add(new LabelVO("代理商服务费分成比例",(String)valueTable.get(AgentSigning.agencyService.name())));

            List<PlaceHolder> placeHolders = new ArrayList<PlaceHolder>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,(String)valueTable.get(AgentSigning.partyA3.name()),labelVOList));
            String templateId = "2157075351207939075";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2157075613544873990L,(String)valueTable.get(AgentSigning.phoneB.name()),
                    (String)valueTable.get(AgentSigning.partyB3.name()),(String)valueTable.get(AgentSigning.partyB3.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }

    /**
     * 补充协议
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO supplementaryAgreement(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(SupplementaryAgreement.contractNo.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(SupplementaryAgreement.partyA4.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(SupplementaryAgreement.partyB4.name())));
            labelVOList.add(new LabelVO("签订时间",DateTimeUtil.getDateToString((Date)valueTable.get(SupplementaryAgreement.signDate.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("原协议",(String)valueTable.get(SupplementaryAgreement.contractName.name())));
            labelVOList.add(new LabelVO("合同条款",(String)valueTable.get(SupplementaryAgreement.contractTerms.name())));
            labelVOList.add(new LabelVO("原合同条款内容",(String)valueTable.get(SupplementaryAgreement.originalContents.name())));
            labelVOList.add(new LabelVO("补充协议条款内容",(String)valueTable.get(SupplementaryAgreement.newContents.name())));
            labelVOList.add(new LabelVO("合同份数",(String)valueTable.get(SupplementaryAgreement.contractNumber.name())));
            labelVOList.add(new LabelVO("乙方执合同数",(String)valueTable.get(SupplementaryAgreement.partyBHolds.name())));
            labelVOList.add(new LabelVO("甲方授权代表人",(String)valueTable.get(SupplementaryAgreement.partyARepresentative.name())));
            labelVOList.add(new LabelVO("乙方授权代表人",(String)valueTable.get(SupplementaryAgreement.partyBRepresentative.name())));

            List<PlaceHolder> placeHolders = new ArrayList<PlaceHolder>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,(String)valueTable.get(SupplementaryAgreement.partyA4.name()),labelVOList));
            String templateId = "2157034443280220166";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2157034727654031361L,(String)valueTable.get(SupplementaryAgreement.phoneB.name()),
                    (String)valueTable.get(SupplementaryAgreement.partyB4.name()),(String)valueTable.get(SupplementaryAgreement.partyB4.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }


    /**
     * 新能源汽车充电设施委托运营合同(三方)
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO operatingContract(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(OperatingContract.contractNo.name())));
            labelVOList.add(new LabelVO("签订时间",DateTimeUtil.getDateToString((Date)valueTable.get(OperatingContract.signDate.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(OperatingContract.partyA5.name())));
            labelVOList.add(new LabelVO("甲方证件号",(String)valueTable.get(OperatingContract.certificateNoA5.name())));
            labelVOList.add(new LabelVO("甲方联系人",(String)valueTable.get(OperatingContract.contactsA5.name())));
            labelVOList.add(new LabelVO("甲方电话",(String)valueTable.get(OperatingContract.telA5.name())));
            labelVOList.add(new LabelVO("乙方联系人",(String)valueTable.get(OperatingContract.partyB5.name())));
            labelVOList.add(new LabelVO("乙方联系方式",(String)valueTable.get(OperatingContract.telB5.name())));

            labelVOList.add(new LabelVO("委托期限",(String)valueTable.get(OperatingContract.entrustPeriod5.name())));
            labelVOList.add(new LabelVO("委托开始时间",DateTimeUtil.getDateToString((Date)valueTable.get(OperatingContract.entrustStartDate.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("委托结束时间",DateTimeUtil.getDateToString((Date)valueTable.get(OperatingContract.entrustEndDate.name()),"yyyy-MM-dd")));


            labelVOList.add(new LabelVO("是/否",(String)valueTable.get(OperatingContract.openUse5.name())));
            labelVOList.add(new LabelVO("开放时间段",(String)valueTable.get(OperatingContract.usePeriod5.name())));
            labelVOList.add(new LabelVO("停车费",(String)valueTable.get(OperatingContract.parkingRate5.name())));
            labelVOList.add(new LabelVO("甲方账号",(String)valueTable.get(OperatingContract.bankAccountA5.name())));

            labelVOList.add(new LabelVO("A类收费标准",(String)valueTable.get(OperatingContract.standardA.name())));
            labelVOList.add(new LabelVO("B类收费标准",(String)valueTable.get(OperatingContract.standardB.name())));
            labelVOList.add(new LabelVO("A类桩数量",(String)valueTable.get(OperatingContract.stubQtyA5.name())));
            labelVOList.add(new LabelVO("B类桩数量",(String)valueTable.get(OperatingContract.stubQtyB5.name())));
            labelVOList.add(new LabelVO("C类桩数量",(String)valueTable.get(OperatingContract.stubQtyC5.name())));
            labelVOList.add(new LabelVO("一年合计价格",(String)valueTable.get(OperatingContract.stubPriceTotal5.name())));
            labelVOList.add(new LabelVO("托管费支付方式",(String)valueTable.get(OperatingContract.trustFeePayMethod5.name())));
            labelVOList.add(new LabelVO("运营付款期限",(String)valueTable.get(OperatingContract.operatePayPeriod5.name())));
            labelVOList.add(new LabelVO("定制服务费单价",(String)valueTable.get(OperatingContract.servicePrice5.name())));
            labelVOList.add(new LabelVO("客诉处理",(String)valueTable.get(OperatingContract.customerComplaint5.name())));
            labelVOList.add(new LabelVO("满意度调查",(String)valueTable.get(OperatingContract.customerSatisfate5.name())));
            labelVOList.add(new LabelVO("用户关怀",(String)valueTable.get(OperatingContract.customerUserCare5.name())));
            labelVOList.add(new LabelVO("录音存贮",(String)valueTable.get(OperatingContract.customerRecordStorage5.name())));
            labelVOList.add(new LabelVO("延保",(String)valueTable.get(OperatingContract.equipmentExtendInsurance5.name())));
            labelVOList.add(new LabelVO("设备检修",(String)valueTable.get(OperatingContract.equipmentRepair5.name())));
            labelVOList.add(new LabelVO("派单系统（手机端）",(String)valueTable.get(OperatingContract.equipmentDispatch5.name())));
            labelVOList.add(new LabelVO("最新协议升级",(String)valueTable.get(OperatingContract.platformProtocolUpgrade5.name())));
            labelVOList.add(new LabelVO("新功能升级",(String)valueTable.get(OperatingContract.platformNewFunction5.name())));
            labelVOList.add(new LabelVO("24小时响应",(String)valueTable.get(OperatingContract.platformResponse5.name())));
            labelVOList.add(new LabelVO("数据提取",(String)valueTable.get(OperatingContract.dataFetch5.name())));
            labelVOList.add(new LabelVO("定制报表",(String)valueTable.get(OperatingContract.dataReport5.name())));
            labelVOList.add(new LabelVO("数据存贮",(String)valueTable.get(OperatingContract.dataStore5.name())));
            labelVOList.add(new LabelVO("离线报告",(String)valueTable.get(OperatingContract.safetyOfflineReport5.name())));
            labelVOList.add(new LabelVO("故障代码报告",(String)valueTable.get(OperatingContract.safetyFaultReport5.name())));
            labelVOList.add(new LabelVO("充电桩运营方式",(String)valueTable.get(OperatingContract.stubOperationMode5.name())));
            labelVOList.add(new LabelVO("合同有效期",(String)valueTable.get(OperatingContract.validityDate.name())));
            labelVOList.add(new LabelVO("同意/不同意",(String)valueTable.get(OperatingContract.openSharing.name())));
            labelVOList.add(new LabelVO("甲方授权代表人",(String)valueTable.get(OperatingContract.authRepresentA5.name())));
            labelVOList.add(new LabelVO("乙方授权代表人",(String)valueTable.get(OperatingContract.authRepresentB5.name())));


            List<PlaceHolder> placeHolders = new ArrayList<PlaceHolder>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,(String)valueTable.get(OperatingContract.partyB.name()),labelVOList));
            String templateId = "2215811359646419973";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2215813081500483585L,(String)valueTable.get(OperatingContract.telA5.name()),(String)valueTable.get(OperatingContract.partyA5.name()),(String)valueTable.get(OperatingContract.partyA5.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }



    /**
     * 新能源电动汽车充电设施委托合作协议（没有丙方）
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO agreementNoPartyC(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(AgreementNoPartyC.contractNo.name())));
            labelVOList.add(new LabelVO("签订时间",(String)valueTable.get(AgreementNoPartyC.signTime.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(AgreementNoPartyC.partyA6.name())));
            labelVOList.add(new LabelVO("甲方证件号",(String)valueTable.get(AgreementNoPartyC.certificateNoA6.name())));
            labelVOList.add(new LabelVO("甲方联系人",(String)valueTable.get(AgreementNoPartyC.contactsA6.name())));
            labelVOList.add(new LabelVO("甲方联系电话",(String)valueTable.get(AgreementNoPartyC.telA6.name())));
            labelVOList.add(new LabelVO("充电桩位置",(String)valueTable.get(AgreementNoPartyC.stubAddress6.name())));
            labelVOList.add(new LabelVO("对外公开使用",(String)valueTable.get(AgreementNoPartyC.openUse6.name())));
            labelVOList.add(new LabelVO("使用时间段",(String)valueTable.get(AgreementNoPartyC.usePeriod6.name())));
            labelVOList.add(new LabelVO("是否有停车费",(String)valueTable.get(AgreementNoPartyC.needParkingRate6.name())));
            labelVOList.add(new LabelVO("停车费",(String)valueTable.get(AgreementNoPartyC.parkingRate6.name())));
            labelVOList.add(new LabelVO("甲方账号",(String)valueTable.get(AgreementNoPartyC.bankAccountA6.name())));
            labelVOList.add(new LabelVO("委托期限",(String)valueTable.get(AgreementNoPartyC.entrustPeriod6.name())));

            labelVOList.add(new LabelVO("委托开始时间",DateTimeUtil.getDateToString((Date)valueTable.get(AgreementNoPartyC.entrustStartDate.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("委托结束时间",DateTimeUtil.getDateToString((Date)valueTable.get(AgreementNoPartyC.entrustEndDate.name()),"yyyy-MM-dd")));

            labelVOList.add(new LabelVO("A类收费标准",(String)valueTable.get(AgreementNoPartyC.standardA.name())));
            labelVOList.add(new LabelVO("B类收费标准",(String)valueTable.get(AgreementNoPartyC.standardB.name())));
            labelVOList.add(new LabelVO("C类收费标准",(String)valueTable.get(AgreementNoPartyC.standardC.name())));

            labelVOList.add(new LabelVO("A类桩数量",(String)valueTable.get(AgreementNoPartyC.stubQtyA6.name())));
            labelVOList.add(new LabelVO("B类桩数量",(String)valueTable.get(AgreementNoPartyC.stubQtyB6.name())));
            labelVOList.add(new LabelVO("C类桩数量",(String)valueTable.get(AgreementNoPartyC.stubQtyC6.name())));
            labelVOList.add(new LabelVO("一年合计价格",(String)valueTable.get(AgreementNoPartyC.stubPriceTotal6.name())));
            labelVOList.add(new LabelVO("托管费支付方式",(String)valueTable.get(AgreementNoPartyC.trustFeePayMethod6.name())));
            labelVOList.add(new LabelVO("运营付款期限",(String)valueTable.get(AgreementNoPartyC.operatePayPeriod6.name())));
            labelVOList.add(new LabelVO("定制服务费单价",(String)valueTable.get(AgreementNoPartyC.servicePrice6.name())));
            labelVOList.add(new LabelVO("客诉处理",(String)valueTable.get(AgreementNoPartyC.customerComplaint6.name())));
            labelVOList.add(new LabelVO("满意度调查",(String)valueTable.get(AgreementNoPartyC.customerSatisfate6.name())));
            labelVOList.add(new LabelVO("用户关怀",(String)valueTable.get(AgreementNoPartyC.customerUserCare6.name())));
            labelVOList.add(new LabelVO("录音存贮",(String)valueTable.get(AgreementNoPartyC.customerRecordStorage6.name())));
            labelVOList.add(new LabelVO("延保",(String)valueTable.get(AgreementNoPartyC.equipmentExtendInsurance6.name())));
            labelVOList.add(new LabelVO("设备检修",(String)valueTable.get(AgreementNoPartyC.equipmentRepair6.name())));
            labelVOList.add(new LabelVO("派单系统（手机端）",(String)valueTable.get(AgreementNoPartyC.equipmentDispatch6.name())));
            labelVOList.add(new LabelVO("最新协议升级",(String)valueTable.get(AgreementNoPartyC.platformProtocolUpgrade6.name())));
            labelVOList.add(new LabelVO("新功能升级",(String)valueTable.get(AgreementNoPartyC.platformNewFunction6.name())));
            labelVOList.add(new LabelVO("24小时响应",(String)valueTable.get(AgreementNoPartyC.platformResponse6.name())));
            labelVOList.add(new LabelVO("数据提取",(String)valueTable.get(AgreementNoPartyC.dataFetch6.name())));
            labelVOList.add(new LabelVO("定制报表",(String)valueTable.get(AgreementNoPartyC.dataReport6.name())));
            labelVOList.add(new LabelVO("数据存贮",(String)valueTable.get(AgreementNoPartyC.dataStore6.name())));
            labelVOList.add(new LabelVO("离线报告",(String)valueTable.get(AgreementNoPartyC.safetyOfflineReport6.name())));
            labelVOList.add(new LabelVO("故障代码报告",(String)valueTable.get(AgreementNoPartyC.safetyFaultReport6.name())));
            labelVOList.add(new LabelVO("充电桩运营方式",(String)valueTable.get(AgreementNoPartyC.stubOperationMode6.name())));
            labelVOList.add(new LabelVO("合同有效期",(String)valueTable.get(AgreementNoPartyC.validityDate.name())));
            labelVOList.add(new LabelVO("同意/不同意",(String)valueTable.get(AgreementNoPartyC.openSharing.name())));
            labelVOList.add(new LabelVO("甲方授权代表人",(String)valueTable.get(AgreementNoPartyC.authRepresentA6.name())));
            labelVOList.add(new LabelVO("乙方授权代表人",(String)valueTable.get(AgreementNoPartyC.authRepresentB6.name())));
            labelVOList.add(new LabelVO("丙方授权代表",(String)valueTable.get(AgreementNoPartyC.authRepresentC6.name())));

            List<PlaceHolder> placeHolders = new ArrayList<>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,(String)valueTable.get(AgreementNoPartyC.OurUnitName.name()),labelVOList));
            String templateId = "2157077170369855497";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2157077371243462662L,(String)valueTable.get(AgreementNoPartyC.telA6.name()),
                    (String)valueTable.get(AgreementNoPartyC.partyA6.name()),(String)valueTable.get(AgreementNoPartyC.partyA6.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }


    /**
     * 星星充电充电桩群/站投资业务代理合作协议
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO cooperationAgreement(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(CooperationAgreement.contractNo.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(CooperationAgreement.partyA7.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(CooperationAgreement.partyB7.name())));
            labelVOList.add(new LabelVO("签署地址",(String)valueTable.get(CooperationAgreement.signAddress7.name())));
            labelVOList.add(new LabelVO("地域范围",(String)valueTable.get(CooperationAgreement.cooperateAreaScope.name())));
            labelVOList.add(new LabelVO("保证金（大写）",(String)valueTable.get(CooperationAgreement.bond.name())));
            labelVOList.add(new LabelVO("保证金",(String)valueTable.get(CooperationAgreement.override.name())));

            labelVOList.add(new LabelVO("其他",(String)valueTable.get(CooperationAgreement.other.name())));

            labelVOList.add(new LabelVO("生效日期",DateTimeUtil.getDateToString((Date)valueTable.get(CooperationAgreement.effectiveDate1.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("签订时间",DateTimeUtil.getDateToString((Date)valueTable.get(CooperationAgreement.signingTime1.name()),"yyyy-MM-dd")));

            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(CooperationAgreement.addressA7.name())));
            labelVOList.add(new LabelVO("甲方授权代表人",(String)valueTable.get(CooperationAgreement.authRepresentA7.name())));
            labelVOList.add(new LabelVO("甲方开户银行",(String)valueTable.get(CooperationAgreement.openBankA7.name())));
            labelVOList.add(new LabelVO("甲方账号",(String)valueTable.get(CooperationAgreement.bankAccountA7.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(CooperationAgreement.addressB7.name())));
            labelVOList.add(new LabelVO("乙方授权代表",(String)valueTable.get(CooperationAgreement.authRepresentB7.name())));
            labelVOList.add(new LabelVO("乙方开户银行",(String)valueTable.get(CooperationAgreement.openBankB7.name())));
            labelVOList.add(new LabelVO("乙方账号",(String)valueTable.get(CooperationAgreement.bankAccountB7.name())));

            //构造子表
            try {
                BizObject[] zbBizObjects = (BizObject[]) valueTable.get(CooperationAgreement.sublist.name());
                for (int i = 1; i <= zbBizObjects.length; i++) {
                    Map<String, Object> zbValueTable = zbBizObjects[i - 1].getValueTable();
                    String str = "";
                    if (i != 1) {
                        str = String.valueOf(i);
                    }
                    labelVOList.add(new LabelVO("贡献值" + str, String.valueOf(zbValueTable.get(CooperationAgreement.contribution.name()))));
                    labelVOList.add(new LabelVO("单价" + str, String.valueOf(zbValueTable.get(CooperationAgreement.unitPrice.name()))));
                }
            } catch (Exception e) {

            }

            List<PlaceHolder> placeHolders = new ArrayList<>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,(String)valueTable.get(CooperationAgreement.partyA7.name()),labelVOList));
            String templateId = "2157046410250289154";
            List<Role> roles = new ArrayList<>();

            roles.add(new Role(2157047802994101251L,(String)valueTable.get(CooperationAgreement.phoneB.name()),
                    (String)valueTable.get(CooperationAgreement.partyB7.name()),(String)valueTable.get(CooperationAgreement.partyB7.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }


    /**
     * 合作协议
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO cooperationAgree(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(CooperationAgree.contractNo.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(CooperationAgree.OurUnitName.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(CooperationAgree.partyB8.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(CooperationAgree.addressB8.name())));

            labelVOList.add(new LabelVO("签订时间",DateTimeUtil.getDateToString((Date)valueTable.get(CooperationAgree.signingTime1.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("软件名称",(String)valueTable.get(CooperationAgree.appNameB.name())));
            labelVOList.add(new LabelVO("桩群范围",(String)valueTable.get(CooperationAgree.stubGroupScope.name())));
            labelVOList.add(new LabelVO("金额",(String)valueTable.get(CooperationAgree.manageFee.name())));
            labelVOList.add(new LabelVO("结算方式",(String)valueTable.get(CooperationAgree.accountMethod8.name())));
            labelVOList.add(new LabelVO("预付费",(String)valueTable.get(CooperationAgree.advancePay.name())));
            labelVOList.add(new LabelVO("大写",(String)valueTable.get(CooperationAgree.upAccount.name())));
            labelVOList.add(new LabelVO("其他结算方式",(String)valueTable.get(CooperationAgree.otherAccount.name())));
            labelVOList.add(new LabelVO("合同有效期",(String)valueTable.get(CooperationAgree.contractValidPeriod.name())));

            labelVOList.add(new LabelVO("合同开始时间",DateTimeUtil.getDateToString((Date)valueTable.get(CooperationAgree.contractStartDate1.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("合同结束时间",DateTimeUtil.getDateToString((Date)valueTable.get(CooperationAgree.contractEndDate1.name()),"yyyy-MM-dd")));

            labelVOList.add(new LabelVO("其他",(String)valueTable.get(CooperationAgree.otherAppoint.name())));
            labelVOList.add(new LabelVO("甲方授权代表人",(String)valueTable.get(CooperationAgree.authRepresentA8.name())));
            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(CooperationAgree.addressA8.name())));
            labelVOList.add(new LabelVO("甲方联系电话",(String)valueTable.get(CooperationAgree.telA8.name())));
            labelVOList.add(new LabelVO("乙方授权代表人",(String)valueTable.get(CooperationAgree.authRepresentB8.name())));
            labelVOList.add(new LabelVO("乙方联系方式",(String)valueTable.get(CooperationAgree.telB8.name())));

            List<PlaceHolder> placeHolders = new ArrayList<PlaceHolder>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,(String)valueTable.get(CooperationAgree.OurUnitName.name()),labelVOList));
            String templateId = "2157034905005981702";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2157035241028455425L,(String)valueTable.get(CooperationAgree.telB8.name()),
                    (String)valueTable.get(CooperationAgree.partyB8.name()),(String)valueTable.get(CooperationAgree.partyB8.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }


    /**
     * 合作协议
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO energyVehicles(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(EnergyVehicles.contractNo.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(EnergyVehicles.partyA9.name())));
            labelVOList.add(new LabelVO("统一信用代码",(String)valueTable.get(EnergyVehicles.partyCreditCode.name())));
            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(EnergyVehicles.partyADomicile9.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(EnergyVehicles.partyB9.name())));
            labelVOList.add(new LabelVO("证件号",(String)valueTable.get(EnergyVehicles.partyBCertificate.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(EnergyVehicles.partyBDomicile9.name())));
            labelVOList.add(new LabelVO("所有权人",(String)valueTable.get(EnergyVehicles.siteOwner.name())));
            labelVOList.add(new LabelVO("场地地址",(String)valueTable.get(EnergyVehicles.location.name())));
            labelVOList.add(new LabelVO("土地证号",(String)valueTable.get(EnergyVehicles.landCard.name())));
            labelVOList.add(new LabelVO("房屋证号",(String)valueTable.get(EnergyVehicles.houseNumber.name())));
            labelVOList.add(new LabelVO("房屋层高",(String)valueTable.get(EnergyVehicles.houseFloor.name())));
            labelVOList.add(new LabelVO("面积",(String)valueTable.get(EnergyVehicles.totalArea.name())));
            labelVOList.add(new LabelVO("是/否",(String)valueTable.get(EnergyVehicles.isLimit.name())));

            labelVOList.add(new LabelVO("设备投资额",(String)valueTable.get(EnergyVehicles.equipmentInvestment.name())));
            labelVOList.add(new LabelVO("辅材投资额",(String)valueTable.get(EnergyVehicles.auxiliaryInvestment.name())));
            labelVOList.add(new LabelVO("安装费",(String)valueTable.get(EnergyVehicles.installation.name())));
            labelVOList.add(new LabelVO("广告费",(String)valueTable.get(EnergyVehicles.advertising.name())));
            labelVOList.add(new LabelVO("总投资",(String)valueTable.get(EnergyVehicles.totalInvestment.name())));
            labelVOList.add(new LabelVO("开放时间段",(String)valueTable.get(EnergyVehicles.openingPeriod9.name())));
            labelVOList.add(new LabelVO("基础电价",(String)valueTable.get(EnergyVehicles.basedElectricity.name())));
            labelVOList.add(new LabelVO("充电服务费",(String)valueTable.get(EnergyVehicles.chargingCharge9.name())));
            labelVOList.add(new LabelVO("选填",(String)valueTable.get(EnergyVehicles.transferWay.name())));
            labelVOList.add(new LabelVO("合作期限",(String)valueTable.get(EnergyVehicles.signDeadline.name())));
            labelVOList.add(new LabelVO("合同开始时间",DateTimeUtil.getDateToString((Date)valueTable.get(EnergyVehicles.contractStartTime.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("合同结束时间",DateTimeUtil.getDateToString((Date)valueTable.get(EnergyVehicles.contractEndTime.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("利润",(String)valueTable.get(EnergyVehicles.profit.name())));
            labelVOList.add(new LabelVO("生效日期",(String)valueTable.get(EnergyVehicles.effectiveDate1.name())));
            labelVOList.add(new LabelVO("甲方法定代表人",(String)valueTable.get(EnergyVehicles.legalRepresentA9.name())));
            labelVOList.add(new LabelVO("甲方电话",(String)valueTable.get(EnergyVehicles.telA9.name())));
            labelVOList.add(new LabelVO("电子邮箱",(String)valueTable.get(EnergyVehicles.partyAEmail9.name())));
            labelVOList.add(new LabelVO("甲方开户银行",(String)valueTable.get(EnergyVehicles.openBankA9.name())));
            labelVOList.add(new LabelVO("甲方账号",(String)valueTable.get(EnergyVehicles.bankAccountA9.name())));
            labelVOList.add(new LabelVO("甲方税号",(String)valueTable.get(EnergyVehicles.partyAein9.name())));
            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(EnergyVehicles.addressA9.name())));
            labelVOList.add(new LabelVO("乙方法定代表人",(String)valueTable.get(EnergyVehicles.legalRepresentB9.name())));
            labelVOList.add(new LabelVO("乙方电话",(String)valueTable.get(EnergyVehicles.telB9.name())));
            labelVOList.add(new LabelVO("乙方电子邮箱",(String)valueTable.get(EnergyVehicles.partyBEmail9.name())));
            labelVOList.add(new LabelVO("乙方开户银行",(String)valueTable.get(EnergyVehicles.openBankB9.name())));
            labelVOList.add(new LabelVO("乙方账号",(String)valueTable.get(EnergyVehicles.bankAccountB9.name())));
            labelVOList.add(new LabelVO("乙方税号",(String)valueTable.get(EnergyVehicles.partyBein9.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(EnergyVehicles.addressB9.name())));
            labelVOList.add(new LabelVO("对接人联系方式",(String)valueTable.get(EnergyVehicles.contactInformation.name())));

            //构造子表
            try {
                BizObject[] zbBizObjects = (BizObject[]) valueTable.get(EnergyVehicles.energyVehiclesDetail.name());
                for (int i = 1; i <= zbBizObjects.length; i++) {
                    Map<String, Object> zbValueTable = zbBizObjects[i - 1].getValueTable();
                    String str = "";
                    if (i != 1) {
                        str = String.valueOf(i);
                    }
                    labelVOList.add(new LabelVO("产品名称" + str, String.valueOf(zbValueTable.get(EnergyVehicles.productName.name()))));
                    labelVOList.add(new LabelVO("规格型号" + str, String.valueOf(zbValueTable.get(EnergyVehicles.specificationType.name()))));
                    labelVOList.add(new LabelVO("数量" + str, String.valueOf(zbValueTable.get(EnergyVehicles.theNumber.name()))));
                    labelVOList.add(new LabelVO("单位" + str, String.valueOf(zbValueTable.get(EnergyVehicles.gun.name()))));
                }
            } catch (Exception e) {

            }

            List<PlaceHolder> placeHolders = new ArrayList<>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,(String)valueTable.get(EnergyVehicles.partyA9.name()),labelVOList));
            String templateId = "2157081475873246216";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2157081776512568323L,(String)valueTable.get(EnergyVehicles.telB9.name()),
                    (String)valueTable.get(EnergyVehicles.partyB9.name()),(String)valueTable.get(EnergyVehicles.partyB9.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }

    /**
     * 供货合同
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO supplyContract(InstanceData instanceData) {
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            //构造主表数据
            labelVOList.add(new LabelVO("合同编号",String.valueOf(valueTable.get(SupplyContract.contractNo.name()))));
            labelVOList.add(new LabelVO("甲方",String.valueOf(valueTable.get(SupplyContract.partyA10.name()))));
            labelVOList.add(new LabelVO("乙方",String.valueOf(valueTable.get(SupplyContract.partyB10.name()))));
            labelVOList.add(new LabelVO("合同总价",String.valueOf(valueTable.get(SupplyContract.productSumUpper.name()))));
            labelVOList.add(new LabelVO("合计总价（大写）",String.valueOf(valueTable.get(SupplyContract.totalAmountUpper.name()))));
            labelVOList.add(new LabelVO("运营费",String.valueOf(valueTable.get(SupplyContract.operatingExpenses.name()))));
            labelVOList.add(new LabelVO("运营年限",String.valueOf(valueTable.get(SupplyContract.operationYears.name()))));
            labelVOList.add(new LabelVO("其他费用",String.valueOf(valueTable.get(SupplyContract.otherExpenses.name()))));


            labelVOList.add(new LabelVO("质保期",String.valueOf(valueTable.get(SupplyContract.warrantyPeriod.name()))));
            labelVOList.add(new LabelVO("交货期",String.valueOf(valueTable.get(SupplyContract.deliveryDate.name()))));
            labelVOList.add(new LabelVO("交货地点",String.valueOf(valueTable.get(SupplyContract.deliveryPlace.name()))));
            labelVOList.add(new LabelVO("交付期限",String.valueOf(valueTable.get(SupplyContract.acceptancePeriod.name()))));
            labelVOList.add(new LabelVO("设备总价款",String.valueOf(valueTable.get(SupplyContract.equipmentPrice.name()))));
            labelVOList.add(new LabelVO("设备预付款",String.valueOf(valueTable.get(SupplyContract.downPayment.name()))));
            labelVOList.add(new LabelVO("托管费支付方式",String.valueOf(valueTable.get(SupplyContract.methodPayment.name()))));
            labelVOList.add(new LabelVO("付款周期",String.valueOf(valueTable.get(SupplyContract.paymentPeriod.name()))));
            labelVOList.add(new LabelVO("月支付",String.valueOf(valueTable.get(SupplyContract.monthlyPayment.name()))));
            labelVOList.add(new LabelVO("其他",String.valueOf(valueTable.get(SupplyContract.other.name()))));

            labelVOList.add(new LabelVO("电子邮箱",String.valueOf(valueTable.get(SupplyContract.emailA.name()))));
            labelVOList.add(new LabelVO("乙方电子邮箱",String.valueOf(valueTable.get(SupplyContract.emailB.name()))));

            labelVOList.add(new LabelVO("交流终端运营费",String.valueOf(valueTable.get(SupplyContract.operatingCost.name()))));
            labelVOList.add(new LabelVO("直流终端运营费",String.valueOf(valueTable.get(SupplyContract.operationFee.name()))));
            labelVOList.add(new LabelVO("一年合计价格",String.valueOf(valueTable.get(SupplyContract.totalCost.name()))));

            labelVOList.add(new LabelVO("委托开始时间",DateTimeUtil.getDateToString((Date)valueTable.get(SupplyContract.delegateStartDate1.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("委托结束时间",DateTimeUtil.getDateToString((Date)valueTable.get(SupplyContract.delegateEndDate1.name()),"yyyy-MM-dd")));

            labelVOList.add(new LabelVO("年限",String.valueOf(valueTable.get(SupplyContract.fixedYear.name()))));
            labelVOList.add(new LabelVO("甲方地址",String.valueOf(valueTable.get(SupplyContract.addressA10.name()))));
            labelVOList.add(new LabelVO("甲方法定代表人",String.valueOf(valueTable.get(SupplyContract.legalRepresentA10.name()))));
            labelVOList.add(new LabelVO("甲方法定代表人职务",String.valueOf(valueTable.get(SupplyContract.legalRepresentPostA10.name()))));
            labelVOList.add(new LabelVO("甲方授权代表人",String.valueOf(valueTable.get(SupplyContract.authRepresentA10.name()))));
            labelVOList.add(new LabelVO("甲方授权代表人职务",String.valueOf(valueTable.get(SupplyContract.authRepresentPostA10.name()))));
            labelVOList.add(new LabelVO("甲方电话",String.valueOf(valueTable.get(SupplyContract.telA10.name()))));
            labelVOList.add(new LabelVO("甲方开户行",String.valueOf(valueTable.get(SupplyContract.openBankA10.name()))));
            labelVOList.add(new LabelVO("甲方税务登记号",String.valueOf(valueTable.get(SupplyContract.taxRegistNoA.name()))));
            labelVOList.add(new LabelVO("甲方账号",String.valueOf(valueTable.get(SupplyContract.bankAccountA10.name()))));
            labelVOList.add(new LabelVO("乙方授权代表人",String.valueOf(valueTable.get(SupplyContract.authRepresentB10.name()))));
            labelVOList.add(new LabelVO("乙方授权代表人职务",String.valueOf(valueTable.get(SupplyContract.authRepresentPostB10.name()))));
            labelVOList.add(new LabelVO("客户选型",String.valueOf(valueTable.get(SupplyContract.customerChoose.name()))));
            labelVOList.add(new LabelVO("单价",String.valueOf(valueTable.get(SupplyContract.price.name()))));
            labelVOList.add(new LabelVO("客诉处理",String.valueOf(valueTable.get(SupplyContract.customerComplaint10.name()))));
            labelVOList.add(new LabelVO("满意度调查",String.valueOf(valueTable.get(SupplyContract.customerSatisfate10.name()))));
            labelVOList.add(new LabelVO("用户关怀",String.valueOf(valueTable.get(SupplyContract.customerUserCare10.name()))));
            labelVOList.add(new LabelVO("录音存贮",String.valueOf(valueTable.get(SupplyContract.customerRecordStorage10.name()))));
            labelVOList.add(new LabelVO("延保",String.valueOf(valueTable.get(SupplyContract.equipmentExtendInsurance10.name()))));
            labelVOList.add(new LabelVO("设备检修",String.valueOf(valueTable.get(SupplyContract.equipmentRepair10.name()))));
            labelVOList.add(new LabelVO("派单系统（手机端）",String.valueOf(valueTable.get(SupplyContract.equipmentDispatch10.name()))));
            labelVOList.add(new LabelVO("最新协议升级",String.valueOf(valueTable.get(SupplyContract.platformProtocolUpgrade10.name()))));
            labelVOList.add(new LabelVO("新功能升级 ",String.valueOf(valueTable.get(SupplyContract.platformNewFunction10.name()))));
            labelVOList.add(new LabelVO("24小时响应",String.valueOf(valueTable.get(SupplyContract.platformResponse10.name()))));
            labelVOList.add(new LabelVO("数据提取",String.valueOf(valueTable.get(SupplyContract.dataFetch10.name()))));
            labelVOList.add(new LabelVO("定制报表",String.valueOf(valueTable.get(SupplyContract.dataReport10.name()))));
            labelVOList.add(new LabelVO("数据存贮",String.valueOf(valueTable.get(SupplyContract.dataStore10.name()))));
            labelVOList.add(new LabelVO("离线报告",String.valueOf(valueTable.get(SupplyContract.safetyOfflineReport10.name()))));
            labelVOList.add(new LabelVO("故障代码报告",String.valueOf(valueTable.get(SupplyContract.safetyFaultReport10.name()))));

            //构造子表
            try {
                BizObject[] zbBizObjects = (BizObject[]) valueTable.get(SupplyContract.equipmentInventory.name());
                for (int i = 1; i <= zbBizObjects.length; i++) {
                    Map<String, Object> zbValueTable = zbBizObjects[i - 1].getValueTable();
                    String str = "";
                    if (i != 1) {
                        str = String.valueOf(i);
                    }
                    labelVOList.add(new LabelVO("产品名称" + str, String.valueOf(zbValueTable.get(SupplyContract.productName.name()))));
                    labelVOList.add(new LabelVO("规格型号" + str, String.valueOf(zbValueTable.get(SupplyContract.productType.name()))));
                    labelVOList.add(new LabelVO("数量" + str, String.valueOf(zbValueTable.get(SupplyContract.productCount.name()))));
                    labelVOList.add(new LabelVO("单价" + str, String.valueOf(zbValueTable.get(SupplyContract.productPrice.name()))));
                    labelVOList.add(new LabelVO("总价" + str, String.valueOf(zbValueTable.get(SupplyContract.productTotal.name()))));
                    labelVOList.add(new LabelVO("备注" + str, String.valueOf(zbValueTable.get(SupplyContract.productMemo.name()))));
                }
            } catch (Exception e) {

            }
            List<PlaceHolder> placeHolders = new ArrayList<PlaceHolder>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,String.valueOf(valueTable.get(SupplyContract.partyB10.name())),labelVOList));
            String templateId = "2157051686844107776";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2157051940825992193L,String.valueOf(valueTable.get(SupplyContract.telA10.name())),
                    String.valueOf(valueTable.get(SupplyContract.partyA10.name())),String.valueOf(valueTable.get(SupplyContract.partyA10.name()))));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }

    /**
     * 新能源汽车充电设施推广合作协议（增容）
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO chargingFacilities(InstanceData instanceData){
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(ChargingFacilities.contractNo.name())));
            labelVOList.add(new LabelVO("签订地",(String)valueTable.get(ChargingFacilities.signAddress11.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(ChargingFacilities.partyA11.name())));
            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(ChargingFacilities.partyADomicile11.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(ChargingFacilities.partyB11.name())));
            labelVOList.add(new LabelVO("桩群名称",(String)valueTable.get(ChargingFacilities.partyBGroupName11.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(ChargingFacilities.partyBDomicile11.name())));
            labelVOList.add(new LabelVO("充电桩详细位置",(String)valueTable.get(ChargingFacilities.chargingPosition11.name())));
            labelVOList.add(new LabelVO("收费类型",(String)valueTable.get(ChargingFacilities.chargesType11.name())));
            labelVOList.add(new LabelVO("收费标准",(String)valueTable.get(ChargingFacilities.chargingStandard11.name())));
            labelVOList.add(new LabelVO("开放时间段",(String)valueTable.get(ChargingFacilities.openingPeriod11.name())));
            labelVOList.add(new LabelVO("报装容量",(String)valueTable.get(ChargingFacilities.capacityPower.name())));
            labelVOList.add(new LabelVO("增容方式",(String)valueTable.get(ChargingFacilities.expansionWay.name())));
            labelVOList.add(new LabelVO("结算方式",(String)valueTable.get(ChargingFacilities.payment11.name())));
            labelVOList.add(new LabelVO("充电服务费",(String)valueTable.get(ChargingFacilities.chargingCharge11.name())));
            labelVOList.add(new LabelVO("分享服务费（%）",(String)valueTable.get(ChargingFacilities.sharingService11.name())));
            labelVOList.add(new LabelVO("协议顺延年限",(String)valueTable.get(ChargingFacilities.agreementExtend.name())));
            labelVOList.add(new LabelVO("充电设施确认期限",(String)valueTable.get(ChargingFacilities.confirmDeadline11.name())));
            labelVOList.add(new LabelVO("合同有效期",(String)valueTable.get(ChargingFacilities.thereTime.name())));
            labelVOList.add(new LabelVO("其他",(String)valueTable.get(ChargingFacilities.specificAgreement11.name())));
            labelVOList.add(new LabelVO("合同份数",(String)valueTable.get(ChargingFacilities.contractNumber.name())));
            labelVOList.add(new LabelVO("乙方执合同数",(String)valueTable.get(ChargingFacilities.contractNumberB.name())));
            labelVOList.add(new LabelVO("甲方法定代表人",(String)valueTable.get(ChargingFacilities.legalRepresentA11.name())));
            labelVOList.add(new LabelVO("甲方电话",(String)valueTable.get(ChargingFacilities.telA11.name())));
            labelVOList.add(new LabelVO("电子邮箱",(String)valueTable.get(ChargingFacilities.partyAEmail11.name())));
            labelVOList.add(new LabelVO("甲方开户银行",(String)valueTable.get(ChargingFacilities.openBankA11.name())));
            labelVOList.add(new LabelVO("甲方账号",(String)valueTable.get(ChargingFacilities.bankAccountA11.name())));
            labelVOList.add(new LabelVO("甲方税号",(String)valueTable.get(ChargingFacilities.partyAein11.name())));
            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(ChargingFacilities.addressA11.name())));
            labelVOList.add(new LabelVO("甲方联系人",(String)valueTable.get(ChargingFacilities.contactsA11.name())));
            labelVOList.add(new LabelVO("乙方法定代表人",(String)valueTable.get(ChargingFacilities.legalRepresentB11.name())));
            labelVOList.add(new LabelVO("乙方电话",(String)valueTable.get(ChargingFacilities.telB11.name())));
            labelVOList.add(new LabelVO("乙方电子邮箱",(String)valueTable.get(ChargingFacilities.partyBEmail11.name())));
            labelVOList.add(new LabelVO("乙方开户银行",(String)valueTable.get(ChargingFacilities.openBankB11.name())));
            labelVOList.add(new LabelVO("乙方账号",(String)valueTable.get(ChargingFacilities.bankAccountB11.name())));
            labelVOList.add(new LabelVO("乙方税号",(String)valueTable.get(ChargingFacilities.partyBein11.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(ChargingFacilities.addressB11.name())));
            labelVOList.add(new LabelVO("乙方联系人",(String)valueTable.get(ChargingFacilities.contactsB11.name())));
            labelVOList.add(new LabelVO("证明人",(String)valueTable.get(ChargingFacilities.certifier.name())));
            labelVOList.add(new LabelVO("三证合一",(String)valueTable.get(ChargingFacilities.threeCertificates.name())));
            labelVOList.add(new LabelVO("房产证",(String)valueTable.get(ChargingFacilities.housePropertyCard.name())));
            labelVOList.add(new LabelVO("土地证",(String)valueTable.get(ChargingFacilities.certificate.name())));
            labelVOList.add(new LabelVO("租赁协议",(String)valueTable.get(ChargingFacilities.leaseAgreement.name())));
            labelVOList.add(new LabelVO("往期电费发票",(String)valueTable.get(ChargingFacilities.invoiceElectricityCharges.name())));
            labelVOList.add(new LabelVO("同意建桩证明",(String)valueTable.get(ChargingFacilities.certificateConstruction.name())));
            labelVOList.add(new LabelVO("报装清单",(String)valueTable.get(ChargingFacilities.newListing.name())));
            labelVOList.add(new LabelVO("备注",(String)valueTable.get(ChargingFacilities.remake1.name())));
            labelVOList.add(new LabelVO("备注2",(String)valueTable.get(ChargingFacilities.remake2.name())));
            labelVOList.add(new LabelVO("备注3",(String)valueTable.get(ChargingFacilities.remake3.name())));
            labelVOList.add(new LabelVO("备注4",(String)valueTable.get(ChargingFacilities.remake4.name())));
            labelVOList.add(new LabelVO("备注5",(String)valueTable.get(ChargingFacilities.remake5.name())));
            //构造子表
            try {
                BizObject[] zbBizObjects = (BizObject[]) valueTable.get(ChargingFacilities.chargingFacilitiesDateil.name());
                for (int i = 1; i <= zbBizObjects.length; i++) {
                    Map<String, Object> zbValueTable = zbBizObjects[i - 1].getValueTable();
                    String str = "";
                    if (i != 1) {
                        str = String.valueOf(i);
                    }
                    labelVOList.add(new LabelVO("产品名称" + str, String.valueOf(zbValueTable.get(ChargingFacilities.productName.name()))));
                    labelVOList.add(new LabelVO("规格型号" + str, String.valueOf(zbValueTable.get(ChargingFacilities.specificationType.name()))));
                    labelVOList.add(new LabelVO("数量" + str, String.valueOf(zbValueTable.get(ChargingFacilities.theNumber.name()))));
                    labelVOList.add(new LabelVO("单位" + str, String.valueOf(zbValueTable.get(ChargingFacilities.gun.name()))));
                }
            } catch (Exception e) {
            }
            List<PlaceHolder> placeHolders = new ArrayList<>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,(String)valueTable.get(ChargingFacilities.partyA11.name()),labelVOList));
            String templateId = "2157031550804298757";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2157031974714212360L,(String)valueTable.get(ChargingFacilities.telB11.name()),
                    (String)valueTable.get(ChargingFacilities.partyB11.name()),(String)valueTable.get(ChargingFacilities.partyB11.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
        }
        return sendContractsSyncVO;
    }

    /**
     *  场地租赁合同
     * @param instanceData
     * @return
     */
    public static SendContractsSyncVO siteLeaseContract(InstanceData instanceData){
        SendContractsSyncVO sendContractsSyncVO = null;
        try {
            Map<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            List<LabelVO> labelVOList = new ArrayList<>();
            labelVOList.add(new LabelVO("合同编号",(String)valueTable.get(SiteLeaseContract.contractNo.name())));
            labelVOList.add(new LabelVO("甲方",(String)valueTable.get(SiteLeaseContract.partyA12.name())));
            labelVOList.add(new LabelVO("甲方法定代表人",(String)valueTable.get(SiteLeaseContract.legalRepresentA12.name())));
            labelVOList.add(new LabelVO("甲方地址",(String)valueTable.get(SiteLeaseContract.addressA12.name())));
            labelVOList.add(new LabelVO("甲方联系方式",(String)valueTable.get(SiteLeaseContract.telA12.name())));
            labelVOList.add(new LabelVO("乙方",(String)valueTable.get(SiteLeaseContract.partyB12.name())));
            labelVOList.add(new LabelVO("乙方法定代表人",(String)valueTable.get(SiteLeaseContract.legalRepresentB12.name())));
            labelVOList.add(new LabelVO("乙方地址",(String)valueTable.get(SiteLeaseContract.addressB12.name())));
            labelVOList.add(new LabelVO("乙方联系方式",(String)valueTable.get(SiteLeaseContract.telB12.name())));
            labelVOList.add(new LabelVO("所有权",(String)valueTable.get(SiteLeaseContract.ownership12.name())));
            labelVOList.add(new LabelVO("处分权",(String)valueTable.get(SiteLeaseContract.rightOfDisposition12.name())));
            labelVOList.add(new LabelVO("使用权",(String)valueTable.get(SiteLeaseContract.usufruct12.name())));
            labelVOList.add(new LabelVO("收益权",(String)valueTable.get(SiteLeaseContract.incomeRight12.name())));
            labelVOList.add(new LabelVO("转租权",(String)valueTable.get(SiteLeaseContract.leasehold12.name())));
            labelVOList.add(new LabelVO("选填",(String)valueTable.get(SiteLeaseContract.leaseType.name())));
            labelVOList.add(new LabelVO("所有权人",(String)valueTable.get(SiteLeaseContract.landOwners12.name())));
            labelVOList.add(new LabelVO("土地证号",(String)valueTable.get(SiteLeaseContract.landNo12.name())));
            labelVOList.add(new LabelVO("房屋证号",(String)valueTable.get(SiteLeaseContract.houseNo12.name())));
            labelVOList.add(new LabelVO("坐落地址",(String)valueTable.get(SiteLeaseContract.address12.name())));
            labelVOList.add(new LabelVO("房屋层高",(String)valueTable.get(SiteLeaseContract.layer12.name())));
            labelVOList.add(new LabelVO("面积",(String)valueTable.get(SiteLeaseContract.totalArea12.name())));
            labelVOList.add(new LabelVO("租用面积",(String)valueTable.get(SiteLeaseContract.rentalArea12.name())));
            labelVOList.add(new LabelVO("存在权益的限制",(String)valueTable.get(SiteLeaseContract.isLimit12.name())));
            labelVOList.add(new LabelVO("车位所有权人",(String)valueTable.get(SiteLeaseContract.parkingLot12.name())));
            labelVOList.add(new LabelVO("车位土地证号",(String)valueTable.get(SiteLeaseContract.parkingLotNo12.name())));
            labelVOList.add(new LabelVO("数量",(String)valueTable.get(SiteLeaseContract.parkingLotpNum12.name())));
            labelVOList.add(new LabelVO("车位落座地址",(String)valueTable.get(SiteLeaseContract.parkingLotAdd12.name())));
            labelVOList.add(new LabelVO("车位面积",(String)valueTable.get(SiteLeaseContract.parkingLotTotal12.name())));
            labelVOList.add(new LabelVO("配电容量",(String)valueTable.get(SiteLeaseContract.DistCapacity12.name())));
            labelVOList.add(new LabelVO("合同开始时间",DateTimeUtil.getDateToString((Date)valueTable.get(SiteLeaseContract.contractStartTime.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("合同结束时间",DateTimeUtil.getDateToString((Date)valueTable.get(SiteLeaseContract.contractEndTime.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("合同有效期",(String)valueTable.get(SiteLeaseContract.contractTerm12.name())));
            labelVOList.add(new LabelVO("时限规定",(String)valueTable.get(SiteLeaseContract.timesRegulations12.name())));
            labelVOList.add(new LabelVO("协议顺延年限",(String)valueTable.get(SiteLeaseContract.postponeYears12.name())));
            labelVOList.add(new LabelVO("租金",(String)valueTable.get(SiteLeaseContract.rent12.name())));
            labelVOList.add(new LabelVO("大写",(String)valueTable.get(SiteLeaseContract.rentsD12.name())));
            labelVOList.add(new LabelVO("水费单价",(String)valueTable.get(SiteLeaseContract.waterRate12.name())));
            labelVOList.add(new LabelVO("电费单价",(String)valueTable.get(SiteLeaseContract.powerRate12.name())));
            labelVOList.add(new LabelVO("物业费单价",(String)valueTable.get(SiteLeaseContract.property12.name())));
            labelVOList.add(new LabelVO("其他",(String)valueTable.get(SiteLeaseContract.other12.name())));
            labelVOList.add(new LabelVO("桩群名称",(String)valueTable.get(SiteLeaseContract.pileGroup12.name())));
            labelVOList.add(new LabelVO("桩群上线日期",DateTimeUtil.getDateToString((Date)valueTable.get(SiteLeaseContract.onlineDate.name()),"yyyy-MM-dd")));
            labelVOList.add(new LabelVO("合同份数",(String)valueTable.get(SiteLeaseContract.contractSum.name())));
            labelVOList.add(new LabelVO("甲方执合同数",(String)valueTable.get(SiteLeaseContract.contractSumA.name())));
            labelVOList.add(new LabelVO("乙方执合同数",(String)valueTable.get(SiteLeaseContract.contractSumB.name())));
            //构造子表
            try {
                BizObject[] zbBizObjects = (BizObject[]) valueTable.get(SiteLeaseContract.siteLeaseContractDeteil.name());
                for (int i = 1; i <= zbBizObjects.length; i++) {
                    Map<String, Object> zbValueTable = zbBizObjects[i - 1].getValueTable();
                    String str = "";
                    if (i != 1) {
                        str = String.valueOf(i);
                    }
                    labelVOList.add(new LabelVO("产品名称" + str, String.valueOf(zbValueTable.get(SiteLeaseContract.productName.name()))));
                    labelVOList.add(new LabelVO("规格型号" + str, String.valueOf(zbValueTable.get(SiteLeaseContract.specificationType.name()))));
                    labelVOList.add(new LabelVO("数量" + str, String.valueOf(zbValueTable.get(SiteLeaseContract.theNumber.name()))));
                    labelVOList.add(new LabelVO("单位" + str, String.valueOf(zbValueTable.get(SiteLeaseContract.gun.name()))));
                }
            } catch (Exception e) {
            }
            List<PlaceHolder> placeHolders = new ArrayList<>();
            placeHolders.add(new PlaceHolder(ACCOUNT,USERNAME,(String)valueTable.get(SiteLeaseContract.partyB12.name()),labelVOList));
            String templateId = "2157088732807692289";
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(2157089430328839177L,(String)valueTable.get(SiteLeaseContract.telA12.name()),
                    (String)valueTable.get(SiteLeaseContract.partyA12.name()),(String)valueTable.get(SiteLeaseContract.partyA12.name())));
            sendContractsSyncVO = new SendContractsSyncVO(new Sender("",""),templateId,placeHolders,roles);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendContractsSyncVO;
    }
}
