package com.h3bpm.starcharge.bean.k3c;

/**
 * MaterialInfo class
 *
 * @author llongago
 * @date 2019/3/12
 */
public class MaterialInfo {
    private String useOrgId;
    private String materialId;
    private String materialCode;
    private String materialName;
    private String specification;
    private String brand;
    private String materialGroup;
    private String tallyType;
    private String remark;
    private String producingArea;
    private String certificate;
    private String machineNo;
    private String color;
    private String spearBrand;
    private String cardType;
    private String spearLength;
    private String materialProperty;
    private String stockType;
    private String unit;
    private String safeStock;
    private String stock;
    private String fixedLeadTime;
    private String minNum;
    private String batchIncrement;
    private String planner;
    private String sendType;
    private String controType;
    private String sendStock;

    public String getUseOrgId() {
        return useOrgId;
    }

    public void setUseOrgId(String useOrgId) {
        this.useOrgId = useOrgId;
    }

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getMaterialGroup() {
        return materialGroup;
    }

    public void setMaterialGroup(String materialGroup) {
        this.materialGroup = materialGroup;
    }

    public String getTallyType() {
        return tallyType;
    }

    public void setTallyType(String tallyType) {
        this.tallyType = tallyType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getProducingArea() {
        return producingArea;
    }

    public void setProducingArea(String producingArea) {
        this.producingArea = producingArea;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getMachineNo() {
        return machineNo;
    }

    public void setMachineNo(String machineNo) {
        this.machineNo = machineNo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSpearBrand() {
        return spearBrand;
    }

    public void setSpearBrand(String spearBrand) {
        this.spearBrand = spearBrand;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getSpearLength() {
        return spearLength;
    }

    public void setSpearLength(String spearLength) {
        this.spearLength = spearLength;
    }

    public String getMaterialProperty() {
        return materialProperty;
    }

    public void setMaterialProperty(String materialProperty) {
        this.materialProperty = materialProperty;
    }

    public String getStockType() {
        return stockType;
    }

    public void setStockType(String stockType) {
        this.stockType = stockType;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSafeStock() {
        return safeStock;
    }

    public void setSafeStock(String safeStock) {
        this.safeStock = safeStock;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getFixedLeadTime() {
        return fixedLeadTime;
    }

    public void setFixedLeadTime(String fixedLeadTime) {
        this.fixedLeadTime = fixedLeadTime;
    }

    public String getMinNum() {
        return minNum;
    }

    public void setMinNum(String minNum) {
        this.minNum = minNum;
    }

    public String getBatchIncrement() {
        return batchIncrement;
    }

    public void setBatchIncrement(String batchIncrement) {
        this.batchIncrement = batchIncrement;
    }

    public String getPlanner() {
        return planner;
    }

    public void setPlanner(String planner) {
        this.planner = planner;
    }

    public String getSendType() {
        return sendType;
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    public String getControType() {
        return controType;
    }

    public void setControType(String controType) {
        this.controType = controType;
    }

    public String getSendStock() {
        return sendStock;
    }

    public void setSendStock(String sendStock) {
        this.sendStock = sendStock;
    }
}