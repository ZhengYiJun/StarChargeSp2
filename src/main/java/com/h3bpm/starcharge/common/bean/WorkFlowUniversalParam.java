package com.h3bpm.starcharge.common.bean;

/**
 * 工作流paramValue通用Bean
 *
 * @author LLongAgo
 * @date 2019/2/22
 * @since 1.0.0
 */
public class WorkFlowUniversalParam {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
