package com.h3bpm.starcharge.service.bpm;

import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.starcharge.common.bean.RetResult;

import java.util.List;

public interface UserService {

    RetResult addByExcel(InstanceData instanceData) throws Exception;

    RetResult changeDeptByExcel(InstanceData instanceData) throws Exception;

    List<List<String>> getAllUsers() throws Exception;

    List<List<String>> getDeptLeaders() throws Exception;

    List<List<String>> getDeptObjects() throws Exception;

    RetResult getUserPost(String objectId) throws Exception;
}
