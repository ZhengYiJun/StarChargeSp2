package com.h3bpm.starcharge.service.k3c;

import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.starcharge.common.bean.RetResult;

/**
 * K3C客户服务
 *
 * @author LLongAgo
 * @date 2019/4/9
 * @since 1.0.0
 */
public interface CustomerService {

    RetResult save(InstanceData instanceData) throws Exception;

    RetResult info(String number) throws Exception;

    RetResult edit(InstanceData instanceData) throws Exception;
}