package cn.bestsign.ultimate.delta.api.test;


import api.client.BestSignClient;

/**
 * @author whthomas
 * @date 2018/6/27
 */
public class BestSignClientTest {

    protected final BestSignClient bestSignClient;

    public BestSignClientTest() {

        String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALGlGFI3d/KZD6snTL7ftk9Tj1+M6vhCPDmZnKttH1W6uKeWYRpjjeWkO3DFjTllJuDabcCI4MtNOfP4YATLzcA900oE+ssTwhoLwZ4PHRtWHLPtQiWrtRS8Ri10Z2nthhrxRopCcCDLEde7tLQsvIqFLiBDldtg07z6NLryBCK7AgMBAAECgYAdyArCxc/TGPv6epUwLrsWo3CW2T4PLrOACJOuXZXyPmJ0ng5baTNBu33Hkybw51dUPOpHa+sbyi+cChi21SACAhjJFPhvjTlm8oBuEJwydQW3Uy7VpVHwhguZeeZZabl1/TItRaValcvCrcRkq/eXL7Yi/i7tb9CuskWtlvXQKQJBANhEePRxT1Li2u4PK8EnBwySRh8HVZ9eP+3zhgA9bAXRe3R9fJZ32d65DzwarVbUYX7czaK9cB2V1qHoUMQzKcUCQQDSSB6XG3h/0ZylP8vG0Eje8CBhk68TIRTJ8IZJLdo1FrNS7ylnOC8tQNe/cVWPC27rHmUw76KJcVW4ouiXYWJ/AkEAsSFb4/HOY3fUkniwgJHjjyNa9Vz/fvkl6VG0pguopxdNQzroSSE+u9HcrYC4Ck8Gz24vqqP36PNpiNCLHjc9dQJBAM9At621LJnGLb8NVrnYNcpNH40ssuSGuWejQadEowmqGwpFoCgUzyKqAzqz6GD5ItRne9K0jR0r8BHvtpDlMd8CQF8J8UG6zZu6dyf4icV8YrCNvBsdofebdHbC/QdO9y05cysdePry7+5V33/oy0PE6370aU2ifn6kisiZMRw8ZQE=";

        bestSignClient = new BestSignClient(
                "https://api.bestsign.info",
                "AKly5Uj2H5fmmNglzhrCEFTPPrgdpmqT",
                "ZxgrrftsWOWYTvqFafDqWGIe0hkr6vZp",
                privateKey
        );

    }
}
