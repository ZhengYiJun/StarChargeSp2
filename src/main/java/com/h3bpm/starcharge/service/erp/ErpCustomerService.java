package com.h3bpm.starcharge.service.erp;

import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.starcharge.common.bean.BaseResult;
import com.h3bpm.starcharge.common.bean.JsonResult;
import com.h3bpm.starcharge.common.bean.RetResult;


import java.util.List;

public interface ErpCustomerService {
    RetResult save(InstanceData instanceData) throws Exception;


    JsonResult<List<BaseResult>> getClassList() throws Exception;

}
