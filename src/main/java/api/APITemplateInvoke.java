package api;

import api.client.BestSignClient;
import api.domain.template.create.SendContractsFromTemplateVO;

/**
 * @author whthomas
 * @date 2018/6/27
 */
public class APITemplateInvoke {

    private BestSignClient bestSignClient;

    public APITemplateInvoke(BestSignClient bestSignClient) {
        this.bestSignClient = bestSignClient;
    }

    public void sendContractsFromTemplate(SendContractsFromTemplateVO sendContractsFromTemplateVO) {

        final String result = bestSignClient.executeRequest("/api/templates/send-contracts", "post", sendContractsFromTemplateVO);

        System.out.println(result);

    }

    public void queryTemplate(Long templateId) {

        final String result = bestSignClient.executeRequest("/api/templates/" + templateId, "get", null);

        System.out.println(result);

    }

}
