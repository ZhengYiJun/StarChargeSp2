package com.h3bpm.starcharge.controller;

import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.base.res.ResBody;
import com.h3bpm.starcharge.service.ems.DocumentPushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *  ems 文档系统对接控制器
 */
@Controller
@RequestMapping("/ems/documentPush")
public class DocumentPushController extends ControllerBase {
    @Override
    public String getFunctionCode() {
        return null;
    }

    @Autowired
    private DocumentPushService documentPushService;


    /**
     * 根据流程id推附件
     *
     * @return JsonResult
     */
    @RequestMapping(value = "/pushInstanceAttm", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResBody pushInstanceAttm(String instanceId) {
        try {
            InstanceData instanceData = new InstanceData(getEngine(),instanceId, User.AdministratorID);
            return  documentPushService.pushAttachment(instanceData);
        } catch (Exception e) {
            e.printStackTrace();
            return ResBody.buildFailResBody();
        }
    }

}
