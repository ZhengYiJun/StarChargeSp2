package com.h3bpm.starcharge.service.eseal;

/**
 * @ClassName ESealService
 * @Desciption 电子章业务层
 * @Author lvyz
 * @Date 2019/6/10 20:31
 **/
public interface ESealService {

	/**
	 * 同步合同到上上签
	 * @param instanceId
	 * @param contractCode
	 * @return
	 */
	String synContract(String instanceId, String contractCode);

	/**
	 * 同步非标准合同到上上签（上传）
	 * @param instanceId
	 * @return
	 */
	String upLoadContract(String instanceId);

	/**
	 * 签章
	 * @param contractId
	 * @param sealName 章名
	 * @param company 公司名称
	 */
	void signature(String contractId, String sealName, String company);
}
