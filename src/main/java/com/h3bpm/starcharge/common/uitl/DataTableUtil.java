package com.h3bpm.starcharge.common.uitl;

import OThinker.Common.Data.Database.ICommand;
import OThinker.Common.DotNetToJavaStringHelper;
import OThinker.H3.Controller.AppCode.Admin.ConstantString;
import com.h3bpm.base.util.AppUtility;
import data.DataColumn;
import data.DataColumnCollection;
import data.DataTable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DataTableUtil {


    /**
     * 获取List<Object>格式的表单数据
     *
     * @param dataTable
     *            表单
     * @return json
     * @throws Exception
     */
    public static List<Object> getRowList(DataTable dataTable) throws Exception {
        List<Object> rowList = new ArrayList<Object>();
        if (dataTable != null) {
            Map<String, String> columnDictionary = new LinkedHashMap<>(10);
            DataColumnCollection columns = dataTable.getColumns();
            for (int i = 0; i < columns.size(); i++) {
                DataColumn c = columns.get(i);
                if (!columnDictionary.containsKey(c.getColumnName())) {
                    columnDictionary.put("".equals(c.getColumnName()) ? ConstantString.DefaultName : c.getColumnName(),
                            "");
                }
            }

            if (dataTable.getRows().size() > 0) {
                for (int i = 0; i < dataTable.getRows().size(); i++) {
                    Map<String, String> temp = new LinkedHashMap<String, String>();
                    for (String key : columnDictionary.keySet()) {
                        temp.put(key, dataTable.getRows().get(i).getObject(key) != null
                                ? dataTable.getRows().get(i).getObject(key).toString() : "");
                    }
                    rowList.add(temp);
                }
            }
        }
        return rowList;
    }

    /**
     * 通用 根据SQL 查询数据
     *
     * @param sql
     *            SQL
     * @param args
     *            拼接参数
     * @return DataTable
     * @throws Exception
     */
    public static DataTable getDataTable(String sql, Object... args) throws Exception {
        ICommand command = AppUtility.getEngine().getEngineConfig().getCommandFactory().CreateCommand();
        // 是否多个参数
        if (args != null && args.length > 0) {
            sql = String.format(sql, args);
        }

        // 如果是查询操作
        if (sql != null && sql.toUpperCase().startsWith("SELECT")) {
            return command.ExecuteDataTable(sql);
        }
        // 其他操作
        command.ExecuteNonQuery(sql);
        command.Commit();
        return null;
    }

    /**
     * DataTable数据转成List数据  带HTML类型
     *
     * @param dataTable
     * @return
     * @throws Exception
     * @author linjh
     */
    public static List<Map<String, String>> dataTableParseBlobToString(DataTable dataTable)
            throws Exception {
        List<Map<String, String>> rowList = new ArrayList<Map<String, String>>();
        String nameBlob = "";
        if (dataTable != null) {
            Map<String, String> columnDictionary = new LinkedHashMap<String, String>();
            DataColumnCollection columns = dataTable.getColumns();
            for (int i = 0; i < columns.size(); i++) {
                DataColumn c = columns.get(i);
                if ("BLOB".equals(c.getDataTypeName())) {
                    nameBlob = c.getColumnName();
                }
                if (!columnDictionary.containsKey(c.getColumnName())) {
                    columnDictionary.put(
                            "".equals(c.getColumnName()) ? ConstantString.DefaultName : c.getColumnName(), "");
                }
            }
            if (dataTable.getRows().size() > 0) {
                for (int i = 0; i < dataTable.getRows().size(); i++) {
                    Map<String, String> temp = new LinkedHashMap<String, String>();
                    for (String key : columnDictionary.keySet()) {
                        if (!DotNetToJavaStringHelper.isNullOrEmpty(nameBlob) && nameBlob.equals(key)) {
                            String t = "";
                            if (dataTable.getRows().get(i).getObject(key) != null) {
                                t =
                                        new String(dataTable.getRows().get(i).getBytes(key), "UTF-8")
                                                .replaceAll("↵", "");
                            }
                            if (DotNetToJavaStringHelper.isNullOrEmpty(t)){
                                temp.put(key, t);
                            } else{
                                temp.put(key, t.substring(7));
                            }

                        } else {
                            temp.put(
                                    key,
                                    dataTable.getRows().get(i).getObject(key) != null
                                            ? dataTable.getRows().get(i).getObject(key).toString()
                                            : "");
                        }
                    }
                    rowList.add(temp);
                }
            }
        }
        return rowList;
    }

}
