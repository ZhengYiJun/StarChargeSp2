package cn.bestsign.ultimate.delta.api.test;

import api.client.BestSignClient;
import api.domain.contract.create.CreateContractVO;
import api.domain.contract.create.CreateDocumentVO;
import api.domain.contract.create.CreateReceiverVO;
import api.domain.contract.create.SignFlowConstants;
import api.domain.contract.download.DownloadFileVo;
import api.domain.contract.returnvo.ReturnVO;
import api.domain.contract.revoke.RevokeVO;
import api.domain.contract.sign.SignContractVO;
import api.domain.contract.sign.Signer;
import api.domain.preview.PreviewVO;
import api.domain.template.create.LabelVO;
import api.domain.template.create.PlaceHolder;
import api.domain.template.create.Role;
import api.domain.template.create.SendContractsSyncVO;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.h3bpm.starcharge.common.uitl.BestsignPropertiesUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class PreviewTest {

    protected  BestSignClient bestSignClient = BestsignPropertiesUtil.getBestSignClient();

    private  static String PreviewAfteruRL = BestsignPropertiesUtil.getProperty("previewAfteruRL");

    private  static String PreviewUrl = BestsignPropertiesUtil.getProperty("previewUrl");

    private  static String TemplatesUrl = BestsignPropertiesUtil.getProperty("templatesUrl");

    private  static String SendContractsSyncUrl = BestsignPropertiesUtil.getProperty("sendContractsSyncUrl");

    private  static String DownloadFileUrl = BestsignPropertiesUtil.getProperty("downloadFileUrl");

    private  static String RevokeUrl = BestsignPropertiesUtil.getProperty("revokeUrl");

    private  static String SignUrl = BestsignPropertiesUtil.getProperty("signUrl");

    private static String POST = "POST";

    private static String GET = "GET";

    private final ObjectMapper objectMapper = new ObjectMapper();


    @Test
    public void testPreview() throws IOException {

        PreviewVO previewVO = new PreviewVO();
        previewVO.setTemplateId("2136850031443968008");
        List<LabelVO> labelVOList = new ArrayList<>();
        labelVOList.add(new LabelVO("城市","常州"));
       // labelVOList.add(new LabelVO("金额","1000"));
        labelVOList.add(new LabelVO("充电度数","10"));
        labelVOList.add(new LabelVO("乙方公司","奥哲"));
        previewVO.setTextLabels(labelVOList);
        String body = objectMapper.writeValueAsString(previewVO);

        byte[] result =  bestSignClient.executeRequest2(PreviewUrl,POST,previewVO,3);
        FileOutputStream fileOutputStream = new FileOutputStream(new File("D://y.pdf"));
        fileOutputStream.write(result);
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println(result);
    }

    @Test
    public void testTemplates() throws IOException {
        String templateId = "2216517843371953154";
       // templateId.split("\\+");
        String result =  bestSignClient.executeRequest(TemplatesUrl+templateId,GET,null);
        System.out.println(result);
    }

    @Test
    public void testPreviewAfter() throws IOException {
        String url = String.format(PreviewAfteruRL,"2163145395880858625","0","1");
        String result =  bestSignClient.executeRequest(url,GET,null);
        System.out.println(result);
    }

    @Test
    public void testSendContractsSync() throws IOException {
        List<PlaceHolder> placeHolders = new ArrayList<PlaceHolder>();
        List<LabelVO> labelVOList = new ArrayList<>();
        labelVOList.add(new LabelVO("城市","常州"));
        labelVOList.add(new LabelVO("金额","1000"));
        labelVOList.add(new LabelVO("充电度数","10"));
        labelVOList.add(new LabelVO("乙方公司","奥哲"));
        labelVOList.add(new LabelVO("子公司名称","奥哲111"));

        placeHolders.add(new PlaceHolder("18668122470","郑一君","万帮新能源投资集团有限公司",labelVOList));
        String templateId = "2157031550804298757";
        List<Role> roles = new ArrayList<Role>();
//        roles.add(new Role(2136850369211269129L,"18668122470","郑一君","万帮新能源投资集团有限公司"));
//        roles.add(new Role(2146982963697418240L,"18668122470","郑一君","万帮新能源投资集团有限公司"));
        SendContractsSyncVO  sendContractsSyncVO = new SendContractsSyncVO(null,templateId,placeHolders,null);
        String body = objectMapper.writeValueAsString(sendContractsSyncVO);
        String result =  bestSignClient.executeRequest(SendContractsSyncUrl,POST,sendContractsSyncVO);
        System.out.println(result);
    }

    @Test
    public void testDownloadFile() throws IOException {
        byte[] result = bestSignClient.executeRequest2("/api/contracts/download-file",POST, new DownloadFileVo( Arrays.asList("2163145395880858625")),3);
        FileOutputStream fileOutputStream = new FileOutputStream(new File("D://xq.zip"));
        fileOutputStream.write(result);
        fileOutputStream.flush();
        fileOutputStream.close();
       // String result = bestSignClient.executeRequest(DownloadFileUrl, "post", new DownloadFileVo( Arrays.asList("2155659121351720963")));
        //System.out.println(result);
        //Files.write(Paths.get("D:\\Users\\2074548388677812233测试.PDF"),result.getBytes());
       // byte[] bytes =Base64.getDecoder().decode(result);
        System.out.println(result);

    }


    @Test
    public void testRevoke() {

        String result = bestSignClient.executeRequest(String.format(RevokeUrl,"2155658418906464259"), "post", new RevokeVO(""));

        System.out.println(result);

    }


    @Test
    public void testSign(){
        SignContractVO signContractVO = new SignContractVO();
        signContractVO.setSigner(new Signer("starcharge.dehe@wanbangauto.com","万帮充电设备有限公司 "));
        signContractVO.setSealName("万帮充电合同章");
        List<Long> contractIds = Arrays.asList(2164353031045387270L);
        signContractVO.setContractIds(contractIds);
        String string = bestSignClient.executeRequest(SignUrl,"POST",signContractVO);

        System.out.println( string);
    }


    /**
     * 6.4.1 关键字定位
     *
     */
    //@Test
    public String calculatePositions() throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject j = new JSONObject();
        JSONArray documents = new JSONArray();


        j.put("content", getFileBase64Content("D:\\y.pdf")); //文档的BASE64编码
        j.put("order", 0);//支持多文档,有多个文档用多个  order必须有序增长
        j.put("fileName","y.pdf"); //上传的文件名称 ，必须要有后缀，如  .pdf

        documents.add(j);

        jsonObject.put("documents", documents);
        jsonObject.put("keyword", "签章后生效");  //在上传的文件中，获取到“签章”的位置信息

        String string = bestSignClient.executeRequest("/api/contracts/calculate-positions", "POST", jsonObject);
        System.out.println( string);
        return string;
    }

    private String getFileBase64Content(String filePath) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(filePath);
        byte[] bytes = new byte[fileInputStream.available()];
        fileInputStream.read(bytes);
        return Base64.getEncoder().encodeToString(bytes);
    }


    @Test
    public void testCreateContract() throws IOException {

        CreateContractVO createContractVO = new CreateContractVO();
        createContractVO.setContractDescription("测试api合同5");
        createContractVO.setContractTitle("测试api合同5");
        createContractVO.setSignDeadline(new Date(System.currentTimeMillis()+ 86400000));
        createContractVO.setSignOrdered(false);
        List<CreateDocumentVO> documents = new ArrayList<>();
        CreateDocumentVO documentVO = new CreateDocumentVO();
        documentVO.setFileName("y.pdf");
        documentVO.setContent(getFileBase64Content("D:\\y.pdf"));
        documents.add(documentVO);
        documentVO.setOrder(0);
        createContractVO.setDocuments(documents);

        CreateReceiverVO receiverVO= new CreateReceiverVO();
        receiverVO.setUserAccount("czqc.jituan@wanbangauto.com");
        receiverVO.setUserName("czqc.jituan@wanbangauto.com");
        receiverVO.setEnterpriseName("万帮新能源投资集团有限公司");
        receiverVO.setSignImmediately(true);
        receiverVO.setSignSignatureFileName("充充科技公章");
        receiverVO.setUserType(SignFlowConstants.UserType.ENTERPRISE);
        receiverVO.setReceiverType(SignFlowConstants.ReceiverType.SIGNER);
        String str  =  calculatePositions();

        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ReturnVO.class, CreateReceiverVO.class);

        ReturnVO<CreateReceiverVO> returnVO = objectMapper.readValue(str,javaType);
        receiverVO.setLabels(returnVO.getData().getLabels());

        List<CreateReceiverVO> receivers = new ArrayList<>();
        receivers.add(receiverVO);
        createContractVO.setReceivers(receivers);

        String string = bestSignClient.executeRequest("/api/contracts/","POST",createContractVO);

        System.out.println( string);
    }

}
