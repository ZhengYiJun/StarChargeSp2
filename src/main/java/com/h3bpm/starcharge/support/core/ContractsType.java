package com.h3bpm.starcharge.support.core;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName Contracts
 * @Desciption 合同类型枚举
 * @Author lvyz
 * @Date 2019/6/10 20:23
 **/
public enum ContractsType {

	STANDARD_ADD_CAPACITY("StandardAddCapacity","新能源汽车充电设施推广合作协议（增容）"),
	STANDARD_BOT("StandardBOT", "新能源汽车充电设施推广合作协议（BOT）"),
	STANDARD_COMMODITY("StandardCommodity","新能源汽车充电设施推广合作协议（场站金融）"),
	STANDARD_NO_ADD_CAPACITY("StandardNoAddCapacity","新能源汽车充电设施推广合作协议（非增容）"),
	STANDARD_VENUE_RENTAL("StandardVenueRental","新能源汽车充电设施推广合作协议（以租代售）");

	ContractsType(String code, String name) {
		this.code = code;
		this.name = name;
	}

	private static final Map<String, ContractsType> map = new HashMap<>();

	static {
		for (ContractsType contractsType : ContractsType.values()) {
			map.put(contractsType.code, contractsType);
		}
	}

	private String code;

	private String name;

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static ContractsType getContractsType(String type) {
		return map.get(type);
	}
}
