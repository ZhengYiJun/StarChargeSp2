package com.h3bpm.starcharge.controller.k3c;

import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.ErrorUtil;
import com.h3bpm.starcharge.service.k3c.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 经营性付款控制器
 *
 * @author LLongAgo
 * @date 2019/4/22
 * @since 1.0.0
 */
@Controller
@RequestMapping("/k3c/payment")
public class PaymentController extends ControllerBase {

    @Autowired
    private PaymentService paymentService;

    @Override
    public String getFunctionCode() {
        return null;
    }

    /**
     * 电费预付
     * @param instanceId    instanceId
     * @return RetResult
     */
    @RequestMapping(value = "/electricChargePre", produces = "application/json;charset=utf-8")
    @ResponseBody
    public RetResult electricChargePre(@RequestParam String instanceId) {
        try {
            InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            return paymentService.electricChargePre(instanceData);
        } catch (Exception e) {
            return ErrorUtil.generalException(e);
        }
    }
}