package api.domain.contract.returnvo;

import java.util.List;

public class ReturnSign {

    /**
     * 总的签署合同份
     */
    private int totalAmount;
    /**
     * 成功的签署合同份数
     */
    private int successAmount;
    /**
     * 失败的签署合同份数
     */
    private int failureAmount;
    /**
     * 签署的错误信息列表
     */
    private List<String> errorInfos;

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getSuccessAmount() {
        return successAmount;
    }

    public void setSuccessAmount(int successAmount) {
        this.successAmount = successAmount;
    }

    public int getFailureAmount() {
        return failureAmount;
    }

    public void setFailureAmount(int failureAmount) {
        this.failureAmount = failureAmount;
    }

    public List<String> getErrorInfos() {
        return errorInfos;
    }

    public void setErrorInfos(List<String> errorInfos) {
        this.errorInfos = errorInfos;
    }
}
