package com.h3bpm.starcharge.service.k3c;

import com.h3bpm.starcharge.bean.k3c.MaterialBase;
import com.h3bpm.starcharge.common.bean.BaseResult;
import com.h3bpm.starcharge.common.bean.JsonResult;

import java.util.List;

public interface BaseDataService {
    /**
     * 计量单位
     * @return JsonResult
     * @throws Exception
     */
    JsonResult<List<BaseResult>> getUnit() throws Exception;

    /**
     * 存货类别
     * @return JsonResult
     * @throws Exception
     */
    JsonResult<List<BaseResult>> getMaterialCateGory() throws Exception;

    /**
     * 仓库
     * @return JsonResult
     * @throws Exception
     */
    JsonResult<List<BaseResult>> getStock(String useOrgId) throws Exception;

    /**
     * 财务记账类别
     * @return JsonResult
     * @throws Exception
     */
    JsonResult<List<BaseResult>> getTallyType() throws Exception;

    /**
     * 组织机构
     * @param  searchCode String
     * @return JsonResult
     */
    JsonResult<List<BaseResult>> getOrganizationList(String searchCode) throws Exception;

    JsonResult<List<BaseResult>> getOrganizationBySql(String sqlStr) throws Exception;

    /**
     * 物料分组
     * @param name 分组名字
     * @return JsonResult
     */
    JsonResult<List<MaterialBase>> getMaterialGroup(String name) throws Exception;

    /**
     * 仓位
     * @param number 仓库number
     * @return JsonResult
     */
    JsonResult<List<BaseResult>> getStockLoc(String number) throws Exception;
}
