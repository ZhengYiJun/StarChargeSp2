package api.domain.template.create;

import java.util.List;

public class SendContractsSyncVO {

    /**
     * 发件人，必填
     */
    private Sender sender;
    /**
     * 合同模板Id，必填
     */
    private String templateId;
    /**
     * 可变签署人,内含合同数据。placeHolders的数量决定本次调用所发送的合同的数量，必填
     */
    private List<PlaceHolder> placeHolders;
    /**
     * 固定签署人，非必填
     */
    private List<Role> roles;

    public SendContractsSyncVO(Sender sender, String templateId, List<PlaceHolder> placeHolders, List<Role> roles) {
        this.sender = sender;
        this.templateId = templateId;
        this.placeHolders = placeHolders;
        this.roles = roles;
    }

    public SendContractsSyncVO() {
    }

    public SendContractsSyncVO(String templateId, List<PlaceHolder> placeHolders) {
        this.templateId = templateId;
        this.placeHolders = placeHolders;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public List<PlaceHolder> getPlaceHolders() {
        return placeHolders;
    }

    public void setPlaceHolders(List<PlaceHolder> placeHolders) {
        this.placeHolders = placeHolders;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "SendContractsSyncVO{" +
                "sender=" + sender +
                ", templateId='" + templateId + '\'' +
                ", placeHolders=" + placeHolders +
                ", roles=" + roles +
                '}';
    }
}
