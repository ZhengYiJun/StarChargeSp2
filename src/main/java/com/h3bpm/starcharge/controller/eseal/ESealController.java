package com.h3bpm.starcharge.controller.eseal;

import OThinker.Common.DotNetToJavaStringHelper;
import com.h3bpm.starcharge.service.eseal.ESealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName ESealController
 * @Desciption 电子章相关业务控制层
 * @Author lvyz
 * @Date 2019/6/10 21:25
 **/
@Controller
@RequestMapping("/eSeal")
public class ESealController {

	@Autowired
	private ESealService eSealService;

	/**
	 * 标准合同同步到上上签
	 * 返回 上上签合同编号
	 *
	 * @param instanceId
	 * @return
	 */
	@ResponseBody
	@PostMapping(value = "/synContract")
	public Map<String, String> synContract(@RequestParam String instanceId, @RequestParam String contractCode) {

		String contractId = this.eSealService.synContract(instanceId, contractCode);

		Map<String, String> resultMap = new HashMap<>();

		resultMap.put("contractId", contractId);

		return resultMap;
	}

	/**
	 * 非标准合同同步到上上签
	 * 返回 上上签合同编号
	 * @param instanceId
	 * @return
	 */
	@ResponseBody
	@PostMapping(value = "/upLoadContract")
	public Map<String, String> upLoadContract(String instanceId){

		if (DotNetToJavaStringHelper.isNullOrEmpty(instanceId) || DotNetToJavaStringHelper.isNullOrEmpty(instanceId)) {
			return null;
		}

		String contractId = eSealService.upLoadContract(instanceId);

		Map<String, String> resultMap = new HashMap<>();

		resultMap.put("contractId", contractId);

		return resultMap;

	}


	/**
	 * 签名，盖章
	 *
	 * @param contractId
	 */
	@ResponseBody
	@PostMapping(value = "/signature")
	public void signature(String contractId, String sealName, String companyName) {

		eSealService.signature(contractId, sealName, companyName);
	}


}
