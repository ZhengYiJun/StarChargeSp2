package api.domain.template.create;

/**
 * @author whthomas
 * @date 2018/6/27
 */
public enum RoleType {
    /**
     * 固定签署人，一般指发送合同的公司方
     */
    SPECIFIC_USER,
    /**
     * 非固定签署人
     */
    PLACEHOLDER
}
