package com.h3bpm.starcharge.service.k3c;

import OThinker.H3.Entity.Instance.Data.InstanceData;

import com.alibaba.fastjson.JSONArray;
import com.h3bpm.starcharge.bean.k3c.MaterialBase;
import com.h3bpm.starcharge.bean.k3c.MaterialInfo;
import com.h3bpm.starcharge.common.bean.JsonResult;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.ret.k3c.K3cBase;

import java.util.List;

/**
 * K3C物料接口
 *
 * @author LLongAgo
 */
public interface K3cMaterialService {

    /**
     * 保存物料
     * @param instanceData  InstanceData
     * @return RetResult
     * @throws Exception
     */
    RetResult save(InstanceData instanceData) throws Exception;

    /**
     * 批量保存
     */
    JsonResult<K3cBase> allSave(String formId, String param) throws Exception;

    /**
     * 物料维护
     */
    RetResult maintenance(InstanceData instanceData, String tableId) throws Exception;

    /**
     * 下推
     */
    JsonResult<K3cBase> push(String formId, String param) throws Exception;

    /**
     * 分配
     */
    JsonResult<K3cBase> allocate(String ids, String orgIds) throws Exception;

    /**
     * 提交
     */
    void submit(String formId, String param) throws Exception;

    /**
     * 删除
     */
    void delete(String formId, String param) throws Exception;

    /**
     * 审核
     */
    JsonResult<K3cBase> audit(String formId, String param) throws Exception;

    JSONArray getOrderIdList(String formId, String serchKeys, String filterString) throws Exception;

    /**
     * 反审核
     *
     * @param formId 表单id
     * @param param  参数
     * @return JsonResult<K3cBase>
     * @throws Exception Exception
     */
    JsonResult<K3cBase> unAudit(String formId, String param) throws Exception;

    JsonResult<List<MaterialBase>> getList(String materialCode, String useOrgId) throws Exception;

    JsonResult<List<MaterialBase>> getNormalList(String formId, String serchKeys, String filterString) throws Exception;

    /**
     * 根据id查询物料详情
     *
     * @param id 物料id
     * @return JsonResult
     */
    JsonResult<MaterialInfo> getInfo(String id) throws Exception;

    /**
     * 反审核物料
     */
    JsonResult<K3cBase> unAuditMaterial(String ids) throws Exception;

}
