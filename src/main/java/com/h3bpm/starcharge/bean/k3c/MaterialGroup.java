package com.h3bpm.starcharge.bean.k3c;

import java.util.List;

/**
 * materialGroup
 *
 * @author LLongAgo
 * @date 2019/3/15
 * @since 1.0.0
 */
public class MaterialGroup {

    /**
     * code : 0
     * msg : ok
     * data : [{"name":"塑壳断路器","one":"A","two":"DQ","three":"MCC","four":"","five":""},{"name":"微型断路器","one":"A","two":"DQ","three":"MCB","four":"","five":""},{"name":"框架断路器","one":"A","two":"DQ","three":"FCB","four":"","five":""}]
     */

    private String code;
    private String msg;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : 塑壳断路器
         * one : A
         * two : DQ
         * three : MCC
         * four :
         * five :
         */

        private String name;
        private String one;
        private String two;
        private String three;
        private String four;
        private String five;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOne() {
            return one;
        }

        public void setOne(String one) {
            this.one = one;
        }

        public String getTwo() {
            return two;
        }

        public void setTwo(String two) {
            this.two = two;
        }

        public String getThree() {
            return three;
        }

        public void setThree(String three) {
            this.three = three;
        }

        public String getFour() {
            return four;
        }

        public void setFour(String four) {
            this.four = four;
        }

        public String getFive() {
            return five;
        }

        public void setFive(String five) {
            this.five = five;
        }
    }
}