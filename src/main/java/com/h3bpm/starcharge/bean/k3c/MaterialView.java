package com.h3bpm.starcharge.bean.k3c;

import java.util.List;

/**
 * MaterialView class
 *
 * @author llongago
 * @date 2019/3/12
 */
@SuppressWarnings("ALL")
public class MaterialView {

    /**
     * Result : {"ResponseStatus":null,"Result":{"Id":505063,"msterID":260295,"DocumentStatus":"C","ForbidStatus":"A","MultiLanguageText":[{"PkId":379257,"LocaleId":2052,"Name":"二维码（星星充电SAAS版）","Description":" ","Specification":"DH-AC0070SAAS-001，113*58.5mm"}],"Name":[{"Key":2052,"Value":"二维码（星星充电SAAS版）"}],"Number":"AJGLAB0018","Description":[{"Key":2052,"Value":" "}],"CreateOrgId_Id":1,"CreateOrgId":{"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"},"UseOrgId_Id":500232,"UseOrgId":{"Id":500232,"MultiLanguageText":[{"PkId":100088,"LocaleId":2052,"Name":"温州星之帮新能源科技有限公司"}],"Name":[{"Key":2052,"Value":"温州星之帮新能源科技有限公司"}],"Number":"102059"},"CreatorId_Id":129041,"CreatorId":{"Id":129041,"Name":"何谐","UserAccount":" "},"ModifierId_Id":129041,"ModifierId":{"Id":129041,"Name":"何谐","UserAccount":" "},"CreateDate":"2019-01-10T17:05:01.987","ModifyDate":"2019-01-10T17:05:01.5","MnemonicCode":" ","Specification":[{"Key":2052,"Value":"DH-AC0070SAAS-001，113*58.5mm"}],"ForbidderId_Id":0,"ForbidderId":null,"ForbidDate":null,"ApproveDate":"2019-01-10T17:05:02.483","ApproverId_Id":129041,"ApproverId":{"Id":129041,"Name":"何谐","UserAccount":" "},"Image":null,"OldNumber":"IDC.0014","MaterialGroup_Id":159777,"MaterialGroup":{"Id":159777,"Number":"LAB","MultiLanguageText":[{"PkId":100078,"LocaleId":2052,"Name":"标贴"}],"Name":[{"Key":2052,"Value":"标贴"}]},"PLMMaterialId":" ","MaterialSRC":"B","IsValidate":false,"ImageFileServer":" ","ImgStorageType":"B","IsImgDataBase":null,"IsImgFileServer":null,"IsSalseByNet":false,"F_ztys":" ","F_pp":" ","F_qpp":" ","F_klx":" ","F_qcd":" ","F_WB_CWJZLB_Id":171751,"F_WB_CWJZLB":{"Id":171751,"msterID":171751,"MultiLanguageText":[{"PkId":100103,"LocaleId":2052,"Name":"德和-原材料-辅材类"}],"Name":[{"Key":2052,"Value":"德和-原材料-辅材类"}],"Number":"1007"},"F_WB_CD":" ","F_WB_DSFRZ":" ","XMGoodsTaxNo":" ","XMTaxPre":false,"XMTaxPreCon":" ","XMZeroTax":"4","XMGoodsNoVer":"30.0","F_WB_JZH":" ","MaterialBase":[{"Id":379204,"ErpClsID":"1","IsInventory":true,"IsSale":true,"IsAsset":false,"IsSubContract":false,"IsProduce":false,"IsPurchase":true,"IsRealTimeAccout":false,"BaseUnitId_Id":10101,"BaseUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"TaxType_Id":"63489f1e86944c989c58d97893a2aec2","TaxType":{"Id":"63489f1e86944c989c58d97893a2aec2","FNumber":"WLDSFL01_SYS","MultiLanguageText":[{"PkId":"912d2a22-2def-42e8-86ad-c91f907c58af","LocaleId":1033,"FDataValue":"Standard Tax Rate"},{"PkId":"1ac1f2dd4a584f838c73c939d1496e43","LocaleId":2052,"FDataValue":"标准税率"}],"FDataValue":[{"Key":1033,"Value":"Standard Tax Rate"},{"Key":2052,"Value":"标准税率"}]},"TypeID_Id":0,"TypeID":null,"CategoryID_Id":237,"CategoryID":{"Id":237,"msterID":237,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"原材料"},{"PkId":8,"LocaleId":1033,"Name":"Raw materials"}],"Name":[{"Key":2052,"Value":"原材料"},{"Key":1033,"Value":"Raw materials"}],"Number":"CHLB01_SYS"},"TaxRateId_Id":264,"TaxRateId":{"Id":264,"msterID":264,"MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"16%增值税"}],"Name":[{"Key":2052,"Value":"16%增值税"}],"Number":"SL31_SYS"},"BARCODE":" ","WEIGHTUNITID_Id":10095,"WEIGHTUNITID":{"Id":10095,"msterID":10095,"MultiLanguageText":[{"PkId":17,"LocaleId":2052,"Name":"千克"},{"PkId":35,"LocaleId":1033,"Name":"Kg"}],"Name":[{"Key":2052,"Value":"千克"},{"Key":1033,"Value":"Kg"}],"Number":"kg","IsBaseUnit":true,"UnitGroupId_Id":10085,"UnitGroupId":{"Id":10085,"msterID":10085,"Number":"Weight","MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"重量"},{"PkId":17,"LocaleId":1033,"Name":"Weight"}],"Name":[{"Key":2052,"Value":"重量"},{"Key":1033,"Value":"Weight"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10114,"ConvertType":"0"}]},"VOLUMEUNITID_Id":10087,"VOLUMEUNITID":{"Id":10087,"msterID":10087,"MultiLanguageText":[{"PkId":15,"LocaleId":2052,"Name":"米"},{"PkId":33,"LocaleId":1033,"Name":"Meter"}],"Name":[{"Key":2052,"Value":"米"},{"Key":1033,"Value":"Meter"}],"Number":"m","IsBaseUnit":true,"UnitGroupId_Id":10081,"UnitGroupId":{"Id":10081,"msterID":10081,"Number":"Length","MultiLanguageText":[{"PkId":6,"LocaleId":2052,"Name":"长度"},{"PkId":15,"LocaleId":1033,"Name":"Length"}],"Name":[{"Key":2052,"Value":"长度"},{"Key":1033,"Value":"Length"}]},"Precision":2,"RoundType":"1","UNITCONVERTRATE":[{"Id":10104,"ConvertType":"0"}]},"GROSSWEIGHT":0,"NETWEIGHT":0,"LENGTH":0,"VOLUME":0,"WIDTH":0,"HEIGHT":0,"CONFIGTYPE":" ","Suite":"0","CostPriceRate":0}],"MaterialStock":[{"Id":379204,"StoreUnitID_Id":10101,"StoreUnitID":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"AuxUnitID_Id":0,"AuxUnitID":null,"StockId_Id":0,"StockId":null,"IsLockStock":true,"BatchRuleID_Id":0,"BatchRuleID":null,"ExpUnit":" ","StockPlaceId_Id":0,"StockPlaceId":null,"OnlineLife":0,"ExpPeriod":0,"StoreURNum":1,"StoreURNom":1,"IsBatchManage":false,"IsKFPeriod":false,"IsExpParToFlot":false,"IsCycleCounting":false,"IsMustCounting":false,"CurrencyId_Id":1,"CurrencyId":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"RefCost":0,"CountCycle":"1","CountDay":1,"IsSNManage":false,"SNCodeRule_Id":0,"SNCodeRule":null,"SNUnit_Id":0,"SNUnit":null,"SafeStock":0,"ReOrderGood":0,"MinStock":0,"MaxStock":0,"UnitConvertDir":"1","IsEnableMinStock":false,"IsEnableSafeStock":false,"IsEnableMaxStock":false,"IsEnableReOrder":false,"EconReOrderQty":0,"IsSNPRDTracy":false,"SNGenerateTime":"1","SNManageType":"1","BoxStandardQty":0}],"MaterialSale":[{"Id":379204,"IsATPCheck":false,"SalePriceUnitId_Id":10101,"SalePriceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"SaleUnitId_Id":10101,"SaleUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"IsInvoice":false,"MaxQty":100000,"IsReturn":true,"MinQty":0,"IsReturnPart":false,"OrderQty":0,"SalePriceURNom":1,"SaleURNum":1,"SalePriceURNum":1,"SaleURNom":1,"OutStockLmtH":0,"OutStockLmtL":0,"AgentSalReduceRate":0,"AllowPublish":false,"ISAFTERSALE":true,"ISPRODUCTFILES":true,"ISWARRANTED":false,"FWARRANTY":0,"WARRANTYUNITID":"D","OutLmtUnit":"SAL","TaxCategoryCodeId_Id":0,"TaxCategoryCodeId":null,"SalGroup_Id":0,"SalGroup":null,"TaxDiscountsType":"0","IsTaxEnjoy":false}],"MaterialPurchase":[{"Id":379204,"PurchaseUnitID_Id":10101,"PurchaseUnitID":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"PurchaserId_Id":0,"PurchaserId":null,"DefaultVendor_Id":0,"DefaultVendor":null,"IsSourceControl":false,"IsPR":false,"ReceiveMinScale":0,"PurchaseGroupId_Id":0,"PurchaseGroupId":null,"ReceiveMaxScale":0,"PurchasePriceUnitId_Id":10101,"PurchasePriceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"IsVendorQualification":false,"ReceiveAdvanceDays":0,"ReceiveDelayDays":0,"PurURNum":1,"PurPriceURNum":1,"PurURNom":1,"PurPriceURNom":1,"IsQuota":false,"QuotaType":"1","AgentPurPlusRate":0,"ChargeID_Id":0,"ChargeID":null,"MinSplitQty":0,"BaseMinSplitQty":0,"IsVmiBusiness":false,"IsReturnMaterial":true,"EnableSL":false,"PurchaseOrgId_Id":0,"PurchaseOrgId":null,"DefBarCodeRuleId_Id":0,"DefBarCodeRuleId":null,"MinPackCount":0,"PrintCount":1,"POBillTypeId_Id":"93591469feb54ca2b08eb635f8b79de3","POBillTypeId":{"Id":"93591469feb54ca2b08eb635f8b79de3","MultiLanguageText":[{"PkId":"65a98b68-868c-407d-b337-e3d321635b44","LocaleId":1033,"Name":"Standard Purchase Requisition"},{"PkId":"a7b70d95ba34432387653c6937cae696","LocaleId":2052,"Name":"标准采购申请"}],"Name":[{"Key":1033,"Value":"Standard Purchase Requisition"},{"Key":2052,"Value":"标准采购申请"}],"Number":"CGSQD01_SYS","IsDefault":true}}],"MaterialPlan":[{"Id":379204,"PlanerID_Id":0,"PlanerID":null,"EOQ":1,"PlanningStrategy":"1","OrderPolicy":"0","PlanWorkshop_Id":0,"PlanWorkshop":null,"FixLeadTimeType":"1","FixLeadTime":15,"VarLeadTimeType":"1","VarLeadTime":0,"CheckLeadTimeType":"1","CheckLeadTime":0,"OrderIntervalTimeType":"3","OrderIntervalTime":0,"PlanIntervalsDays":1,"PlanBatchSplitQty":0,"PlanTimeZone":0,"RequestTimeZone":0,"IsMrpComReq":false,"ReserveType":"1","CanLeadDays":0,"LeadExtendDay":0,"DelayExtendDay":0,"CanDelayDays":999,"PlanOffsetTimeType":"1","PlanOffsetTime":0,"MinPOQty":50,"IncreaseQty":0,"MaxPOQty":100000,"VarLeadTimeLotSize":1,"BaseVarLeadTimeLotSize":1,"PlanGroupId_Id":0,"PlanGroupId":null,"MfgPolicyId_Id":40042,"MfgPolicyId":{"Id":40042,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"MTS10(考虑库存)"},{"PkId":6,"LocaleId":1033,"Name":"MTS10 (Include inventory)"}],"Name":[{"Key":2052,"Value":"MTS10(考虑库存)"},{"Key":1033,"Value":"MTS10 (Include inventory)"}],"Number":"ZZCL001_SYS","PlanMode":"0","Ato":false},"SupplySourceId_Id":0,"SupplySourceId":null,"TimeFactorId_Id":0,"TimeFactorId":null,"QtyFactorId_Id":0,"QtyFactorId":null,"PlanMode":"0","AllowPartDelay":true,"AllowPartAhead":false,"PLANSAFESTOCKQTY":0,"ATOSchemeId_Id":0,"ATOSchemeId":null}],"MaterialProduce":[{"Id":379204,"PickStockId_Id":0,"PickStockId":null,"BOMUnitId_Id":10101,"BOMUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"WorkShopId_Id":0,"WorkShopId":null,"IssueType":"1","ProduceUnitId_Id":10101,"ProduceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"IsKitting":false,"DefaultRouting_Id":0,"DefaultRouting":null,"IsCoby":false,"PerUnitStandHour":0,"BKFLTime":" ","FinishReceiptOverRate":0,"FinishReceiptShortRate":0,"PickBinId_Id":0,"PickBinId":null,"PrdURNum":1,"PrdURNom":1,"BOMURNum":1,"BOMURNom":1,"IsMainPrd":false,"IsCompleteSet":false,"OverControlMode":"3","MinIssueQty":1,"StdLaborPrePareTime":0,"StdLaborProcessTime":0,"StdMachinePrepareTime":0,"StdMachineProcessTime":0,"ConsumVolatility":0,"IsProductLine":false,"ProduceBillType_Id":" ","ProduceBillType":null,"OrgTrustBillType_Id":" ","OrgTrustBillType":null,"ISMinIssueQty":false,"IsECN":false,"MinIssueUnitId_Id":10101,"MinIssueUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"MDLID_Id":0,"MDLID":null,"MdlMaterialId_Id":0,"MdlMaterialId":null,"LossPercent":0,"IsSNCarryToParent":false,"StandHourUnitId":"3600","BackFlushType":"1"}],"MaterialAuxPty":[],"MaterialInvPty":[{"Id":1495940,"IsEnable":true,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10001,"InvPtyId":{"Id":10001,"msterID":10001,"Number":"01","MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"仓库"},{"PkId":7,"LocaleId":1033,"Name":"Warehouse"}],"Name":[{"Key":2052,"Value":"仓库"},{"Key":1033,"Value":"Warehouse"}]}},{"Id":1495941,"IsEnable":true,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10002,"InvPtyId":{"Id":10002,"msterID":10002,"Number":"02","MultiLanguageText":[{"PkId":2,"LocaleId":2052,"Name":"仓位"},{"PkId":8,"LocaleId":1033,"Name":"Bin"}],"Name":[{"Key":2052,"Value":"仓位"},{"Key":1033,"Value":"Bin"}]}},{"Id":1495942,"IsEnable":false,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10003,"InvPtyId":{"Id":10003,"msterID":10003,"Number":"03","MultiLanguageText":[{"PkId":3,"LocaleId":2052,"Name":"BOM版本"},{"PkId":9,"LocaleId":1033,"Name":"BOM Version"}],"Name":[{"Key":2052,"Value":"BOM版本"},{"Key":1033,"Value":"BOM Version"}]}},{"Id":1495943,"IsEnable":false,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10004,"InvPtyId":{"Id":10004,"msterID":10004,"Number":"04","MultiLanguageText":[{"PkId":4,"LocaleId":2052,"Name":"批号"},{"PkId":10,"LocaleId":1033,"Name":"Lot No."}],"Name":[{"Key":2052,"Value":"批号"},{"Key":1033,"Value":"Lot No."}]}},{"Id":1495944,"IsEnable":false,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10006,"InvPtyId":{"Id":10006,"msterID":10006,"Number":"06","MultiLanguageText":[{"PkId":5,"LocaleId":2052,"Name":"计划跟踪号"},{"PkId":11,"LocaleId":1033,"Name":"Plan Tracking No."}],"Name":[{"Key":2052,"Value":"计划跟踪号"},{"Key":1033,"Value":"Plan Tracking No."}]}}],"MaterialSubcon":[{"Id":379204,"SubconUnitId_Id":10101,"SubconUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"SubconPriceUnitId_Id":10101,"SubconPriceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"SUBCONURNUM":1,"SUBCONURNOM":1,"SUBCONPRICEURNUM":1,"SUBCONPRICEURNOM":1,"SUBBILLTYPE_Id":"93904bf745d84ae0ad08da30949754e0","SUBBILLTYPE":{"Id":"93904bf745d84ae0ad08da30949754e0","MultiLanguageText":[{"PkId":"c0a9cc13-6893-4629-a0a1-0ea3fbcea629","LocaleId":1033,"Name":"Common Subcontract Order"},{"PkId":"ae166c94ae10412b8197a89f86d903e7","LocaleId":2052,"Name":"普通委外订单"}],"Name":[{"Key":1033,"Value":"Common Subcontract Order"},{"Key":2052,"Value":"普通委外订单"}],"Number":"WWDD01_SYS","IsDefault":true}}],"MaterialQM":[{"Id":379204,"CheckProduct":true,"CheckIncoming":true,"IncSampSchemeId_Id":0,"IncSampSchemeId":null,"IncQcSchemeId_Id":0,"IncQcSchemeId":null,"CheckStock":false,"EnableCyclistQCSTK":false,"EnableCyclistQCSTKEW":false,"EWLeadDay":0,"StockCycle":0,"CheckDelivery":false,"CheckReturn":false,"InspectGroupId_Id":0,"InspectGroupId":null,"InspectorId_Id":0,"InspectorId":null,"CheckEntrusted":false,"CheckOther":false}]}}
     */

    private ResultBeanX Result;

    public ResultBeanX getResult() {
        return Result;
    }

    public void setResult(ResultBeanX Result) {
        this.Result = Result;
    }

    public static class ResultBeanX {
        /**
         * ResponseStatus : null
         * Result : {"Id":505063,"msterID":260295,"DocumentStatus":"C","ForbidStatus":"A","MultiLanguageText":[{"PkId":379257,"LocaleId":2052,"Name":"二维码（星星充电SAAS版）","Description":" ","Specification":"DH-AC0070SAAS-001，113*58.5mm"}],"Name":[{"Key":2052,"Value":"二维码（星星充电SAAS版）"}],"Number":"AJGLAB0018","Description":[{"Key":2052,"Value":" "}],"CreateOrgId_Id":1,"CreateOrgId":{"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"},"UseOrgId_Id":500232,"UseOrgId":{"Id":500232,"MultiLanguageText":[{"PkId":100088,"LocaleId":2052,"Name":"温州星之帮新能源科技有限公司"}],"Name":[{"Key":2052,"Value":"温州星之帮新能源科技有限公司"}],"Number":"102059"},"CreatorId_Id":129041,"CreatorId":{"Id":129041,"Name":"何谐","UserAccount":" "},"ModifierId_Id":129041,"ModifierId":{"Id":129041,"Name":"何谐","UserAccount":" "},"CreateDate":"2019-01-10T17:05:01.987","ModifyDate":"2019-01-10T17:05:01.5","MnemonicCode":" ","Specification":[{"Key":2052,"Value":"DH-AC0070SAAS-001，113*58.5mm"}],"ForbidderId_Id":0,"ForbidderId":null,"ForbidDate":null,"ApproveDate":"2019-01-10T17:05:02.483","ApproverId_Id":129041,"ApproverId":{"Id":129041,"Name":"何谐","UserAccount":" "},"Image":null,"OldNumber":"IDC.0014","MaterialGroup_Id":159777,"MaterialGroup":{"Id":159777,"Number":"LAB","MultiLanguageText":[{"PkId":100078,"LocaleId":2052,"Name":"标贴"}],"Name":[{"Key":2052,"Value":"标贴"}]},"PLMMaterialId":" ","MaterialSRC":"B","IsValidate":false,"ImageFileServer":" ","ImgStorageType":"B","IsImgDataBase":null,"IsImgFileServer":null,"IsSalseByNet":false,"F_ztys":" ","F_pp":" ","F_qpp":" ","F_klx":" ","F_qcd":" ","F_WB_CWJZLB_Id":171751,"F_WB_CWJZLB":{"Id":171751,"msterID":171751,"MultiLanguageText":[{"PkId":100103,"LocaleId":2052,"Name":"德和-原材料-辅材类"}],"Name":[{"Key":2052,"Value":"德和-原材料-辅材类"}],"Number":"1007"},"F_WB_CD":" ","F_WB_DSFRZ":" ","XMGoodsTaxNo":" ","XMTaxPre":false,"XMTaxPreCon":" ","XMZeroTax":"4","XMGoodsNoVer":"30.0","F_WB_JZH":" ","MaterialBase":[{"Id":379204,"ErpClsID":"1","IsInventory":true,"IsSale":true,"IsAsset":false,"IsSubContract":false,"IsProduce":false,"IsPurchase":true,"IsRealTimeAccout":false,"BaseUnitId_Id":10101,"BaseUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"TaxType_Id":"63489f1e86944c989c58d97893a2aec2","TaxType":{"Id":"63489f1e86944c989c58d97893a2aec2","FNumber":"WLDSFL01_SYS","MultiLanguageText":[{"PkId":"912d2a22-2def-42e8-86ad-c91f907c58af","LocaleId":1033,"FDataValue":"Standard Tax Rate"},{"PkId":"1ac1f2dd4a584f838c73c939d1496e43","LocaleId":2052,"FDataValue":"标准税率"}],"FDataValue":[{"Key":1033,"Value":"Standard Tax Rate"},{"Key":2052,"Value":"标准税率"}]},"TypeID_Id":0,"TypeID":null,"CategoryID_Id":237,"CategoryID":{"Id":237,"msterID":237,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"原材料"},{"PkId":8,"LocaleId":1033,"Name":"Raw materials"}],"Name":[{"Key":2052,"Value":"原材料"},{"Key":1033,"Value":"Raw materials"}],"Number":"CHLB01_SYS"},"TaxRateId_Id":264,"TaxRateId":{"Id":264,"msterID":264,"MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"16%增值税"}],"Name":[{"Key":2052,"Value":"16%增值税"}],"Number":"SL31_SYS"},"BARCODE":" ","WEIGHTUNITID_Id":10095,"WEIGHTUNITID":{"Id":10095,"msterID":10095,"MultiLanguageText":[{"PkId":17,"LocaleId":2052,"Name":"千克"},{"PkId":35,"LocaleId":1033,"Name":"Kg"}],"Name":[{"Key":2052,"Value":"千克"},{"Key":1033,"Value":"Kg"}],"Number":"kg","IsBaseUnit":true,"UnitGroupId_Id":10085,"UnitGroupId":{"Id":10085,"msterID":10085,"Number":"Weight","MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"重量"},{"PkId":17,"LocaleId":1033,"Name":"Weight"}],"Name":[{"Key":2052,"Value":"重量"},{"Key":1033,"Value":"Weight"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10114,"ConvertType":"0"}]},"VOLUMEUNITID_Id":10087,"VOLUMEUNITID":{"Id":10087,"msterID":10087,"MultiLanguageText":[{"PkId":15,"LocaleId":2052,"Name":"米"},{"PkId":33,"LocaleId":1033,"Name":"Meter"}],"Name":[{"Key":2052,"Value":"米"},{"Key":1033,"Value":"Meter"}],"Number":"m","IsBaseUnit":true,"UnitGroupId_Id":10081,"UnitGroupId":{"Id":10081,"msterID":10081,"Number":"Length","MultiLanguageText":[{"PkId":6,"LocaleId":2052,"Name":"长度"},{"PkId":15,"LocaleId":1033,"Name":"Length"}],"Name":[{"Key":2052,"Value":"长度"},{"Key":1033,"Value":"Length"}]},"Precision":2,"RoundType":"1","UNITCONVERTRATE":[{"Id":10104,"ConvertType":"0"}]},"GROSSWEIGHT":0,"NETWEIGHT":0,"LENGTH":0,"VOLUME":0,"WIDTH":0,"HEIGHT":0,"CONFIGTYPE":" ","Suite":"0","CostPriceRate":0}],"MaterialStock":[{"Id":379204,"StoreUnitID_Id":10101,"StoreUnitID":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"AuxUnitID_Id":0,"AuxUnitID":null,"StockId_Id":0,"StockId":null,"IsLockStock":true,"BatchRuleID_Id":0,"BatchRuleID":null,"ExpUnit":" ","StockPlaceId_Id":0,"StockPlaceId":null,"OnlineLife":0,"ExpPeriod":0,"StoreURNum":1,"StoreURNom":1,"IsBatchManage":false,"IsKFPeriod":false,"IsExpParToFlot":false,"IsCycleCounting":false,"IsMustCounting":false,"CurrencyId_Id":1,"CurrencyId":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"RefCost":0,"CountCycle":"1","CountDay":1,"IsSNManage":false,"SNCodeRule_Id":0,"SNCodeRule":null,"SNUnit_Id":0,"SNUnit":null,"SafeStock":0,"ReOrderGood":0,"MinStock":0,"MaxStock":0,"UnitConvertDir":"1","IsEnableMinStock":false,"IsEnableSafeStock":false,"IsEnableMaxStock":false,"IsEnableReOrder":false,"EconReOrderQty":0,"IsSNPRDTracy":false,"SNGenerateTime":"1","SNManageType":"1","BoxStandardQty":0}],"MaterialSale":[{"Id":379204,"IsATPCheck":false,"SalePriceUnitId_Id":10101,"SalePriceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"SaleUnitId_Id":10101,"SaleUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"IsInvoice":false,"MaxQty":100000,"IsReturn":true,"MinQty":0,"IsReturnPart":false,"OrderQty":0,"SalePriceURNom":1,"SaleURNum":1,"SalePriceURNum":1,"SaleURNom":1,"OutStockLmtH":0,"OutStockLmtL":0,"AgentSalReduceRate":0,"AllowPublish":false,"ISAFTERSALE":true,"ISPRODUCTFILES":true,"ISWARRANTED":false,"FWARRANTY":0,"WARRANTYUNITID":"D","OutLmtUnit":"SAL","TaxCategoryCodeId_Id":0,"TaxCategoryCodeId":null,"SalGroup_Id":0,"SalGroup":null,"TaxDiscountsType":"0","IsTaxEnjoy":false}],"MaterialPurchase":[{"Id":379204,"PurchaseUnitID_Id":10101,"PurchaseUnitID":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"PurchaserId_Id":0,"PurchaserId":null,"DefaultVendor_Id":0,"DefaultVendor":null,"IsSourceControl":false,"IsPR":false,"ReceiveMinScale":0,"PurchaseGroupId_Id":0,"PurchaseGroupId":null,"ReceiveMaxScale":0,"PurchasePriceUnitId_Id":10101,"PurchasePriceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"IsVendorQualification":false,"ReceiveAdvanceDays":0,"ReceiveDelayDays":0,"PurURNum":1,"PurPriceURNum":1,"PurURNom":1,"PurPriceURNom":1,"IsQuota":false,"QuotaType":"1","AgentPurPlusRate":0,"ChargeID_Id":0,"ChargeID":null,"MinSplitQty":0,"BaseMinSplitQty":0,"IsVmiBusiness":false,"IsReturnMaterial":true,"EnableSL":false,"PurchaseOrgId_Id":0,"PurchaseOrgId":null,"DefBarCodeRuleId_Id":0,"DefBarCodeRuleId":null,"MinPackCount":0,"PrintCount":1,"POBillTypeId_Id":"93591469feb54ca2b08eb635f8b79de3","POBillTypeId":{"Id":"93591469feb54ca2b08eb635f8b79de3","MultiLanguageText":[{"PkId":"65a98b68-868c-407d-b337-e3d321635b44","LocaleId":1033,"Name":"Standard Purchase Requisition"},{"PkId":"a7b70d95ba34432387653c6937cae696","LocaleId":2052,"Name":"标准采购申请"}],"Name":[{"Key":1033,"Value":"Standard Purchase Requisition"},{"Key":2052,"Value":"标准采购申请"}],"Number":"CGSQD01_SYS","IsDefault":true}}],"MaterialPlan":[{"Id":379204,"PlanerID_Id":0,"PlanerID":null,"EOQ":1,"PlanningStrategy":"1","OrderPolicy":"0","PlanWorkshop_Id":0,"PlanWorkshop":null,"FixLeadTimeType":"1","FixLeadTime":15,"VarLeadTimeType":"1","VarLeadTime":0,"CheckLeadTimeType":"1","CheckLeadTime":0,"OrderIntervalTimeType":"3","OrderIntervalTime":0,"PlanIntervalsDays":1,"PlanBatchSplitQty":0,"PlanTimeZone":0,"RequestTimeZone":0,"IsMrpComReq":false,"ReserveType":"1","CanLeadDays":0,"LeadExtendDay":0,"DelayExtendDay":0,"CanDelayDays":999,"PlanOffsetTimeType":"1","PlanOffsetTime":0,"MinPOQty":50,"IncreaseQty":0,"MaxPOQty":100000,"VarLeadTimeLotSize":1,"BaseVarLeadTimeLotSize":1,"PlanGroupId_Id":0,"PlanGroupId":null,"MfgPolicyId_Id":40042,"MfgPolicyId":{"Id":40042,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"MTS10(考虑库存)"},{"PkId":6,"LocaleId":1033,"Name":"MTS10 (Include inventory)"}],"Name":[{"Key":2052,"Value":"MTS10(考虑库存)"},{"Key":1033,"Value":"MTS10 (Include inventory)"}],"Number":"ZZCL001_SYS","PlanMode":"0","Ato":false},"SupplySourceId_Id":0,"SupplySourceId":null,"TimeFactorId_Id":0,"TimeFactorId":null,"QtyFactorId_Id":0,"QtyFactorId":null,"PlanMode":"0","AllowPartDelay":true,"AllowPartAhead":false,"PLANSAFESTOCKQTY":0,"ATOSchemeId_Id":0,"ATOSchemeId":null}],"MaterialProduce":[{"Id":379204,"PickStockId_Id":0,"PickStockId":null,"BOMUnitId_Id":10101,"BOMUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"WorkShopId_Id":0,"WorkShopId":null,"IssueType":"1","ProduceUnitId_Id":10101,"ProduceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"IsKitting":false,"DefaultRouting_Id":0,"DefaultRouting":null,"IsCoby":false,"PerUnitStandHour":0,"BKFLTime":" ","FinishReceiptOverRate":0,"FinishReceiptShortRate":0,"PickBinId_Id":0,"PickBinId":null,"PrdURNum":1,"PrdURNom":1,"BOMURNum":1,"BOMURNom":1,"IsMainPrd":false,"IsCompleteSet":false,"OverControlMode":"3","MinIssueQty":1,"StdLaborPrePareTime":0,"StdLaborProcessTime":0,"StdMachinePrepareTime":0,"StdMachineProcessTime":0,"ConsumVolatility":0,"IsProductLine":false,"ProduceBillType_Id":" ","ProduceBillType":null,"OrgTrustBillType_Id":" ","OrgTrustBillType":null,"ISMinIssueQty":false,"IsECN":false,"MinIssueUnitId_Id":10101,"MinIssueUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"MDLID_Id":0,"MDLID":null,"MdlMaterialId_Id":0,"MdlMaterialId":null,"LossPercent":0,"IsSNCarryToParent":false,"StandHourUnitId":"3600","BackFlushType":"1"}],"MaterialAuxPty":[],"MaterialInvPty":[{"Id":1495940,"IsEnable":true,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10001,"InvPtyId":{"Id":10001,"msterID":10001,"Number":"01","MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"仓库"},{"PkId":7,"LocaleId":1033,"Name":"Warehouse"}],"Name":[{"Key":2052,"Value":"仓库"},{"Key":1033,"Value":"Warehouse"}]}},{"Id":1495941,"IsEnable":true,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10002,"InvPtyId":{"Id":10002,"msterID":10002,"Number":"02","MultiLanguageText":[{"PkId":2,"LocaleId":2052,"Name":"仓位"},{"PkId":8,"LocaleId":1033,"Name":"Bin"}],"Name":[{"Key":2052,"Value":"仓位"},{"Key":1033,"Value":"Bin"}]}},{"Id":1495942,"IsEnable":false,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10003,"InvPtyId":{"Id":10003,"msterID":10003,"Number":"03","MultiLanguageText":[{"PkId":3,"LocaleId":2052,"Name":"BOM版本"},{"PkId":9,"LocaleId":1033,"Name":"BOM Version"}],"Name":[{"Key":2052,"Value":"BOM版本"},{"Key":1033,"Value":"BOM Version"}]}},{"Id":1495943,"IsEnable":false,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10004,"InvPtyId":{"Id":10004,"msterID":10004,"Number":"04","MultiLanguageText":[{"PkId":4,"LocaleId":2052,"Name":"批号"},{"PkId":10,"LocaleId":1033,"Name":"Lot No."}],"Name":[{"Key":2052,"Value":"批号"},{"Key":1033,"Value":"Lot No."}]}},{"Id":1495944,"IsEnable":false,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10006,"InvPtyId":{"Id":10006,"msterID":10006,"Number":"06","MultiLanguageText":[{"PkId":5,"LocaleId":2052,"Name":"计划跟踪号"},{"PkId":11,"LocaleId":1033,"Name":"Plan Tracking No."}],"Name":[{"Key":2052,"Value":"计划跟踪号"},{"Key":1033,"Value":"Plan Tracking No."}]}}],"MaterialSubcon":[{"Id":379204,"SubconUnitId_Id":10101,"SubconUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"SubconPriceUnitId_Id":10101,"SubconPriceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"SUBCONURNUM":1,"SUBCONURNOM":1,"SUBCONPRICEURNUM":1,"SUBCONPRICEURNOM":1,"SUBBILLTYPE_Id":"93904bf745d84ae0ad08da30949754e0","SUBBILLTYPE":{"Id":"93904bf745d84ae0ad08da30949754e0","MultiLanguageText":[{"PkId":"c0a9cc13-6893-4629-a0a1-0ea3fbcea629","LocaleId":1033,"Name":"Common Subcontract Order"},{"PkId":"ae166c94ae10412b8197a89f86d903e7","LocaleId":2052,"Name":"普通委外订单"}],"Name":[{"Key":1033,"Value":"Common Subcontract Order"},{"Key":2052,"Value":"普通委外订单"}],"Number":"WWDD01_SYS","IsDefault":true}}],"MaterialQM":[{"Id":379204,"CheckProduct":true,"CheckIncoming":true,"IncSampSchemeId_Id":0,"IncSampSchemeId":null,"IncQcSchemeId_Id":0,"IncQcSchemeId":null,"CheckStock":false,"EnableCyclistQCSTK":false,"EnableCyclistQCSTKEW":false,"EWLeadDay":0,"StockCycle":0,"CheckDelivery":false,"CheckReturn":false,"InspectGroupId_Id":0,"InspectGroupId":null,"InspectorId_Id":0,"InspectorId":null,"CheckEntrusted":false,"CheckOther":false}]}
         */

        private Object ResponseStatus;
        private ResultBean Result;

        public Object getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(Object ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public ResultBean getResult() {
            return Result;
        }

        public void setResult(ResultBean Result) {
            this.Result = Result;
        }

        public static class ResultBean {
            /**
             * Id : 505063
             * msterID : 260295
             * DocumentStatus : C
             * ForbidStatus : A
             * MultiLanguageText : [{"PkId":379257,"LocaleId":2052,"Name":"二维码（星星充电SAAS版）","Description":" ","Specification":"DH-AC0070SAAS-001，113*58.5mm"}]
             * Name : [{"Key":2052,"Value":"二维码（星星充电SAAS版）"}]
             * Number : AJGLAB0018
             * Description : [{"Key":2052,"Value":" "}]
             * CreateOrgId_Id : 1
             * CreateOrgId : {"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"}
             * UseOrgId_Id : 500232
             * UseOrgId : {"Id":500232,"MultiLanguageText":[{"PkId":100088,"LocaleId":2052,"Name":"温州星之帮新能源科技有限公司"}],"Name":[{"Key":2052,"Value":"温州星之帮新能源科技有限公司"}],"Number":"102059"}
             * CreatorId_Id : 129041
             * CreatorId : {"Id":129041,"Name":"何谐","UserAccount":" "}
             * ModifierId_Id : 129041
             * ModifierId : {"Id":129041,"Name":"何谐","UserAccount":" "}
             * CreateDate : 2019-01-10T17:05:01.987
             * ModifyDate : 2019-01-10T17:05:01.5
             * MnemonicCode :
             * Specification : [{"Key":2052,"Value":"DH-AC0070SAAS-001，113*58.5mm"}]
             * ForbidderId_Id : 0
             * ForbidderId : null
             * ForbidDate : null
             * ApproveDate : 2019-01-10T17:05:02.483
             * ApproverId_Id : 129041
             * ApproverId : {"Id":129041,"Name":"何谐","UserAccount":" "}
             * Image : null
             * OldNumber : IDC.0014
             * MaterialGroup_Id : 159777
             * MaterialGroup : {"Id":159777,"Number":"LAB","MultiLanguageText":[{"PkId":100078,"LocaleId":2052,"Name":"标贴"}],"Name":[{"Key":2052,"Value":"标贴"}]}
             * PLMMaterialId :
             * MaterialSRC : B
             * IsValidate : false
             * ImageFileServer :
             * ImgStorageType : B
             * IsImgDataBase : null
             * IsImgFileServer : null
             * IsSalseByNet : false
             * F_ztys :
             * F_pp :
             * F_qpp :
             * F_klx :
             * F_qcd :
             * F_WB_CWJZLB_Id : 171751
             * F_WB_CWJZLB : {"Id":171751,"msterID":171751,"MultiLanguageText":[{"PkId":100103,"LocaleId":2052,"Name":"德和-原材料-辅材类"}],"Name":[{"Key":2052,"Value":"德和-原材料-辅材类"}],"Number":"1007"}
             * F_WB_CD :
             * F_WB_DSFRZ :
             * XMGoodsTaxNo :
             * XMTaxPre : false
             * XMTaxPreCon :
             * XMZeroTax : 4
             * XMGoodsNoVer : 30.0
             * F_WB_JZH :
             * MaterialBase : [{"Id":379204,"ErpClsID":"1","IsInventory":true,"IsSale":true,"IsAsset":false,"IsSubContract":false,"IsProduce":false,"IsPurchase":true,"IsRealTimeAccout":false,"BaseUnitId_Id":10101,"BaseUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"TaxType_Id":"63489f1e86944c989c58d97893a2aec2","TaxType":{"Id":"63489f1e86944c989c58d97893a2aec2","FNumber":"WLDSFL01_SYS","MultiLanguageText":[{"PkId":"912d2a22-2def-42e8-86ad-c91f907c58af","LocaleId":1033,"FDataValue":"Standard Tax Rate"},{"PkId":"1ac1f2dd4a584f838c73c939d1496e43","LocaleId":2052,"FDataValue":"标准税率"}],"FDataValue":[{"Key":1033,"Value":"Standard Tax Rate"},{"Key":2052,"Value":"标准税率"}]},"TypeID_Id":0,"TypeID":null,"CategoryID_Id":237,"CategoryID":{"Id":237,"msterID":237,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"原材料"},{"PkId":8,"LocaleId":1033,"Name":"Raw materials"}],"Name":[{"Key":2052,"Value":"原材料"},{"Key":1033,"Value":"Raw materials"}],"Number":"CHLB01_SYS"},"TaxRateId_Id":264,"TaxRateId":{"Id":264,"msterID":264,"MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"16%增值税"}],"Name":[{"Key":2052,"Value":"16%增值税"}],"Number":"SL31_SYS"},"BARCODE":" ","WEIGHTUNITID_Id":10095,"WEIGHTUNITID":{"Id":10095,"msterID":10095,"MultiLanguageText":[{"PkId":17,"LocaleId":2052,"Name":"千克"},{"PkId":35,"LocaleId":1033,"Name":"Kg"}],"Name":[{"Key":2052,"Value":"千克"},{"Key":1033,"Value":"Kg"}],"Number":"kg","IsBaseUnit":true,"UnitGroupId_Id":10085,"UnitGroupId":{"Id":10085,"msterID":10085,"Number":"Weight","MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"重量"},{"PkId":17,"LocaleId":1033,"Name":"Weight"}],"Name":[{"Key":2052,"Value":"重量"},{"Key":1033,"Value":"Weight"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10114,"ConvertType":"0"}]},"VOLUMEUNITID_Id":10087,"VOLUMEUNITID":{"Id":10087,"msterID":10087,"MultiLanguageText":[{"PkId":15,"LocaleId":2052,"Name":"米"},{"PkId":33,"LocaleId":1033,"Name":"Meter"}],"Name":[{"Key":2052,"Value":"米"},{"Key":1033,"Value":"Meter"}],"Number":"m","IsBaseUnit":true,"UnitGroupId_Id":10081,"UnitGroupId":{"Id":10081,"msterID":10081,"Number":"Length","MultiLanguageText":[{"PkId":6,"LocaleId":2052,"Name":"长度"},{"PkId":15,"LocaleId":1033,"Name":"Length"}],"Name":[{"Key":2052,"Value":"长度"},{"Key":1033,"Value":"Length"}]},"Precision":2,"RoundType":"1","UNITCONVERTRATE":[{"Id":10104,"ConvertType":"0"}]},"GROSSWEIGHT":0,"NETWEIGHT":0,"LENGTH":0,"VOLUME":0,"WIDTH":0,"HEIGHT":0,"CONFIGTYPE":" ","Suite":"0","CostPriceRate":0}]
             * MaterialStock : [{"Id":379204,"StoreUnitID_Id":10101,"StoreUnitID":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"AuxUnitID_Id":0,"AuxUnitID":null,"StockId_Id":0,"StockId":null,"IsLockStock":true,"BatchRuleID_Id":0,"BatchRuleID":null,"ExpUnit":" ","StockPlaceId_Id":0,"StockPlaceId":null,"OnlineLife":0,"ExpPeriod":0,"StoreURNum":1,"StoreURNom":1,"IsBatchManage":false,"IsKFPeriod":false,"IsExpParToFlot":false,"IsCycleCounting":false,"IsMustCounting":false,"CurrencyId_Id":1,"CurrencyId":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"RefCost":0,"CountCycle":"1","CountDay":1,"IsSNManage":false,"SNCodeRule_Id":0,"SNCodeRule":null,"SNUnit_Id":0,"SNUnit":null,"SafeStock":0,"ReOrderGood":0,"MinStock":0,"MaxStock":0,"UnitConvertDir":"1","IsEnableMinStock":false,"IsEnableSafeStock":false,"IsEnableMaxStock":false,"IsEnableReOrder":false,"EconReOrderQty":0,"IsSNPRDTracy":false,"SNGenerateTime":"1","SNManageType":"1","BoxStandardQty":0}]
             * MaterialSale : [{"Id":379204,"IsATPCheck":false,"SalePriceUnitId_Id":10101,"SalePriceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"SaleUnitId_Id":10101,"SaleUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"IsInvoice":false,"MaxQty":100000,"IsReturn":true,"MinQty":0,"IsReturnPart":false,"OrderQty":0,"SalePriceURNom":1,"SaleURNum":1,"SalePriceURNum":1,"SaleURNom":1,"OutStockLmtH":0,"OutStockLmtL":0,"AgentSalReduceRate":0,"AllowPublish":false,"ISAFTERSALE":true,"ISPRODUCTFILES":true,"ISWARRANTED":false,"FWARRANTY":0,"WARRANTYUNITID":"D","OutLmtUnit":"SAL","TaxCategoryCodeId_Id":0,"TaxCategoryCodeId":null,"SalGroup_Id":0,"SalGroup":null,"TaxDiscountsType":"0","IsTaxEnjoy":false}]
             * MaterialPurchase : [{"Id":379204,"PurchaseUnitID_Id":10101,"PurchaseUnitID":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"PurchaserId_Id":0,"PurchaserId":null,"DefaultVendor_Id":0,"DefaultVendor":null,"IsSourceControl":false,"IsPR":false,"ReceiveMinScale":0,"PurchaseGroupId_Id":0,"PurchaseGroupId":null,"ReceiveMaxScale":0,"PurchasePriceUnitId_Id":10101,"PurchasePriceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"IsVendorQualification":false,"ReceiveAdvanceDays":0,"ReceiveDelayDays":0,"PurURNum":1,"PurPriceURNum":1,"PurURNom":1,"PurPriceURNom":1,"IsQuota":false,"QuotaType":"1","AgentPurPlusRate":0,"ChargeID_Id":0,"ChargeID":null,"MinSplitQty":0,"BaseMinSplitQty":0,"IsVmiBusiness":false,"IsReturnMaterial":true,"EnableSL":false,"PurchaseOrgId_Id":0,"PurchaseOrgId":null,"DefBarCodeRuleId_Id":0,"DefBarCodeRuleId":null,"MinPackCount":0,"PrintCount":1,"POBillTypeId_Id":"93591469feb54ca2b08eb635f8b79de3","POBillTypeId":{"Id":"93591469feb54ca2b08eb635f8b79de3","MultiLanguageText":[{"PkId":"65a98b68-868c-407d-b337-e3d321635b44","LocaleId":1033,"Name":"Standard Purchase Requisition"},{"PkId":"a7b70d95ba34432387653c6937cae696","LocaleId":2052,"Name":"标准采购申请"}],"Name":[{"Key":1033,"Value":"Standard Purchase Requisition"},{"Key":2052,"Value":"标准采购申请"}],"Number":"CGSQD01_SYS","IsDefault":true}}]
             * MaterialPlan : [{"Id":379204,"PlanerID_Id":0,"PlanerID":null,"EOQ":1,"PlanningStrategy":"1","OrderPolicy":"0","PlanWorkshop_Id":0,"PlanWorkshop":null,"FixLeadTimeType":"1","FixLeadTime":15,"VarLeadTimeType":"1","VarLeadTime":0,"CheckLeadTimeType":"1","CheckLeadTime":0,"OrderIntervalTimeType":"3","OrderIntervalTime":0,"PlanIntervalsDays":1,"PlanBatchSplitQty":0,"PlanTimeZone":0,"RequestTimeZone":0,"IsMrpComReq":false,"ReserveType":"1","CanLeadDays":0,"LeadExtendDay":0,"DelayExtendDay":0,"CanDelayDays":999,"PlanOffsetTimeType":"1","PlanOffsetTime":0,"MinPOQty":50,"IncreaseQty":0,"MaxPOQty":100000,"VarLeadTimeLotSize":1,"BaseVarLeadTimeLotSize":1,"PlanGroupId_Id":0,"PlanGroupId":null,"MfgPolicyId_Id":40042,"MfgPolicyId":{"Id":40042,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"MTS10(考虑库存)"},{"PkId":6,"LocaleId":1033,"Name":"MTS10 (Include inventory)"}],"Name":[{"Key":2052,"Value":"MTS10(考虑库存)"},{"Key":1033,"Value":"MTS10 (Include inventory)"}],"Number":"ZZCL001_SYS","PlanMode":"0","Ato":false},"SupplySourceId_Id":0,"SupplySourceId":null,"TimeFactorId_Id":0,"TimeFactorId":null,"QtyFactorId_Id":0,"QtyFactorId":null,"PlanMode":"0","AllowPartDelay":true,"AllowPartAhead":false,"PLANSAFESTOCKQTY":0,"ATOSchemeId_Id":0,"ATOSchemeId":null}]
             * MaterialProduce : [{"Id":379204,"PickStockId_Id":0,"PickStockId":null,"BOMUnitId_Id":10101,"BOMUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"WorkShopId_Id":0,"WorkShopId":null,"IssueType":"1","ProduceUnitId_Id":10101,"ProduceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"IsKitting":false,"DefaultRouting_Id":0,"DefaultRouting":null,"IsCoby":false,"PerUnitStandHour":0,"BKFLTime":" ","FinishReceiptOverRate":0,"FinishReceiptShortRate":0,"PickBinId_Id":0,"PickBinId":null,"PrdURNum":1,"PrdURNom":1,"BOMURNum":1,"BOMURNom":1,"IsMainPrd":false,"IsCompleteSet":false,"OverControlMode":"3","MinIssueQty":1,"StdLaborPrePareTime":0,"StdLaborProcessTime":0,"StdMachinePrepareTime":0,"StdMachineProcessTime":0,"ConsumVolatility":0,"IsProductLine":false,"ProduceBillType_Id":" ","ProduceBillType":null,"OrgTrustBillType_Id":" ","OrgTrustBillType":null,"ISMinIssueQty":false,"IsECN":false,"MinIssueUnitId_Id":10101,"MinIssueUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"MDLID_Id":0,"MDLID":null,"MdlMaterialId_Id":0,"MdlMaterialId":null,"LossPercent":0,"IsSNCarryToParent":false,"StandHourUnitId":"3600","BackFlushType":"1"}]
             * MaterialAuxPty : []
             * MaterialInvPty : [{"Id":1495940,"IsEnable":true,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10001,"InvPtyId":{"Id":10001,"msterID":10001,"Number":"01","MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"仓库"},{"PkId":7,"LocaleId":1033,"Name":"Warehouse"}],"Name":[{"Key":2052,"Value":"仓库"},{"Key":1033,"Value":"Warehouse"}]}},{"Id":1495941,"IsEnable":true,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10002,"InvPtyId":{"Id":10002,"msterID":10002,"Number":"02","MultiLanguageText":[{"PkId":2,"LocaleId":2052,"Name":"仓位"},{"PkId":8,"LocaleId":1033,"Name":"Bin"}],"Name":[{"Key":2052,"Value":"仓位"},{"Key":1033,"Value":"Bin"}]}},{"Id":1495942,"IsEnable":false,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10003,"InvPtyId":{"Id":10003,"msterID":10003,"Number":"03","MultiLanguageText":[{"PkId":3,"LocaleId":2052,"Name":"BOM版本"},{"PkId":9,"LocaleId":1033,"Name":"BOM Version"}],"Name":[{"Key":2052,"Value":"BOM版本"},{"Key":1033,"Value":"BOM Version"}]}},{"Id":1495943,"IsEnable":false,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10004,"InvPtyId":{"Id":10004,"msterID":10004,"Number":"04","MultiLanguageText":[{"PkId":4,"LocaleId":2052,"Name":"批号"},{"PkId":10,"LocaleId":1033,"Name":"Lot No."}],"Name":[{"Key":2052,"Value":"批号"},{"Key":1033,"Value":"Lot No."}]}},{"Id":1495944,"IsEnable":false,"IsAffectPrice":false,"IsAffectPlan":false,"IsAffectCost":false,"InvPtyId_Id":10006,"InvPtyId":{"Id":10006,"msterID":10006,"Number":"06","MultiLanguageText":[{"PkId":5,"LocaleId":2052,"Name":"计划跟踪号"},{"PkId":11,"LocaleId":1033,"Name":"Plan Tracking No."}],"Name":[{"Key":2052,"Value":"计划跟踪号"},{"Key":1033,"Value":"Plan Tracking No."}]}}]
             * MaterialSubcon : [{"Id":379204,"SubconUnitId_Id":10101,"SubconUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"SubconPriceUnitId_Id":10101,"SubconPriceUnitId":{"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]},"SUBCONURNUM":1,"SUBCONURNOM":1,"SUBCONPRICEURNUM":1,"SUBCONPRICEURNOM":1,"SUBBILLTYPE_Id":"93904bf745d84ae0ad08da30949754e0","SUBBILLTYPE":{"Id":"93904bf745d84ae0ad08da30949754e0","MultiLanguageText":[{"PkId":"c0a9cc13-6893-4629-a0a1-0ea3fbcea629","LocaleId":1033,"Name":"Common Subcontract Order"},{"PkId":"ae166c94ae10412b8197a89f86d903e7","LocaleId":2052,"Name":"普通委外订单"}],"Name":[{"Key":1033,"Value":"Common Subcontract Order"},{"Key":2052,"Value":"普通委外订单"}],"Number":"WWDD01_SYS","IsDefault":true}}]
             * MaterialQM : [{"Id":379204,"CheckProduct":true,"CheckIncoming":true,"IncSampSchemeId_Id":0,"IncSampSchemeId":null,"IncQcSchemeId_Id":0,"IncQcSchemeId":null,"CheckStock":false,"EnableCyclistQCSTK":false,"EnableCyclistQCSTKEW":false,"EWLeadDay":0,"StockCycle":0,"CheckDelivery":false,"CheckReturn":false,"InspectGroupId_Id":0,"InspectGroupId":null,"InspectorId_Id":0,"InspectorId":null,"CheckEntrusted":false,"CheckOther":false}]
             */

            private int Id;
            private int msterID;
            private String DocumentStatus;
            private String ForbidStatus;
            private String Number;
            private int CreateOrgId_Id;
            private CreateOrgIdBean CreateOrgId;
            private int UseOrgId_Id;
            private UseOrgIdBean UseOrgId;
            private int CreatorId_Id;
            private CreatorIdBean CreatorId;
            private int ModifierId_Id;
            private ModifierIdBean ModifierId;
            private String CreateDate;
            private String ModifyDate;
            private String MnemonicCode;
            private int ForbidderId_Id;
            private Object ForbidderId;
            private Object ForbidDate;
            private String ApproveDate;
            private int ApproverId_Id;
            private ApproverIdBean ApproverId;
            private Object Image;
            private String OldNumber;
            private int MaterialGroup_Id;
            private MaterialGroupBean MaterialGroup;
            private String PLMMaterialId;
            private String MaterialSRC;
            private boolean IsValidate;
            private String ImageFileServer;
            private String ImgStorageType;
            private Object IsImgDataBase;
            private Object IsImgFileServer;
            private boolean IsSalseByNet;
            private String F_ztys;
            private String F_pp;
            private String F_qpp;
            private String F_klx;
            private String F_qcd;
            private int F_WB_CWJZLB_Id;
            private FWBCWJZLBBean F_WB_CWJZLB;
            private String F_WB_CD;
            private String F_WB_DSFRZ;
            private String XMGoodsTaxNo;
            private boolean XMTaxPre;
            private String XMTaxPreCon;
            private String XMZeroTax;
            private String XMGoodsNoVer;
            private String F_WB_JZH;
            private List<MultiLanguageTextBeanXXXX> MultiLanguageText;
            private List<NameBeanXXXX> Name;
            private List<DescriptionBean> Description;
            private List<SpecificationBean> Specification;
            private List<MaterialBaseBean> MaterialBase;
            private List<MaterialStockBean> MaterialStock;
            private List<MaterialSaleBean> MaterialSale;
            private List<MaterialPurchaseBean> MaterialPurchase;
            private List<MaterialPlanBean> MaterialPlan;
            private List<MaterialProduceBean> MaterialProduce;
            private List<?> MaterialAuxPty;
            private List<MaterialInvPtyBean> MaterialInvPty;
            private List<MaterialSubconBean> MaterialSubcon;
            private List<MaterialQMBean> MaterialQM;

            public int getId() {
                return Id;
            }

            public void setId(int Id) {
                this.Id = Id;
            }

            public int getMsterID() {
                return msterID;
            }

            public void setMsterID(int msterID) {
                this.msterID = msterID;
            }

            public String getDocumentStatus() {
                return DocumentStatus;
            }

            public void setDocumentStatus(String DocumentStatus) {
                this.DocumentStatus = DocumentStatus;
            }

            public String getForbidStatus() {
                return ForbidStatus;
            }

            public void setForbidStatus(String ForbidStatus) {
                this.ForbidStatus = ForbidStatus;
            }

            public String getNumber() {
                return Number;
            }

            public void setNumber(String Number) {
                this.Number = Number;
            }

            public int getCreateOrgId_Id() {
                return CreateOrgId_Id;
            }

            public void setCreateOrgId_Id(int CreateOrgId_Id) {
                this.CreateOrgId_Id = CreateOrgId_Id;
            }

            public CreateOrgIdBean getCreateOrgId() {
                return CreateOrgId;
            }

            public void setCreateOrgId(CreateOrgIdBean CreateOrgId) {
                this.CreateOrgId = CreateOrgId;
            }

            public int getUseOrgId_Id() {
                return UseOrgId_Id;
            }

            public void setUseOrgId_Id(int UseOrgId_Id) {
                this.UseOrgId_Id = UseOrgId_Id;
            }

            public UseOrgIdBean getUseOrgId() {
                return UseOrgId;
            }

            public void setUseOrgId(UseOrgIdBean UseOrgId) {
                this.UseOrgId = UseOrgId;
            }

            public int getCreatorId_Id() {
                return CreatorId_Id;
            }

            public void setCreatorId_Id(int CreatorId_Id) {
                this.CreatorId_Id = CreatorId_Id;
            }

            public CreatorIdBean getCreatorId() {
                return CreatorId;
            }

            public void setCreatorId(CreatorIdBean CreatorId) {
                this.CreatorId = CreatorId;
            }

            public int getModifierId_Id() {
                return ModifierId_Id;
            }

            public void setModifierId_Id(int ModifierId_Id) {
                this.ModifierId_Id = ModifierId_Id;
            }

            public ModifierIdBean getModifierId() {
                return ModifierId;
            }

            public void setModifierId(ModifierIdBean ModifierId) {
                this.ModifierId = ModifierId;
            }

            public String getCreateDate() {
                return CreateDate;
            }

            public void setCreateDate(String CreateDate) {
                this.CreateDate = CreateDate;
            }

            public String getModifyDate() {
                return ModifyDate;
            }

            public void setModifyDate(String ModifyDate) {
                this.ModifyDate = ModifyDate;
            }

            public String getMnemonicCode() {
                return MnemonicCode;
            }

            public void setMnemonicCode(String MnemonicCode) {
                this.MnemonicCode = MnemonicCode;
            }

            public int getForbidderId_Id() {
                return ForbidderId_Id;
            }

            public void setForbidderId_Id(int ForbidderId_Id) {
                this.ForbidderId_Id = ForbidderId_Id;
            }

            public Object getForbidderId() {
                return ForbidderId;
            }

            public void setForbidderId(Object ForbidderId) {
                this.ForbidderId = ForbidderId;
            }

            public Object getForbidDate() {
                return ForbidDate;
            }

            public void setForbidDate(Object ForbidDate) {
                this.ForbidDate = ForbidDate;
            }

            public String getApproveDate() {
                return ApproveDate;
            }

            public void setApproveDate(String ApproveDate) {
                this.ApproveDate = ApproveDate;
            }

            public int getApproverId_Id() {
                return ApproverId_Id;
            }

            public void setApproverId_Id(int ApproverId_Id) {
                this.ApproverId_Id = ApproverId_Id;
            }

            public ApproverIdBean getApproverId() {
                return ApproverId;
            }

            public void setApproverId(ApproverIdBean ApproverId) {
                this.ApproverId = ApproverId;
            }

            public Object getImage() {
                return Image;
            }

            public void setImage(Object Image) {
                this.Image = Image;
            }

            public String getOldNumber() {
                return OldNumber;
            }

            public void setOldNumber(String OldNumber) {
                this.OldNumber = OldNumber;
            }

            public int getMaterialGroup_Id() {
                return MaterialGroup_Id;
            }

            public void setMaterialGroup_Id(int MaterialGroup_Id) {
                this.MaterialGroup_Id = MaterialGroup_Id;
            }

            public MaterialGroupBean getMaterialGroup() {
                return MaterialGroup;
            }

            public void setMaterialGroup(MaterialGroupBean MaterialGroup) {
                this.MaterialGroup = MaterialGroup;
            }

            public String getPLMMaterialId() {
                return PLMMaterialId;
            }

            public void setPLMMaterialId(String PLMMaterialId) {
                this.PLMMaterialId = PLMMaterialId;
            }

            public String getMaterialSRC() {
                return MaterialSRC;
            }

            public void setMaterialSRC(String MaterialSRC) {
                this.MaterialSRC = MaterialSRC;
            }

            public boolean isIsValidate() {
                return IsValidate;
            }

            public void setIsValidate(boolean IsValidate) {
                this.IsValidate = IsValidate;
            }

            public String getImageFileServer() {
                return ImageFileServer;
            }

            public void setImageFileServer(String ImageFileServer) {
                this.ImageFileServer = ImageFileServer;
            }

            public String getImgStorageType() {
                return ImgStorageType;
            }

            public void setImgStorageType(String ImgStorageType) {
                this.ImgStorageType = ImgStorageType;
            }

            public Object getIsImgDataBase() {
                return IsImgDataBase;
            }

            public void setIsImgDataBase(Object IsImgDataBase) {
                this.IsImgDataBase = IsImgDataBase;
            }

            public Object getIsImgFileServer() {
                return IsImgFileServer;
            }

            public void setIsImgFileServer(Object IsImgFileServer) {
                this.IsImgFileServer = IsImgFileServer;
            }

            public boolean isIsSalseByNet() {
                return IsSalseByNet;
            }

            public void setIsSalseByNet(boolean IsSalseByNet) {
                this.IsSalseByNet = IsSalseByNet;
            }

            public String getF_ztys() {
                return F_ztys;
            }

            public void setF_ztys(String F_ztys) {
                this.F_ztys = F_ztys;
            }

            public String getF_pp() {
                return F_pp;
            }

            public void setF_pp(String F_pp) {
                this.F_pp = F_pp;
            }

            public String getF_qpp() {
                return F_qpp;
            }

            public void setF_qpp(String F_qpp) {
                this.F_qpp = F_qpp;
            }

            public String getF_klx() {
                return F_klx;
            }

            public void setF_klx(String F_klx) {
                this.F_klx = F_klx;
            }

            public String getF_qcd() {
                return F_qcd;
            }

            public void setF_qcd(String F_qcd) {
                this.F_qcd = F_qcd;
            }

            public int getF_WB_CWJZLB_Id() {
                return F_WB_CWJZLB_Id;
            }

            public void setF_WB_CWJZLB_Id(int F_WB_CWJZLB_Id) {
                this.F_WB_CWJZLB_Id = F_WB_CWJZLB_Id;
            }

            public FWBCWJZLBBean getF_WB_CWJZLB() {
                return F_WB_CWJZLB;
            }

            public void setF_WB_CWJZLB(FWBCWJZLBBean F_WB_CWJZLB) {
                this.F_WB_CWJZLB = F_WB_CWJZLB;
            }

            public String getF_WB_CD() {
                return F_WB_CD;
            }

            public void setF_WB_CD(String F_WB_CD) {
                this.F_WB_CD = F_WB_CD;
            }

            public String getF_WB_DSFRZ() {
                return F_WB_DSFRZ;
            }

            public void setF_WB_DSFRZ(String F_WB_DSFRZ) {
                this.F_WB_DSFRZ = F_WB_DSFRZ;
            }

            public String getXMGoodsTaxNo() {
                return XMGoodsTaxNo;
            }

            public void setXMGoodsTaxNo(String XMGoodsTaxNo) {
                this.XMGoodsTaxNo = XMGoodsTaxNo;
            }

            public boolean isXMTaxPre() {
                return XMTaxPre;
            }

            public void setXMTaxPre(boolean XMTaxPre) {
                this.XMTaxPre = XMTaxPre;
            }

            public String getXMTaxPreCon() {
                return XMTaxPreCon;
            }

            public void setXMTaxPreCon(String XMTaxPreCon) {
                this.XMTaxPreCon = XMTaxPreCon;
            }

            public String getXMZeroTax() {
                return XMZeroTax;
            }

            public void setXMZeroTax(String XMZeroTax) {
                this.XMZeroTax = XMZeroTax;
            }

            public String getXMGoodsNoVer() {
                return XMGoodsNoVer;
            }

            public void setXMGoodsNoVer(String XMGoodsNoVer) {
                this.XMGoodsNoVer = XMGoodsNoVer;
            }

            public String getF_WB_JZH() {
                return F_WB_JZH;
            }

            public void setF_WB_JZH(String F_WB_JZH) {
                this.F_WB_JZH = F_WB_JZH;
            }

            public List<MultiLanguageTextBeanXXXX> getMultiLanguageText() {
                return MultiLanguageText;
            }

            public void setMultiLanguageText(List<MultiLanguageTextBeanXXXX> MultiLanguageText) {
                this.MultiLanguageText = MultiLanguageText;
            }

            public List<NameBeanXXXX> getName() {
                return Name;
            }

            public void setName(List<NameBeanXXXX> Name) {
                this.Name = Name;
            }

            public List<DescriptionBean> getDescription() {
                return Description;
            }

            public void setDescription(List<DescriptionBean> Description) {
                this.Description = Description;
            }

            public List<SpecificationBean> getSpecification() {
                return Specification;
            }

            public void setSpecification(List<SpecificationBean> Specification) {
                this.Specification = Specification;
            }

            public List<MaterialBaseBean> getMaterialBase() {
                return MaterialBase;
            }

            public void setMaterialBase(List<MaterialBaseBean> MaterialBase) {
                this.MaterialBase = MaterialBase;
            }

            public List<MaterialStockBean> getMaterialStock() {
                return MaterialStock;
            }

            public void setMaterialStock(List<MaterialStockBean> MaterialStock) {
                this.MaterialStock = MaterialStock;
            }

            public List<MaterialSaleBean> getMaterialSale() {
                return MaterialSale;
            }

            public void setMaterialSale(List<MaterialSaleBean> MaterialSale) {
                this.MaterialSale = MaterialSale;
            }

            public List<MaterialPurchaseBean> getMaterialPurchase() {
                return MaterialPurchase;
            }

            public void setMaterialPurchase(List<MaterialPurchaseBean> MaterialPurchase) {
                this.MaterialPurchase = MaterialPurchase;
            }

            public List<MaterialPlanBean> getMaterialPlan() {
                return MaterialPlan;
            }

            public void setMaterialPlan(List<MaterialPlanBean> MaterialPlan) {
                this.MaterialPlan = MaterialPlan;
            }

            public List<MaterialProduceBean> getMaterialProduce() {
                return MaterialProduce;
            }

            public void setMaterialProduce(List<MaterialProduceBean> MaterialProduce) {
                this.MaterialProduce = MaterialProduce;
            }

            public List<?> getMaterialAuxPty() {
                return MaterialAuxPty;
            }

            public void setMaterialAuxPty(List<?> MaterialAuxPty) {
                this.MaterialAuxPty = MaterialAuxPty;
            }

            public List<MaterialInvPtyBean> getMaterialInvPty() {
                return MaterialInvPty;
            }

            public void setMaterialInvPty(List<MaterialInvPtyBean> MaterialInvPty) {
                this.MaterialInvPty = MaterialInvPty;
            }

            public List<MaterialSubconBean> getMaterialSubcon() {
                return MaterialSubcon;
            }

            public void setMaterialSubcon(List<MaterialSubconBean> MaterialSubcon) {
                this.MaterialSubcon = MaterialSubcon;
            }

            public List<MaterialQMBean> getMaterialQM() {
                return MaterialQM;
            }

            public void setMaterialQM(List<MaterialQMBean> MaterialQM) {
                this.MaterialQM = MaterialQM;
            }

            public static class CreateOrgIdBean {
                /**
                 * Id : 1
                 * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}]
                 * Name : [{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}]
                 * Number : 999
                 */

                private int Id;
                private String Number;
                private List<MultiLanguageTextBean> MultiLanguageText;
                private List<NameBean> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBean> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBean> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBean> getName() {
                    return Name;
                }

                public void setName(List<NameBean> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBean {
                    /**
                     * PkId : 1
                     * LocaleId : 2052
                     * Name : 万帮集团
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBean {
                    /**
                     * Key : 2052
                     * Value : 万帮集团
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class UseOrgIdBean {
                /**
                 * Id : 500232
                 * MultiLanguageText : [{"PkId":100088,"LocaleId":2052,"Name":"温州星之帮新能源科技有限公司"}]
                 * Name : [{"Key":2052,"Value":"温州星之帮新能源科技有限公司"}]
                 * Number : 102059
                 */

                private int Id;
                private String Number;
                private List<MultiLanguageTextBeanX> MultiLanguageText;
                private List<NameBeanX> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBeanX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBeanX> getName() {
                    return Name;
                }

                public void setName(List<NameBeanX> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBeanX {
                    /**
                     * PkId : 100088
                     * LocaleId : 2052
                     * Name : 温州星之帮新能源科技有限公司
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBeanX {
                    /**
                     * Key : 2052
                     * Value : 温州星之帮新能源科技有限公司
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class CreatorIdBean {
                /**
                 * Id : 129041
                 * Name : 何谐
                 * UserAccount :
                 */

                private int Id;
                private String Name;
                private String UserAccount;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getUserAccount() {
                    return UserAccount;
                }

                public void setUserAccount(String UserAccount) {
                    this.UserAccount = UserAccount;
                }
            }

            public static class ModifierIdBean {
                /**
                 * Id : 129041
                 * Name : 何谐
                 * UserAccount :
                 */

                private int Id;
                private String Name;
                private String UserAccount;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getUserAccount() {
                    return UserAccount;
                }

                public void setUserAccount(String UserAccount) {
                    this.UserAccount = UserAccount;
                }
            }

            public static class ApproverIdBean {
                /**
                 * Id : 129041
                 * Name : 何谐
                 * UserAccount :
                 */

                private int Id;
                private String Name;
                private String UserAccount;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getUserAccount() {
                    return UserAccount;
                }

                public void setUserAccount(String UserAccount) {
                    this.UserAccount = UserAccount;
                }
            }

            public static class MaterialGroupBean {
                /**
                 * Id : 159777
                 * Number : LAB
                 * MultiLanguageText : [{"PkId":100078,"LocaleId":2052,"Name":"标贴"}]
                 * Name : [{"Key":2052,"Value":"标贴"}]
                 */

                private int Id;
                private String Number;
                private List<MultiLanguageTextBeanXX> MultiLanguageText;
                private List<NameBeanXX> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBeanXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBeanXX> getName() {
                    return Name;
                }

                public void setName(List<NameBeanXX> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBeanXX {
                    /**
                     * PkId : 100078
                     * LocaleId : 2052
                     * Name : 标贴
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBeanXX {
                    /**
                     * Key : 2052
                     * Value : 标贴
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class FWBCWJZLBBean {
                /**
                 * Id : 171751
                 * msterID : 171751
                 * MultiLanguageText : [{"PkId":100103,"LocaleId":2052,"Name":"德和-原材料-辅材类"}]
                 * Name : [{"Key":2052,"Value":"德和-原材料-辅材类"}]
                 * Number : 1007
                 */

                private int Id;
                private int msterID;
                private String Number;
                private List<MultiLanguageTextBeanXXX> MultiLanguageText;
                private List<NameBeanXXX> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getMsterID() {
                    return msterID;
                }

                public void setMsterID(int msterID) {
                    this.msterID = msterID;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBeanXXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBeanXXX> getName() {
                    return Name;
                }

                public void setName(List<NameBeanXXX> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBeanXXX {
                    /**
                     * PkId : 100103
                     * LocaleId : 2052
                     * Name : 德和-原材料-辅材类
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBeanXXX {
                    /**
                     * Key : 2052
                     * Value : 德和-原材料-辅材类
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class MultiLanguageTextBeanXXXX {
                /**
                 * PkId : 379257
                 * LocaleId : 2052
                 * Name : 二维码（星星充电SAAS版）
                 * Description :
                 * Specification : DH-AC0070SAAS-001，113*58.5mm
                 */

                private int PkId;
                private int LocaleId;
                private String Name;
                private String Description;
                private String Specification;

                public int getPkId() {
                    return PkId;
                }

                public void setPkId(int PkId) {
                    this.PkId = PkId;
                }

                public int getLocaleId() {
                    return LocaleId;
                }

                public void setLocaleId(int LocaleId) {
                    this.LocaleId = LocaleId;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getDescription() {
                    return Description;
                }

                public void setDescription(String Description) {
                    this.Description = Description;
                }

                public String getSpecification() {
                    return Specification;
                }

                public void setSpecification(String Specification) {
                    this.Specification = Specification;
                }
            }

            public static class NameBeanXXXX {
                /**
                 * Key : 2052
                 * Value : 二维码（星星充电SAAS版）
                 */

                private int Key;
                private String Value;

                public int getKey() {
                    return Key;
                }

                public void setKey(int Key) {
                    this.Key = Key;
                }

                public String getValue() {
                    return Value;
                }

                public void setValue(String Value) {
                    this.Value = Value;
                }
            }

            public static class DescriptionBean {
                /**
                 * Key : 2052
                 * Value :
                 */

                private int Key;
                private String Value;

                public int getKey() {
                    return Key;
                }

                public void setKey(int Key) {
                    this.Key = Key;
                }

                public String getValue() {
                    return Value;
                }

                public void setValue(String Value) {
                    this.Value = Value;
                }
            }

            public static class SpecificationBean {
                /**
                 * Key : 2052
                 * Value : DH-AC0070SAAS-001，113*58.5mm
                 */

                private int Key;
                private String Value;

                public int getKey() {
                    return Key;
                }

                public void setKey(int Key) {
                    this.Key = Key;
                }

                public String getValue() {
                    return Value;
                }

                public void setValue(String Value) {
                    this.Value = Value;
                }
            }

            public static class MaterialBaseBean {
                /**
                 * Id : 379204
                 * ErpClsID : 1
                 * IsInventory : true
                 * IsSale : true
                 * IsAsset : false
                 * IsSubContract : false
                 * IsProduce : false
                 * IsPurchase : true
                 * IsRealTimeAccout : false
                 * BaseUnitId_Id : 10101
                 * BaseUnitId : {"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]}
                 * TaxType_Id : 63489f1e86944c989c58d97893a2aec2
                 * TaxType : {"Id":"63489f1e86944c989c58d97893a2aec2","FNumber":"WLDSFL01_SYS","MultiLanguageText":[{"PkId":"912d2a22-2def-42e8-86ad-c91f907c58af","LocaleId":1033,"FDataValue":"Standard Tax Rate"},{"PkId":"1ac1f2dd4a584f838c73c939d1496e43","LocaleId":2052,"FDataValue":"标准税率"}],"FDataValue":[{"Key":1033,"Value":"Standard Tax Rate"},{"Key":2052,"Value":"标准税率"}]}
                 * TypeID_Id : 0
                 * TypeID : null
                 * CategoryID_Id : 237
                 * CategoryID : {"Id":237,"msterID":237,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"原材料"},{"PkId":8,"LocaleId":1033,"Name":"Raw materials"}],"Name":[{"Key":2052,"Value":"原材料"},{"Key":1033,"Value":"Raw materials"}],"Number":"CHLB01_SYS"}
                 * TaxRateId_Id : 264
                 * TaxRateId : {"Id":264,"msterID":264,"MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"16%增值税"}],"Name":[{"Key":2052,"Value":"16%增值税"}],"Number":"SL31_SYS"}
                 * BARCODE :
                 * WEIGHTUNITID_Id : 10095
                 * WEIGHTUNITID : {"Id":10095,"msterID":10095,"MultiLanguageText":[{"PkId":17,"LocaleId":2052,"Name":"千克"},{"PkId":35,"LocaleId":1033,"Name":"Kg"}],"Name":[{"Key":2052,"Value":"千克"},{"Key":1033,"Value":"Kg"}],"Number":"kg","IsBaseUnit":true,"UnitGroupId_Id":10085,"UnitGroupId":{"Id":10085,"msterID":10085,"Number":"Weight","MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"重量"},{"PkId":17,"LocaleId":1033,"Name":"Weight"}],"Name":[{"Key":2052,"Value":"重量"},{"Key":1033,"Value":"Weight"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10114,"ConvertType":"0"}]}
                 * VOLUMEUNITID_Id : 10087
                 * VOLUMEUNITID : {"Id":10087,"msterID":10087,"MultiLanguageText":[{"PkId":15,"LocaleId":2052,"Name":"米"},{"PkId":33,"LocaleId":1033,"Name":"Meter"}],"Name":[{"Key":2052,"Value":"米"},{"Key":1033,"Value":"Meter"}],"Number":"m","IsBaseUnit":true,"UnitGroupId_Id":10081,"UnitGroupId":{"Id":10081,"msterID":10081,"Number":"Length","MultiLanguageText":[{"PkId":6,"LocaleId":2052,"Name":"长度"},{"PkId":15,"LocaleId":1033,"Name":"Length"}],"Name":[{"Key":2052,"Value":"长度"},{"Key":1033,"Value":"Length"}]},"Precision":2,"RoundType":"1","UNITCONVERTRATE":[{"Id":10104,"ConvertType":"0"}]}
                 * GROSSWEIGHT : 0
                 * NETWEIGHT : 0
                 * LENGTH : 0
                 * VOLUME : 0
                 * WIDTH : 0
                 * HEIGHT : 0
                 * CONFIGTYPE :
                 * Suite : 0
                 * CostPriceRate : 0
                 */

                private int Id;
                private String ErpClsID;
                private boolean IsInventory;
                private boolean IsSale;
                private boolean IsAsset;
                private boolean IsSubContract;
                private boolean IsProduce;
                private boolean IsPurchase;
                private boolean IsRealTimeAccout;
                private int BaseUnitId_Id;
                private BaseUnitIdBean BaseUnitId;
                private String TaxType_Id;
                private TaxTypeBean TaxType;
                private int TypeID_Id;
                private Object TypeID;
                private int CategoryID_Id;
                private CategoryIDBean CategoryID;
                private int TaxRateId_Id;
                private TaxRateIdBean TaxRateId;
                private String BARCODE;
                private int WEIGHTUNITID_Id;
                private WEIGHTUNITIDBean WEIGHTUNITID;
                private int VOLUMEUNITID_Id;
                private VOLUMEUNITIDBean VOLUMEUNITID;
                private int GROSSWEIGHT;
                private int NETWEIGHT;
                private int LENGTH;
                private int VOLUME;
                private int WIDTH;
                private int HEIGHT;
                private String CONFIGTYPE;
                private String Suite;
                private int CostPriceRate;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getErpClsID() {
                    return ErpClsID;
                }

                public void setErpClsID(String ErpClsID) {
                    this.ErpClsID = ErpClsID;
                }

                public boolean isIsInventory() {
                    return IsInventory;
                }

                public void setIsInventory(boolean IsInventory) {
                    this.IsInventory = IsInventory;
                }

                public boolean isIsSale() {
                    return IsSale;
                }

                public void setIsSale(boolean IsSale) {
                    this.IsSale = IsSale;
                }

                public boolean isIsAsset() {
                    return IsAsset;
                }

                public void setIsAsset(boolean IsAsset) {
                    this.IsAsset = IsAsset;
                }

                public boolean isIsSubContract() {
                    return IsSubContract;
                }

                public void setIsSubContract(boolean IsSubContract) {
                    this.IsSubContract = IsSubContract;
                }

                public boolean isIsProduce() {
                    return IsProduce;
                }

                public void setIsProduce(boolean IsProduce) {
                    this.IsProduce = IsProduce;
                }

                public boolean isIsPurchase() {
                    return IsPurchase;
                }

                public void setIsPurchase(boolean IsPurchase) {
                    this.IsPurchase = IsPurchase;
                }

                public boolean isIsRealTimeAccout() {
                    return IsRealTimeAccout;
                }

                public void setIsRealTimeAccout(boolean IsRealTimeAccout) {
                    this.IsRealTimeAccout = IsRealTimeAccout;
                }

                public int getBaseUnitId_Id() {
                    return BaseUnitId_Id;
                }

                public void setBaseUnitId_Id(int BaseUnitId_Id) {
                    this.BaseUnitId_Id = BaseUnitId_Id;
                }

                public BaseUnitIdBean getBaseUnitId() {
                    return BaseUnitId;
                }

                public void setBaseUnitId(BaseUnitIdBean BaseUnitId) {
                    this.BaseUnitId = BaseUnitId;
                }

                public String getTaxType_Id() {
                    return TaxType_Id;
                }

                public void setTaxType_Id(String TaxType_Id) {
                    this.TaxType_Id = TaxType_Id;
                }

                public TaxTypeBean getTaxType() {
                    return TaxType;
                }

                public void setTaxType(TaxTypeBean TaxType) {
                    this.TaxType = TaxType;
                }

                public int getTypeID_Id() {
                    return TypeID_Id;
                }

                public void setTypeID_Id(int TypeID_Id) {
                    this.TypeID_Id = TypeID_Id;
                }

                public Object getTypeID() {
                    return TypeID;
                }

                public void setTypeID(Object TypeID) {
                    this.TypeID = TypeID;
                }

                public int getCategoryID_Id() {
                    return CategoryID_Id;
                }

                public void setCategoryID_Id(int CategoryID_Id) {
                    this.CategoryID_Id = CategoryID_Id;
                }

                public CategoryIDBean getCategoryID() {
                    return CategoryID;
                }

                public void setCategoryID(CategoryIDBean CategoryID) {
                    this.CategoryID = CategoryID;
                }

                public int getTaxRateId_Id() {
                    return TaxRateId_Id;
                }

                public void setTaxRateId_Id(int TaxRateId_Id) {
                    this.TaxRateId_Id = TaxRateId_Id;
                }

                public TaxRateIdBean getTaxRateId() {
                    return TaxRateId;
                }

                public void setTaxRateId(TaxRateIdBean TaxRateId) {
                    this.TaxRateId = TaxRateId;
                }

                public String getBARCODE() {
                    return BARCODE;
                }

                public void setBARCODE(String BARCODE) {
                    this.BARCODE = BARCODE;
                }

                public int getWEIGHTUNITID_Id() {
                    return WEIGHTUNITID_Id;
                }

                public void setWEIGHTUNITID_Id(int WEIGHTUNITID_Id) {
                    this.WEIGHTUNITID_Id = WEIGHTUNITID_Id;
                }

                public WEIGHTUNITIDBean getWEIGHTUNITID() {
                    return WEIGHTUNITID;
                }

                public void setWEIGHTUNITID(WEIGHTUNITIDBean WEIGHTUNITID) {
                    this.WEIGHTUNITID = WEIGHTUNITID;
                }

                public int getVOLUMEUNITID_Id() {
                    return VOLUMEUNITID_Id;
                }

                public void setVOLUMEUNITID_Id(int VOLUMEUNITID_Id) {
                    this.VOLUMEUNITID_Id = VOLUMEUNITID_Id;
                }

                public VOLUMEUNITIDBean getVOLUMEUNITID() {
                    return VOLUMEUNITID;
                }

                public void setVOLUMEUNITID(VOLUMEUNITIDBean VOLUMEUNITID) {
                    this.VOLUMEUNITID = VOLUMEUNITID;
                }

                public int getGROSSWEIGHT() {
                    return GROSSWEIGHT;
                }

                public void setGROSSWEIGHT(int GROSSWEIGHT) {
                    this.GROSSWEIGHT = GROSSWEIGHT;
                }

                public int getNETWEIGHT() {
                    return NETWEIGHT;
                }

                public void setNETWEIGHT(int NETWEIGHT) {
                    this.NETWEIGHT = NETWEIGHT;
                }

                public int getLENGTH() {
                    return LENGTH;
                }

                public void setLENGTH(int LENGTH) {
                    this.LENGTH = LENGTH;
                }

                public int getVOLUME() {
                    return VOLUME;
                }

                public void setVOLUME(int VOLUME) {
                    this.VOLUME = VOLUME;
                }

                public int getWIDTH() {
                    return WIDTH;
                }

                public void setWIDTH(int WIDTH) {
                    this.WIDTH = WIDTH;
                }

                public int getHEIGHT() {
                    return HEIGHT;
                }

                public void setHEIGHT(int HEIGHT) {
                    this.HEIGHT = HEIGHT;
                }

                public String getCONFIGTYPE() {
                    return CONFIGTYPE;
                }

                public void setCONFIGTYPE(String CONFIGTYPE) {
                    this.CONFIGTYPE = CONFIGTYPE;
                }

                public String getSuite() {
                    return Suite;
                }

                public void setSuite(String Suite) {
                    this.Suite = Suite;
                }

                public int getCostPriceRate() {
                    return CostPriceRate;
                }

                public void setCostPriceRate(int CostPriceRate) {
                    this.CostPriceRate = CostPriceRate;
                }

                public static class BaseUnitIdBean {
                    /**
                     * Id : 10101
                     * msterID : 10101
                     * MultiLanguageText : [{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}]
                     * Name : [{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}]
                     * Number : Pcs
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10086
                     * UnitGroupId : {"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10118,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBean UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXX> Name;
                    private List<UNITCONVERTRATEBean> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBean getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBean UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBean> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBean> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBean {
                        /**
                         * Id : 10086
                         * msterID : 10086
                         * Number : Quantity
                         * MultiLanguageText : [{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}]
                         * Name : [{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXX {
                            /**
                             * PkId : 9
                             * LocaleId : 2052
                             * Name : 数量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXX {
                            /**
                             * Key : 2052
                             * Value : 数量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXX {
                        /**
                         * PkId : 23
                         * LocaleId : 2052
                         * Name : 个
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 个
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBean {
                        /**
                         * Id : 10118
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }

                public static class TaxTypeBean {
                    /**
                     * Id : 63489f1e86944c989c58d97893a2aec2
                     * FNumber : WLDSFL01_SYS
                     * MultiLanguageText : [{"PkId":"912d2a22-2def-42e8-86ad-c91f907c58af","LocaleId":1033,"FDataValue":"Standard Tax Rate"},{"PkId":"1ac1f2dd4a584f838c73c939d1496e43","LocaleId":2052,"FDataValue":"标准税率"}]
                     * FDataValue : [{"Key":1033,"Value":"Standard Tax Rate"},{"Key":2052,"Value":"标准税率"}]
                     */

                    private String Id;
                    private String FNumber;
                    private List<MultiLanguageTextBeanXXXXXXX> MultiLanguageText;
                    private List<FDataValueBean> FDataValue;

                    public String getId() {
                        return Id;
                    }

                    public void setId(String Id) {
                        this.Id = Id;
                    }

                    public String getFNumber() {
                        return FNumber;
                    }

                    public void setFNumber(String FNumber) {
                        this.FNumber = FNumber;
                    }

                    public List<MultiLanguageTextBeanXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<FDataValueBean> getFDataValue() {
                        return FDataValue;
                    }

                    public void setFDataValue(List<FDataValueBean> FDataValue) {
                        this.FDataValue = FDataValue;
                    }

                    public static class MultiLanguageTextBeanXXXXXXX {
                        /**
                         * PkId : 912d2a22-2def-42e8-86ad-c91f907c58af
                         * LocaleId : 1033
                         * FDataValue : Standard Tax Rate
                         */

                        private String PkId;
                        private int LocaleId;
                        private String FDataValue;

                        public String getPkId() {
                            return PkId;
                        }

                        public void setPkId(String PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getFDataValue() {
                            return FDataValue;
                        }

                        public void setFDataValue(String FDataValue) {
                            this.FDataValue = FDataValue;
                        }
                    }

                    public static class FDataValueBean {
                        /**
                         * Key : 1033
                         * Value : Standard Tax Rate
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class CategoryIDBean {
                    /**
                     * Id : 237
                     * msterID : 237
                     * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"原材料"},{"PkId":8,"LocaleId":1033,"Name":"Raw materials"}]
                     * Name : [{"Key":2052,"Value":"原材料"},{"Key":1033,"Value":"Raw materials"}]
                     * Number : CHLB01_SYS
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private List<MultiLanguageTextBeanXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXX {
                        /**
                         * PkId : 1
                         * LocaleId : 2052
                         * Name : 原材料
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 原材料
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class TaxRateIdBean {
                    /**
                     * Id : 264
                     * msterID : 264
                     * MultiLanguageText : [{"PkId":8,"LocaleId":2052,"Name":"16%增值税"}]
                     * Name : [{"Key":2052,"Value":"16%增值税"}]
                     * Number : SL31_SYS
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private List<MultiLanguageTextBeanXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXX {
                        /**
                         * PkId : 8
                         * LocaleId : 2052
                         * Name : 16%增值税
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 16%增值税
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class WEIGHTUNITIDBean {
                    /**
                     * Id : 10095
                     * msterID : 10095
                     * MultiLanguageText : [{"PkId":17,"LocaleId":2052,"Name":"千克"},{"PkId":35,"LocaleId":1033,"Name":"Kg"}]
                     * Name : [{"Key":2052,"Value":"千克"},{"Key":1033,"Value":"Kg"}]
                     * Number : kg
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10085
                     * UnitGroupId : {"Id":10085,"msterID":10085,"Number":"Weight","MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"重量"},{"PkId":17,"LocaleId":1033,"Name":"Weight"}],"Name":[{"Key":2052,"Value":"重量"},{"Key":1033,"Value":"Weight"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10114,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanX {
                        /**
                         * Id : 10085
                         * msterID : 10085
                         * Number : Weight
                         * MultiLanguageText : [{"PkId":8,"LocaleId":2052,"Name":"重量"},{"PkId":17,"LocaleId":1033,"Name":"Weight"}]
                         * Name : [{"Key":2052,"Value":"重量"},{"Key":1033,"Value":"Weight"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXX {
                            /**
                             * PkId : 8
                             * LocaleId : 2052
                             * Name : 重量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 重量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXX {
                        /**
                         * PkId : 17
                         * LocaleId : 2052
                         * Name : 千克
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 千克
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanX {
                        /**
                         * Id : 10114
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }

                public static class VOLUMEUNITIDBean {
                    /**
                     * Id : 10087
                     * msterID : 10087
                     * MultiLanguageText : [{"PkId":15,"LocaleId":2052,"Name":"米"},{"PkId":33,"LocaleId":1033,"Name":"Meter"}]
                     * Name : [{"Key":2052,"Value":"米"},{"Key":1033,"Value":"Meter"}]
                     * Number : m
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10081
                     * UnitGroupId : {"Id":10081,"msterID":10081,"Number":"Length","MultiLanguageText":[{"PkId":6,"LocaleId":2052,"Name":"长度"},{"PkId":15,"LocaleId":1033,"Name":"Length"}],"Name":[{"Key":2052,"Value":"长度"},{"Key":1033,"Value":"Length"}]}
                     * Precision : 2
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10104,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanXX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanXX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanXX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanXX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanXX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanXX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanXX {
                        /**
                         * Id : 10081
                         * msterID : 10081
                         * Number : Length
                         * MultiLanguageText : [{"PkId":6,"LocaleId":2052,"Name":"长度"},{"PkId":15,"LocaleId":1033,"Name":"Length"}]
                         * Name : [{"Key":2052,"Value":"长度"},{"Key":1033,"Value":"Length"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXXXX {
                            /**
                             * PkId : 6
                             * LocaleId : 2052
                             * Name : 长度
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 长度
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXX {
                        /**
                         * PkId : 15
                         * LocaleId : 2052
                         * Name : 米
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 米
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanXX {
                        /**
                         * Id : 10104
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }
            }

            public static class MaterialStockBean {
                /**
                 * Id : 379204
                 * StoreUnitID_Id : 10101
                 * StoreUnitID : {"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]}
                 * AuxUnitID_Id : 0
                 * AuxUnitID : null
                 * StockId_Id : 0
                 * StockId : null
                 * IsLockStock : true
                 * BatchRuleID_Id : 0
                 * BatchRuleID : null
                 * ExpUnit :
                 * StockPlaceId_Id : 0
                 * StockPlaceId : null
                 * OnlineLife : 0
                 * ExpPeriod : 0
                 * StoreURNum : 1
                 * StoreURNom : 1
                 * IsBatchManage : false
                 * IsKFPeriod : false
                 * IsExpParToFlot : false
                 * IsCycleCounting : false
                 * IsMustCounting : false
                 * CurrencyId_Id : 1
                 * CurrencyId : {"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"}
                 * RefCost : 0
                 * CountCycle : 1
                 * CountDay : 1
                 * IsSNManage : false
                 * SNCodeRule_Id : 0
                 * SNCodeRule : null
                 * SNUnit_Id : 0
                 * SNUnit : null
                 * SafeStock : 0
                 * ReOrderGood : 0
                 * MinStock : 0
                 * MaxStock : 0
                 * UnitConvertDir : 1
                 * IsEnableMinStock : false
                 * IsEnableSafeStock : false
                 * IsEnableMaxStock : false
                 * IsEnableReOrder : false
                 * EconReOrderQty : 0
                 * IsSNPRDTracy : false
                 * SNGenerateTime : 1
                 * SNManageType : 1
                 * BoxStandardQty : 0
                 */

                private int Id;
                private int StoreUnitID_Id;
                private StoreUnitIDBean StoreUnitID;
                private int AuxUnitID_Id;
                private Object AuxUnitID;
                private int StockId_Id;
                private Object StockId;
                private boolean IsLockStock;
                private int BatchRuleID_Id;
                private Object BatchRuleID;
                private String ExpUnit;
                private int StockPlaceId_Id;
                private Object StockPlaceId;
                private int OnlineLife;
                private int ExpPeriod;
                private int StoreURNum;
                private int StoreURNom;
                private boolean IsBatchManage;
                private boolean IsKFPeriod;
                private boolean IsExpParToFlot;
                private boolean IsCycleCounting;
                private boolean IsMustCounting;
                private int CurrencyId_Id;
                private CurrencyIdBean CurrencyId;
                private int RefCost;
                private String CountCycle;
                private int CountDay;
                private boolean IsSNManage;
                private int SNCodeRule_Id;
                private Object SNCodeRule;
                private int SNUnit_Id;
                private Object SNUnit;
                private int SafeStock;
                private int ReOrderGood;
                private int MinStock;
                private int MaxStock;
                private String UnitConvertDir;
                private boolean IsEnableMinStock;
                private boolean IsEnableSafeStock;
                private boolean IsEnableMaxStock;
                private boolean IsEnableReOrder;
                private int EconReOrderQty;
                private boolean IsSNPRDTracy;
                private String SNGenerateTime;
                private String SNManageType;
                private int BoxStandardQty;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getStoreUnitID_Id() {
                    return StoreUnitID_Id;
                }

                public void setStoreUnitID_Id(int StoreUnitID_Id) {
                    this.StoreUnitID_Id = StoreUnitID_Id;
                }

                public StoreUnitIDBean getStoreUnitID() {
                    return StoreUnitID;
                }

                public void setStoreUnitID(StoreUnitIDBean StoreUnitID) {
                    this.StoreUnitID = StoreUnitID;
                }

                public int getAuxUnitID_Id() {
                    return AuxUnitID_Id;
                }

                public void setAuxUnitID_Id(int AuxUnitID_Id) {
                    this.AuxUnitID_Id = AuxUnitID_Id;
                }

                public Object getAuxUnitID() {
                    return AuxUnitID;
                }

                public void setAuxUnitID(Object AuxUnitID) {
                    this.AuxUnitID = AuxUnitID;
                }

                public int getStockId_Id() {
                    return StockId_Id;
                }

                public void setStockId_Id(int StockId_Id) {
                    this.StockId_Id = StockId_Id;
                }

                public Object getStockId() {
                    return StockId;
                }

                public void setStockId(Object StockId) {
                    this.StockId = StockId;
                }

                public boolean isIsLockStock() {
                    return IsLockStock;
                }

                public void setIsLockStock(boolean IsLockStock) {
                    this.IsLockStock = IsLockStock;
                }

                public int getBatchRuleID_Id() {
                    return BatchRuleID_Id;
                }

                public void setBatchRuleID_Id(int BatchRuleID_Id) {
                    this.BatchRuleID_Id = BatchRuleID_Id;
                }

                public Object getBatchRuleID() {
                    return BatchRuleID;
                }

                public void setBatchRuleID(Object BatchRuleID) {
                    this.BatchRuleID = BatchRuleID;
                }

                public String getExpUnit() {
                    return ExpUnit;
                }

                public void setExpUnit(String ExpUnit) {
                    this.ExpUnit = ExpUnit;
                }

                public int getStockPlaceId_Id() {
                    return StockPlaceId_Id;
                }

                public void setStockPlaceId_Id(int StockPlaceId_Id) {
                    this.StockPlaceId_Id = StockPlaceId_Id;
                }

                public Object getStockPlaceId() {
                    return StockPlaceId;
                }

                public void setStockPlaceId(Object StockPlaceId) {
                    this.StockPlaceId = StockPlaceId;
                }

                public int getOnlineLife() {
                    return OnlineLife;
                }

                public void setOnlineLife(int OnlineLife) {
                    this.OnlineLife = OnlineLife;
                }

                public int getExpPeriod() {
                    return ExpPeriod;
                }

                public void setExpPeriod(int ExpPeriod) {
                    this.ExpPeriod = ExpPeriod;
                }

                public int getStoreURNum() {
                    return StoreURNum;
                }

                public void setStoreURNum(int StoreURNum) {
                    this.StoreURNum = StoreURNum;
                }

                public int getStoreURNom() {
                    return StoreURNom;
                }

                public void setStoreURNom(int StoreURNom) {
                    this.StoreURNom = StoreURNom;
                }

                public boolean isIsBatchManage() {
                    return IsBatchManage;
                }

                public void setIsBatchManage(boolean IsBatchManage) {
                    this.IsBatchManage = IsBatchManage;
                }

                public boolean isIsKFPeriod() {
                    return IsKFPeriod;
                }

                public void setIsKFPeriod(boolean IsKFPeriod) {
                    this.IsKFPeriod = IsKFPeriod;
                }

                public boolean isIsExpParToFlot() {
                    return IsExpParToFlot;
                }

                public void setIsExpParToFlot(boolean IsExpParToFlot) {
                    this.IsExpParToFlot = IsExpParToFlot;
                }

                public boolean isIsCycleCounting() {
                    return IsCycleCounting;
                }

                public void setIsCycleCounting(boolean IsCycleCounting) {
                    this.IsCycleCounting = IsCycleCounting;
                }

                public boolean isIsMustCounting() {
                    return IsMustCounting;
                }

                public void setIsMustCounting(boolean IsMustCounting) {
                    this.IsMustCounting = IsMustCounting;
                }

                public int getCurrencyId_Id() {
                    return CurrencyId_Id;
                }

                public void setCurrencyId_Id(int CurrencyId_Id) {
                    this.CurrencyId_Id = CurrencyId_Id;
                }

                public CurrencyIdBean getCurrencyId() {
                    return CurrencyId;
                }

                public void setCurrencyId(CurrencyIdBean CurrencyId) {
                    this.CurrencyId = CurrencyId;
                }

                public int getRefCost() {
                    return RefCost;
                }

                public void setRefCost(int RefCost) {
                    this.RefCost = RefCost;
                }

                public String getCountCycle() {
                    return CountCycle;
                }

                public void setCountCycle(String CountCycle) {
                    this.CountCycle = CountCycle;
                }

                public int getCountDay() {
                    return CountDay;
                }

                public void setCountDay(int CountDay) {
                    this.CountDay = CountDay;
                }

                public boolean isIsSNManage() {
                    return IsSNManage;
                }

                public void setIsSNManage(boolean IsSNManage) {
                    this.IsSNManage = IsSNManage;
                }

                public int getSNCodeRule_Id() {
                    return SNCodeRule_Id;
                }

                public void setSNCodeRule_Id(int SNCodeRule_Id) {
                    this.SNCodeRule_Id = SNCodeRule_Id;
                }

                public Object getSNCodeRule() {
                    return SNCodeRule;
                }

                public void setSNCodeRule(Object SNCodeRule) {
                    this.SNCodeRule = SNCodeRule;
                }

                public int getSNUnit_Id() {
                    return SNUnit_Id;
                }

                public void setSNUnit_Id(int SNUnit_Id) {
                    this.SNUnit_Id = SNUnit_Id;
                }

                public Object getSNUnit() {
                    return SNUnit;
                }

                public void setSNUnit(Object SNUnit) {
                    this.SNUnit = SNUnit;
                }

                public int getSafeStock() {
                    return SafeStock;
                }

                public void setSafeStock(int SafeStock) {
                    this.SafeStock = SafeStock;
                }

                public int getReOrderGood() {
                    return ReOrderGood;
                }

                public void setReOrderGood(int ReOrderGood) {
                    this.ReOrderGood = ReOrderGood;
                }

                public int getMinStock() {
                    return MinStock;
                }

                public void setMinStock(int MinStock) {
                    this.MinStock = MinStock;
                }

                public int getMaxStock() {
                    return MaxStock;
                }

                public void setMaxStock(int MaxStock) {
                    this.MaxStock = MaxStock;
                }

                public String getUnitConvertDir() {
                    return UnitConvertDir;
                }

                public void setUnitConvertDir(String UnitConvertDir) {
                    this.UnitConvertDir = UnitConvertDir;
                }

                public boolean isIsEnableMinStock() {
                    return IsEnableMinStock;
                }

                public void setIsEnableMinStock(boolean IsEnableMinStock) {
                    this.IsEnableMinStock = IsEnableMinStock;
                }

                public boolean isIsEnableSafeStock() {
                    return IsEnableSafeStock;
                }

                public void setIsEnableSafeStock(boolean IsEnableSafeStock) {
                    this.IsEnableSafeStock = IsEnableSafeStock;
                }

                public boolean isIsEnableMaxStock() {
                    return IsEnableMaxStock;
                }

                public void setIsEnableMaxStock(boolean IsEnableMaxStock) {
                    this.IsEnableMaxStock = IsEnableMaxStock;
                }

                public boolean isIsEnableReOrder() {
                    return IsEnableReOrder;
                }

                public void setIsEnableReOrder(boolean IsEnableReOrder) {
                    this.IsEnableReOrder = IsEnableReOrder;
                }

                public int getEconReOrderQty() {
                    return EconReOrderQty;
                }

                public void setEconReOrderQty(int EconReOrderQty) {
                    this.EconReOrderQty = EconReOrderQty;
                }

                public boolean isIsSNPRDTracy() {
                    return IsSNPRDTracy;
                }

                public void setIsSNPRDTracy(boolean IsSNPRDTracy) {
                    this.IsSNPRDTracy = IsSNPRDTracy;
                }

                public String getSNGenerateTime() {
                    return SNGenerateTime;
                }

                public void setSNGenerateTime(String SNGenerateTime) {
                    this.SNGenerateTime = SNGenerateTime;
                }

                public String getSNManageType() {
                    return SNManageType;
                }

                public void setSNManageType(String SNManageType) {
                    this.SNManageType = SNManageType;
                }

                public int getBoxStandardQty() {
                    return BoxStandardQty;
                }

                public void setBoxStandardQty(int BoxStandardQty) {
                    this.BoxStandardQty = BoxStandardQty;
                }

                public static class StoreUnitIDBean {
                    /**
                     * Id : 10101
                     * msterID : 10101
                     * MultiLanguageText : [{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}]
                     * Name : [{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}]
                     * Number : Pcs
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10086
                     * UnitGroupId : {"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10118,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanXXX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanXXX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanXXX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanXXX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanXXX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanXXX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanXXX {
                        /**
                         * Id : 10086
                         * msterID : 10086
                         * Number : Quantity
                         * MultiLanguageText : [{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}]
                         * Name : [{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXXXXXX {
                            /**
                             * PkId : 9
                             * LocaleId : 2052
                             * Name : 数量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 数量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 23
                         * LocaleId : 2052
                         * Name : 个
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 个
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanXXX {
                        /**
                         * Id : 10118
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }

                public static class CurrencyIdBean {
                    /**
                     * Id : 1
                     * msterID : 1
                     * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}]
                     * Name : [{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}]
                     * Number : PRE001
                     * Sysmbol : ¥
                     * PriceDigits : 6
                     * AmountDigits : 2
                     * IsShowCSymbol : true
                     * FormatOrder : 1
                     * RoundType : 1
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private String Sysmbol;
                    private int PriceDigits;
                    private int AmountDigits;
                    private boolean IsShowCSymbol;
                    private String FormatOrder;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public String getSysmbol() {
                        return Sysmbol;
                    }

                    public void setSysmbol(String Sysmbol) {
                        this.Sysmbol = Sysmbol;
                    }

                    public int getPriceDigits() {
                        return PriceDigits;
                    }

                    public void setPriceDigits(int PriceDigits) {
                        this.PriceDigits = PriceDigits;
                    }

                    public int getAmountDigits() {
                        return AmountDigits;
                    }

                    public void setAmountDigits(int AmountDigits) {
                        this.AmountDigits = AmountDigits;
                    }

                    public boolean isIsShowCSymbol() {
                        return IsShowCSymbol;
                    }

                    public void setIsShowCSymbol(boolean IsShowCSymbol) {
                        this.IsShowCSymbol = IsShowCSymbol;
                    }

                    public String getFormatOrder() {
                        return FormatOrder;
                    }

                    public void setFormatOrder(String FormatOrder) {
                        this.FormatOrder = FormatOrder;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 1
                         * LocaleId : 2052
                         * Name : 人民币
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 人民币
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }
            }

            public static class MaterialSaleBean {
                /**
                 * Id : 379204
                 * IsATPCheck : false
                 * SalePriceUnitId_Id : 10101
                 * SalePriceUnitId : {"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]}
                 * SaleUnitId_Id : 10101
                 * SaleUnitId : {"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]}
                 * IsInvoice : false
                 * MaxQty : 100000
                 * IsReturn : true
                 * MinQty : 0
                 * IsReturnPart : false
                 * OrderQty : 0
                 * SalePriceURNom : 1
                 * SaleURNum : 1
                 * SalePriceURNum : 1
                 * SaleURNom : 1
                 * OutStockLmtH : 0
                 * OutStockLmtL : 0
                 * AgentSalReduceRate : 0
                 * AllowPublish : false
                 * ISAFTERSALE : true
                 * ISPRODUCTFILES : true
                 * ISWARRANTED : false
                 * FWARRANTY : 0
                 * WARRANTYUNITID : D
                 * OutLmtUnit : SAL
                 * TaxCategoryCodeId_Id : 0
                 * TaxCategoryCodeId : null
                 * SalGroup_Id : 0
                 * SalGroup : null
                 * TaxDiscountsType : 0
                 * IsTaxEnjoy : false
                 */

                private int Id;
                private boolean IsATPCheck;
                private int SalePriceUnitId_Id;
                private SalePriceUnitIdBean SalePriceUnitId;
                private int SaleUnitId_Id;
                private SaleUnitIdBean SaleUnitId;
                private boolean IsInvoice;
                private int MaxQty;
                private boolean IsReturn;
                private int MinQty;
                private boolean IsReturnPart;
                private int OrderQty;
                private int SalePriceURNom;
                private int SaleURNum;
                private int SalePriceURNum;
                private int SaleURNom;
                private int OutStockLmtH;
                private int OutStockLmtL;
                private int AgentSalReduceRate;
                private boolean AllowPublish;
                private boolean ISAFTERSALE;
                private boolean ISPRODUCTFILES;
                private boolean ISWARRANTED;
                private int FWARRANTY;
                private String WARRANTYUNITID;
                private String OutLmtUnit;
                private int TaxCategoryCodeId_Id;
                private Object TaxCategoryCodeId;
                private int SalGroup_Id;
                private Object SalGroup;
                private String TaxDiscountsType;
                private boolean IsTaxEnjoy;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public boolean isIsATPCheck() {
                    return IsATPCheck;
                }

                public void setIsATPCheck(boolean IsATPCheck) {
                    this.IsATPCheck = IsATPCheck;
                }

                public int getSalePriceUnitId_Id() {
                    return SalePriceUnitId_Id;
                }

                public void setSalePriceUnitId_Id(int SalePriceUnitId_Id) {
                    this.SalePriceUnitId_Id = SalePriceUnitId_Id;
                }

                public SalePriceUnitIdBean getSalePriceUnitId() {
                    return SalePriceUnitId;
                }

                public void setSalePriceUnitId(SalePriceUnitIdBean SalePriceUnitId) {
                    this.SalePriceUnitId = SalePriceUnitId;
                }

                public int getSaleUnitId_Id() {
                    return SaleUnitId_Id;
                }

                public void setSaleUnitId_Id(int SaleUnitId_Id) {
                    this.SaleUnitId_Id = SaleUnitId_Id;
                }

                public SaleUnitIdBean getSaleUnitId() {
                    return SaleUnitId;
                }

                public void setSaleUnitId(SaleUnitIdBean SaleUnitId) {
                    this.SaleUnitId = SaleUnitId;
                }

                public boolean isIsInvoice() {
                    return IsInvoice;
                }

                public void setIsInvoice(boolean IsInvoice) {
                    this.IsInvoice = IsInvoice;
                }

                public int getMaxQty() {
                    return MaxQty;
                }

                public void setMaxQty(int MaxQty) {
                    this.MaxQty = MaxQty;
                }

                public boolean isIsReturn() {
                    return IsReturn;
                }

                public void setIsReturn(boolean IsReturn) {
                    this.IsReturn = IsReturn;
                }

                public int getMinQty() {
                    return MinQty;
                }

                public void setMinQty(int MinQty) {
                    this.MinQty = MinQty;
                }

                public boolean isIsReturnPart() {
                    return IsReturnPart;
                }

                public void setIsReturnPart(boolean IsReturnPart) {
                    this.IsReturnPart = IsReturnPart;
                }

                public int getOrderQty() {
                    return OrderQty;
                }

                public void setOrderQty(int OrderQty) {
                    this.OrderQty = OrderQty;
                }

                public int getSalePriceURNom() {
                    return SalePriceURNom;
                }

                public void setSalePriceURNom(int SalePriceURNom) {
                    this.SalePriceURNom = SalePriceURNom;
                }

                public int getSaleURNum() {
                    return SaleURNum;
                }

                public void setSaleURNum(int SaleURNum) {
                    this.SaleURNum = SaleURNum;
                }

                public int getSalePriceURNum() {
                    return SalePriceURNum;
                }

                public void setSalePriceURNum(int SalePriceURNum) {
                    this.SalePriceURNum = SalePriceURNum;
                }

                public int getSaleURNom() {
                    return SaleURNom;
                }

                public void setSaleURNom(int SaleURNom) {
                    this.SaleURNom = SaleURNom;
                }

                public int getOutStockLmtH() {
                    return OutStockLmtH;
                }

                public void setOutStockLmtH(int OutStockLmtH) {
                    this.OutStockLmtH = OutStockLmtH;
                }

                public int getOutStockLmtL() {
                    return OutStockLmtL;
                }

                public void setOutStockLmtL(int OutStockLmtL) {
                    this.OutStockLmtL = OutStockLmtL;
                }

                public int getAgentSalReduceRate() {
                    return AgentSalReduceRate;
                }

                public void setAgentSalReduceRate(int AgentSalReduceRate) {
                    this.AgentSalReduceRate = AgentSalReduceRate;
                }

                public boolean isAllowPublish() {
                    return AllowPublish;
                }

                public void setAllowPublish(boolean AllowPublish) {
                    this.AllowPublish = AllowPublish;
                }

                public boolean isISAFTERSALE() {
                    return ISAFTERSALE;
                }

                public void setISAFTERSALE(boolean ISAFTERSALE) {
                    this.ISAFTERSALE = ISAFTERSALE;
                }

                public boolean isISPRODUCTFILES() {
                    return ISPRODUCTFILES;
                }

                public void setISPRODUCTFILES(boolean ISPRODUCTFILES) {
                    this.ISPRODUCTFILES = ISPRODUCTFILES;
                }

                public boolean isISWARRANTED() {
                    return ISWARRANTED;
                }

                public void setISWARRANTED(boolean ISWARRANTED) {
                    this.ISWARRANTED = ISWARRANTED;
                }

                public int getFWARRANTY() {
                    return FWARRANTY;
                }

                public void setFWARRANTY(int FWARRANTY) {
                    this.FWARRANTY = FWARRANTY;
                }

                public String getWARRANTYUNITID() {
                    return WARRANTYUNITID;
                }

                public void setWARRANTYUNITID(String WARRANTYUNITID) {
                    this.WARRANTYUNITID = WARRANTYUNITID;
                }

                public String getOutLmtUnit() {
                    return OutLmtUnit;
                }

                public void setOutLmtUnit(String OutLmtUnit) {
                    this.OutLmtUnit = OutLmtUnit;
                }

                public int getTaxCategoryCodeId_Id() {
                    return TaxCategoryCodeId_Id;
                }

                public void setTaxCategoryCodeId_Id(int TaxCategoryCodeId_Id) {
                    this.TaxCategoryCodeId_Id = TaxCategoryCodeId_Id;
                }

                public Object getTaxCategoryCodeId() {
                    return TaxCategoryCodeId;
                }

                public void setTaxCategoryCodeId(Object TaxCategoryCodeId) {
                    this.TaxCategoryCodeId = TaxCategoryCodeId;
                }

                public int getSalGroup_Id() {
                    return SalGroup_Id;
                }

                public void setSalGroup_Id(int SalGroup_Id) {
                    this.SalGroup_Id = SalGroup_Id;
                }

                public Object getSalGroup() {
                    return SalGroup;
                }

                public void setSalGroup(Object SalGroup) {
                    this.SalGroup = SalGroup;
                }

                public String getTaxDiscountsType() {
                    return TaxDiscountsType;
                }

                public void setTaxDiscountsType(String TaxDiscountsType) {
                    this.TaxDiscountsType = TaxDiscountsType;
                }

                public boolean isIsTaxEnjoy() {
                    return IsTaxEnjoy;
                }

                public void setIsTaxEnjoy(boolean IsTaxEnjoy) {
                    this.IsTaxEnjoy = IsTaxEnjoy;
                }

                public static class SalePriceUnitIdBean {
                    /**
                     * Id : 10101
                     * msterID : 10101
                     * MultiLanguageText : [{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}]
                     * Name : [{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}]
                     * Number : Pcs
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10086
                     * UnitGroupId : {"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10118,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanXXXX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanXXXX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanXXXX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanXXXX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanXXXX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanXXXX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanXXXX {
                        /**
                         * Id : 10086
                         * msterID : 10086
                         * Number : Quantity
                         * MultiLanguageText : [{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}]
                         * Name : [{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXX {
                            /**
                             * PkId : 9
                             * LocaleId : 2052
                             * Name : 数量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 数量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 23
                         * LocaleId : 2052
                         * Name : 个
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 个
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanXXXX {
                        /**
                         * Id : 10118
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }

                public static class SaleUnitIdBean {
                    /**
                     * Id : 10101
                     * msterID : 10101
                     * MultiLanguageText : [{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}]
                     * Name : [{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}]
                     * Number : Pcs
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10086
                     * UnitGroupId : {"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10118,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanXXXXX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanXXXXX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanXXXXX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanXXXXX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanXXXXX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanXXXXX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanXXXXX {
                        /**
                         * Id : 10086
                         * msterID : 10086
                         * Number : Quantity
                         * MultiLanguageText : [{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}]
                         * Name : [{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXXXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXXXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXXXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXX {
                            /**
                             * PkId : 9
                             * LocaleId : 2052
                             * Name : 数量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXXXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 数量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 23
                         * LocaleId : 2052
                         * Name : 个
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 个
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanXXXXX {
                        /**
                         * Id : 10118
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }
            }

            public static class MaterialPurchaseBean {
                /**
                 * Id : 379204
                 * PurchaseUnitID_Id : 10101
                 * PurchaseUnitID : {"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]}
                 * PurchaserId_Id : 0
                 * PurchaserId : null
                 * DefaultVendor_Id : 0
                 * DefaultVendor : null
                 * IsSourceControl : false
                 * IsPR : false
                 * ReceiveMinScale : 0
                 * PurchaseGroupId_Id : 0
                 * PurchaseGroupId : null
                 * ReceiveMaxScale : 0
                 * PurchasePriceUnitId_Id : 10101
                 * PurchasePriceUnitId : {"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]}
                 * IsVendorQualification : false
                 * ReceiveAdvanceDays : 0
                 * ReceiveDelayDays : 0
                 * PurURNum : 1
                 * PurPriceURNum : 1
                 * PurURNom : 1
                 * PurPriceURNom : 1
                 * IsQuota : false
                 * QuotaType : 1
                 * AgentPurPlusRate : 0
                 * ChargeID_Id : 0
                 * ChargeID : null
                 * MinSplitQty : 0
                 * BaseMinSplitQty : 0
                 * IsVmiBusiness : false
                 * IsReturnMaterial : true
                 * EnableSL : false
                 * PurchaseOrgId_Id : 0
                 * PurchaseOrgId : null
                 * DefBarCodeRuleId_Id : 0
                 * DefBarCodeRuleId : null
                 * MinPackCount : 0
                 * PrintCount : 1
                 * POBillTypeId_Id : 93591469feb54ca2b08eb635f8b79de3
                 * POBillTypeId : {"Id":"93591469feb54ca2b08eb635f8b79de3","MultiLanguageText":[{"PkId":"65a98b68-868c-407d-b337-e3d321635b44","LocaleId":1033,"Name":"Standard Purchase Requisition"},{"PkId":"a7b70d95ba34432387653c6937cae696","LocaleId":2052,"Name":"标准采购申请"}],"Name":[{"Key":1033,"Value":"Standard Purchase Requisition"},{"Key":2052,"Value":"标准采购申请"}],"Number":"CGSQD01_SYS","IsDefault":true}
                 */

                private int Id;
                private int PurchaseUnitID_Id;
                private PurchaseUnitIDBean PurchaseUnitID;
                private int PurchaserId_Id;
                private Object PurchaserId;
                private int DefaultVendor_Id;
                private Object DefaultVendor;
                private boolean IsSourceControl;
                private boolean IsPR;
                private int ReceiveMinScale;
                private int PurchaseGroupId_Id;
                private Object PurchaseGroupId;
                private int ReceiveMaxScale;
                private int PurchasePriceUnitId_Id;
                private PurchasePriceUnitIdBean PurchasePriceUnitId;
                private boolean IsVendorQualification;
                private int ReceiveAdvanceDays;
                private int ReceiveDelayDays;
                private int PurURNum;
                private int PurPriceURNum;
                private int PurURNom;
                private int PurPriceURNom;
                private boolean IsQuota;
                private String QuotaType;
                private int AgentPurPlusRate;
                private int ChargeID_Id;
                private Object ChargeID;
                private int MinSplitQty;
                private int BaseMinSplitQty;
                private boolean IsVmiBusiness;
                private boolean IsReturnMaterial;
                private boolean EnableSL;
                private int PurchaseOrgId_Id;
                private Object PurchaseOrgId;
                private int DefBarCodeRuleId_Id;
                private Object DefBarCodeRuleId;
                private int MinPackCount;
                private int PrintCount;
                private String POBillTypeId_Id;
                private POBillTypeIdBean POBillTypeId;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getPurchaseUnitID_Id() {
                    return PurchaseUnitID_Id;
                }

                public void setPurchaseUnitID_Id(int PurchaseUnitID_Id) {
                    this.PurchaseUnitID_Id = PurchaseUnitID_Id;
                }

                public PurchaseUnitIDBean getPurchaseUnitID() {
                    return PurchaseUnitID;
                }

                public void setPurchaseUnitID(PurchaseUnitIDBean PurchaseUnitID) {
                    this.PurchaseUnitID = PurchaseUnitID;
                }

                public int getPurchaserId_Id() {
                    return PurchaserId_Id;
                }

                public void setPurchaserId_Id(int PurchaserId_Id) {
                    this.PurchaserId_Id = PurchaserId_Id;
                }

                public Object getPurchaserId() {
                    return PurchaserId;
                }

                public void setPurchaserId(Object PurchaserId) {
                    this.PurchaserId = PurchaserId;
                }

                public int getDefaultVendor_Id() {
                    return DefaultVendor_Id;
                }

                public void setDefaultVendor_Id(int DefaultVendor_Id) {
                    this.DefaultVendor_Id = DefaultVendor_Id;
                }

                public Object getDefaultVendor() {
                    return DefaultVendor;
                }

                public void setDefaultVendor(Object DefaultVendor) {
                    this.DefaultVendor = DefaultVendor;
                }

                public boolean isIsSourceControl() {
                    return IsSourceControl;
                }

                public void setIsSourceControl(boolean IsSourceControl) {
                    this.IsSourceControl = IsSourceControl;
                }

                public boolean isIsPR() {
                    return IsPR;
                }

                public void setIsPR(boolean IsPR) {
                    this.IsPR = IsPR;
                }

                public int getReceiveMinScale() {
                    return ReceiveMinScale;
                }

                public void setReceiveMinScale(int ReceiveMinScale) {
                    this.ReceiveMinScale = ReceiveMinScale;
                }

                public int getPurchaseGroupId_Id() {
                    return PurchaseGroupId_Id;
                }

                public void setPurchaseGroupId_Id(int PurchaseGroupId_Id) {
                    this.PurchaseGroupId_Id = PurchaseGroupId_Id;
                }

                public Object getPurchaseGroupId() {
                    return PurchaseGroupId;
                }

                public void setPurchaseGroupId(Object PurchaseGroupId) {
                    this.PurchaseGroupId = PurchaseGroupId;
                }

                public int getReceiveMaxScale() {
                    return ReceiveMaxScale;
                }

                public void setReceiveMaxScale(int ReceiveMaxScale) {
                    this.ReceiveMaxScale = ReceiveMaxScale;
                }

                public int getPurchasePriceUnitId_Id() {
                    return PurchasePriceUnitId_Id;
                }

                public void setPurchasePriceUnitId_Id(int PurchasePriceUnitId_Id) {
                    this.PurchasePriceUnitId_Id = PurchasePriceUnitId_Id;
                }

                public PurchasePriceUnitIdBean getPurchasePriceUnitId() {
                    return PurchasePriceUnitId;
                }

                public void setPurchasePriceUnitId(PurchasePriceUnitIdBean PurchasePriceUnitId) {
                    this.PurchasePriceUnitId = PurchasePriceUnitId;
                }

                public boolean isIsVendorQualification() {
                    return IsVendorQualification;
                }

                public void setIsVendorQualification(boolean IsVendorQualification) {
                    this.IsVendorQualification = IsVendorQualification;
                }

                public int getReceiveAdvanceDays() {
                    return ReceiveAdvanceDays;
                }

                public void setReceiveAdvanceDays(int ReceiveAdvanceDays) {
                    this.ReceiveAdvanceDays = ReceiveAdvanceDays;
                }

                public int getReceiveDelayDays() {
                    return ReceiveDelayDays;
                }

                public void setReceiveDelayDays(int ReceiveDelayDays) {
                    this.ReceiveDelayDays = ReceiveDelayDays;
                }

                public int getPurURNum() {
                    return PurURNum;
                }

                public void setPurURNum(int PurURNum) {
                    this.PurURNum = PurURNum;
                }

                public int getPurPriceURNum() {
                    return PurPriceURNum;
                }

                public void setPurPriceURNum(int PurPriceURNum) {
                    this.PurPriceURNum = PurPriceURNum;
                }

                public int getPurURNom() {
                    return PurURNom;
                }

                public void setPurURNom(int PurURNom) {
                    this.PurURNom = PurURNom;
                }

                public int getPurPriceURNom() {
                    return PurPriceURNom;
                }

                public void setPurPriceURNom(int PurPriceURNom) {
                    this.PurPriceURNom = PurPriceURNom;
                }

                public boolean isIsQuota() {
                    return IsQuota;
                }

                public void setIsQuota(boolean IsQuota) {
                    this.IsQuota = IsQuota;
                }

                public String getQuotaType() {
                    return QuotaType;
                }

                public void setQuotaType(String QuotaType) {
                    this.QuotaType = QuotaType;
                }

                public int getAgentPurPlusRate() {
                    return AgentPurPlusRate;
                }

                public void setAgentPurPlusRate(int AgentPurPlusRate) {
                    this.AgentPurPlusRate = AgentPurPlusRate;
                }

                public int getChargeID_Id() {
                    return ChargeID_Id;
                }

                public void setChargeID_Id(int ChargeID_Id) {
                    this.ChargeID_Id = ChargeID_Id;
                }

                public Object getChargeID() {
                    return ChargeID;
                }

                public void setChargeID(Object ChargeID) {
                    this.ChargeID = ChargeID;
                }

                public int getMinSplitQty() {
                    return MinSplitQty;
                }

                public void setMinSplitQty(int MinSplitQty) {
                    this.MinSplitQty = MinSplitQty;
                }

                public int getBaseMinSplitQty() {
                    return BaseMinSplitQty;
                }

                public void setBaseMinSplitQty(int BaseMinSplitQty) {
                    this.BaseMinSplitQty = BaseMinSplitQty;
                }

                public boolean isIsVmiBusiness() {
                    return IsVmiBusiness;
                }

                public void setIsVmiBusiness(boolean IsVmiBusiness) {
                    this.IsVmiBusiness = IsVmiBusiness;
                }

                public boolean isIsReturnMaterial() {
                    return IsReturnMaterial;
                }

                public void setIsReturnMaterial(boolean IsReturnMaterial) {
                    this.IsReturnMaterial = IsReturnMaterial;
                }

                public boolean isEnableSL() {
                    return EnableSL;
                }

                public void setEnableSL(boolean EnableSL) {
                    this.EnableSL = EnableSL;
                }

                public int getPurchaseOrgId_Id() {
                    return PurchaseOrgId_Id;
                }

                public void setPurchaseOrgId_Id(int PurchaseOrgId_Id) {
                    this.PurchaseOrgId_Id = PurchaseOrgId_Id;
                }

                public Object getPurchaseOrgId() {
                    return PurchaseOrgId;
                }

                public void setPurchaseOrgId(Object PurchaseOrgId) {
                    this.PurchaseOrgId = PurchaseOrgId;
                }

                public int getDefBarCodeRuleId_Id() {
                    return DefBarCodeRuleId_Id;
                }

                public void setDefBarCodeRuleId_Id(int DefBarCodeRuleId_Id) {
                    this.DefBarCodeRuleId_Id = DefBarCodeRuleId_Id;
                }

                public Object getDefBarCodeRuleId() {
                    return DefBarCodeRuleId;
                }

                public void setDefBarCodeRuleId(Object DefBarCodeRuleId) {
                    this.DefBarCodeRuleId = DefBarCodeRuleId;
                }

                public int getMinPackCount() {
                    return MinPackCount;
                }

                public void setMinPackCount(int MinPackCount) {
                    this.MinPackCount = MinPackCount;
                }

                public int getPrintCount() {
                    return PrintCount;
                }

                public void setPrintCount(int PrintCount) {
                    this.PrintCount = PrintCount;
                }

                public String getPOBillTypeId_Id() {
                    return POBillTypeId_Id;
                }

                public void setPOBillTypeId_Id(String POBillTypeId_Id) {
                    this.POBillTypeId_Id = POBillTypeId_Id;
                }

                public POBillTypeIdBean getPOBillTypeId() {
                    return POBillTypeId;
                }

                public void setPOBillTypeId(POBillTypeIdBean POBillTypeId) {
                    this.POBillTypeId = POBillTypeId;
                }

                public static class PurchaseUnitIDBean {
                    /**
                     * Id : 10101
                     * msterID : 10101
                     * MultiLanguageText : [{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}]
                     * Name : [{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}]
                     * Number : Pcs
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10086
                     * UnitGroupId : {"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10118,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanXXXXXX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanXXXXXX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanXXXXXX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanXXXXXX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanXXXXXX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanXXXXXX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanXXXXXX {
                        /**
                         * Id : 10086
                         * msterID : 10086
                         * Number : Quantity
                         * MultiLanguageText : [{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}]
                         * Name : [{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXXXXXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXXXXXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * PkId : 9
                             * LocaleId : 2052
                             * Name : 数量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 数量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 23
                         * LocaleId : 2052
                         * Name : 个
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 个
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanXXXXXX {
                        /**
                         * Id : 10118
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }

                public static class PurchasePriceUnitIdBean {
                    /**
                     * Id : 10101
                     * msterID : 10101
                     * MultiLanguageText : [{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}]
                     * Name : [{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}]
                     * Number : Pcs
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10086
                     * UnitGroupId : {"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10118,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanXXXXXXX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanXXXXXXX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanXXXXXXX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanXXXXXXX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanXXXXXXX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanXXXXXXX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanXXXXXXX {
                        /**
                         * Id : 10086
                         * msterID : 10086
                         * Number : Quantity
                         * MultiLanguageText : [{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}]
                         * Name : [{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXXXXXXXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXXXXXXXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * PkId : 9
                             * LocaleId : 2052
                             * Name : 数量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 数量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 23
                         * LocaleId : 2052
                         * Name : 个
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 个
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanXXXXXXX {
                        /**
                         * Id : 10118
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }

                public static class POBillTypeIdBean {
                    /**
                     * Id : 93591469feb54ca2b08eb635f8b79de3
                     * MultiLanguageText : [{"PkId":"65a98b68-868c-407d-b337-e3d321635b44","LocaleId":1033,"Name":"Standard Purchase Requisition"},{"PkId":"a7b70d95ba34432387653c6937cae696","LocaleId":2052,"Name":"标准采购申请"}]
                     * Name : [{"Key":1033,"Value":"Standard Purchase Requisition"},{"Key":2052,"Value":"标准采购申请"}]
                     * Number : CGSQD01_SYS
                     * IsDefault : true
                     */

                    private String Id;
                    private String Number;
                    private boolean IsDefault;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXX> Name;

                    public String getId() {
                        return Id;
                    }

                    public void setId(String Id) {
                        this.Id = Id;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsDefault() {
                        return IsDefault;
                    }

                    public void setIsDefault(boolean IsDefault) {
                        this.IsDefault = IsDefault;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 65a98b68-868c-407d-b337-e3d321635b44
                         * LocaleId : 1033
                         * Name : Standard Purchase Requisition
                         */

                        private String PkId;
                        private int LocaleId;
                        private String Name;

                        public String getPkId() {
                            return PkId;
                        }

                        public void setPkId(String PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 1033
                         * Value : Standard Purchase Requisition
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }
            }

            public static class MaterialPlanBean {
                /**
                 * Id : 379204
                 * PlanerID_Id : 0
                 * PlanerID : null
                 * EOQ : 1
                 * PlanningStrategy : 1
                 * OrderPolicy : 0
                 * PlanWorkshop_Id : 0
                 * PlanWorkshop : null
                 * FixLeadTimeType : 1
                 * FixLeadTime : 15
                 * VarLeadTimeType : 1
                 * VarLeadTime : 0
                 * CheckLeadTimeType : 1
                 * CheckLeadTime : 0
                 * OrderIntervalTimeType : 3
                 * OrderIntervalTime : 0
                 * PlanIntervalsDays : 1
                 * PlanBatchSplitQty : 0
                 * PlanTimeZone : 0
                 * RequestTimeZone : 0
                 * IsMrpComReq : false
                 * ReserveType : 1
                 * CanLeadDays : 0
                 * LeadExtendDay : 0
                 * DelayExtendDay : 0
                 * CanDelayDays : 999
                 * PlanOffsetTimeType : 1
                 * PlanOffsetTime : 0
                 * MinPOQty : 50
                 * IncreaseQty : 0
                 * MaxPOQty : 100000
                 * VarLeadTimeLotSize : 1
                 * BaseVarLeadTimeLotSize : 1
                 * PlanGroupId_Id : 0
                 * PlanGroupId : null
                 * MfgPolicyId_Id : 40042
                 * MfgPolicyId : {"Id":40042,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"MTS10(考虑库存)"},{"PkId":6,"LocaleId":1033,"Name":"MTS10 (Include inventory)"}],"Name":[{"Key":2052,"Value":"MTS10(考虑库存)"},{"Key":1033,"Value":"MTS10 (Include inventory)"}],"Number":"ZZCL001_SYS","PlanMode":"0","Ato":false}
                 * SupplySourceId_Id : 0
                 * SupplySourceId : null
                 * TimeFactorId_Id : 0
                 * TimeFactorId : null
                 * QtyFactorId_Id : 0
                 * QtyFactorId : null
                 * PlanMode : 0
                 * AllowPartDelay : true
                 * AllowPartAhead : false
                 * PLANSAFESTOCKQTY : 0
                 * ATOSchemeId_Id : 0
                 * ATOSchemeId : null
                 */

                private int Id;
                private int PlanerID_Id;
                private Object PlanerID;
                private int EOQ;
                private String PlanningStrategy;
                private String OrderPolicy;
                private int PlanWorkshop_Id;
                private Object PlanWorkshop;
                private String FixLeadTimeType;
                private int FixLeadTime;
                private String VarLeadTimeType;
                private int VarLeadTime;
                private String CheckLeadTimeType;
                private int CheckLeadTime;
                private String OrderIntervalTimeType;
                private int OrderIntervalTime;
                private int PlanIntervalsDays;
                private int PlanBatchSplitQty;
                private int PlanTimeZone;
                private int RequestTimeZone;
                private boolean IsMrpComReq;
                private String ReserveType;
                private int CanLeadDays;
                private int LeadExtendDay;
                private int DelayExtendDay;
                private int CanDelayDays;
                private String PlanOffsetTimeType;
                private int PlanOffsetTime;
                private int MinPOQty;
                private int IncreaseQty;
                private int MaxPOQty;
                private int VarLeadTimeLotSize;
                private int BaseVarLeadTimeLotSize;
                private int PlanGroupId_Id;
                private Object PlanGroupId;
                private int MfgPolicyId_Id;
                private MfgPolicyIdBean MfgPolicyId;
                private int SupplySourceId_Id;
                private Object SupplySourceId;
                private int TimeFactorId_Id;
                private Object TimeFactorId;
                private int QtyFactorId_Id;
                private Object QtyFactorId;
                private String PlanMode;
                private boolean AllowPartDelay;
                private boolean AllowPartAhead;
                private int PLANSAFESTOCKQTY;
                private int ATOSchemeId_Id;
                private Object ATOSchemeId;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getPlanerID_Id() {
                    return PlanerID_Id;
                }

                public void setPlanerID_Id(int PlanerID_Id) {
                    this.PlanerID_Id = PlanerID_Id;
                }

                public Object getPlanerID() {
                    return PlanerID;
                }

                public void setPlanerID(Object PlanerID) {
                    this.PlanerID = PlanerID;
                }

                public int getEOQ() {
                    return EOQ;
                }

                public void setEOQ(int EOQ) {
                    this.EOQ = EOQ;
                }

                public String getPlanningStrategy() {
                    return PlanningStrategy;
                }

                public void setPlanningStrategy(String PlanningStrategy) {
                    this.PlanningStrategy = PlanningStrategy;
                }

                public String getOrderPolicy() {
                    return OrderPolicy;
                }

                public void setOrderPolicy(String OrderPolicy) {
                    this.OrderPolicy = OrderPolicy;
                }

                public int getPlanWorkshop_Id() {
                    return PlanWorkshop_Id;
                }

                public void setPlanWorkshop_Id(int PlanWorkshop_Id) {
                    this.PlanWorkshop_Id = PlanWorkshop_Id;
                }

                public Object getPlanWorkshop() {
                    return PlanWorkshop;
                }

                public void setPlanWorkshop(Object PlanWorkshop) {
                    this.PlanWorkshop = PlanWorkshop;
                }

                public String getFixLeadTimeType() {
                    return FixLeadTimeType;
                }

                public void setFixLeadTimeType(String FixLeadTimeType) {
                    this.FixLeadTimeType = FixLeadTimeType;
                }

                public int getFixLeadTime() {
                    return FixLeadTime;
                }

                public void setFixLeadTime(int FixLeadTime) {
                    this.FixLeadTime = FixLeadTime;
                }

                public String getVarLeadTimeType() {
                    return VarLeadTimeType;
                }

                public void setVarLeadTimeType(String VarLeadTimeType) {
                    this.VarLeadTimeType = VarLeadTimeType;
                }

                public int getVarLeadTime() {
                    return VarLeadTime;
                }

                public void setVarLeadTime(int VarLeadTime) {
                    this.VarLeadTime = VarLeadTime;
                }

                public String getCheckLeadTimeType() {
                    return CheckLeadTimeType;
                }

                public void setCheckLeadTimeType(String CheckLeadTimeType) {
                    this.CheckLeadTimeType = CheckLeadTimeType;
                }

                public int getCheckLeadTime() {
                    return CheckLeadTime;
                }

                public void setCheckLeadTime(int CheckLeadTime) {
                    this.CheckLeadTime = CheckLeadTime;
                }

                public String getOrderIntervalTimeType() {
                    return OrderIntervalTimeType;
                }

                public void setOrderIntervalTimeType(String OrderIntervalTimeType) {
                    this.OrderIntervalTimeType = OrderIntervalTimeType;
                }

                public int getOrderIntervalTime() {
                    return OrderIntervalTime;
                }

                public void setOrderIntervalTime(int OrderIntervalTime) {
                    this.OrderIntervalTime = OrderIntervalTime;
                }

                public int getPlanIntervalsDays() {
                    return PlanIntervalsDays;
                }

                public void setPlanIntervalsDays(int PlanIntervalsDays) {
                    this.PlanIntervalsDays = PlanIntervalsDays;
                }

                public int getPlanBatchSplitQty() {
                    return PlanBatchSplitQty;
                }

                public void setPlanBatchSplitQty(int PlanBatchSplitQty) {
                    this.PlanBatchSplitQty = PlanBatchSplitQty;
                }

                public int getPlanTimeZone() {
                    return PlanTimeZone;
                }

                public void setPlanTimeZone(int PlanTimeZone) {
                    this.PlanTimeZone = PlanTimeZone;
                }

                public int getRequestTimeZone() {
                    return RequestTimeZone;
                }

                public void setRequestTimeZone(int RequestTimeZone) {
                    this.RequestTimeZone = RequestTimeZone;
                }

                public boolean isIsMrpComReq() {
                    return IsMrpComReq;
                }

                public void setIsMrpComReq(boolean IsMrpComReq) {
                    this.IsMrpComReq = IsMrpComReq;
                }

                public String getReserveType() {
                    return ReserveType;
                }

                public void setReserveType(String ReserveType) {
                    this.ReserveType = ReserveType;
                }

                public int getCanLeadDays() {
                    return CanLeadDays;
                }

                public void setCanLeadDays(int CanLeadDays) {
                    this.CanLeadDays = CanLeadDays;
                }

                public int getLeadExtendDay() {
                    return LeadExtendDay;
                }

                public void setLeadExtendDay(int LeadExtendDay) {
                    this.LeadExtendDay = LeadExtendDay;
                }

                public int getDelayExtendDay() {
                    return DelayExtendDay;
                }

                public void setDelayExtendDay(int DelayExtendDay) {
                    this.DelayExtendDay = DelayExtendDay;
                }

                public int getCanDelayDays() {
                    return CanDelayDays;
                }

                public void setCanDelayDays(int CanDelayDays) {
                    this.CanDelayDays = CanDelayDays;
                }

                public String getPlanOffsetTimeType() {
                    return PlanOffsetTimeType;
                }

                public void setPlanOffsetTimeType(String PlanOffsetTimeType) {
                    this.PlanOffsetTimeType = PlanOffsetTimeType;
                }

                public int getPlanOffsetTime() {
                    return PlanOffsetTime;
                }

                public void setPlanOffsetTime(int PlanOffsetTime) {
                    this.PlanOffsetTime = PlanOffsetTime;
                }

                public int getMinPOQty() {
                    return MinPOQty;
                }

                public void setMinPOQty(int MinPOQty) {
                    this.MinPOQty = MinPOQty;
                }

                public int getIncreaseQty() {
                    return IncreaseQty;
                }

                public void setIncreaseQty(int IncreaseQty) {
                    this.IncreaseQty = IncreaseQty;
                }

                public int getMaxPOQty() {
                    return MaxPOQty;
                }

                public void setMaxPOQty(int MaxPOQty) {
                    this.MaxPOQty = MaxPOQty;
                }

                public int getVarLeadTimeLotSize() {
                    return VarLeadTimeLotSize;
                }

                public void setVarLeadTimeLotSize(int VarLeadTimeLotSize) {
                    this.VarLeadTimeLotSize = VarLeadTimeLotSize;
                }

                public int getBaseVarLeadTimeLotSize() {
                    return BaseVarLeadTimeLotSize;
                }

                public void setBaseVarLeadTimeLotSize(int BaseVarLeadTimeLotSize) {
                    this.BaseVarLeadTimeLotSize = BaseVarLeadTimeLotSize;
                }

                public int getPlanGroupId_Id() {
                    return PlanGroupId_Id;
                }

                public void setPlanGroupId_Id(int PlanGroupId_Id) {
                    this.PlanGroupId_Id = PlanGroupId_Id;
                }

                public Object getPlanGroupId() {
                    return PlanGroupId;
                }

                public void setPlanGroupId(Object PlanGroupId) {
                    this.PlanGroupId = PlanGroupId;
                }

                public int getMfgPolicyId_Id() {
                    return MfgPolicyId_Id;
                }

                public void setMfgPolicyId_Id(int MfgPolicyId_Id) {
                    this.MfgPolicyId_Id = MfgPolicyId_Id;
                }

                public MfgPolicyIdBean getMfgPolicyId() {
                    return MfgPolicyId;
                }

                public void setMfgPolicyId(MfgPolicyIdBean MfgPolicyId) {
                    this.MfgPolicyId = MfgPolicyId;
                }

                public int getSupplySourceId_Id() {
                    return SupplySourceId_Id;
                }

                public void setSupplySourceId_Id(int SupplySourceId_Id) {
                    this.SupplySourceId_Id = SupplySourceId_Id;
                }

                public Object getSupplySourceId() {
                    return SupplySourceId;
                }

                public void setSupplySourceId(Object SupplySourceId) {
                    this.SupplySourceId = SupplySourceId;
                }

                public int getTimeFactorId_Id() {
                    return TimeFactorId_Id;
                }

                public void setTimeFactorId_Id(int TimeFactorId_Id) {
                    this.TimeFactorId_Id = TimeFactorId_Id;
                }

                public Object getTimeFactorId() {
                    return TimeFactorId;
                }

                public void setTimeFactorId(Object TimeFactorId) {
                    this.TimeFactorId = TimeFactorId;
                }

                public int getQtyFactorId_Id() {
                    return QtyFactorId_Id;
                }

                public void setQtyFactorId_Id(int QtyFactorId_Id) {
                    this.QtyFactorId_Id = QtyFactorId_Id;
                }

                public Object getQtyFactorId() {
                    return QtyFactorId;
                }

                public void setQtyFactorId(Object QtyFactorId) {
                    this.QtyFactorId = QtyFactorId;
                }

                public String getPlanMode() {
                    return PlanMode;
                }

                public void setPlanMode(String PlanMode) {
                    this.PlanMode = PlanMode;
                }

                public boolean isAllowPartDelay() {
                    return AllowPartDelay;
                }

                public void setAllowPartDelay(boolean AllowPartDelay) {
                    this.AllowPartDelay = AllowPartDelay;
                }

                public boolean isAllowPartAhead() {
                    return AllowPartAhead;
                }

                public void setAllowPartAhead(boolean AllowPartAhead) {
                    this.AllowPartAhead = AllowPartAhead;
                }

                public int getPLANSAFESTOCKQTY() {
                    return PLANSAFESTOCKQTY;
                }

                public void setPLANSAFESTOCKQTY(int PLANSAFESTOCKQTY) {
                    this.PLANSAFESTOCKQTY = PLANSAFESTOCKQTY;
                }

                public int getATOSchemeId_Id() {
                    return ATOSchemeId_Id;
                }

                public void setATOSchemeId_Id(int ATOSchemeId_Id) {
                    this.ATOSchemeId_Id = ATOSchemeId_Id;
                }

                public Object getATOSchemeId() {
                    return ATOSchemeId;
                }

                public void setATOSchemeId(Object ATOSchemeId) {
                    this.ATOSchemeId = ATOSchemeId;
                }

                public static class MfgPolicyIdBean {
                    /**
                     * Id : 40042
                     * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"MTS10(考虑库存)"},{"PkId":6,"LocaleId":1033,"Name":"MTS10 (Include inventory)"}]
                     * Name : [{"Key":2052,"Value":"MTS10(考虑库存)"},{"Key":1033,"Value":"MTS10 (Include inventory)"}]
                     * Number : ZZCL001_SYS
                     * PlanMode : 0
                     * Ato : false
                     */

                    private int Id;
                    private String Number;
                    private String PlanMode;
                    private boolean Ato;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public String getPlanMode() {
                        return PlanMode;
                    }

                    public void setPlanMode(String PlanMode) {
                        this.PlanMode = PlanMode;
                    }

                    public boolean isAto() {
                        return Ato;
                    }

                    public void setAto(boolean Ato) {
                        this.Ato = Ato;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 1
                         * LocaleId : 2052
                         * Name : MTS10(考虑库存)
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : MTS10(考虑库存)
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }
            }

            public static class MaterialProduceBean {
                /**
                 * Id : 379204
                 * PickStockId_Id : 0
                 * PickStockId : null
                 * BOMUnitId_Id : 10101
                 * BOMUnitId : {"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]}
                 * WorkShopId_Id : 0
                 * WorkShopId : null
                 * IssueType : 1
                 * ProduceUnitId_Id : 10101
                 * ProduceUnitId : {"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]}
                 * IsKitting : false
                 * DefaultRouting_Id : 0
                 * DefaultRouting : null
                 * IsCoby : false
                 * PerUnitStandHour : 0
                 * BKFLTime :
                 * FinishReceiptOverRate : 0
                 * FinishReceiptShortRate : 0
                 * PickBinId_Id : 0
                 * PickBinId : null
                 * PrdURNum : 1
                 * PrdURNom : 1
                 * BOMURNum : 1
                 * BOMURNom : 1
                 * IsMainPrd : false
                 * IsCompleteSet : false
                 * OverControlMode : 3
                 * MinIssueQty : 1
                 * StdLaborPrePareTime : 0
                 * StdLaborProcessTime : 0
                 * StdMachinePrepareTime : 0
                 * StdMachineProcessTime : 0
                 * ConsumVolatility : 0
                 * IsProductLine : false
                 * ProduceBillType_Id :
                 * ProduceBillType : null
                 * OrgTrustBillType_Id :
                 * OrgTrustBillType : null
                 * ISMinIssueQty : false
                 * IsECN : false
                 * MinIssueUnitId_Id : 10101
                 * MinIssueUnitId : {"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]}
                 * MDLID_Id : 0
                 * MDLID : null
                 * MdlMaterialId_Id : 0
                 * MdlMaterialId : null
                 * LossPercent : 0
                 * IsSNCarryToParent : false
                 * StandHourUnitId : 3600
                 * BackFlushType : 1
                 */

                private int Id;
                private int PickStockId_Id;
                private Object PickStockId;
                private int BOMUnitId_Id;
                private BOMUnitIdBean BOMUnitId;
                private int WorkShopId_Id;
                private Object WorkShopId;
                private String IssueType;
                private int ProduceUnitId_Id;
                private ProduceUnitIdBean ProduceUnitId;
                private boolean IsKitting;
                private int DefaultRouting_Id;
                private Object DefaultRouting;
                private boolean IsCoby;
                private int PerUnitStandHour;
                private String BKFLTime;
                private int FinishReceiptOverRate;
                private int FinishReceiptShortRate;
                private int PickBinId_Id;
                private Object PickBinId;
                private int PrdURNum;
                private int PrdURNom;
                private int BOMURNum;
                private int BOMURNom;
                private boolean IsMainPrd;
                private boolean IsCompleteSet;
                private String OverControlMode;
                private int MinIssueQty;
                private int StdLaborPrePareTime;
                private int StdLaborProcessTime;
                private int StdMachinePrepareTime;
                private int StdMachineProcessTime;
                private int ConsumVolatility;
                private boolean IsProductLine;
                private String ProduceBillType_Id;
                private Object ProduceBillType;
                private String OrgTrustBillType_Id;
                private Object OrgTrustBillType;
                private boolean ISMinIssueQty;
                private boolean IsECN;
                private int MinIssueUnitId_Id;
                private MinIssueUnitIdBean MinIssueUnitId;
                private int MDLID_Id;
                private Object MDLID;
                private int MdlMaterialId_Id;
                private Object MdlMaterialId;
                private int LossPercent;
                private boolean IsSNCarryToParent;
                private String StandHourUnitId;
                private String BackFlushType;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getPickStockId_Id() {
                    return PickStockId_Id;
                }

                public void setPickStockId_Id(int PickStockId_Id) {
                    this.PickStockId_Id = PickStockId_Id;
                }

                public Object getPickStockId() {
                    return PickStockId;
                }

                public void setPickStockId(Object PickStockId) {
                    this.PickStockId = PickStockId;
                }

                public int getBOMUnitId_Id() {
                    return BOMUnitId_Id;
                }

                public void setBOMUnitId_Id(int BOMUnitId_Id) {
                    this.BOMUnitId_Id = BOMUnitId_Id;
                }

                public BOMUnitIdBean getBOMUnitId() {
                    return BOMUnitId;
                }

                public void setBOMUnitId(BOMUnitIdBean BOMUnitId) {
                    this.BOMUnitId = BOMUnitId;
                }

                public int getWorkShopId_Id() {
                    return WorkShopId_Id;
                }

                public void setWorkShopId_Id(int WorkShopId_Id) {
                    this.WorkShopId_Id = WorkShopId_Id;
                }

                public Object getWorkShopId() {
                    return WorkShopId;
                }

                public void setWorkShopId(Object WorkShopId) {
                    this.WorkShopId = WorkShopId;
                }

                public String getIssueType() {
                    return IssueType;
                }

                public void setIssueType(String IssueType) {
                    this.IssueType = IssueType;
                }

                public int getProduceUnitId_Id() {
                    return ProduceUnitId_Id;
                }

                public void setProduceUnitId_Id(int ProduceUnitId_Id) {
                    this.ProduceUnitId_Id = ProduceUnitId_Id;
                }

                public ProduceUnitIdBean getProduceUnitId() {
                    return ProduceUnitId;
                }

                public void setProduceUnitId(ProduceUnitIdBean ProduceUnitId) {
                    this.ProduceUnitId = ProduceUnitId;
                }

                public boolean isIsKitting() {
                    return IsKitting;
                }

                public void setIsKitting(boolean IsKitting) {
                    this.IsKitting = IsKitting;
                }

                public int getDefaultRouting_Id() {
                    return DefaultRouting_Id;
                }

                public void setDefaultRouting_Id(int DefaultRouting_Id) {
                    this.DefaultRouting_Id = DefaultRouting_Id;
                }

                public Object getDefaultRouting() {
                    return DefaultRouting;
                }

                public void setDefaultRouting(Object DefaultRouting) {
                    this.DefaultRouting = DefaultRouting;
                }

                public boolean isIsCoby() {
                    return IsCoby;
                }

                public void setIsCoby(boolean IsCoby) {
                    this.IsCoby = IsCoby;
                }

                public int getPerUnitStandHour() {
                    return PerUnitStandHour;
                }

                public void setPerUnitStandHour(int PerUnitStandHour) {
                    this.PerUnitStandHour = PerUnitStandHour;
                }

                public String getBKFLTime() {
                    return BKFLTime;
                }

                public void setBKFLTime(String BKFLTime) {
                    this.BKFLTime = BKFLTime;
                }

                public int getFinishReceiptOverRate() {
                    return FinishReceiptOverRate;
                }

                public void setFinishReceiptOverRate(int FinishReceiptOverRate) {
                    this.FinishReceiptOverRate = FinishReceiptOverRate;
                }

                public int getFinishReceiptShortRate() {
                    return FinishReceiptShortRate;
                }

                public void setFinishReceiptShortRate(int FinishReceiptShortRate) {
                    this.FinishReceiptShortRate = FinishReceiptShortRate;
                }

                public int getPickBinId_Id() {
                    return PickBinId_Id;
                }

                public void setPickBinId_Id(int PickBinId_Id) {
                    this.PickBinId_Id = PickBinId_Id;
                }

                public Object getPickBinId() {
                    return PickBinId;
                }

                public void setPickBinId(Object PickBinId) {
                    this.PickBinId = PickBinId;
                }

                public int getPrdURNum() {
                    return PrdURNum;
                }

                public void setPrdURNum(int PrdURNum) {
                    this.PrdURNum = PrdURNum;
                }

                public int getPrdURNom() {
                    return PrdURNom;
                }

                public void setPrdURNom(int PrdURNom) {
                    this.PrdURNom = PrdURNom;
                }

                public int getBOMURNum() {
                    return BOMURNum;
                }

                public void setBOMURNum(int BOMURNum) {
                    this.BOMURNum = BOMURNum;
                }

                public int getBOMURNom() {
                    return BOMURNom;
                }

                public void setBOMURNom(int BOMURNom) {
                    this.BOMURNom = BOMURNom;
                }

                public boolean isIsMainPrd() {
                    return IsMainPrd;
                }

                public void setIsMainPrd(boolean IsMainPrd) {
                    this.IsMainPrd = IsMainPrd;
                }

                public boolean isIsCompleteSet() {
                    return IsCompleteSet;
                }

                public void setIsCompleteSet(boolean IsCompleteSet) {
                    this.IsCompleteSet = IsCompleteSet;
                }

                public String getOverControlMode() {
                    return OverControlMode;
                }

                public void setOverControlMode(String OverControlMode) {
                    this.OverControlMode = OverControlMode;
                }

                public int getMinIssueQty() {
                    return MinIssueQty;
                }

                public void setMinIssueQty(int MinIssueQty) {
                    this.MinIssueQty = MinIssueQty;
                }

                public int getStdLaborPrePareTime() {
                    return StdLaborPrePareTime;
                }

                public void setStdLaborPrePareTime(int StdLaborPrePareTime) {
                    this.StdLaborPrePareTime = StdLaborPrePareTime;
                }

                public int getStdLaborProcessTime() {
                    return StdLaborProcessTime;
                }

                public void setStdLaborProcessTime(int StdLaborProcessTime) {
                    this.StdLaborProcessTime = StdLaborProcessTime;
                }

                public int getStdMachinePrepareTime() {
                    return StdMachinePrepareTime;
                }

                public void setStdMachinePrepareTime(int StdMachinePrepareTime) {
                    this.StdMachinePrepareTime = StdMachinePrepareTime;
                }

                public int getStdMachineProcessTime() {
                    return StdMachineProcessTime;
                }

                public void setStdMachineProcessTime(int StdMachineProcessTime) {
                    this.StdMachineProcessTime = StdMachineProcessTime;
                }

                public int getConsumVolatility() {
                    return ConsumVolatility;
                }

                public void setConsumVolatility(int ConsumVolatility) {
                    this.ConsumVolatility = ConsumVolatility;
                }

                public boolean isIsProductLine() {
                    return IsProductLine;
                }

                public void setIsProductLine(boolean IsProductLine) {
                    this.IsProductLine = IsProductLine;
                }

                public String getProduceBillType_Id() {
                    return ProduceBillType_Id;
                }

                public void setProduceBillType_Id(String ProduceBillType_Id) {
                    this.ProduceBillType_Id = ProduceBillType_Id;
                }

                public Object getProduceBillType() {
                    return ProduceBillType;
                }

                public void setProduceBillType(Object ProduceBillType) {
                    this.ProduceBillType = ProduceBillType;
                }

                public String getOrgTrustBillType_Id() {
                    return OrgTrustBillType_Id;
                }

                public void setOrgTrustBillType_Id(String OrgTrustBillType_Id) {
                    this.OrgTrustBillType_Id = OrgTrustBillType_Id;
                }

                public Object getOrgTrustBillType() {
                    return OrgTrustBillType;
                }

                public void setOrgTrustBillType(Object OrgTrustBillType) {
                    this.OrgTrustBillType = OrgTrustBillType;
                }

                public boolean isISMinIssueQty() {
                    return ISMinIssueQty;
                }

                public void setISMinIssueQty(boolean ISMinIssueQty) {
                    this.ISMinIssueQty = ISMinIssueQty;
                }

                public boolean isIsECN() {
                    return IsECN;
                }

                public void setIsECN(boolean IsECN) {
                    this.IsECN = IsECN;
                }

                public int getMinIssueUnitId_Id() {
                    return MinIssueUnitId_Id;
                }

                public void setMinIssueUnitId_Id(int MinIssueUnitId_Id) {
                    this.MinIssueUnitId_Id = MinIssueUnitId_Id;
                }

                public MinIssueUnitIdBean getMinIssueUnitId() {
                    return MinIssueUnitId;
                }

                public void setMinIssueUnitId(MinIssueUnitIdBean MinIssueUnitId) {
                    this.MinIssueUnitId = MinIssueUnitId;
                }

                public int getMDLID_Id() {
                    return MDLID_Id;
                }

                public void setMDLID_Id(int MDLID_Id) {
                    this.MDLID_Id = MDLID_Id;
                }

                public Object getMDLID() {
                    return MDLID;
                }

                public void setMDLID(Object MDLID) {
                    this.MDLID = MDLID;
                }

                public int getMdlMaterialId_Id() {
                    return MdlMaterialId_Id;
                }

                public void setMdlMaterialId_Id(int MdlMaterialId_Id) {
                    this.MdlMaterialId_Id = MdlMaterialId_Id;
                }

                public Object getMdlMaterialId() {
                    return MdlMaterialId;
                }

                public void setMdlMaterialId(Object MdlMaterialId) {
                    this.MdlMaterialId = MdlMaterialId;
                }

                public int getLossPercent() {
                    return LossPercent;
                }

                public void setLossPercent(int LossPercent) {
                    this.LossPercent = LossPercent;
                }

                public boolean isIsSNCarryToParent() {
                    return IsSNCarryToParent;
                }

                public void setIsSNCarryToParent(boolean IsSNCarryToParent) {
                    this.IsSNCarryToParent = IsSNCarryToParent;
                }

                public String getStandHourUnitId() {
                    return StandHourUnitId;
                }

                public void setStandHourUnitId(String StandHourUnitId) {
                    this.StandHourUnitId = StandHourUnitId;
                }

                public String getBackFlushType() {
                    return BackFlushType;
                }

                public void setBackFlushType(String BackFlushType) {
                    this.BackFlushType = BackFlushType;
                }

                public static class BOMUnitIdBean {
                    /**
                     * Id : 10101
                     * msterID : 10101
                     * MultiLanguageText : [{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}]
                     * Name : [{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}]
                     * Number : Pcs
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10086
                     * UnitGroupId : {"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10118,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanXXXXXXXX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanXXXXXXXX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanXXXXXXXX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanXXXXXXXX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanXXXXXXXX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanXXXXXXXX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanXXXXXXXX {
                        /**
                         * Id : 10086
                         * msterID : 10086
                         * Number : Quantity
                         * MultiLanguageText : [{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}]
                         * Name : [{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * PkId : 9
                             * LocaleId : 2052
                             * Name : 数量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 数量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 23
                         * LocaleId : 2052
                         * Name : 个
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 个
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanXXXXXXXX {
                        /**
                         * Id : 10118
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }

                public static class ProduceUnitIdBean {
                    /**
                     * Id : 10101
                     * msterID : 10101
                     * MultiLanguageText : [{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}]
                     * Name : [{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}]
                     * Number : Pcs
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10086
                     * UnitGroupId : {"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10118,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanXXXXXXXXX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanXXXXXXXXX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanXXXXXXXXX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanXXXXXXXXX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanXXXXXXXXX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanXXXXXXXXX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanXXXXXXXXX {
                        /**
                         * Id : 10086
                         * msterID : 10086
                         * Number : Quantity
                         * MultiLanguageText : [{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}]
                         * Name : [{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * PkId : 9
                             * LocaleId : 2052
                             * Name : 数量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 数量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 23
                         * LocaleId : 2052
                         * Name : 个
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 个
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanXXXXXXXXX {
                        /**
                         * Id : 10118
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }

                public static class MinIssueUnitIdBean {
                    /**
                     * Id : 10101
                     * msterID : 10101
                     * MultiLanguageText : [{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}]
                     * Name : [{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}]
                     * Number : Pcs
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10086
                     * UnitGroupId : {"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10118,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanXXXXXXXXXX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanXXXXXXXXXX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanXXXXXXXXXX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanXXXXXXXXXX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanXXXXXXXXXX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanXXXXXXXXXX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanXXXXXXXXXX {
                        /**
                         * Id : 10086
                         * msterID : 10086
                         * Number : Quantity
                         * MultiLanguageText : [{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}]
                         * Name : [{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * PkId : 9
                             * LocaleId : 2052
                             * Name : 数量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 数量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 23
                         * LocaleId : 2052
                         * Name : 个
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 个
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanXXXXXXXXXX {
                        /**
                         * Id : 10118
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }
            }

            public static class MaterialInvPtyBean {
                /**
                 * Id : 1495940
                 * IsEnable : true
                 * IsAffectPrice : false
                 * IsAffectPlan : false
                 * IsAffectCost : false
                 * InvPtyId_Id : 10001
                 * InvPtyId : {"Id":10001,"msterID":10001,"Number":"01","MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"仓库"},{"PkId":7,"LocaleId":1033,"Name":"Warehouse"}],"Name":[{"Key":2052,"Value":"仓库"},{"Key":1033,"Value":"Warehouse"}]}
                 */

                private int Id;
                private boolean IsEnable;
                private boolean IsAffectPrice;
                private boolean IsAffectPlan;
                private boolean IsAffectCost;
                private int InvPtyId_Id;
                private InvPtyIdBean InvPtyId;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public boolean isIsEnable() {
                    return IsEnable;
                }

                public void setIsEnable(boolean IsEnable) {
                    this.IsEnable = IsEnable;
                }

                public boolean isIsAffectPrice() {
                    return IsAffectPrice;
                }

                public void setIsAffectPrice(boolean IsAffectPrice) {
                    this.IsAffectPrice = IsAffectPrice;
                }

                public boolean isIsAffectPlan() {
                    return IsAffectPlan;
                }

                public void setIsAffectPlan(boolean IsAffectPlan) {
                    this.IsAffectPlan = IsAffectPlan;
                }

                public boolean isIsAffectCost() {
                    return IsAffectCost;
                }

                public void setIsAffectCost(boolean IsAffectCost) {
                    this.IsAffectCost = IsAffectCost;
                }

                public int getInvPtyId_Id() {
                    return InvPtyId_Id;
                }

                public void setInvPtyId_Id(int InvPtyId_Id) {
                    this.InvPtyId_Id = InvPtyId_Id;
                }

                public InvPtyIdBean getInvPtyId() {
                    return InvPtyId;
                }

                public void setInvPtyId(InvPtyIdBean InvPtyId) {
                    this.InvPtyId = InvPtyId;
                }

                public static class InvPtyIdBean {
                    /**
                     * Id : 10001
                     * msterID : 10001
                     * Number : 01
                     * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"仓库"},{"PkId":7,"LocaleId":1033,"Name":"Warehouse"}]
                     * Name : [{"Key":2052,"Value":"仓库"},{"Key":1033,"Value":"Warehouse"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 1
                         * LocaleId : 2052
                         * Name : 仓库
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 仓库
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }
            }

            public static class MaterialSubconBean {
                /**
                 * Id : 379204
                 * SubconUnitId_Id : 10101
                 * SubconUnitId : {"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]}
                 * SubconPriceUnitId_Id : 10101
                 * SubconPriceUnitId : {"Id":10101,"msterID":10101,"MultiLanguageText":[{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}],"Name":[{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}],"Number":"Pcs","IsBaseUnit":true,"UnitGroupId_Id":10086,"UnitGroupId":{"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]},"Precision":3,"RoundType":"1","UNITCONVERTRATE":[{"Id":10118,"ConvertType":"0"}]}
                 * SUBCONURNUM : 1
                 * SUBCONURNOM : 1
                 * SUBCONPRICEURNUM : 1
                 * SUBCONPRICEURNOM : 1
                 * SUBBILLTYPE_Id : 93904bf745d84ae0ad08da30949754e0
                 * SUBBILLTYPE : {"Id":"93904bf745d84ae0ad08da30949754e0","MultiLanguageText":[{"PkId":"c0a9cc13-6893-4629-a0a1-0ea3fbcea629","LocaleId":1033,"Name":"Common Subcontract Order"},{"PkId":"ae166c94ae10412b8197a89f86d903e7","LocaleId":2052,"Name":"普通委外订单"}],"Name":[{"Key":1033,"Value":"Common Subcontract Order"},{"Key":2052,"Value":"普通委外订单"}],"Number":"WWDD01_SYS","IsDefault":true}
                 */

                private int Id;
                private int SubconUnitId_Id;
                private SubconUnitIdBean SubconUnitId;
                private int SubconPriceUnitId_Id;
                private SubconPriceUnitIdBean SubconPriceUnitId;
                private int SUBCONURNUM;
                private int SUBCONURNOM;
                private int SUBCONPRICEURNUM;
                private int SUBCONPRICEURNOM;
                private String SUBBILLTYPE_Id;
                private SUBBILLTYPEBean SUBBILLTYPE;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getSubconUnitId_Id() {
                    return SubconUnitId_Id;
                }

                public void setSubconUnitId_Id(int SubconUnitId_Id) {
                    this.SubconUnitId_Id = SubconUnitId_Id;
                }

                public SubconUnitIdBean getSubconUnitId() {
                    return SubconUnitId;
                }

                public void setSubconUnitId(SubconUnitIdBean SubconUnitId) {
                    this.SubconUnitId = SubconUnitId;
                }

                public int getSubconPriceUnitId_Id() {
                    return SubconPriceUnitId_Id;
                }

                public void setSubconPriceUnitId_Id(int SubconPriceUnitId_Id) {
                    this.SubconPriceUnitId_Id = SubconPriceUnitId_Id;
                }

                public SubconPriceUnitIdBean getSubconPriceUnitId() {
                    return SubconPriceUnitId;
                }

                public void setSubconPriceUnitId(SubconPriceUnitIdBean SubconPriceUnitId) {
                    this.SubconPriceUnitId = SubconPriceUnitId;
                }

                public int getSUBCONURNUM() {
                    return SUBCONURNUM;
                }

                public void setSUBCONURNUM(int SUBCONURNUM) {
                    this.SUBCONURNUM = SUBCONURNUM;
                }

                public int getSUBCONURNOM() {
                    return SUBCONURNOM;
                }

                public void setSUBCONURNOM(int SUBCONURNOM) {
                    this.SUBCONURNOM = SUBCONURNOM;
                }

                public int getSUBCONPRICEURNUM() {
                    return SUBCONPRICEURNUM;
                }

                public void setSUBCONPRICEURNUM(int SUBCONPRICEURNUM) {
                    this.SUBCONPRICEURNUM = SUBCONPRICEURNUM;
                }

                public int getSUBCONPRICEURNOM() {
                    return SUBCONPRICEURNOM;
                }

                public void setSUBCONPRICEURNOM(int SUBCONPRICEURNOM) {
                    this.SUBCONPRICEURNOM = SUBCONPRICEURNOM;
                }

                public String getSUBBILLTYPE_Id() {
                    return SUBBILLTYPE_Id;
                }

                public void setSUBBILLTYPE_Id(String SUBBILLTYPE_Id) {
                    this.SUBBILLTYPE_Id = SUBBILLTYPE_Id;
                }

                public SUBBILLTYPEBean getSUBBILLTYPE() {
                    return SUBBILLTYPE;
                }

                public void setSUBBILLTYPE(SUBBILLTYPEBean SUBBILLTYPE) {
                    this.SUBBILLTYPE = SUBBILLTYPE;
                }

                public static class SubconUnitIdBean {
                    /**
                     * Id : 10101
                     * msterID : 10101
                     * MultiLanguageText : [{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}]
                     * Name : [{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}]
                     * Number : Pcs
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10086
                     * UnitGroupId : {"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10118,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanXXXXXXXXXXX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanXXXXXXXXXXX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanXXXXXXXXXXX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanXXXXXXXXXXX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanXXXXXXXXXXX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanXXXXXXXXXXX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanXXXXXXXXXXX {
                        /**
                         * Id : 10086
                         * msterID : 10086
                         * Number : Quantity
                         * MultiLanguageText : [{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}]
                         * Name : [{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * PkId : 9
                             * LocaleId : 2052
                             * Name : 数量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 数量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 23
                         * LocaleId : 2052
                         * Name : 个
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 个
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanXXXXXXXXXXX {
                        /**
                         * Id : 10118
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }

                public static class SubconPriceUnitIdBean {
                    /**
                     * Id : 10101
                     * msterID : 10101
                     * MultiLanguageText : [{"PkId":23,"LocaleId":2052,"Name":"个"},{"PkId":41,"LocaleId":1033,"Name":"ge"}]
                     * Name : [{"Key":2052,"Value":"个"},{"Key":1033,"Value":"ge"}]
                     * Number : Pcs
                     * IsBaseUnit : true
                     * UnitGroupId_Id : 10086
                     * UnitGroupId : {"Id":10086,"msterID":10086,"Number":"Quantity","MultiLanguageText":[{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}],"Name":[{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]}
                     * Precision : 3
                     * RoundType : 1
                     * UNITCONVERTRATE : [{"Id":10118,"ConvertType":"0"}]
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private boolean IsBaseUnit;
                    private int UnitGroupId_Id;
                    private UnitGroupIdBeanXXXXXXXXXXXX UnitGroupId;
                    private int Precision;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;
                    private List<UNITCONVERTRATEBeanXXXXXXXXXXXX> UNITCONVERTRATE;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsBaseUnit() {
                        return IsBaseUnit;
                    }

                    public void setIsBaseUnit(boolean IsBaseUnit) {
                        this.IsBaseUnit = IsBaseUnit;
                    }

                    public int getUnitGroupId_Id() {
                        return UnitGroupId_Id;
                    }

                    public void setUnitGroupId_Id(int UnitGroupId_Id) {
                        this.UnitGroupId_Id = UnitGroupId_Id;
                    }

                    public UnitGroupIdBeanXXXXXXXXXXXX getUnitGroupId() {
                        return UnitGroupId;
                    }

                    public void setUnitGroupId(UnitGroupIdBeanXXXXXXXXXXXX UnitGroupId) {
                        this.UnitGroupId = UnitGroupId;
                    }

                    public int getPrecision() {
                        return Precision;
                    }

                    public void setPrecision(int Precision) {
                        this.Precision = Precision;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public List<UNITCONVERTRATEBeanXXXXXXXXXXXX> getUNITCONVERTRATE() {
                        return UNITCONVERTRATE;
                    }

                    public void setUNITCONVERTRATE(List<UNITCONVERTRATEBeanXXXXXXXXXXXX> UNITCONVERTRATE) {
                        this.UNITCONVERTRATE = UNITCONVERTRATE;
                    }

                    public static class UnitGroupIdBeanXXXXXXXXXXXX {
                        /**
                         * Id : 10086
                         * msterID : 10086
                         * Number : Quantity
                         * MultiLanguageText : [{"PkId":9,"LocaleId":2052,"Name":"数量"},{"PkId":18,"LocaleId":1033,"Name":"Qty"}]
                         * Name : [{"Key":2052,"Value":"数量"},{"Key":1033,"Value":"Qty"}]
                         */

                        private int Id;
                        private int msterID;
                        private String Number;
                        private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                        private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public int getMsterID() {
                            return msterID;
                        }

                        public void setMsterID(int msterID) {
                            this.msterID = msterID;
                        }

                        public String getNumber() {
                            return Number;
                        }

                        public void setNumber(String Number) {
                            this.Number = Number;
                        }

                        public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                            return MultiLanguageText;
                        }

                        public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                            this.MultiLanguageText = MultiLanguageText;
                        }

                        public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                            return Name;
                        }

                        public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                            this.Name = Name;
                        }

                        public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * PkId : 9
                             * LocaleId : 2052
                             * Name : 数量
                             */

                            private int PkId;
                            private int LocaleId;
                            private String Name;

                            public int getPkId() {
                                return PkId;
                            }

                            public void setPkId(int PkId) {
                                this.PkId = PkId;
                            }

                            public int getLocaleId() {
                                return LocaleId;
                            }

                            public void setLocaleId(int LocaleId) {
                                this.LocaleId = LocaleId;
                            }

                            public String getName() {
                                return Name;
                            }

                            public void setName(String Name) {
                                this.Name = Name;
                            }
                        }

                        public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                            /**
                             * Key : 2052
                             * Value : 数量
                             */

                            private int Key;
                            private String Value;

                            public int getKey() {
                                return Key;
                            }

                            public void setKey(int Key) {
                                this.Key = Key;
                            }

                            public String getValue() {
                                return Value;
                            }

                            public void setValue(String Value) {
                                this.Value = Value;
                            }
                        }
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 23
                         * LocaleId : 2052
                         * Name : 个
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 个
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }

                    public static class UNITCONVERTRATEBeanXXXXXXXXXXXX {
                        /**
                         * Id : 10118
                         * ConvertType : 0
                         */

                        private int Id;
                        private String ConvertType;

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.Id = Id;
                        }

                        public String getConvertType() {
                            return ConvertType;
                        }

                        public void setConvertType(String ConvertType) {
                            this.ConvertType = ConvertType;
                        }
                    }
                }

                public static class SUBBILLTYPEBean {
                    /**
                     * Id : 93904bf745d84ae0ad08da30949754e0
                     * MultiLanguageText : [{"PkId":"c0a9cc13-6893-4629-a0a1-0ea3fbcea629","LocaleId":1033,"Name":"Common Subcontract Order"},{"PkId":"ae166c94ae10412b8197a89f86d903e7","LocaleId":2052,"Name":"普通委外订单"}]
                     * Name : [{"Key":1033,"Value":"Common Subcontract Order"},{"Key":2052,"Value":"普通委外订单"}]
                     * Number : WWDD01_SYS
                     * IsDefault : true
                     */

                    private String Id;
                    private String Number;
                    private boolean IsDefault;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name;

                    public String getId() {
                        return Id;
                    }

                    public void setId(String Id) {
                        this.Id = Id;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public boolean isIsDefault() {
                        return IsDefault;
                    }

                    public void setIsDefault(boolean IsDefault) {
                        this.IsDefault = IsDefault;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : c0a9cc13-6893-4629-a0a1-0ea3fbcea629
                         * LocaleId : 1033
                         * Name : Common Subcontract Order
                         */

                        private String PkId;
                        private int LocaleId;
                        private String Name;

                        public String getPkId() {
                            return PkId;
                        }

                        public void setPkId(String PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {
                        /**
                         * Key : 1033
                         * Value : Common Subcontract Order
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }
            }

            public static class MaterialQMBean {
                /**
                 * Id : 379204
                 * CheckProduct : true
                 * CheckIncoming : true
                 * IncSampSchemeId_Id : 0
                 * IncSampSchemeId : null
                 * IncQcSchemeId_Id : 0
                 * IncQcSchemeId : null
                 * CheckStock : false
                 * EnableCyclistQCSTK : false
                 * EnableCyclistQCSTKEW : false
                 * EWLeadDay : 0
                 * StockCycle : 0
                 * CheckDelivery : false
                 * CheckReturn : false
                 * InspectGroupId_Id : 0
                 * InspectGroupId : null
                 * InspectorId_Id : 0
                 * InspectorId : null
                 * CheckEntrusted : false
                 * CheckOther : false
                 */

                private int Id;
                private boolean CheckProduct;
                private boolean CheckIncoming;
                private int IncSampSchemeId_Id;
                private Object IncSampSchemeId;
                private int IncQcSchemeId_Id;
                private Object IncQcSchemeId;
                private boolean CheckStock;
                private boolean EnableCyclistQCSTK;
                private boolean EnableCyclistQCSTKEW;
                private int EWLeadDay;
                private int StockCycle;
                private boolean CheckDelivery;
                private boolean CheckReturn;
                private int InspectGroupId_Id;
                private Object InspectGroupId;
                private int InspectorId_Id;
                private Object InspectorId;
                private boolean CheckEntrusted;
                private boolean CheckOther;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public boolean isCheckProduct() {
                    return CheckProduct;
                }

                public void setCheckProduct(boolean CheckProduct) {
                    this.CheckProduct = CheckProduct;
                }

                public boolean isCheckIncoming() {
                    return CheckIncoming;
                }

                public void setCheckIncoming(boolean CheckIncoming) {
                    this.CheckIncoming = CheckIncoming;
                }

                public int getIncSampSchemeId_Id() {
                    return IncSampSchemeId_Id;
                }

                public void setIncSampSchemeId_Id(int IncSampSchemeId_Id) {
                    this.IncSampSchemeId_Id = IncSampSchemeId_Id;
                }

                public Object getIncSampSchemeId() {
                    return IncSampSchemeId;
                }

                public void setIncSampSchemeId(Object IncSampSchemeId) {
                    this.IncSampSchemeId = IncSampSchemeId;
                }

                public int getIncQcSchemeId_Id() {
                    return IncQcSchemeId_Id;
                }

                public void setIncQcSchemeId_Id(int IncQcSchemeId_Id) {
                    this.IncQcSchemeId_Id = IncQcSchemeId_Id;
                }

                public Object getIncQcSchemeId() {
                    return IncQcSchemeId;
                }

                public void setIncQcSchemeId(Object IncQcSchemeId) {
                    this.IncQcSchemeId = IncQcSchemeId;
                }

                public boolean isCheckStock() {
                    return CheckStock;
                }

                public void setCheckStock(boolean CheckStock) {
                    this.CheckStock = CheckStock;
                }

                public boolean isEnableCyclistQCSTK() {
                    return EnableCyclistQCSTK;
                }

                public void setEnableCyclistQCSTK(boolean EnableCyclistQCSTK) {
                    this.EnableCyclistQCSTK = EnableCyclistQCSTK;
                }

                public boolean isEnableCyclistQCSTKEW() {
                    return EnableCyclistQCSTKEW;
                }

                public void setEnableCyclistQCSTKEW(boolean EnableCyclistQCSTKEW) {
                    this.EnableCyclistQCSTKEW = EnableCyclistQCSTKEW;
                }

                public int getEWLeadDay() {
                    return EWLeadDay;
                }

                public void setEWLeadDay(int EWLeadDay) {
                    this.EWLeadDay = EWLeadDay;
                }

                public int getStockCycle() {
                    return StockCycle;
                }

                public void setStockCycle(int StockCycle) {
                    this.StockCycle = StockCycle;
                }

                public boolean isCheckDelivery() {
                    return CheckDelivery;
                }

                public void setCheckDelivery(boolean CheckDelivery) {
                    this.CheckDelivery = CheckDelivery;
                }

                public boolean isCheckReturn() {
                    return CheckReturn;
                }

                public void setCheckReturn(boolean CheckReturn) {
                    this.CheckReturn = CheckReturn;
                }

                public int getInspectGroupId_Id() {
                    return InspectGroupId_Id;
                }

                public void setInspectGroupId_Id(int InspectGroupId_Id) {
                    this.InspectGroupId_Id = InspectGroupId_Id;
                }

                public Object getInspectGroupId() {
                    return InspectGroupId;
                }

                public void setInspectGroupId(Object InspectGroupId) {
                    this.InspectGroupId = InspectGroupId;
                }

                public int getInspectorId_Id() {
                    return InspectorId_Id;
                }

                public void setInspectorId_Id(int InspectorId_Id) {
                    this.InspectorId_Id = InspectorId_Id;
                }

                public Object getInspectorId() {
                    return InspectorId;
                }

                public void setInspectorId(Object InspectorId) {
                    this.InspectorId = InspectorId;
                }

                public boolean isCheckEntrusted() {
                    return CheckEntrusted;
                }

                public void setCheckEntrusted(boolean CheckEntrusted) {
                    this.CheckEntrusted = CheckEntrusted;
                }

                public boolean isCheckOther() {
                    return CheckOther;
                }

                public void setCheckOther(boolean CheckOther) {
                    this.CheckOther = CheckOther;
                }
            }
        }
    }
}