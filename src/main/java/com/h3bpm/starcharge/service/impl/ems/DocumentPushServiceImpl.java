package com.h3bpm.starcharge.service.impl.ems;

import OThinker.Common.Data.BoolMatchValue;
import OThinker.Common.Organization.Models.User;
import OThinker.H3.Entity.Data.Attachment.Attachment;
import OThinker.H3.Entity.Data.Attachment.AttachmentHeader;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.base.res.ResBody;
import com.h3bpm.base.util.AppUtility;
import com.h3bpm.starcharge.common.bean.Bestsign_file;
import com.h3bpm.starcharge.common.uitl.ems.UploadToServer;
import com.h3bpm.starcharge.service.ems.DocumentPushService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class DocumentPushServiceImpl implements DocumentPushService {

    @Value("${documectPost}")
    private String documectPost;

    private static String filePath = "D:/";

    @Override
    public ResBody pushAttachment(InstanceData instanceData) throws Exception {
        List<AttachmentHeader> headers = AppUtility.getEngine().getBizObjectManager().QueryAttachment(
                instanceData.getSchemaCode(),instanceData.getBizObject().getObjectID(), "approvalPrice", BoolMatchValue.True, "");
        String res = "";
        for (int i = 0 ; i < headers.size(); i++) {
            AttachmentHeader header = headers.get(i);
            Attachment attachment = AppUtility.getEngine().getBizObjectManager().GetAttachment(User.AdministratorID,
                    instanceData.getSchemaCode(),
                    instanceData.getBizObject().getObjectID(),
                    header.getObjectID());
            Map<String, String> param = new HashMap<>(6);
            param.put("file_name",attachment.getFileName());
            param.put("proc_no",instanceData.getInstanceId());
            param.put("creator",instanceData.getInstanceContext().getOriginatorName());
            param.put("approvor","曹晓琭");
            param.put("proc_status","已完成");
            param.put("remark",instanceData.getItem("contractId").getValue().toString());
            File tmpFile = new File(filePath+ instanceData.getItem("contractId").getValue()+"."+ attachment.getFileName().split("\\.")[1]);
            try {
                FileUtils.writeByteArrayToFile(tmpFile,attachment.getContent(), false);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                 res =  UploadToServer.postFile(param,tmpFile,documectPost);

            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("========pushAttachment====="+res);
            tmpFile.delete();
        }
        return  ResBody.buildSuccessResBody(res);
    }

    @Override
    public ResBody postFileToEMS(Bestsign_file bestsignFile) throws Exception {
        return  ResBody.buildSuccessResBody(UploadToServer.postFileToEMS(bestsignFile,documectPost));
    }
}
