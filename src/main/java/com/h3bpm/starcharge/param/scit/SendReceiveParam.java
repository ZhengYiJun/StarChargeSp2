package com.h3bpm.starcharge.param.scit;

/**
 * scit回调接口参数
 *
 * @author LLongAgo
 * @date 2019/2/20
 * @since 1.0.0
 */
public class SendReceiveParam {

    /**
     * processInstanceId : 123123
     * result : OK
     * remark : xxxxxx
     */

    private String processInstanceId;
    private String result;
    private String remark;

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}