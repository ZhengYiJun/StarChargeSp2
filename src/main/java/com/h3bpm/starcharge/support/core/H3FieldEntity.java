package com.h3bpm.starcharge.support.core;

import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @ClassName FieldEntity
 * @Desciption 表单属性
 * @Author lvyz
 * @Date 2019/6/10 18:09
 **/
public class H3FieldEntity {

	// 显示名
	private String displayName;

	// 字段名
	private String name;

	private List<H3FieldEntity> childList;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<H3FieldEntity> getChildList() {
		return childList;
	}

	public void setChildList(List<H3FieldEntity> childList) {
		this.childList = childList;
	}

	public boolean hasChild(){
		return !CollectionUtils.isEmpty(this.childList);
	}
}
