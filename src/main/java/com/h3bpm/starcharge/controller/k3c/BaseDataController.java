package com.h3bpm.starcharge.controller.k3c;

import com.h3bpm.starcharge.common.bean.JsonResult;
import com.h3bpm.starcharge.common.uitl.ErrorUtil;
import com.h3bpm.starcharge.service.k3c.BaseDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 基础数据控制器
 *
 * @author LLongAgo
 * @date 2019/3/13
 * @since 1.0.0
 */
@Controller
@RequestMapping("/k3c/basedata")
public class BaseDataController {

    @Autowired
    private BaseDataService service;

    /**
     * 计量单位
     *
     * @return JsonResult
     */
    @RequestMapping(value = "/unitList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public JsonResult getUnit() {
        try {
            return service.getUnit();
        } catch (Exception e) {
            return ErrorUtil.packageServerError(e);
        }
    }

    /**
     * 存货类别
     *
     * @return JsonResult
     */
    @RequestMapping(value = "/materialCateGoryList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public JsonResult getMaterialCateGory() {
        try {
            return service.getMaterialCateGory();
        } catch (Exception e) {
            return ErrorUtil.packageServerError(e);
        }
    }

    /**
     * 仓库
     *
     * @param  useOrgId 使用组织id
     * @return JsonResult
     */
    @RequestMapping(value = "/stockList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public JsonResult getStock(String useOrgId) {
        try {
            return service.getStock(useOrgId);
        } catch (Exception e) {
            return ErrorUtil.packageServerError(e);
        }
    }

    /**
     * 财务记账类型
     *
     * @return JsonResult
     */
    @RequestMapping(value = "/tallyTypeList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public JsonResult getTallyType() {
        try {
            return service.getTallyType();
        } catch (Exception e) {
            return ErrorUtil.packageServerError(e);
        }
    }

    /**
     * 组织机构
     *
     * @param  searchCode String
     * @return JsonResult
     */
    @RequestMapping(value = "/organizationList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public JsonResult getOrganizationList(String searchCode) {
        try {
            return service.getOrganizationList(searchCode != null ? searchCode : "");
        } catch (Exception e) {
            return ErrorUtil.packageServerError(e);
        }
    }

    /**
     * 物料分组
     * @param name 分组名字
     * @return JsonResult
     */
    @RequestMapping(value = "/materialGroup", produces = "application/json;charset=utf-8")
    @ResponseBody
    public JsonResult getMaterialGroup(String name) {
        try {
            return service.getMaterialGroup(name);
        } catch (Exception e) {
            return ErrorUtil.packageServerError(e);
        }
    }

    /**
     * 仓位
     * @param number 仓库number
     * @return JsonResult
     */
    @RequestMapping(value = "/stockLoc", produces = "application/json;charset=utf-8")
    @ResponseBody
    public JsonResult getStockLoc(String number) {
        try {
            return service.getStockLoc(number);
        } catch (Exception e) {
            return ErrorUtil.packageServerError(e);
        }
    }

}