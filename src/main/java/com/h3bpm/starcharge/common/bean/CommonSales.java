package com.h3bpm.starcharge.common.bean;

/**
 * @Author : Coco
 * @Description :
 * @Date : Create in 18:11 2019/1/2
 * @Modified :
 */
public class CommonSales {
    // 合同类型
    public final static String PURCHASECONTRACTNAME = "采购";
    public final static String CONSTRUCTIONCONTRACTNAME = "施工";
    public final static String ENTRUSTCONTRACTNAME = "委托";

    // 合同模板id
    /**
     * 采购
     */
    public final static String PURCHASE_ID = "2156410776833753089";
    // 测试2159248247539892227
    //public final static String PURCHASE_ID = "2159248247539892227";
    /**
     * 施工
     */
    public final static String CONSTRUCTION_ID = "2156330220678810631";
    /**
     * 采购+施工组合
     */
    public final static String PURCHASE_CONSTRUCTION_ID = "2156451137807453185";
    /**
     * 采购+委托组合
     */
    public final static String PURCHASE_ENTRUST_ID = "2157130551411081225";
    /**
     * 采购+施工+委托组合
     */
    public final static String PURCHASE_CONSTRUCTION_ENTRUST_ID = "2156397915931675650";
}
