package com.h3bpm.starcharge.bean.k3c;

import java.util.List;

/**
 * @author LLongAgo
 * @date 2019/4/11
 * @since 1.0.0
 */
public class SupplierView {

    /**
     * Result : {"ResponseStatus":null,"Result":{"Id":164442,"msterID":164442,"DocumentStatus":"C","ForbidStatus":"A","MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司","Description":" ","ShortName":" "}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006","Description":[{"Key":2052,"Value":" "}],"CreateOrgId_Id":1,"CreateOrgId":{"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"},"UseOrgId_Id":1,"UseOrgId":{"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"},"CreatorId_Id":110362,"CreatorId":{"Id":110362,"Name":"admin","UserAccount":"admin"},"ModifierId_Id":671703,"ModifierId":{"Id":671703,"Name":"张敏","UserAccount":"张敏"},"CreateDate":"2018-12-17T17:01:14.183","FModifyDate":"2019-04-12T09:35:08.773","ShortName":[{"Key":2052,"Value":" "}],"ForbiderId_Id":0,"ForbiderId":null,"ForbidDate":null,"FAuditDate":"2019-04-12T09:35:10.093","AuditorId_Id":671703,"AuditorId":{"Id":671703,"Name":"张敏","UserAccount":"张敏"},"FGroup_Id":110373,"FGroup":{"Id":110373,"Number":"11","MultiLanguageText":[{"PkId":100003,"LocaleId":2052,"Name":"北京"}],"Name":[{"Key":2052,"Value":"北京"}]},"CorrespondOrgId_Id":0,"CorrespondOrgId":null,"SYNCGYOWNERSTATUS":false,"RegNumber":" ","F_GSDH_Text":"88888888","F_ZLQ":"88","F_WB_GYSGLLX_Id":"5c1369eb71455b","F_WB_GYSGLLX":{"Id":"5c1369eb71455b","FNumber":"1002","MultiLanguageText":[{"PkId":"5c1369eb71455c","LocaleId":2052,"FDataValue":"非关联"}],"FDataValue":[{"Key":2052,"Value":"非关联"}]},"F_WB_YFKTJ":"原付款条件","F_WB_DLSTXRZ":"代理商体系认证","F_WB_DLSZSYXQ":"2019-04-27T00:00:00","F_WB_YCTXRZ":"原厂体系认证","F_WB_JGCTXRZ":"加工厂体系认证","F_WB_YCZSYXQ":"2019-04-21T00:00:00","F_WB_Date":"2019-04-28T00:00:00","SupplierBase":[{"Id":101885,"Country_Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","Country":{"Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","FNumber":"China","MultiLanguageText":[{"PkId":"ae6420c4-0e15-4303-aa64-8297b511860c","LocaleId":1033,"FDataValue":"China"},{"PkId":"1001","LocaleId":2052,"FDataValue":"中国"}],"FDataValue":[{"Key":1033,"Value":"China"},{"Key":2052,"Value":"中国"}]},"Provincial_Id":" ","Provincial":null,"Zip":" ","Language_Id":" ","Language":null,"Address":"北京市海淀区体院路甲2号14楼一层西190室","WebSite":" ","RegisterAddress":"注册地址","Trade_Id":" ","Trade":null,"CompanyNature_Id":" ","CompanyNature":null,"FoundDate":null,"LegalPerson":" ","RegisterFund":0,"RegisterCode":" ","TendPermit":" ","DeptId_Id":0,"DeptId":null,"FStaffId_Id":0,"FStaffId":null,"SupplierClassify_Id":"5bbaf52d30c87b","SupplierClassify":{"Id":"5bbaf52d30c87b","FNumber":"A","MultiLanguageText":[{"PkId":"5bbaf52d30c87c","LocaleId":2052,"FDataValue":"A"}],"FDataValue":[{"Key":2052,"Value":"A"}]},"CompanyClassify_Id":" ","CompanyClassify":null,"SupplierGrade_Id":"5bbaf85a30c883","SupplierGrade":{"Id":"5bbaf85a30c883","FNumber":"2","MultiLanguageText":[{"PkId":"5bbaf85a30c884","LocaleId":2052,"FDataValue":"合格"}],"FDataValue":[{"Key":2052,"Value":"合格"}]},"StartDate":"2018-12-17T00:00:00","EndDate":"2100-01-01T00:00:00","CompanyScale_Id":" ","CompanyScale":null,"SupplyClassify":"CG","SOCIALCRECODE":"统一社会信用代码"}],"SupplierBusiness":[{"Id":101885,"PurchaseGroupId_Id":0,"PurchaseGroupId":null,"MinPOValue":0,"NeedConfirm":false,"ParentSupplierId_Id":0,"ParentSupplierId":null,"BusinessStatus":"A","FreezeLimit":" ","SettleTypeId_Id":2,"SettleTypeId":{"Id":2,"msterID":0,"MultiLanguageText":[{"PkId":2,"LocaleId":2052,"Name":"现金支票"},{"PkId":17,"LocaleId":1033,"Name":"Cash Cheque"}],"Name":[{"Key":2052,"Value":"现金支票"},{"Key":1033,"Value":"Cash Cheque"}],"Number":"JSFS02_SYS","TYPE":"2"},"PriceListID_Id":0,"PriceListID":null,"WipStockPlaceId_Id":0,"WipStockPlaceId":null,"WipStockId_Id":0,"WipStockId":null,"DiscountListId_Id":0,"DiscountListId":null,"ProviderId_Id":164442,"ProviderId":{"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"},"FreezeOperator_Id":0,"FreezeOperator":null,"FreezeDate":null,"VmiStockId_Id":0,"VmiStockId":null,"VmiBusiness":false,"EnableSL":false,"DepositRatio":0}],"SupplierFinance":[{"Id":101885,"PayCurrencyId_Id":1,"PayCurrencyId":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"CustomerId_Id":0,"CustomerId":null,"PayAdvanceAmount":0,"TaxRegisterCode":"税务登记号","TendType_Id":" ","TendType":null,"FTaxType_Id":"9e855eb97bec43e7b50c3e0e0bf51210","FTaxType":{"Id":"9e855eb97bec43e7b50c3e0e0bf51210","FNumber":"SFL02_SYS","MultiLanguageText":[{"PkId":"6de0d14c-e106-4986-a732-5bbf127ec7dc","LocaleId":1033,"FDataValue":"General Taxpayer"},{"PkId":"7841e26e9e5a4fe6a83e7cdbe0c24175","LocaleId":2052,"FDataValue":"一般纳税人"},{"PkId":"2942b840514242b299738f8b9bbe2095","LocaleId":3076,"FDataValue":"一般納稅人"}],"FDataValue":[{"Key":1033,"Value":"General Taxpayer"},{"Key":2052,"Value":"一般纳税人"},{"Key":3076,"Value":"一般納稅人"}]},"Description":" ","PayCondition_Id":159799,"PayCondition":{"Id":159799,"MultiLanguageText":[{"PkId":100007,"LocaleId":2052,"Name":"票到90天付款"},{"PkId":100008,"LocaleId":1033,"Name":"Pay after 30 days"}],"Name":[{"Key":2052,"Value":"票到90天付款"},{"Key":1033,"Value":"Pay after 30 days"}],"Number":"003"},"InvoiceType":"1","SettleId_Id":164442,"SettleId":{"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"},"ChargeId_Id":164442,"ChargeId":{"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"},"TaxRateId_Id":264,"TaxRateId":{"Id":264,"msterID":264,"MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"16%增值税"}],"Name":[{"Key":2052,"Value":"16%增值税"}],"Number":"SL31_SYS","TaxRate":16}}],"SupplierBank":[{"Id":101399,"Country_Id":" ","Country":null,"BankCode":"0200296209200043000","BankHolder":"账户名称","IsDefault":false,"Description":" ","CurrencyId_Id":1,"CurrencyId":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"MultiLanguageText":[{"PkId":101397,"LocaleId":2052,"OpenBankName":"工商银行北京西四环支行"}],"OpenBankName":[{"Key":2052,"Value":"工商银行北京西四环支行"}],"BankTypeRec_Id":0,"BankTypeRec":null,"OpenAddressRec":"开户银行地址","CNAPS":" ","SwiftCode":" ","FTextBankDetail":" "}],"SupplierLocation":[{"Id":100215,"TransportDays":0,"PlanDeliveryPeriod":0,"Description":" ","Number":"BIZ20190412093226","Name":"地点名称","Address":"详细地址","IsUsed":true,"Tel":" ","Mobile":"88888888","Fax":" ","EMail":"51212@qq.com","IsDefaultSupply":false,"IsDefaultSettle":false,"IsDefaultPayee":false,"ContactId_Id":135670,"ContactId":{"Id":135670,"Contact":"张三","Tel":" ","Mobile":"88888888","Fax":" ","EMail":"51212@qq.com","CONTACTNUMBER":"CXR000094"},"NEWCONTACTID_Id":733333,"NEWCONTACTID":{"Id":733333,"msterID":733333,"MultiLanguageText":[{"PkId":100881,"LocaleId":2052,"Name":"张三"}],"Name":[{"Key":2052,"Value":"张三"}],"Number":"CXR000094","Sex_Id":" ","Sex":null,"Post":"测试","Tel":" ","Mobile":"88888888","Fax":" ","Email":"51212@qq.com","BizLocNumber":"BIZ20190412093226"}}],"SupplierContact":[{"Id":135670,"Contact":"张三","FPost":"测试","Tel":" ","Mobile":"88888888","Fax":" ","EMail":"51212@qq.com","FIMCode":" ","IsDefault":false,"Description":" ","CommonContactId_Id":733333,"CommonContactId":{"Id":733333,"msterID":733333,"MultiLanguageText":[{"PkId":100881,"LocaleId":2052,"Name":"张三"}],"Name":[{"Key":2052,"Value":"张三"}],"Number":"CXR000094","Sex_Id":" ","Sex":null,"Post":"测试","Tel":" ","Mobile":"88888888","Fax":" ","Email":"51212@qq.com","BizLocNumber":"BIZ20190412093226"},"ContactNumber":"CXR000094","ConForbidStatus":"A"}]}}
     */

    private ResultBeanX Result;

    public ResultBeanX getResult() {
        return Result;
    }

    public void setResult(ResultBeanX Result) {
        this.Result = Result;
    }

    public static class ResultBeanX {
        /**
         * ResponseStatus : null
         * Result : {"Id":164442,"msterID":164442,"DocumentStatus":"C","ForbidStatus":"A","MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司","Description":" ","ShortName":" "}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006","Description":[{"Key":2052,"Value":" "}],"CreateOrgId_Id":1,"CreateOrgId":{"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"},"UseOrgId_Id":1,"UseOrgId":{"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"},"CreatorId_Id":110362,"CreatorId":{"Id":110362,"Name":"admin","UserAccount":"admin"},"ModifierId_Id":671703,"ModifierId":{"Id":671703,"Name":"张敏","UserAccount":"张敏"},"CreateDate":"2018-12-17T17:01:14.183","FModifyDate":"2019-04-12T09:35:08.773","ShortName":[{"Key":2052,"Value":" "}],"ForbiderId_Id":0,"ForbiderId":null,"ForbidDate":null,"FAuditDate":"2019-04-12T09:35:10.093","AuditorId_Id":671703,"AuditorId":{"Id":671703,"Name":"张敏","UserAccount":"张敏"},"FGroup_Id":110373,"FGroup":{"Id":110373,"Number":"11","MultiLanguageText":[{"PkId":100003,"LocaleId":2052,"Name":"北京"}],"Name":[{"Key":2052,"Value":"北京"}]},"CorrespondOrgId_Id":0,"CorrespondOrgId":null,"SYNCGYOWNERSTATUS":false,"RegNumber":" ","F_GSDH_Text":"88888888","F_ZLQ":"88","F_WB_GYSGLLX_Id":"5c1369eb71455b","F_WB_GYSGLLX":{"Id":"5c1369eb71455b","FNumber":"1002","MultiLanguageText":[{"PkId":"5c1369eb71455c","LocaleId":2052,"FDataValue":"非关联"}],"FDataValue":[{"Key":2052,"Value":"非关联"}]},"F_WB_YFKTJ":"原付款条件","F_WB_DLSTXRZ":"代理商体系认证","F_WB_DLSZSYXQ":"2019-04-27T00:00:00","F_WB_YCTXRZ":"原厂体系认证","F_WB_JGCTXRZ":"加工厂体系认证","F_WB_YCZSYXQ":"2019-04-21T00:00:00","F_WB_Date":"2019-04-28T00:00:00","SupplierBase":[{"Id":101885,"Country_Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","Country":{"Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","FNumber":"China","MultiLanguageText":[{"PkId":"ae6420c4-0e15-4303-aa64-8297b511860c","LocaleId":1033,"FDataValue":"China"},{"PkId":"1001","LocaleId":2052,"FDataValue":"中国"}],"FDataValue":[{"Key":1033,"Value":"China"},{"Key":2052,"Value":"中国"}]},"Provincial_Id":" ","Provincial":null,"Zip":" ","Language_Id":" ","Language":null,"Address":"北京市海淀区体院路甲2号14楼一层西190室","WebSite":" ","RegisterAddress":"注册地址","Trade_Id":" ","Trade":null,"CompanyNature_Id":" ","CompanyNature":null,"FoundDate":null,"LegalPerson":" ","RegisterFund":0,"RegisterCode":" ","TendPermit":" ","DeptId_Id":0,"DeptId":null,"FStaffId_Id":0,"FStaffId":null,"SupplierClassify_Id":"5bbaf52d30c87b","SupplierClassify":{"Id":"5bbaf52d30c87b","FNumber":"A","MultiLanguageText":[{"PkId":"5bbaf52d30c87c","LocaleId":2052,"FDataValue":"A"}],"FDataValue":[{"Key":2052,"Value":"A"}]},"CompanyClassify_Id":" ","CompanyClassify":null,"SupplierGrade_Id":"5bbaf85a30c883","SupplierGrade":{"Id":"5bbaf85a30c883","FNumber":"2","MultiLanguageText":[{"PkId":"5bbaf85a30c884","LocaleId":2052,"FDataValue":"合格"}],"FDataValue":[{"Key":2052,"Value":"合格"}]},"StartDate":"2018-12-17T00:00:00","EndDate":"2100-01-01T00:00:00","CompanyScale_Id":" ","CompanyScale":null,"SupplyClassify":"CG","SOCIALCRECODE":"统一社会信用代码"}],"SupplierBusiness":[{"Id":101885,"PurchaseGroupId_Id":0,"PurchaseGroupId":null,"MinPOValue":0,"NeedConfirm":false,"ParentSupplierId_Id":0,"ParentSupplierId":null,"BusinessStatus":"A","FreezeLimit":" ","SettleTypeId_Id":2,"SettleTypeId":{"Id":2,"msterID":0,"MultiLanguageText":[{"PkId":2,"LocaleId":2052,"Name":"现金支票"},{"PkId":17,"LocaleId":1033,"Name":"Cash Cheque"}],"Name":[{"Key":2052,"Value":"现金支票"},{"Key":1033,"Value":"Cash Cheque"}],"Number":"JSFS02_SYS","TYPE":"2"},"PriceListID_Id":0,"PriceListID":null,"WipStockPlaceId_Id":0,"WipStockPlaceId":null,"WipStockId_Id":0,"WipStockId":null,"DiscountListId_Id":0,"DiscountListId":null,"ProviderId_Id":164442,"ProviderId":{"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"},"FreezeOperator_Id":0,"FreezeOperator":null,"FreezeDate":null,"VmiStockId_Id":0,"VmiStockId":null,"VmiBusiness":false,"EnableSL":false,"DepositRatio":0}],"SupplierFinance":[{"Id":101885,"PayCurrencyId_Id":1,"PayCurrencyId":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"CustomerId_Id":0,"CustomerId":null,"PayAdvanceAmount":0,"TaxRegisterCode":"税务登记号","TendType_Id":" ","TendType":null,"FTaxType_Id":"9e855eb97bec43e7b50c3e0e0bf51210","FTaxType":{"Id":"9e855eb97bec43e7b50c3e0e0bf51210","FNumber":"SFL02_SYS","MultiLanguageText":[{"PkId":"6de0d14c-e106-4986-a732-5bbf127ec7dc","LocaleId":1033,"FDataValue":"General Taxpayer"},{"PkId":"7841e26e9e5a4fe6a83e7cdbe0c24175","LocaleId":2052,"FDataValue":"一般纳税人"},{"PkId":"2942b840514242b299738f8b9bbe2095","LocaleId":3076,"FDataValue":"一般納稅人"}],"FDataValue":[{"Key":1033,"Value":"General Taxpayer"},{"Key":2052,"Value":"一般纳税人"},{"Key":3076,"Value":"一般納稅人"}]},"Description":" ","PayCondition_Id":159799,"PayCondition":{"Id":159799,"MultiLanguageText":[{"PkId":100007,"LocaleId":2052,"Name":"票到90天付款"},{"PkId":100008,"LocaleId":1033,"Name":"Pay after 30 days"}],"Name":[{"Key":2052,"Value":"票到90天付款"},{"Key":1033,"Value":"Pay after 30 days"}],"Number":"003"},"InvoiceType":"1","SettleId_Id":164442,"SettleId":{"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"},"ChargeId_Id":164442,"ChargeId":{"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"},"TaxRateId_Id":264,"TaxRateId":{"Id":264,"msterID":264,"MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"16%增值税"}],"Name":[{"Key":2052,"Value":"16%增值税"}],"Number":"SL31_SYS","TaxRate":16}}],"SupplierBank":[{"Id":101399,"Country_Id":" ","Country":null,"BankCode":"0200296209200043000","BankHolder":"账户名称","IsDefault":false,"Description":" ","CurrencyId_Id":1,"CurrencyId":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"MultiLanguageText":[{"PkId":101397,"LocaleId":2052,"OpenBankName":"工商银行北京西四环支行"}],"OpenBankName":[{"Key":2052,"Value":"工商银行北京西四环支行"}],"BankTypeRec_Id":0,"BankTypeRec":null,"OpenAddressRec":"开户银行地址","CNAPS":" ","SwiftCode":" ","FTextBankDetail":" "}],"SupplierLocation":[{"Id":100215,"TransportDays":0,"PlanDeliveryPeriod":0,"Description":" ","Number":"BIZ20190412093226","Name":"地点名称","Address":"详细地址","IsUsed":true,"Tel":" ","Mobile":"88888888","Fax":" ","EMail":"51212@qq.com","IsDefaultSupply":false,"IsDefaultSettle":false,"IsDefaultPayee":false,"ContactId_Id":135670,"ContactId":{"Id":135670,"Contact":"张三","Tel":" ","Mobile":"88888888","Fax":" ","EMail":"51212@qq.com","CONTACTNUMBER":"CXR000094"},"NEWCONTACTID_Id":733333,"NEWCONTACTID":{"Id":733333,"msterID":733333,"MultiLanguageText":[{"PkId":100881,"LocaleId":2052,"Name":"张三"}],"Name":[{"Key":2052,"Value":"张三"}],"Number":"CXR000094","Sex_Id":" ","Sex":null,"Post":"测试","Tel":" ","Mobile":"88888888","Fax":" ","Email":"51212@qq.com","BizLocNumber":"BIZ20190412093226"}}],"SupplierContact":[{"Id":135670,"Contact":"张三","FPost":"测试","Tel":" ","Mobile":"88888888","Fax":" ","EMail":"51212@qq.com","FIMCode":" ","IsDefault":false,"Description":" ","CommonContactId_Id":733333,"CommonContactId":{"Id":733333,"msterID":733333,"MultiLanguageText":[{"PkId":100881,"LocaleId":2052,"Name":"张三"}],"Name":[{"Key":2052,"Value":"张三"}],"Number":"CXR000094","Sex_Id":" ","Sex":null,"Post":"测试","Tel":" ","Mobile":"88888888","Fax":" ","Email":"51212@qq.com","BizLocNumber":"BIZ20190412093226"},"ContactNumber":"CXR000094","ConForbidStatus":"A"}]}
         */

        private Object ResponseStatus;
        private ResultBean Result;

        public Object getResponseStatus() {
            return ResponseStatus;
        }

        public void setResponseStatus(Object ResponseStatus) {
            this.ResponseStatus = ResponseStatus;
        }

        public ResultBean getResult() {
            return Result;
        }

        public void setResult(ResultBean Result) {
            this.Result = Result;
        }

        public static class ResultBean {
            /**
             * Id : 164442
             * msterID : 164442
             * DocumentStatus : C
             * ForbidStatus : A
             * MultiLanguageText : [{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司","Description":" ","ShortName":" "}]
             * Name : [{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}]
             * Number : 11000006
             * Description : [{"Key":2052,"Value":" "}]
             * CreateOrgId_Id : 1
             * CreateOrgId : {"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"}
             * UseOrgId_Id : 1
             * UseOrgId : {"Id":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}],"Name":[{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}],"Number":"999"}
             * CreatorId_Id : 110362
             * CreatorId : {"Id":110362,"Name":"admin","UserAccount":"admin"}
             * ModifierId_Id : 671703
             * ModifierId : {"Id":671703,"Name":"张敏","UserAccount":"张敏"}
             * CreateDate : 2018-12-17T17:01:14.183
             * FModifyDate : 2019-04-12T09:35:08.773
             * ShortName : [{"Key":2052,"Value":" "}]
             * ForbiderId_Id : 0
             * ForbiderId : null
             * ForbidDate : null
             * FAuditDate : 2019-04-12T09:35:10.093
             * AuditorId_Id : 671703
             * AuditorId : {"Id":671703,"Name":"张敏","UserAccount":"张敏"}
             * FGroup_Id : 110373
             * FGroup : {"Id":110373,"Number":"11","MultiLanguageText":[{"PkId":100003,"LocaleId":2052,"Name":"北京"}],"Name":[{"Key":2052,"Value":"北京"}]}
             * CorrespondOrgId_Id : 0
             * CorrespondOrgId : null
             * SYNCGYOWNERSTATUS : false
             * RegNumber :
             * F_GSDH_Text : 88888888
             * F_ZLQ : 88
             * F_WB_GYSGLLX_Id : 5c1369eb71455b
             * F_WB_GYSGLLX : {"Id":"5c1369eb71455b","FNumber":"1002","MultiLanguageText":[{"PkId":"5c1369eb71455c","LocaleId":2052,"FDataValue":"非关联"}],"FDataValue":[{"Key":2052,"Value":"非关联"}]}
             * F_WB_YFKTJ : 原付款条件
             * F_WB_DLSTXRZ : 代理商体系认证
             * F_WB_DLSZSYXQ : 2019-04-27T00:00:00
             * F_WB_YCTXRZ : 原厂体系认证
             * F_WB_JGCTXRZ : 加工厂体系认证
             * F_WB_YCZSYXQ : 2019-04-21T00:00:00
             * F_WB_Date : 2019-04-28T00:00:00
             * SupplierBase : [{"Id":101885,"Country_Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","Country":{"Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","FNumber":"China","MultiLanguageText":[{"PkId":"ae6420c4-0e15-4303-aa64-8297b511860c","LocaleId":1033,"FDataValue":"China"},{"PkId":"1001","LocaleId":2052,"FDataValue":"中国"}],"FDataValue":[{"Key":1033,"Value":"China"},{"Key":2052,"Value":"中国"}]},"Provincial_Id":" ","Provincial":null,"Zip":" ","Language_Id":" ","Language":null,"Address":"北京市海淀区体院路甲2号14楼一层西190室","WebSite":" ","RegisterAddress":"注册地址","Trade_Id":" ","Trade":null,"CompanyNature_Id":" ","CompanyNature":null,"FoundDate":null,"LegalPerson":" ","RegisterFund":0,"RegisterCode":" ","TendPermit":" ","DeptId_Id":0,"DeptId":null,"FStaffId_Id":0,"FStaffId":null,"SupplierClassify_Id":"5bbaf52d30c87b","SupplierClassify":{"Id":"5bbaf52d30c87b","FNumber":"A","MultiLanguageText":[{"PkId":"5bbaf52d30c87c","LocaleId":2052,"FDataValue":"A"}],"FDataValue":[{"Key":2052,"Value":"A"}]},"CompanyClassify_Id":" ","CompanyClassify":null,"SupplierGrade_Id":"5bbaf85a30c883","SupplierGrade":{"Id":"5bbaf85a30c883","FNumber":"2","MultiLanguageText":[{"PkId":"5bbaf85a30c884","LocaleId":2052,"FDataValue":"合格"}],"FDataValue":[{"Key":2052,"Value":"合格"}]},"StartDate":"2018-12-17T00:00:00","EndDate":"2100-01-01T00:00:00","CompanyScale_Id":" ","CompanyScale":null,"SupplyClassify":"CG","SOCIALCRECODE":"统一社会信用代码"}]
             * SupplierBusiness : [{"Id":101885,"PurchaseGroupId_Id":0,"PurchaseGroupId":null,"MinPOValue":0,"NeedConfirm":false,"ParentSupplierId_Id":0,"ParentSupplierId":null,"BusinessStatus":"A","FreezeLimit":" ","SettleTypeId_Id":2,"SettleTypeId":{"Id":2,"msterID":0,"MultiLanguageText":[{"PkId":2,"LocaleId":2052,"Name":"现金支票"},{"PkId":17,"LocaleId":1033,"Name":"Cash Cheque"}],"Name":[{"Key":2052,"Value":"现金支票"},{"Key":1033,"Value":"Cash Cheque"}],"Number":"JSFS02_SYS","TYPE":"2"},"PriceListID_Id":0,"PriceListID":null,"WipStockPlaceId_Id":0,"WipStockPlaceId":null,"WipStockId_Id":0,"WipStockId":null,"DiscountListId_Id":0,"DiscountListId":null,"ProviderId_Id":164442,"ProviderId":{"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"},"FreezeOperator_Id":0,"FreezeOperator":null,"FreezeDate":null,"VmiStockId_Id":0,"VmiStockId":null,"VmiBusiness":false,"EnableSL":false,"DepositRatio":0}]
             * SupplierFinance : [{"Id":101885,"PayCurrencyId_Id":1,"PayCurrencyId":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"CustomerId_Id":0,"CustomerId":null,"PayAdvanceAmount":0,"TaxRegisterCode":"税务登记号","TendType_Id":" ","TendType":null,"FTaxType_Id":"9e855eb97bec43e7b50c3e0e0bf51210","FTaxType":{"Id":"9e855eb97bec43e7b50c3e0e0bf51210","FNumber":"SFL02_SYS","MultiLanguageText":[{"PkId":"6de0d14c-e106-4986-a732-5bbf127ec7dc","LocaleId":1033,"FDataValue":"General Taxpayer"},{"PkId":"7841e26e9e5a4fe6a83e7cdbe0c24175","LocaleId":2052,"FDataValue":"一般纳税人"},{"PkId":"2942b840514242b299738f8b9bbe2095","LocaleId":3076,"FDataValue":"一般納稅人"}],"FDataValue":[{"Key":1033,"Value":"General Taxpayer"},{"Key":2052,"Value":"一般纳税人"},{"Key":3076,"Value":"一般納稅人"}]},"Description":" ","PayCondition_Id":159799,"PayCondition":{"Id":159799,"MultiLanguageText":[{"PkId":100007,"LocaleId":2052,"Name":"票到90天付款"},{"PkId":100008,"LocaleId":1033,"Name":"Pay after 30 days"}],"Name":[{"Key":2052,"Value":"票到90天付款"},{"Key":1033,"Value":"Pay after 30 days"}],"Number":"003"},"InvoiceType":"1","SettleId_Id":164442,"SettleId":{"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"},"ChargeId_Id":164442,"ChargeId":{"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"},"TaxRateId_Id":264,"TaxRateId":{"Id":264,"msterID":264,"MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"16%增值税"}],"Name":[{"Key":2052,"Value":"16%增值税"}],"Number":"SL31_SYS","TaxRate":16}}]
             * SupplierBank : [{"Id":101399,"Country_Id":" ","Country":null,"BankCode":"0200296209200043000","BankHolder":"账户名称","IsDefault":false,"Description":" ","CurrencyId_Id":1,"CurrencyId":{"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"},"MultiLanguageText":[{"PkId":101397,"LocaleId":2052,"OpenBankName":"工商银行北京西四环支行"}],"OpenBankName":[{"Key":2052,"Value":"工商银行北京西四环支行"}],"BankTypeRec_Id":0,"BankTypeRec":null,"OpenAddressRec":"开户银行地址","CNAPS":" ","SwiftCode":" ","FTextBankDetail":" "}]
             * SupplierLocation : [{"Id":100215,"TransportDays":0,"PlanDeliveryPeriod":0,"Description":" ","Number":"BIZ20190412093226","Name":"地点名称","Address":"详细地址","IsUsed":true,"Tel":" ","Mobile":"88888888","Fax":" ","EMail":"51212@qq.com","IsDefaultSupply":false,"IsDefaultSettle":false,"IsDefaultPayee":false,"ContactId_Id":135670,"ContactId":{"Id":135670,"Contact":"张三","Tel":" ","Mobile":"88888888","Fax":" ","EMail":"51212@qq.com","CONTACTNUMBER":"CXR000094"},"NEWCONTACTID_Id":733333,"NEWCONTACTID":{"Id":733333,"msterID":733333,"MultiLanguageText":[{"PkId":100881,"LocaleId":2052,"Name":"张三"}],"Name":[{"Key":2052,"Value":"张三"}],"Number":"CXR000094","Sex_Id":" ","Sex":null,"Post":"测试","Tel":" ","Mobile":"88888888","Fax":" ","Email":"51212@qq.com","BizLocNumber":"BIZ20190412093226"}}]
             * SupplierContact : [{"Id":135670,"Contact":"张三","FPost":"测试","Tel":" ","Mobile":"88888888","Fax":" ","EMail":"51212@qq.com","FIMCode":" ","IsDefault":false,"Description":" ","CommonContactId_Id":733333,"CommonContactId":{"Id":733333,"msterID":733333,"MultiLanguageText":[{"PkId":100881,"LocaleId":2052,"Name":"张三"}],"Name":[{"Key":2052,"Value":"张三"}],"Number":"CXR000094","Sex_Id":" ","Sex":null,"Post":"测试","Tel":" ","Mobile":"88888888","Fax":" ","Email":"51212@qq.com","BizLocNumber":"BIZ20190412093226"},"ContactNumber":"CXR000094","ConForbidStatus":"A"}]
             */

            private int Id;
            private int msterID;
            private String DocumentStatus;
            private String ForbidStatus;
            private String Number;
            private int CreateOrgId_Id;
            private CreateOrgIdBean CreateOrgId;
            private int UseOrgId_Id;
            private UseOrgIdBean UseOrgId;
            private int CreatorId_Id;
            private CreatorIdBean CreatorId;
            private int ModifierId_Id;
            private ModifierIdBean ModifierId;
            private String CreateDate;
            private String FModifyDate;
            private int ForbiderId_Id;
            private Object ForbiderId;
            private Object ForbidDate;
            private String FAuditDate;
            private int AuditorId_Id;
            private AuditorIdBean AuditorId;
            private int FGroup_Id;
            private FGroupBean FGroup;
            private int CorrespondOrgId_Id;
            private Object CorrespondOrgId;
            private boolean SYNCGYOWNERSTATUS;
            private String RegNumber;
            private String F_GSDH_Text;
            private String F_ZLQ;
            private String F_WB_GYSGLLX_Id;
            private FWBGYSGLLXBean F_WB_GYSGLLX;
            private String F_WB_YFKTJ;
            private String F_WB_DLSTXRZ;
            private String F_WB_DLSZSYXQ;
            private String F_WB_YCTXRZ;
            private String F_WB_JGCTXRZ;
            private String F_WB_YCZSYXQ;
            private String F_WB_Date;
            private List<MultiLanguageTextBeanXXXX> MultiLanguageText;
            private List<NameBeanXXX> Name;
            private List<DescriptionBean> Description;
            private List<ShortNameBean> ShortName;
            private List<SupplierBaseBean> SupplierBase;
            private List<SupplierBusinessBean> SupplierBusiness;
            private List<SupplierFinanceBean> SupplierFinance;
            private List<SupplierBankBean> SupplierBank;
            private List<SupplierLocationBean> SupplierLocation;
            private List<SupplierContactBean> SupplierContact;

            public int getId() {
                return Id;
            }

            public void setId(int Id) {
                this.Id = Id;
            }

            public int getMsterID() {
                return msterID;
            }

            public void setMsterID(int msterID) {
                this.msterID = msterID;
            }

            public String getDocumentStatus() {
                return DocumentStatus;
            }

            public void setDocumentStatus(String DocumentStatus) {
                this.DocumentStatus = DocumentStatus;
            }

            public String getForbidStatus() {
                return ForbidStatus;
            }

            public void setForbidStatus(String ForbidStatus) {
                this.ForbidStatus = ForbidStatus;
            }

            public String getNumber() {
                return Number;
            }

            public void setNumber(String Number) {
                this.Number = Number;
            }

            public int getCreateOrgId_Id() {
                return CreateOrgId_Id;
            }

            public void setCreateOrgId_Id(int CreateOrgId_Id) {
                this.CreateOrgId_Id = CreateOrgId_Id;
            }

            public CreateOrgIdBean getCreateOrgId() {
                return CreateOrgId;
            }

            public void setCreateOrgId(CreateOrgIdBean CreateOrgId) {
                this.CreateOrgId = CreateOrgId;
            }

            public int getUseOrgId_Id() {
                return UseOrgId_Id;
            }

            public void setUseOrgId_Id(int UseOrgId_Id) {
                this.UseOrgId_Id = UseOrgId_Id;
            }

            public UseOrgIdBean getUseOrgId() {
                return UseOrgId;
            }

            public void setUseOrgId(UseOrgIdBean UseOrgId) {
                this.UseOrgId = UseOrgId;
            }

            public int getCreatorId_Id() {
                return CreatorId_Id;
            }

            public void setCreatorId_Id(int CreatorId_Id) {
                this.CreatorId_Id = CreatorId_Id;
            }

            public CreatorIdBean getCreatorId() {
                return CreatorId;
            }

            public void setCreatorId(CreatorIdBean CreatorId) {
                this.CreatorId = CreatorId;
            }

            public int getModifierId_Id() {
                return ModifierId_Id;
            }

            public void setModifierId_Id(int ModifierId_Id) {
                this.ModifierId_Id = ModifierId_Id;
            }

            public ModifierIdBean getModifierId() {
                return ModifierId;
            }

            public void setModifierId(ModifierIdBean ModifierId) {
                this.ModifierId = ModifierId;
            }

            public String getCreateDate() {
                return CreateDate;
            }

            public void setCreateDate(String CreateDate) {
                this.CreateDate = CreateDate;
            }

            public String getFModifyDate() {
                return FModifyDate;
            }

            public void setFModifyDate(String FModifyDate) {
                this.FModifyDate = FModifyDate;
            }

            public int getForbiderId_Id() {
                return ForbiderId_Id;
            }

            public void setForbiderId_Id(int ForbiderId_Id) {
                this.ForbiderId_Id = ForbiderId_Id;
            }

            public Object getForbiderId() {
                return ForbiderId;
            }

            public void setForbiderId(Object ForbiderId) {
                this.ForbiderId = ForbiderId;
            }

            public Object getForbidDate() {
                return ForbidDate;
            }

            public void setForbidDate(Object ForbidDate) {
                this.ForbidDate = ForbidDate;
            }

            public String getFAuditDate() {
                return FAuditDate;
            }

            public void setFAuditDate(String FAuditDate) {
                this.FAuditDate = FAuditDate;
            }

            public int getAuditorId_Id() {
                return AuditorId_Id;
            }

            public void setAuditorId_Id(int AuditorId_Id) {
                this.AuditorId_Id = AuditorId_Id;
            }

            public AuditorIdBean getAuditorId() {
                return AuditorId;
            }

            public void setAuditorId(AuditorIdBean AuditorId) {
                this.AuditorId = AuditorId;
            }

            public int getFGroup_Id() {
                return FGroup_Id;
            }

            public void setFGroup_Id(int FGroup_Id) {
                this.FGroup_Id = FGroup_Id;
            }

            public FGroupBean getFGroup() {
                return FGroup;
            }

            public void setFGroup(FGroupBean FGroup) {
                this.FGroup = FGroup;
            }

            public int getCorrespondOrgId_Id() {
                return CorrespondOrgId_Id;
            }

            public void setCorrespondOrgId_Id(int CorrespondOrgId_Id) {
                this.CorrespondOrgId_Id = CorrespondOrgId_Id;
            }

            public Object getCorrespondOrgId() {
                return CorrespondOrgId;
            }

            public void setCorrespondOrgId(Object CorrespondOrgId) {
                this.CorrespondOrgId = CorrespondOrgId;
            }

            public boolean isSYNCGYOWNERSTATUS() {
                return SYNCGYOWNERSTATUS;
            }

            public void setSYNCGYOWNERSTATUS(boolean SYNCGYOWNERSTATUS) {
                this.SYNCGYOWNERSTATUS = SYNCGYOWNERSTATUS;
            }

            public String getRegNumber() {
                return RegNumber;
            }

            public void setRegNumber(String RegNumber) {
                this.RegNumber = RegNumber;
            }

            public String getF_GSDH_Text() {
                return F_GSDH_Text;
            }

            public void setF_GSDH_Text(String F_GSDH_Text) {
                this.F_GSDH_Text = F_GSDH_Text;
            }

            public String getF_ZLQ() {
                return F_ZLQ;
            }

            public void setF_ZLQ(String F_ZLQ) {
                this.F_ZLQ = F_ZLQ;
            }

            public String getF_WB_GYSGLLX_Id() {
                return F_WB_GYSGLLX_Id;
            }

            public void setF_WB_GYSGLLX_Id(String F_WB_GYSGLLX_Id) {
                this.F_WB_GYSGLLX_Id = F_WB_GYSGLLX_Id;
            }

            public FWBGYSGLLXBean getF_WB_GYSGLLX() {
                return F_WB_GYSGLLX;
            }

            public void setF_WB_GYSGLLX(FWBGYSGLLXBean F_WB_GYSGLLX) {
                this.F_WB_GYSGLLX = F_WB_GYSGLLX;
            }

            public String getF_WB_YFKTJ() {
                return F_WB_YFKTJ;
            }

            public void setF_WB_YFKTJ(String F_WB_YFKTJ) {
                this.F_WB_YFKTJ = F_WB_YFKTJ;
            }

            public String getF_WB_DLSTXRZ() {
                return F_WB_DLSTXRZ;
            }

            public void setF_WB_DLSTXRZ(String F_WB_DLSTXRZ) {
                this.F_WB_DLSTXRZ = F_WB_DLSTXRZ;
            }

            public String getF_WB_DLSZSYXQ() {
                return F_WB_DLSZSYXQ;
            }

            public void setF_WB_DLSZSYXQ(String F_WB_DLSZSYXQ) {
                this.F_WB_DLSZSYXQ = F_WB_DLSZSYXQ;
            }

            public String getF_WB_YCTXRZ() {
                return F_WB_YCTXRZ;
            }

            public void setF_WB_YCTXRZ(String F_WB_YCTXRZ) {
                this.F_WB_YCTXRZ = F_WB_YCTXRZ;
            }

            public String getF_WB_JGCTXRZ() {
                return F_WB_JGCTXRZ;
            }

            public void setF_WB_JGCTXRZ(String F_WB_JGCTXRZ) {
                this.F_WB_JGCTXRZ = F_WB_JGCTXRZ;
            }

            public String getF_WB_YCZSYXQ() {
                return F_WB_YCZSYXQ;
            }

            public void setF_WB_YCZSYXQ(String F_WB_YCZSYXQ) {
                this.F_WB_YCZSYXQ = F_WB_YCZSYXQ;
            }

            public String getF_WB_Date() {
                return F_WB_Date;
            }

            public void setF_WB_Date(String F_WB_Date) {
                this.F_WB_Date = F_WB_Date;
            }

            public List<MultiLanguageTextBeanXXXX> getMultiLanguageText() {
                return MultiLanguageText;
            }

            public void setMultiLanguageText(List<MultiLanguageTextBeanXXXX> MultiLanguageText) {
                this.MultiLanguageText = MultiLanguageText;
            }

            public List<NameBeanXXX> getName() {
                return Name;
            }

            public void setName(List<NameBeanXXX> Name) {
                this.Name = Name;
            }

            public List<DescriptionBean> getDescription() {
                return Description;
            }

            public void setDescription(List<DescriptionBean> Description) {
                this.Description = Description;
            }

            public List<ShortNameBean> getShortName() {
                return ShortName;
            }

            public void setShortName(List<ShortNameBean> ShortName) {
                this.ShortName = ShortName;
            }

            public List<SupplierBaseBean> getSupplierBase() {
                return SupplierBase;
            }

            public void setSupplierBase(List<SupplierBaseBean> SupplierBase) {
                this.SupplierBase = SupplierBase;
            }

            public List<SupplierBusinessBean> getSupplierBusiness() {
                return SupplierBusiness;
            }

            public void setSupplierBusiness(List<SupplierBusinessBean> SupplierBusiness) {
                this.SupplierBusiness = SupplierBusiness;
            }

            public List<SupplierFinanceBean> getSupplierFinance() {
                return SupplierFinance;
            }

            public void setSupplierFinance(List<SupplierFinanceBean> SupplierFinance) {
                this.SupplierFinance = SupplierFinance;
            }

            public List<SupplierBankBean> getSupplierBank() {
                return SupplierBank;
            }

            public void setSupplierBank(List<SupplierBankBean> SupplierBank) {
                this.SupplierBank = SupplierBank;
            }

            public List<SupplierLocationBean> getSupplierLocation() {
                return SupplierLocation;
            }

            public void setSupplierLocation(List<SupplierLocationBean> SupplierLocation) {
                this.SupplierLocation = SupplierLocation;
            }

            public List<SupplierContactBean> getSupplierContact() {
                return SupplierContact;
            }

            public void setSupplierContact(List<SupplierContactBean> SupplierContact) {
                this.SupplierContact = SupplierContact;
            }

            public static class CreateOrgIdBean {
                /**
                 * Id : 1
                 * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}]
                 * Name : [{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}]
                 * Number : 999
                 */

                private int Id;
                private String Number;
                private List<MultiLanguageTextBean> MultiLanguageText;
                private List<NameBean> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBean> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBean> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBean> getName() {
                    return Name;
                }

                public void setName(List<NameBean> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBean {
                    /**
                     * PkId : 1
                     * LocaleId : 2052
                     * Name : 万帮集团
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBean {
                    /**
                     * Key : 2052
                     * Value : 万帮集团
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class UseOrgIdBean {
                /**
                 * Id : 1
                 * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"万帮集团"},{"PkId":2,"LocaleId":1033,"Name":"万帮集团"},{"PkId":100002,"LocaleId":3076,"Name":"万帮集团"}]
                 * Name : [{"Key":2052,"Value":"万帮集团"},{"Key":1033,"Value":"万帮集团"},{"Key":3076,"Value":"万帮集团"}]
                 * Number : 999
                 */

                private int Id;
                private String Number;
                private List<MultiLanguageTextBeanX> MultiLanguageText;
                private List<NameBeanX> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBeanX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBeanX> getName() {
                    return Name;
                }

                public void setName(List<NameBeanX> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBeanX {
                    /**
                     * PkId : 1
                     * LocaleId : 2052
                     * Name : 万帮集团
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBeanX {
                    /**
                     * Key : 2052
                     * Value : 万帮集团
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class CreatorIdBean {
                /**
                 * Id : 110362
                 * Name : admin
                 * UserAccount : admin
                 */

                private int Id;
                private String Name;
                private String UserAccount;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getUserAccount() {
                    return UserAccount;
                }

                public void setUserAccount(String UserAccount) {
                    this.UserAccount = UserAccount;
                }
            }

            public static class ModifierIdBean {
                /**
                 * Id : 671703
                 * Name : 张敏
                 * UserAccount : 张敏
                 */

                private int Id;
                private String Name;
                private String UserAccount;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getUserAccount() {
                    return UserAccount;
                }

                public void setUserAccount(String UserAccount) {
                    this.UserAccount = UserAccount;
                }
            }

            public static class AuditorIdBean {
                /**
                 * Id : 671703
                 * Name : 张敏
                 * UserAccount : 张敏
                 */

                private int Id;
                private String Name;
                private String UserAccount;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getUserAccount() {
                    return UserAccount;
                }

                public void setUserAccount(String UserAccount) {
                    this.UserAccount = UserAccount;
                }
            }

            public static class FGroupBean {
                /**
                 * Id : 110373
                 * Number : 11
                 * MultiLanguageText : [{"PkId":100003,"LocaleId":2052,"Name":"北京"}]
                 * Name : [{"Key":2052,"Value":"北京"}]
                 */

                private int Id;
                private String Number;
                private List<MultiLanguageTextBeanXX> MultiLanguageText;
                private List<NameBeanXX> Name;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public List<MultiLanguageTextBeanXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<NameBeanXX> getName() {
                    return Name;
                }

                public void setName(List<NameBeanXX> Name) {
                    this.Name = Name;
                }

                public static class MultiLanguageTextBeanXX {
                    /**
                     * PkId : 100003
                     * LocaleId : 2052
                     * Name : 北京
                     */

                    private int PkId;
                    private int LocaleId;
                    private String Name;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }
                }

                public static class NameBeanXX {
                    /**
                     * Key : 2052
                     * Value : 北京
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class FWBGYSGLLXBean {
                /**
                 * Id : 5c1369eb71455b
                 * FNumber : 1002
                 * MultiLanguageText : [{"PkId":"5c1369eb71455c","LocaleId":2052,"FDataValue":"非关联"}]
                 * FDataValue : [{"Key":2052,"Value":"非关联"}]
                 */

                private String Id;
                private String FNumber;
                private List<MultiLanguageTextBeanXXX> MultiLanguageText;
                private List<FDataValueBean> FDataValue;

                public String getId() {
                    return Id;
                }

                public void setId(String Id) {
                    this.Id = Id;
                }

                public String getFNumber() {
                    return FNumber;
                }

                public void setFNumber(String FNumber) {
                    this.FNumber = FNumber;
                }

                public List<MultiLanguageTextBeanXXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<FDataValueBean> getFDataValue() {
                    return FDataValue;
                }

                public void setFDataValue(List<FDataValueBean> FDataValue) {
                    this.FDataValue = FDataValue;
                }

                public static class MultiLanguageTextBeanXXX {
                    /**
                     * PkId : 5c1369eb71455c
                     * LocaleId : 2052
                     * FDataValue : 非关联
                     */

                    private String PkId;
                    private int LocaleId;
                    private String FDataValue;

                    public String getPkId() {
                        return PkId;
                    }

                    public void setPkId(String PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getFDataValue() {
                        return FDataValue;
                    }

                    public void setFDataValue(String FDataValue) {
                        this.FDataValue = FDataValue;
                    }
                }

                public static class FDataValueBean {
                    /**
                     * Key : 2052
                     * Value : 非关联
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class MultiLanguageTextBeanXXXX {
                /**
                 * PkId : 101885
                 * LocaleId : 2052
                 * Name : 北京正丰达旺商贸有限公司
                 * Description :
                 * ShortName :
                 */

                private int PkId;
                private int LocaleId;
                private String Name;
                private String Description;
                private String ShortName;

                public int getPkId() {
                    return PkId;
                }

                public void setPkId(int PkId) {
                    this.PkId = PkId;
                }

                public int getLocaleId() {
                    return LocaleId;
                }

                public void setLocaleId(int LocaleId) {
                    this.LocaleId = LocaleId;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getDescription() {
                    return Description;
                }

                public void setDescription(String Description) {
                    this.Description = Description;
                }

                public String getShortName() {
                    return ShortName;
                }

                public void setShortName(String ShortName) {
                    this.ShortName = ShortName;
                }
            }

            public static class NameBeanXXX {
                /**
                 * Key : 2052
                 * Value : 北京正丰达旺商贸有限公司
                 */

                private int Key;
                private String Value;

                public int getKey() {
                    return Key;
                }

                public void setKey(int Key) {
                    this.Key = Key;
                }

                public String getValue() {
                    return Value;
                }

                public void setValue(String Value) {
                    this.Value = Value;
                }
            }

            public static class DescriptionBean {
                /**
                 * Key : 2052
                 * Value :
                 */

                private int Key;
                private String Value;

                public int getKey() {
                    return Key;
                }

                public void setKey(int Key) {
                    this.Key = Key;
                }

                public String getValue() {
                    return Value;
                }

                public void setValue(String Value) {
                    this.Value = Value;
                }
            }

            public static class ShortNameBean {
                /**
                 * Key : 2052
                 * Value :
                 */

                private int Key;
                private String Value;

                public int getKey() {
                    return Key;
                }

                public void setKey(int Key) {
                    this.Key = Key;
                }

                public String getValue() {
                    return Value;
                }

                public void setValue(String Value) {
                    this.Value = Value;
                }
            }

            public static class SupplierBaseBean {
                /**
                 * Id : 101885
                 * Country_Id : 46a524cf-5797-4e46-bd0a-7203fc426d9c
                 * Country : {"Id":"46a524cf-5797-4e46-bd0a-7203fc426d9c","FNumber":"China","MultiLanguageText":[{"PkId":"ae6420c4-0e15-4303-aa64-8297b511860c","LocaleId":1033,"FDataValue":"China"},{"PkId":"1001","LocaleId":2052,"FDataValue":"中国"}],"FDataValue":[{"Key":1033,"Value":"China"},{"Key":2052,"Value":"中国"}]}
                 * Provincial_Id :
                 * Provincial : null
                 * Zip :
                 * Language_Id :
                 * Language : null
                 * Address : 北京市海淀区体院路甲2号14楼一层西190室
                 * WebSite :
                 * RegisterAddress : 注册地址
                 * Trade_Id :
                 * Trade : null
                 * CompanyNature_Id :
                 * CompanyNature : null
                 * FoundDate : null
                 * LegalPerson :
                 * RegisterFund : 0
                 * RegisterCode :
                 * TendPermit :
                 * DeptId_Id : 0
                 * DeptId : null
                 * FStaffId_Id : 0
                 * FStaffId : null
                 * SupplierClassify_Id : 5bbaf52d30c87b
                 * SupplierClassify : {"Id":"5bbaf52d30c87b","FNumber":"A","MultiLanguageText":[{"PkId":"5bbaf52d30c87c","LocaleId":2052,"FDataValue":"A"}],"FDataValue":[{"Key":2052,"Value":"A"}]}
                 * CompanyClassify_Id :
                 * CompanyClassify : null
                 * SupplierGrade_Id : 5bbaf85a30c883
                 * SupplierGrade : {"Id":"5bbaf85a30c883","FNumber":"2","MultiLanguageText":[{"PkId":"5bbaf85a30c884","LocaleId":2052,"FDataValue":"合格"}],"FDataValue":[{"Key":2052,"Value":"合格"}]}
                 * StartDate : 2018-12-17T00:00:00
                 * EndDate : 2100-01-01T00:00:00
                 * CompanyScale_Id :
                 * CompanyScale : null
                 * SupplyClassify : CG
                 * SOCIALCRECODE : 统一社会信用代码
                 */

                private int Id;
                private String Country_Id;
                private CountryBean Country;
                private String Provincial_Id;
                private Object Provincial;
                private String Zip;
                private String Language_Id;
                private Object Language;
                private String Address;
                private String WebSite;
                private String RegisterAddress;
                private String Trade_Id;
                private Object Trade;
                private String CompanyNature_Id;
                private Object CompanyNature;
                private Object FoundDate;
                private String LegalPerson;
                private int RegisterFund;
                private String RegisterCode;
                private String TendPermit;
                private int DeptId_Id;
                private Object DeptId;
                private int FStaffId_Id;
                private Object FStaffId;
                private String SupplierClassify_Id;
                private SupplierClassifyBean SupplierClassify;
                private String CompanyClassify_Id;
                private Object CompanyClassify;
                private String SupplierGrade_Id;
                private SupplierGradeBean SupplierGrade;
                private String StartDate;
                private String EndDate;
                private String CompanyScale_Id;
                private Object CompanyScale;
                private String SupplyClassify;
                private String SOCIALCRECODE;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getCountry_Id() {
                    return Country_Id;
                }

                public void setCountry_Id(String Country_Id) {
                    this.Country_Id = Country_Id;
                }

                public CountryBean getCountry() {
                    return Country;
                }

                public void setCountry(CountryBean Country) {
                    this.Country = Country;
                }

                public String getProvincial_Id() {
                    return Provincial_Id;
                }

                public void setProvincial_Id(String Provincial_Id) {
                    this.Provincial_Id = Provincial_Id;
                }

                public Object getProvincial() {
                    return Provincial;
                }

                public void setProvincial(Object Provincial) {
                    this.Provincial = Provincial;
                }

                public String getZip() {
                    return Zip;
                }

                public void setZip(String Zip) {
                    this.Zip = Zip;
                }

                public String getLanguage_Id() {
                    return Language_Id;
                }

                public void setLanguage_Id(String Language_Id) {
                    this.Language_Id = Language_Id;
                }

                public Object getLanguage() {
                    return Language;
                }

                public void setLanguage(Object Language) {
                    this.Language = Language;
                }

                public String getAddress() {
                    return Address;
                }

                public void setAddress(String Address) {
                    this.Address = Address;
                }

                public String getWebSite() {
                    return WebSite;
                }

                public void setWebSite(String WebSite) {
                    this.WebSite = WebSite;
                }

                public String getRegisterAddress() {
                    return RegisterAddress;
                }

                public void setRegisterAddress(String RegisterAddress) {
                    this.RegisterAddress = RegisterAddress;
                }

                public String getTrade_Id() {
                    return Trade_Id;
                }

                public void setTrade_Id(String Trade_Id) {
                    this.Trade_Id = Trade_Id;
                }

                public Object getTrade() {
                    return Trade;
                }

                public void setTrade(Object Trade) {
                    this.Trade = Trade;
                }

                public String getCompanyNature_Id() {
                    return CompanyNature_Id;
                }

                public void setCompanyNature_Id(String CompanyNature_Id) {
                    this.CompanyNature_Id = CompanyNature_Id;
                }

                public Object getCompanyNature() {
                    return CompanyNature;
                }

                public void setCompanyNature(Object CompanyNature) {
                    this.CompanyNature = CompanyNature;
                }

                public Object getFoundDate() {
                    return FoundDate;
                }

                public void setFoundDate(Object FoundDate) {
                    this.FoundDate = FoundDate;
                }

                public String getLegalPerson() {
                    return LegalPerson;
                }

                public void setLegalPerson(String LegalPerson) {
                    this.LegalPerson = LegalPerson;
                }

                public int getRegisterFund() {
                    return RegisterFund;
                }

                public void setRegisterFund(int RegisterFund) {
                    this.RegisterFund = RegisterFund;
                }

                public String getRegisterCode() {
                    return RegisterCode;
                }

                public void setRegisterCode(String RegisterCode) {
                    this.RegisterCode = RegisterCode;
                }

                public String getTendPermit() {
                    return TendPermit;
                }

                public void setTendPermit(String TendPermit) {
                    this.TendPermit = TendPermit;
                }

                public int getDeptId_Id() {
                    return DeptId_Id;
                }

                public void setDeptId_Id(int DeptId_Id) {
                    this.DeptId_Id = DeptId_Id;
                }

                public Object getDeptId() {
                    return DeptId;
                }

                public void setDeptId(Object DeptId) {
                    this.DeptId = DeptId;
                }

                public int getFStaffId_Id() {
                    return FStaffId_Id;
                }

                public void setFStaffId_Id(int FStaffId_Id) {
                    this.FStaffId_Id = FStaffId_Id;
                }

                public Object getFStaffId() {
                    return FStaffId;
                }

                public void setFStaffId(Object FStaffId) {
                    this.FStaffId = FStaffId;
                }

                public String getSupplierClassify_Id() {
                    return SupplierClassify_Id;
                }

                public void setSupplierClassify_Id(String SupplierClassify_Id) {
                    this.SupplierClassify_Id = SupplierClassify_Id;
                }

                public SupplierClassifyBean getSupplierClassify() {
                    return SupplierClassify;
                }

                public void setSupplierClassify(SupplierClassifyBean SupplierClassify) {
                    this.SupplierClassify = SupplierClassify;
                }

                public String getCompanyClassify_Id() {
                    return CompanyClassify_Id;
                }

                public void setCompanyClassify_Id(String CompanyClassify_Id) {
                    this.CompanyClassify_Id = CompanyClassify_Id;
                }

                public Object getCompanyClassify() {
                    return CompanyClassify;
                }

                public void setCompanyClassify(Object CompanyClassify) {
                    this.CompanyClassify = CompanyClassify;
                }

                public String getSupplierGrade_Id() {
                    return SupplierGrade_Id;
                }

                public void setSupplierGrade_Id(String SupplierGrade_Id) {
                    this.SupplierGrade_Id = SupplierGrade_Id;
                }

                public SupplierGradeBean getSupplierGrade() {
                    return SupplierGrade;
                }

                public void setSupplierGrade(SupplierGradeBean SupplierGrade) {
                    this.SupplierGrade = SupplierGrade;
                }

                public String getStartDate() {
                    return StartDate;
                }

                public void setStartDate(String StartDate) {
                    this.StartDate = StartDate;
                }

                public String getEndDate() {
                    return EndDate;
                }

                public void setEndDate(String EndDate) {
                    this.EndDate = EndDate;
                }

                public String getCompanyScale_Id() {
                    return CompanyScale_Id;
                }

                public void setCompanyScale_Id(String CompanyScale_Id) {
                    this.CompanyScale_Id = CompanyScale_Id;
                }

                public Object getCompanyScale() {
                    return CompanyScale;
                }

                public void setCompanyScale(Object CompanyScale) {
                    this.CompanyScale = CompanyScale;
                }

                public String getSupplyClassify() {
                    return SupplyClassify;
                }

                public void setSupplyClassify(String SupplyClassify) {
                    this.SupplyClassify = SupplyClassify;
                }

                public String getSOCIALCRECODE() {
                    return SOCIALCRECODE;
                }

                public void setSOCIALCRECODE(String SOCIALCRECODE) {
                    this.SOCIALCRECODE = SOCIALCRECODE;
                }

                public static class CountryBean {
                    /**
                     * Id : 46a524cf-5797-4e46-bd0a-7203fc426d9c
                     * FNumber : China
                     * MultiLanguageText : [{"PkId":"ae6420c4-0e15-4303-aa64-8297b511860c","LocaleId":1033,"FDataValue":"China"},{"PkId":"1001","LocaleId":2052,"FDataValue":"中国"}]
                     * FDataValue : [{"Key":1033,"Value":"China"},{"Key":2052,"Value":"中国"}]
                     */

                    private String Id;
                    private String FNumber;
                    private List<MultiLanguageTextBeanXXXXX> MultiLanguageText;
                    private List<FDataValueBeanX> FDataValue;

                    public String getId() {
                        return Id;
                    }

                    public void setId(String Id) {
                        this.Id = Id;
                    }

                    public String getFNumber() {
                        return FNumber;
                    }

                    public void setFNumber(String FNumber) {
                        this.FNumber = FNumber;
                    }

                    public List<MultiLanguageTextBeanXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<FDataValueBeanX> getFDataValue() {
                        return FDataValue;
                    }

                    public void setFDataValue(List<FDataValueBeanX> FDataValue) {
                        this.FDataValue = FDataValue;
                    }

                    public static class MultiLanguageTextBeanXXXXX {
                        /**
                         * PkId : ae6420c4-0e15-4303-aa64-8297b511860c
                         * LocaleId : 1033
                         * FDataValue : China
                         */

                        private String PkId;
                        private int LocaleId;
                        private String FDataValue;

                        public String getPkId() {
                            return PkId;
                        }

                        public void setPkId(String PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getFDataValue() {
                            return FDataValue;
                        }

                        public void setFDataValue(String FDataValue) {
                            this.FDataValue = FDataValue;
                        }
                    }

                    public static class FDataValueBeanX {
                        /**
                         * Key : 1033
                         * Value : China
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class SupplierClassifyBean {
                    /**
                     * Id : 5bbaf52d30c87b
                     * FNumber : A
                     * MultiLanguageText : [{"PkId":"5bbaf52d30c87c","LocaleId":2052,"FDataValue":"A"}]
                     * FDataValue : [{"Key":2052,"Value":"A"}]
                     */

                    private String Id;
                    private String FNumber;
                    private List<MultiLanguageTextBeanXXXXXX> MultiLanguageText;
                    private List<FDataValueBeanXX> FDataValue;

                    public String getId() {
                        return Id;
                    }

                    public void setId(String Id) {
                        this.Id = Id;
                    }

                    public String getFNumber() {
                        return FNumber;
                    }

                    public void setFNumber(String FNumber) {
                        this.FNumber = FNumber;
                    }

                    public List<MultiLanguageTextBeanXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<FDataValueBeanXX> getFDataValue() {
                        return FDataValue;
                    }

                    public void setFDataValue(List<FDataValueBeanXX> FDataValue) {
                        this.FDataValue = FDataValue;
                    }

                    public static class MultiLanguageTextBeanXXXXXX {
                        /**
                         * PkId : 5bbaf52d30c87c
                         * LocaleId : 2052
                         * FDataValue : A
                         */

                        private String PkId;
                        private int LocaleId;
                        private String FDataValue;

                        public String getPkId() {
                            return PkId;
                        }

                        public void setPkId(String PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getFDataValue() {
                            return FDataValue;
                        }

                        public void setFDataValue(String FDataValue) {
                            this.FDataValue = FDataValue;
                        }
                    }

                    public static class FDataValueBeanXX {
                        /**
                         * Key : 2052
                         * Value : A
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class SupplierGradeBean {
                    /**
                     * Id : 5bbaf85a30c883
                     * FNumber : 2
                     * MultiLanguageText : [{"PkId":"5bbaf85a30c884","LocaleId":2052,"FDataValue":"合格"}]
                     * FDataValue : [{"Key":2052,"Value":"合格"}]
                     */

                    private String Id;
                    private String FNumber;
                    private List<MultiLanguageTextBeanXXXXXXX> MultiLanguageText;
                    private List<FDataValueBeanXXX> FDataValue;

                    public String getId() {
                        return Id;
                    }

                    public void setId(String Id) {
                        this.Id = Id;
                    }

                    public String getFNumber() {
                        return FNumber;
                    }

                    public void setFNumber(String FNumber) {
                        this.FNumber = FNumber;
                    }

                    public List<MultiLanguageTextBeanXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<FDataValueBeanXXX> getFDataValue() {
                        return FDataValue;
                    }

                    public void setFDataValue(List<FDataValueBeanXXX> FDataValue) {
                        this.FDataValue = FDataValue;
                    }

                    public static class MultiLanguageTextBeanXXXXXXX {
                        /**
                         * PkId : 5bbaf85a30c884
                         * LocaleId : 2052
                         * FDataValue : 合格
                         */

                        private String PkId;
                        private int LocaleId;
                        private String FDataValue;

                        public String getPkId() {
                            return PkId;
                        }

                        public void setPkId(String PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getFDataValue() {
                            return FDataValue;
                        }

                        public void setFDataValue(String FDataValue) {
                            this.FDataValue = FDataValue;
                        }
                    }

                    public static class FDataValueBeanXXX {
                        /**
                         * Key : 2052
                         * Value : 合格
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }
            }

            public static class SupplierBusinessBean {
                /**
                 * Id : 101885
                 * PurchaseGroupId_Id : 0
                 * PurchaseGroupId : null
                 * MinPOValue : 0
                 * NeedConfirm : false
                 * ParentSupplierId_Id : 0
                 * ParentSupplierId : null
                 * BusinessStatus : A
                 * FreezeLimit :
                 * SettleTypeId_Id : 2
                 * SettleTypeId : {"Id":2,"msterID":0,"MultiLanguageText":[{"PkId":2,"LocaleId":2052,"Name":"现金支票"},{"PkId":17,"LocaleId":1033,"Name":"Cash Cheque"}],"Name":[{"Key":2052,"Value":"现金支票"},{"Key":1033,"Value":"Cash Cheque"}],"Number":"JSFS02_SYS","TYPE":"2"}
                 * PriceListID_Id : 0
                 * PriceListID : null
                 * WipStockPlaceId_Id : 0
                 * WipStockPlaceId : null
                 * WipStockId_Id : 0
                 * WipStockId : null
                 * DiscountListId_Id : 0
                 * DiscountListId : null
                 * ProviderId_Id : 164442
                 * ProviderId : {"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"}
                 * FreezeOperator_Id : 0
                 * FreezeOperator : null
                 * FreezeDate : null
                 * VmiStockId_Id : 0
                 * VmiStockId : null
                 * VmiBusiness : false
                 * EnableSL : false
                 * DepositRatio : 0
                 */

                private int Id;
                private int PurchaseGroupId_Id;
                private Object PurchaseGroupId;
                private int MinPOValue;
                private boolean NeedConfirm;
                private int ParentSupplierId_Id;
                private Object ParentSupplierId;
                private String BusinessStatus;
                private String FreezeLimit;
                private int SettleTypeId_Id;
                private SettleTypeIdBean SettleTypeId;
                private int PriceListID_Id;
                private Object PriceListID;
                private int WipStockPlaceId_Id;
                private Object WipStockPlaceId;
                private int WipStockId_Id;
                private Object WipStockId;
                private int DiscountListId_Id;
                private Object DiscountListId;
                private int ProviderId_Id;
                private ProviderIdBean ProviderId;
                private int FreezeOperator_Id;
                private Object FreezeOperator;
                private Object FreezeDate;
                private int VmiStockId_Id;
                private Object VmiStockId;
                private boolean VmiBusiness;
                private boolean EnableSL;
                private int DepositRatio;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getPurchaseGroupId_Id() {
                    return PurchaseGroupId_Id;
                }

                public void setPurchaseGroupId_Id(int PurchaseGroupId_Id) {
                    this.PurchaseGroupId_Id = PurchaseGroupId_Id;
                }

                public Object getPurchaseGroupId() {
                    return PurchaseGroupId;
                }

                public void setPurchaseGroupId(Object PurchaseGroupId) {
                    this.PurchaseGroupId = PurchaseGroupId;
                }

                public int getMinPOValue() {
                    return MinPOValue;
                }

                public void setMinPOValue(int MinPOValue) {
                    this.MinPOValue = MinPOValue;
                }

                public boolean isNeedConfirm() {
                    return NeedConfirm;
                }

                public void setNeedConfirm(boolean NeedConfirm) {
                    this.NeedConfirm = NeedConfirm;
                }

                public int getParentSupplierId_Id() {
                    return ParentSupplierId_Id;
                }

                public void setParentSupplierId_Id(int ParentSupplierId_Id) {
                    this.ParentSupplierId_Id = ParentSupplierId_Id;
                }

                public Object getParentSupplierId() {
                    return ParentSupplierId;
                }

                public void setParentSupplierId(Object ParentSupplierId) {
                    this.ParentSupplierId = ParentSupplierId;
                }

                public String getBusinessStatus() {
                    return BusinessStatus;
                }

                public void setBusinessStatus(String BusinessStatus) {
                    this.BusinessStatus = BusinessStatus;
                }

                public String getFreezeLimit() {
                    return FreezeLimit;
                }

                public void setFreezeLimit(String FreezeLimit) {
                    this.FreezeLimit = FreezeLimit;
                }

                public int getSettleTypeId_Id() {
                    return SettleTypeId_Id;
                }

                public void setSettleTypeId_Id(int SettleTypeId_Id) {
                    this.SettleTypeId_Id = SettleTypeId_Id;
                }

                public SettleTypeIdBean getSettleTypeId() {
                    return SettleTypeId;
                }

                public void setSettleTypeId(SettleTypeIdBean SettleTypeId) {
                    this.SettleTypeId = SettleTypeId;
                }

                public int getPriceListID_Id() {
                    return PriceListID_Id;
                }

                public void setPriceListID_Id(int PriceListID_Id) {
                    this.PriceListID_Id = PriceListID_Id;
                }

                public Object getPriceListID() {
                    return PriceListID;
                }

                public void setPriceListID(Object PriceListID) {
                    this.PriceListID = PriceListID;
                }

                public int getWipStockPlaceId_Id() {
                    return WipStockPlaceId_Id;
                }

                public void setWipStockPlaceId_Id(int WipStockPlaceId_Id) {
                    this.WipStockPlaceId_Id = WipStockPlaceId_Id;
                }

                public Object getWipStockPlaceId() {
                    return WipStockPlaceId;
                }

                public void setWipStockPlaceId(Object WipStockPlaceId) {
                    this.WipStockPlaceId = WipStockPlaceId;
                }

                public int getWipStockId_Id() {
                    return WipStockId_Id;
                }

                public void setWipStockId_Id(int WipStockId_Id) {
                    this.WipStockId_Id = WipStockId_Id;
                }

                public Object getWipStockId() {
                    return WipStockId;
                }

                public void setWipStockId(Object WipStockId) {
                    this.WipStockId = WipStockId;
                }

                public int getDiscountListId_Id() {
                    return DiscountListId_Id;
                }

                public void setDiscountListId_Id(int DiscountListId_Id) {
                    this.DiscountListId_Id = DiscountListId_Id;
                }

                public Object getDiscountListId() {
                    return DiscountListId;
                }

                public void setDiscountListId(Object DiscountListId) {
                    this.DiscountListId = DiscountListId;
                }

                public int getProviderId_Id() {
                    return ProviderId_Id;
                }

                public void setProviderId_Id(int ProviderId_Id) {
                    this.ProviderId_Id = ProviderId_Id;
                }

                public ProviderIdBean getProviderId() {
                    return ProviderId;
                }

                public void setProviderId(ProviderIdBean ProviderId) {
                    this.ProviderId = ProviderId;
                }

                public int getFreezeOperator_Id() {
                    return FreezeOperator_Id;
                }

                public void setFreezeOperator_Id(int FreezeOperator_Id) {
                    this.FreezeOperator_Id = FreezeOperator_Id;
                }

                public Object getFreezeOperator() {
                    return FreezeOperator;
                }

                public void setFreezeOperator(Object FreezeOperator) {
                    this.FreezeOperator = FreezeOperator;
                }

                public Object getFreezeDate() {
                    return FreezeDate;
                }

                public void setFreezeDate(Object FreezeDate) {
                    this.FreezeDate = FreezeDate;
                }

                public int getVmiStockId_Id() {
                    return VmiStockId_Id;
                }

                public void setVmiStockId_Id(int VmiStockId_Id) {
                    this.VmiStockId_Id = VmiStockId_Id;
                }

                public Object getVmiStockId() {
                    return VmiStockId;
                }

                public void setVmiStockId(Object VmiStockId) {
                    this.VmiStockId = VmiStockId;
                }

                public boolean isVmiBusiness() {
                    return VmiBusiness;
                }

                public void setVmiBusiness(boolean VmiBusiness) {
                    this.VmiBusiness = VmiBusiness;
                }

                public boolean isEnableSL() {
                    return EnableSL;
                }

                public void setEnableSL(boolean EnableSL) {
                    this.EnableSL = EnableSL;
                }

                public int getDepositRatio() {
                    return DepositRatio;
                }

                public void setDepositRatio(int DepositRatio) {
                    this.DepositRatio = DepositRatio;
                }

                public static class SettleTypeIdBean {
                    /**
                     * Id : 2
                     * msterID : 0
                     * MultiLanguageText : [{"PkId":2,"LocaleId":2052,"Name":"现金支票"},{"PkId":17,"LocaleId":1033,"Name":"Cash Cheque"}]
                     * Name : [{"Key":2052,"Value":"现金支票"},{"Key":1033,"Value":"Cash Cheque"}]
                     * Number : JSFS02_SYS
                     * TYPE : 2
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private String TYPE;
                    private List<MultiLanguageTextBeanXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public String getTYPE() {
                        return TYPE;
                    }

                    public void setTYPE(String TYPE) {
                        this.TYPE = TYPE;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXX {
                        /**
                         * PkId : 2
                         * LocaleId : 2052
                         * Name : 现金支票
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXX {
                        /**
                         * Key : 2052
                         * Value : 现金支票
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class ProviderIdBean {
                    /**
                     * Id : 164442
                     * msterID : 164442
                     * MultiLanguageText : [{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}]
                     * Name : [{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}]
                     * Number : 11000006
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private List<MultiLanguageTextBeanXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXX {
                        /**
                         * PkId : 101885
                         * LocaleId : 2052
                         * Name : 北京正丰达旺商贸有限公司
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXX {
                        /**
                         * Key : 2052
                         * Value : 北京正丰达旺商贸有限公司
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }
            }

            public static class SupplierFinanceBean {
                /**
                 * Id : 101885
                 * PayCurrencyId_Id : 1
                 * PayCurrencyId : {"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"}
                 * CustomerId_Id : 0
                 * CustomerId : null
                 * PayAdvanceAmount : 0
                 * TaxRegisterCode : 税务登记号
                 * TendType_Id :
                 * TendType : null
                 * FTaxType_Id : 9e855eb97bec43e7b50c3e0e0bf51210
                 * FTaxType : {"Id":"9e855eb97bec43e7b50c3e0e0bf51210","FNumber":"SFL02_SYS","MultiLanguageText":[{"PkId":"6de0d14c-e106-4986-a732-5bbf127ec7dc","LocaleId":1033,"FDataValue":"General Taxpayer"},{"PkId":"7841e26e9e5a4fe6a83e7cdbe0c24175","LocaleId":2052,"FDataValue":"一般纳税人"},{"PkId":"2942b840514242b299738f8b9bbe2095","LocaleId":3076,"FDataValue":"一般納稅人"}],"FDataValue":[{"Key":1033,"Value":"General Taxpayer"},{"Key":2052,"Value":"一般纳税人"},{"Key":3076,"Value":"一般納稅人"}]}
                 * Description :
                 * PayCondition_Id : 159799
                 * PayCondition : {"Id":159799,"MultiLanguageText":[{"PkId":100007,"LocaleId":2052,"Name":"票到90天付款"},{"PkId":100008,"LocaleId":1033,"Name":"Pay after 30 days"}],"Name":[{"Key":2052,"Value":"票到90天付款"},{"Key":1033,"Value":"Pay after 30 days"}],"Number":"003"}
                 * InvoiceType : 1
                 * SettleId_Id : 164442
                 * SettleId : {"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"}
                 * ChargeId_Id : 164442
                 * ChargeId : {"Id":164442,"msterID":164442,"MultiLanguageText":[{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}],"Name":[{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}],"Number":"11000006"}
                 * TaxRateId_Id : 264
                 * TaxRateId : {"Id":264,"msterID":264,"MultiLanguageText":[{"PkId":8,"LocaleId":2052,"Name":"16%增值税"}],"Name":[{"Key":2052,"Value":"16%增值税"}],"Number":"SL31_SYS","TaxRate":16}
                 */

                private int Id;
                private int PayCurrencyId_Id;
                private PayCurrencyIdBean PayCurrencyId;
                private int CustomerId_Id;
                private Object CustomerId;
                private int PayAdvanceAmount;
                private String TaxRegisterCode;
                private String TendType_Id;
                private Object TendType;
                private String FTaxType_Id;
                private FTaxTypeBean FTaxType;
                private String Description;
                private int PayCondition_Id;
                private PayConditionBean PayCondition;
                private String InvoiceType;
                private int SettleId_Id;
                private SettleIdBean SettleId;
                private int ChargeId_Id;
                private ChargeIdBean ChargeId;
                private int TaxRateId_Id;
                private TaxRateIdBean TaxRateId;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getPayCurrencyId_Id() {
                    return PayCurrencyId_Id;
                }

                public void setPayCurrencyId_Id(int PayCurrencyId_Id) {
                    this.PayCurrencyId_Id = PayCurrencyId_Id;
                }

                public PayCurrencyIdBean getPayCurrencyId() {
                    return PayCurrencyId;
                }

                public void setPayCurrencyId(PayCurrencyIdBean PayCurrencyId) {
                    this.PayCurrencyId = PayCurrencyId;
                }

                public int getCustomerId_Id() {
                    return CustomerId_Id;
                }

                public void setCustomerId_Id(int CustomerId_Id) {
                    this.CustomerId_Id = CustomerId_Id;
                }

                public Object getCustomerId() {
                    return CustomerId;
                }

                public void setCustomerId(Object CustomerId) {
                    this.CustomerId = CustomerId;
                }

                public int getPayAdvanceAmount() {
                    return PayAdvanceAmount;
                }

                public void setPayAdvanceAmount(int PayAdvanceAmount) {
                    this.PayAdvanceAmount = PayAdvanceAmount;
                }

                public String getTaxRegisterCode() {
                    return TaxRegisterCode;
                }

                public void setTaxRegisterCode(String TaxRegisterCode) {
                    this.TaxRegisterCode = TaxRegisterCode;
                }

                public String getTendType_Id() {
                    return TendType_Id;
                }

                public void setTendType_Id(String TendType_Id) {
                    this.TendType_Id = TendType_Id;
                }

                public Object getTendType() {
                    return TendType;
                }

                public void setTendType(Object TendType) {
                    this.TendType = TendType;
                }

                public String getFTaxType_Id() {
                    return FTaxType_Id;
                }

                public void setFTaxType_Id(String FTaxType_Id) {
                    this.FTaxType_Id = FTaxType_Id;
                }

                public FTaxTypeBean getFTaxType() {
                    return FTaxType;
                }

                public void setFTaxType(FTaxTypeBean FTaxType) {
                    this.FTaxType = FTaxType;
                }

                public String getDescription() {
                    return Description;
                }

                public void setDescription(String Description) {
                    this.Description = Description;
                }

                public int getPayCondition_Id() {
                    return PayCondition_Id;
                }

                public void setPayCondition_Id(int PayCondition_Id) {
                    this.PayCondition_Id = PayCondition_Id;
                }

                public PayConditionBean getPayCondition() {
                    return PayCondition;
                }

                public void setPayCondition(PayConditionBean PayCondition) {
                    this.PayCondition = PayCondition;
                }

                public String getInvoiceType() {
                    return InvoiceType;
                }

                public void setInvoiceType(String InvoiceType) {
                    this.InvoiceType = InvoiceType;
                }

                public int getSettleId_Id() {
                    return SettleId_Id;
                }

                public void setSettleId_Id(int SettleId_Id) {
                    this.SettleId_Id = SettleId_Id;
                }

                public SettleIdBean getSettleId() {
                    return SettleId;
                }

                public void setSettleId(SettleIdBean SettleId) {
                    this.SettleId = SettleId;
                }

                public int getChargeId_Id() {
                    return ChargeId_Id;
                }

                public void setChargeId_Id(int ChargeId_Id) {
                    this.ChargeId_Id = ChargeId_Id;
                }

                public ChargeIdBean getChargeId() {
                    return ChargeId;
                }

                public void setChargeId(ChargeIdBean ChargeId) {
                    this.ChargeId = ChargeId;
                }

                public int getTaxRateId_Id() {
                    return TaxRateId_Id;
                }

                public void setTaxRateId_Id(int TaxRateId_Id) {
                    this.TaxRateId_Id = TaxRateId_Id;
                }

                public TaxRateIdBean getTaxRateId() {
                    return TaxRateId;
                }

                public void setTaxRateId(TaxRateIdBean TaxRateId) {
                    this.TaxRateId = TaxRateId;
                }

                public static class PayCurrencyIdBean {
                    /**
                     * Id : 1
                     * msterID : 1
                     * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}]
                     * Name : [{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}]
                     * Number : PRE001
                     * Sysmbol : ¥
                     * PriceDigits : 6
                     * AmountDigits : 2
                     * IsShowCSymbol : true
                     * FormatOrder : 1
                     * RoundType : 1
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private String Sysmbol;
                    private int PriceDigits;
                    private int AmountDigits;
                    private boolean IsShowCSymbol;
                    private String FormatOrder;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public String getSysmbol() {
                        return Sysmbol;
                    }

                    public void setSysmbol(String Sysmbol) {
                        this.Sysmbol = Sysmbol;
                    }

                    public int getPriceDigits() {
                        return PriceDigits;
                    }

                    public void setPriceDigits(int PriceDigits) {
                        this.PriceDigits = PriceDigits;
                    }

                    public int getAmountDigits() {
                        return AmountDigits;
                    }

                    public void setAmountDigits(int AmountDigits) {
                        this.AmountDigits = AmountDigits;
                    }

                    public boolean isIsShowCSymbol() {
                        return IsShowCSymbol;
                    }

                    public void setIsShowCSymbol(boolean IsShowCSymbol) {
                        this.IsShowCSymbol = IsShowCSymbol;
                    }

                    public String getFormatOrder() {
                        return FormatOrder;
                    }

                    public void setFormatOrder(String FormatOrder) {
                        this.FormatOrder = FormatOrder;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXX {
                        /**
                         * PkId : 1
                         * LocaleId : 2052
                         * Name : 人民币
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 人民币
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class FTaxTypeBean {
                    /**
                     * Id : 9e855eb97bec43e7b50c3e0e0bf51210
                     * FNumber : SFL02_SYS
                     * MultiLanguageText : [{"PkId":"6de0d14c-e106-4986-a732-5bbf127ec7dc","LocaleId":1033,"FDataValue":"General Taxpayer"},{"PkId":"7841e26e9e5a4fe6a83e7cdbe0c24175","LocaleId":2052,"FDataValue":"一般纳税人"},{"PkId":"2942b840514242b299738f8b9bbe2095","LocaleId":3076,"FDataValue":"一般納稅人"}]
                     * FDataValue : [{"Key":1033,"Value":"General Taxpayer"},{"Key":2052,"Value":"一般纳税人"},{"Key":3076,"Value":"一般納稅人"}]
                     */

                    private String Id;
                    private String FNumber;
                    private List<MultiLanguageTextBeanXXXXXXXXXXX> MultiLanguageText;
                    private List<FDataValueBeanXXXX> FDataValue;

                    public String getId() {
                        return Id;
                    }

                    public void setId(String Id) {
                        this.Id = Id;
                    }

                    public String getFNumber() {
                        return FNumber;
                    }

                    public void setFNumber(String FNumber) {
                        this.FNumber = FNumber;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<FDataValueBeanXXXX> getFDataValue() {
                        return FDataValue;
                    }

                    public void setFDataValue(List<FDataValueBeanXXXX> FDataValue) {
                        this.FDataValue = FDataValue;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXX {
                        /**
                         * PkId : 6de0d14c-e106-4986-a732-5bbf127ec7dc
                         * LocaleId : 1033
                         * FDataValue : General Taxpayer
                         */

                        private String PkId;
                        private int LocaleId;
                        private String FDataValue;

                        public String getPkId() {
                            return PkId;
                        }

                        public void setPkId(String PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getFDataValue() {
                            return FDataValue;
                        }

                        public void setFDataValue(String FDataValue) {
                            this.FDataValue = FDataValue;
                        }
                    }

                    public static class FDataValueBeanXXXX {
                        /**
                         * Key : 1033
                         * Value : General Taxpayer
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class PayConditionBean {
                    /**
                     * Id : 159799
                     * MultiLanguageText : [{"PkId":100007,"LocaleId":2052,"Name":"票到90天付款"},{"PkId":100008,"LocaleId":1033,"Name":"Pay after 30 days"}]
                     * Name : [{"Key":2052,"Value":"票到90天付款"},{"Key":1033,"Value":"Pay after 30 days"}]
                     * Number : 003
                     */

                    private int Id;
                    private String Number;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXX {
                        /**
                         * PkId : 100007
                         * LocaleId : 2052
                         * Name : 票到90天付款
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 票到90天付款
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class SettleIdBean {
                    /**
                     * Id : 164442
                     * msterID : 164442
                     * MultiLanguageText : [{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}]
                     * Name : [{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}]
                     * Number : 11000006
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXX {
                        /**
                         * PkId : 101885
                         * LocaleId : 2052
                         * Name : 北京正丰达旺商贸有限公司
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 北京正丰达旺商贸有限公司
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class ChargeIdBean {
                    /**
                     * Id : 164442
                     * msterID : 164442
                     * MultiLanguageText : [{"PkId":101885,"LocaleId":2052,"Name":"北京正丰达旺商贸有限公司"}]
                     * Name : [{"Key":2052,"Value":"北京正丰达旺商贸有限公司"}]
                     * Number : 11000006
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXX {
                        /**
                         * PkId : 101885
                         * LocaleId : 2052
                         * Name : 北京正丰达旺商贸有限公司
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 北京正丰达旺商贸有限公司
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class TaxRateIdBean {
                    /**
                     * Id : 264
                     * msterID : 264
                     * MultiLanguageText : [{"PkId":8,"LocaleId":2052,"Name":"16%增值税"}]
                     * Name : [{"Key":2052,"Value":"16%增值税"}]
                     * Number : SL31_SYS
                     * TaxRate : 16
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private int TaxRate;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public int getTaxRate() {
                        return TaxRate;
                    }

                    public void setTaxRate(int TaxRate) {
                        this.TaxRate = TaxRate;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 8
                         * LocaleId : 2052
                         * Name : 16%增值税
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 16%增值税
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }
            }

            public static class SupplierBankBean {
                /**
                 * Id : 101399
                 * Country_Id :
                 * Country : null
                 * BankCode : 0200296209200043000
                 * BankHolder : 账户名称
                 * IsDefault : false
                 * Description :
                 * CurrencyId_Id : 1
                 * CurrencyId : {"Id":1,"msterID":1,"MultiLanguageText":[{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}],"Name":[{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}],"Number":"PRE001","Sysmbol":"¥","PriceDigits":6,"AmountDigits":2,"IsShowCSymbol":true,"FormatOrder":"1","RoundType":"1"}
                 * MultiLanguageText : [{"PkId":101397,"LocaleId":2052,"OpenBankName":"工商银行北京西四环支行"}]
                 * OpenBankName : [{"Key":2052,"Value":"工商银行北京西四环支行"}]
                 * BankTypeRec_Id : 0
                 * BankTypeRec : null
                 * OpenAddressRec : 开户银行地址
                 * CNAPS :
                 * SwiftCode :
                 * FTextBankDetail :
                 */

                private int Id;
                private String Country_Id;
                private Object Country;
                private String BankCode;
                private String BankHolder;
                private boolean IsDefault;
                private String Description;
                private int CurrencyId_Id;
                private CurrencyIdBean CurrencyId;
                private int BankTypeRec_Id;
                private Object BankTypeRec;
                private String OpenAddressRec;
                private String CNAPS;
                private String SwiftCode;
                private String FTextBankDetail;
                private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXX> MultiLanguageText;
                private List<OpenBankNameBean> OpenBankName;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getCountry_Id() {
                    return Country_Id;
                }

                public void setCountry_Id(String Country_Id) {
                    this.Country_Id = Country_Id;
                }

                public Object getCountry() {
                    return Country;
                }

                public void setCountry(Object Country) {
                    this.Country = Country;
                }

                public String getBankCode() {
                    return BankCode;
                }

                public void setBankCode(String BankCode) {
                    this.BankCode = BankCode;
                }

                public String getBankHolder() {
                    return BankHolder;
                }

                public void setBankHolder(String BankHolder) {
                    this.BankHolder = BankHolder;
                }

                public boolean isIsDefault() {
                    return IsDefault;
                }

                public void setIsDefault(boolean IsDefault) {
                    this.IsDefault = IsDefault;
                }

                public String getDescription() {
                    return Description;
                }

                public void setDescription(String Description) {
                    this.Description = Description;
                }

                public int getCurrencyId_Id() {
                    return CurrencyId_Id;
                }

                public void setCurrencyId_Id(int CurrencyId_Id) {
                    this.CurrencyId_Id = CurrencyId_Id;
                }

                public CurrencyIdBean getCurrencyId() {
                    return CurrencyId;
                }

                public void setCurrencyId(CurrencyIdBean CurrencyId) {
                    this.CurrencyId = CurrencyId;
                }

                public int getBankTypeRec_Id() {
                    return BankTypeRec_Id;
                }

                public void setBankTypeRec_Id(int BankTypeRec_Id) {
                    this.BankTypeRec_Id = BankTypeRec_Id;
                }

                public Object getBankTypeRec() {
                    return BankTypeRec;
                }

                public void setBankTypeRec(Object BankTypeRec) {
                    this.BankTypeRec = BankTypeRec;
                }

                public String getOpenAddressRec() {
                    return OpenAddressRec;
                }

                public void setOpenAddressRec(String OpenAddressRec) {
                    this.OpenAddressRec = OpenAddressRec;
                }

                public String getCNAPS() {
                    return CNAPS;
                }

                public void setCNAPS(String CNAPS) {
                    this.CNAPS = CNAPS;
                }

                public String getSwiftCode() {
                    return SwiftCode;
                }

                public void setSwiftCode(String SwiftCode) {
                    this.SwiftCode = SwiftCode;
                }

                public String getFTextBankDetail() {
                    return FTextBankDetail;
                }

                public void setFTextBankDetail(String FTextBankDetail) {
                    this.FTextBankDetail = FTextBankDetail;
                }

                public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                    return MultiLanguageText;
                }

                public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                    this.MultiLanguageText = MultiLanguageText;
                }

                public List<OpenBankNameBean> getOpenBankName() {
                    return OpenBankName;
                }

                public void setOpenBankName(List<OpenBankNameBean> OpenBankName) {
                    this.OpenBankName = OpenBankName;
                }

                public static class CurrencyIdBean {
                    /**
                     * Id : 1
                     * msterID : 1
                     * MultiLanguageText : [{"PkId":1,"LocaleId":2052,"Name":"人民币"},{"PkId":8,"LocaleId":1033,"Name":"CNY"}]
                     * Name : [{"Key":2052,"Value":"人民币"},{"Key":1033,"Value":"CNY"}]
                     * Number : PRE001
                     * Sysmbol : ¥
                     * PriceDigits : 6
                     * AmountDigits : 2
                     * IsShowCSymbol : true
                     * FormatOrder : 1
                     * RoundType : 1
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private String Sysmbol;
                    private int PriceDigits;
                    private int AmountDigits;
                    private boolean IsShowCSymbol;
                    private String FormatOrder;
                    private String RoundType;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public String getSysmbol() {
                        return Sysmbol;
                    }

                    public void setSysmbol(String Sysmbol) {
                        this.Sysmbol = Sysmbol;
                    }

                    public int getPriceDigits() {
                        return PriceDigits;
                    }

                    public void setPriceDigits(int PriceDigits) {
                        this.PriceDigits = PriceDigits;
                    }

                    public int getAmountDigits() {
                        return AmountDigits;
                    }

                    public void setAmountDigits(int AmountDigits) {
                        this.AmountDigits = AmountDigits;
                    }

                    public boolean isIsShowCSymbol() {
                        return IsShowCSymbol;
                    }

                    public void setIsShowCSymbol(boolean IsShowCSymbol) {
                        this.IsShowCSymbol = IsShowCSymbol;
                    }

                    public String getFormatOrder() {
                        return FormatOrder;
                    }

                    public void setFormatOrder(String FormatOrder) {
                        this.FormatOrder = FormatOrder;
                    }

                    public String getRoundType() {
                        return RoundType;
                    }

                    public void setRoundType(String RoundType) {
                        this.RoundType = RoundType;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 1
                         * LocaleId : 2052
                         * Name : 人民币
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 人民币
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }

                public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXX {
                    /**
                     * PkId : 101397
                     * LocaleId : 2052
                     * OpenBankName : 工商银行北京西四环支行
                     */

                    private int PkId;
                    private int LocaleId;
                    private String OpenBankName;

                    public int getPkId() {
                        return PkId;
                    }

                    public void setPkId(int PkId) {
                        this.PkId = PkId;
                    }

                    public int getLocaleId() {
                        return LocaleId;
                    }

                    public void setLocaleId(int LocaleId) {
                        this.LocaleId = LocaleId;
                    }

                    public String getOpenBankName() {
                        return OpenBankName;
                    }

                    public void setOpenBankName(String OpenBankName) {
                        this.OpenBankName = OpenBankName;
                    }
                }

                public static class OpenBankNameBean {
                    /**
                     * Key : 2052
                     * Value : 工商银行北京西四环支行
                     */

                    private int Key;
                    private String Value;

                    public int getKey() {
                        return Key;
                    }

                    public void setKey(int Key) {
                        this.Key = Key;
                    }

                    public String getValue() {
                        return Value;
                    }

                    public void setValue(String Value) {
                        this.Value = Value;
                    }
                }
            }

            public static class SupplierLocationBean {
                /**
                 * Id : 100215
                 * TransportDays : 0
                 * PlanDeliveryPeriod : 0
                 * Description :
                 * Number : BIZ20190412093226
                 * Name : 地点名称
                 * Address : 详细地址
                 * IsUsed : true
                 * Tel :
                 * Mobile : 88888888
                 * Fax :
                 * EMail : 51212@qq.com
                 * IsDefaultSupply : false
                 * IsDefaultSettle : false
                 * IsDefaultPayee : false
                 * ContactId_Id : 135670
                 * ContactId : {"Id":135670,"Contact":"张三","Tel":" ","Mobile":"88888888","Fax":" ","EMail":"51212@qq.com","CONTACTNUMBER":"CXR000094"}
                 * NEWCONTACTID_Id : 733333
                 * NEWCONTACTID : {"Id":733333,"msterID":733333,"MultiLanguageText":[{"PkId":100881,"LocaleId":2052,"Name":"张三"}],"Name":[{"Key":2052,"Value":"张三"}],"Number":"CXR000094","Sex_Id":" ","Sex":null,"Post":"测试","Tel":" ","Mobile":"88888888","Fax":" ","Email":"51212@qq.com","BizLocNumber":"BIZ20190412093226"}
                 */

                private int Id;
                private int TransportDays;
                private int PlanDeliveryPeriod;
                private String Description;
                private String Number;
                private String Name;
                private String Address;
                private boolean IsUsed;
                private String Tel;
                private String Mobile;
                private String Fax;
                private String EMail;
                private boolean IsDefaultSupply;
                private boolean IsDefaultSettle;
                private boolean IsDefaultPayee;
                private int ContactId_Id;
                private ContactIdBean ContactId;
                private int NEWCONTACTID_Id;
                private NEWCONTACTIDBean NEWCONTACTID;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public int getTransportDays() {
                    return TransportDays;
                }

                public void setTransportDays(int TransportDays) {
                    this.TransportDays = TransportDays;
                }

                public int getPlanDeliveryPeriod() {
                    return PlanDeliveryPeriod;
                }

                public void setPlanDeliveryPeriod(int PlanDeliveryPeriod) {
                    this.PlanDeliveryPeriod = PlanDeliveryPeriod;
                }

                public String getDescription() {
                    return Description;
                }

                public void setDescription(String Description) {
                    this.Description = Description;
                }

                public String getNumber() {
                    return Number;
                }

                public void setNumber(String Number) {
                    this.Number = Number;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getAddress() {
                    return Address;
                }

                public void setAddress(String Address) {
                    this.Address = Address;
                }

                public boolean isIsUsed() {
                    return IsUsed;
                }

                public void setIsUsed(boolean IsUsed) {
                    this.IsUsed = IsUsed;
                }

                public String getTel() {
                    return Tel;
                }

                public void setTel(String Tel) {
                    this.Tel = Tel;
                }

                public String getMobile() {
                    return Mobile;
                }

                public void setMobile(String Mobile) {
                    this.Mobile = Mobile;
                }

                public String getFax() {
                    return Fax;
                }

                public void setFax(String Fax) {
                    this.Fax = Fax;
                }

                public String getEMail() {
                    return EMail;
                }

                public void setEMail(String EMail) {
                    this.EMail = EMail;
                }

                public boolean isIsDefaultSupply() {
                    return IsDefaultSupply;
                }

                public void setIsDefaultSupply(boolean IsDefaultSupply) {
                    this.IsDefaultSupply = IsDefaultSupply;
                }

                public boolean isIsDefaultSettle() {
                    return IsDefaultSettle;
                }

                public void setIsDefaultSettle(boolean IsDefaultSettle) {
                    this.IsDefaultSettle = IsDefaultSettle;
                }

                public boolean isIsDefaultPayee() {
                    return IsDefaultPayee;
                }

                public void setIsDefaultPayee(boolean IsDefaultPayee) {
                    this.IsDefaultPayee = IsDefaultPayee;
                }

                public int getContactId_Id() {
                    return ContactId_Id;
                }

                public void setContactId_Id(int ContactId_Id) {
                    this.ContactId_Id = ContactId_Id;
                }

                public ContactIdBean getContactId() {
                    return ContactId;
                }

                public void setContactId(ContactIdBean ContactId) {
                    this.ContactId = ContactId;
                }

                public int getNEWCONTACTID_Id() {
                    return NEWCONTACTID_Id;
                }

                public void setNEWCONTACTID_Id(int NEWCONTACTID_Id) {
                    this.NEWCONTACTID_Id = NEWCONTACTID_Id;
                }

                public NEWCONTACTIDBean getNEWCONTACTID() {
                    return NEWCONTACTID;
                }

                public void setNEWCONTACTID(NEWCONTACTIDBean NEWCONTACTID) {
                    this.NEWCONTACTID = NEWCONTACTID;
                }

                public static class ContactIdBean {
                    /**
                     * Id : 135670
                     * Contact : 张三
                     * Tel :
                     * Mobile : 88888888
                     * Fax :
                     * EMail : 51212@qq.com
                     * CONTACTNUMBER : CXR000094
                     */

                    private int Id;
                    private String Contact;
                    private String Tel;
                    private String Mobile;
                    private String Fax;
                    private String EMail;
                    private String CONTACTNUMBER;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public String getContact() {
                        return Contact;
                    }

                    public void setContact(String Contact) {
                        this.Contact = Contact;
                    }

                    public String getTel() {
                        return Tel;
                    }

                    public void setTel(String Tel) {
                        this.Tel = Tel;
                    }

                    public String getMobile() {
                        return Mobile;
                    }

                    public void setMobile(String Mobile) {
                        this.Mobile = Mobile;
                    }

                    public String getFax() {
                        return Fax;
                    }

                    public void setFax(String Fax) {
                        this.Fax = Fax;
                    }

                    public String getEMail() {
                        return EMail;
                    }

                    public void setEMail(String EMail) {
                        this.EMail = EMail;
                    }

                    public String getCONTACTNUMBER() {
                        return CONTACTNUMBER;
                    }

                    public void setCONTACTNUMBER(String CONTACTNUMBER) {
                        this.CONTACTNUMBER = CONTACTNUMBER;
                    }
                }

                public static class NEWCONTACTIDBean {
                    /**
                     * Id : 733333
                     * msterID : 733333
                     * MultiLanguageText : [{"PkId":100881,"LocaleId":2052,"Name":"张三"}]
                     * Name : [{"Key":2052,"Value":"张三"}]
                     * Number : CXR000094
                     * Sex_Id :
                     * Sex : null
                     * Post : 测试
                     * Tel :
                     * Mobile : 88888888
                     * Fax :
                     * Email : 51212@qq.com
                     * BizLocNumber : BIZ20190412093226
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private String Sex_Id;
                    private Object Sex;
                    private String Post;
                    private String Tel;
                    private String Mobile;
                    private String Fax;
                    private String Email;
                    private String BizLocNumber;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public String getSex_Id() {
                        return Sex_Id;
                    }

                    public void setSex_Id(String Sex_Id) {
                        this.Sex_Id = Sex_Id;
                    }

                    public Object getSex() {
                        return Sex;
                    }

                    public void setSex(Object Sex) {
                        this.Sex = Sex;
                    }

                    public String getPost() {
                        return Post;
                    }

                    public void setPost(String Post) {
                        this.Post = Post;
                    }

                    public String getTel() {
                        return Tel;
                    }

                    public void setTel(String Tel) {
                        this.Tel = Tel;
                    }

                    public String getMobile() {
                        return Mobile;
                    }

                    public void setMobile(String Mobile) {
                        this.Mobile = Mobile;
                    }

                    public String getFax() {
                        return Fax;
                    }

                    public void setFax(String Fax) {
                        this.Fax = Fax;
                    }

                    public String getEmail() {
                        return Email;
                    }

                    public void setEmail(String Email) {
                        this.Email = Email;
                    }

                    public String getBizLocNumber() {
                        return BizLocNumber;
                    }

                    public void setBizLocNumber(String BizLocNumber) {
                        this.BizLocNumber = BizLocNumber;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 100881
                         * LocaleId : 2052
                         * Name : 张三
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 张三
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }
            }

            public static class SupplierContactBean {
                /**
                 * Id : 135670
                 * Contact : 张三
                 * FPost : 测试
                 * Tel :
                 * Mobile : 88888888
                 * Fax :
                 * EMail : 51212@qq.com
                 * FIMCode :
                 * IsDefault : false
                 * Description :
                 * CommonContactId_Id : 733333
                 * CommonContactId : {"Id":733333,"msterID":733333,"MultiLanguageText":[{"PkId":100881,"LocaleId":2052,"Name":"张三"}],"Name":[{"Key":2052,"Value":"张三"}],"Number":"CXR000094","Sex_Id":" ","Sex":null,"Post":"测试","Tel":" ","Mobile":"88888888","Fax":" ","Email":"51212@qq.com","BizLocNumber":"BIZ20190412093226"}
                 * ContactNumber : CXR000094
                 * ConForbidStatus : A
                 */

                private int Id;
                private String Contact;
                private String FPost;
                private String Tel;
                private String Mobile;
                private String Fax;
                private String EMail;
                private String FIMCode;
                private boolean IsDefault;
                private String Description;
                private int CommonContactId_Id;
                private CommonContactIdBean CommonContactId;
                private String ContactNumber;
                private String ConForbidStatus;

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getContact() {
                    return Contact;
                }

                public void setContact(String Contact) {
                    this.Contact = Contact;
                }

                public String getFPost() {
                    return FPost;
                }

                public void setFPost(String FPost) {
                    this.FPost = FPost;
                }

                public String getTel() {
                    return Tel;
                }

                public void setTel(String Tel) {
                    this.Tel = Tel;
                }

                public String getMobile() {
                    return Mobile;
                }

                public void setMobile(String Mobile) {
                    this.Mobile = Mobile;
                }

                public String getFax() {
                    return Fax;
                }

                public void setFax(String Fax) {
                    this.Fax = Fax;
                }

                public String getEMail() {
                    return EMail;
                }

                public void setEMail(String EMail) {
                    this.EMail = EMail;
                }

                public String getFIMCode() {
                    return FIMCode;
                }

                public void setFIMCode(String FIMCode) {
                    this.FIMCode = FIMCode;
                }

                public boolean isIsDefault() {
                    return IsDefault;
                }

                public void setIsDefault(boolean IsDefault) {
                    this.IsDefault = IsDefault;
                }

                public String getDescription() {
                    return Description;
                }

                public void setDescription(String Description) {
                    this.Description = Description;
                }

                public int getCommonContactId_Id() {
                    return CommonContactId_Id;
                }

                public void setCommonContactId_Id(int CommonContactId_Id) {
                    this.CommonContactId_Id = CommonContactId_Id;
                }

                public CommonContactIdBean getCommonContactId() {
                    return CommonContactId;
                }

                public void setCommonContactId(CommonContactIdBean CommonContactId) {
                    this.CommonContactId = CommonContactId;
                }

                public String getContactNumber() {
                    return ContactNumber;
                }

                public void setContactNumber(String ContactNumber) {
                    this.ContactNumber = ContactNumber;
                }

                public String getConForbidStatus() {
                    return ConForbidStatus;
                }

                public void setConForbidStatus(String ConForbidStatus) {
                    this.ConForbidStatus = ConForbidStatus;
                }

                public static class CommonContactIdBean {
                    /**
                     * Id : 733333
                     * msterID : 733333
                     * MultiLanguageText : [{"PkId":100881,"LocaleId":2052,"Name":"张三"}]
                     * Name : [{"Key":2052,"Value":"张三"}]
                     * Number : CXR000094
                     * Sex_Id :
                     * Sex : null
                     * Post : 测试
                     * Tel :
                     * Mobile : 88888888
                     * Fax :
                     * Email : 51212@qq.com
                     * BizLocNumber : BIZ20190412093226
                     */

                    private int Id;
                    private int msterID;
                    private String Number;
                    private String Sex_Id;
                    private Object Sex;
                    private String Post;
                    private String Tel;
                    private String Mobile;
                    private String Fax;
                    private String Email;
                    private String BizLocNumber;
                    private List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXX> MultiLanguageText;
                    private List<NameBeanXXXXXXXXXXXXX> Name;

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public int getMsterID() {
                        return msterID;
                    }

                    public void setMsterID(int msterID) {
                        this.msterID = msterID;
                    }

                    public String getNumber() {
                        return Number;
                    }

                    public void setNumber(String Number) {
                        this.Number = Number;
                    }

                    public String getSex_Id() {
                        return Sex_Id;
                    }

                    public void setSex_Id(String Sex_Id) {
                        this.Sex_Id = Sex_Id;
                    }

                    public Object getSex() {
                        return Sex;
                    }

                    public void setSex(Object Sex) {
                        this.Sex = Sex;
                    }

                    public String getPost() {
                        return Post;
                    }

                    public void setPost(String Post) {
                        this.Post = Post;
                    }

                    public String getTel() {
                        return Tel;
                    }

                    public void setTel(String Tel) {
                        this.Tel = Tel;
                    }

                    public String getMobile() {
                        return Mobile;
                    }

                    public void setMobile(String Mobile) {
                        this.Mobile = Mobile;
                    }

                    public String getFax() {
                        return Fax;
                    }

                    public void setFax(String Fax) {
                        this.Fax = Fax;
                    }

                    public String getEmail() {
                        return Email;
                    }

                    public void setEmail(String Email) {
                        this.Email = Email;
                    }

                    public String getBizLocNumber() {
                        return BizLocNumber;
                    }

                    public void setBizLocNumber(String BizLocNumber) {
                        this.BizLocNumber = BizLocNumber;
                    }

                    public List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXX> getMultiLanguageText() {
                        return MultiLanguageText;
                    }

                    public void setMultiLanguageText(List<MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXX> MultiLanguageText) {
                        this.MultiLanguageText = MultiLanguageText;
                    }

                    public List<NameBeanXXXXXXXXXXXXX> getName() {
                        return Name;
                    }

                    public void setName(List<NameBeanXXXXXXXXXXXXX> Name) {
                        this.Name = Name;
                    }

                    public static class MultiLanguageTextBeanXXXXXXXXXXXXXXXXXXX {
                        /**
                         * PkId : 100881
                         * LocaleId : 2052
                         * Name : 张三
                         */

                        private int PkId;
                        private int LocaleId;
                        private String Name;

                        public int getPkId() {
                            return PkId;
                        }

                        public void setPkId(int PkId) {
                            this.PkId = PkId;
                        }

                        public int getLocaleId() {
                            return LocaleId;
                        }

                        public void setLocaleId(int LocaleId) {
                            this.LocaleId = LocaleId;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }
                    }

                    public static class NameBeanXXXXXXXXXXXXX {
                        /**
                         * Key : 2052
                         * Value : 张三
                         */

                        private int Key;
                        private String Value;

                        public int getKey() {
                            return Key;
                        }

                        public void setKey(int Key) {
                            this.Key = Key;
                        }

                        public String getValue() {
                            return Value;
                        }

                        public void setValue(String Value) {
                            this.Value = Value;
                        }
                    }
                }
            }
        }
    }
}