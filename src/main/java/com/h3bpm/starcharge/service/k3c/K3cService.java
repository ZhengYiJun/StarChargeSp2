package com.h3bpm.starcharge.service.k3c;

import com.h3bpm.starcharge.common.bean.RetResult;

/**
 * K3cService
 *
 * @author LLongAgo
 * @date 2019/3/22
 * @since 1.0.0
 */
public interface K3cService {

    /**
     * 金蝶登录接口
     *
     * @param url     接口地址
     * @param content 查询参数
     */
    RetResult login(String url, String content);

    /**
     * 保存接口
     *
     * @param url     接口地址
     * @param cookie  登录cookie
     * @param content json格式参数
     */
    RetResult save(String url, String cookie, String content);

    /**
     * 构造登录参数
     *
     * @param dbid     账套id
     * @param userName 用户名
     * @param password 密码
     * @param lang     语言
     * @return String
     */
    String buildLogin(String dbid, String userName, String password, int lang);

    /**
     * 构造提交、反审核、审核参数
     *
     * @param formId  表单id
     * @param ids     id
     * @param numbers 编码 多个编码以,分隔
     * @param flags   审核标示 多个以,分隔 和编码一一对应
     * @return String
     */
    String buildParam(String formId, String ids, String numbers, String flags);

    /**
     * 构造查看参数
     *
     * @param formId 表单id
     * @param number 编码
     * @param id     id
     * @return String
     */
    String buildViewParam(String formId, String number, String id);

    /**
     * 构造分配参数
     *
     * @param formId 表单id
     * @param orgId  分配组织id
     * @param id     id
     * @return String
     */
    String buildAllocateParam(String formId, String orgId, String id);
}