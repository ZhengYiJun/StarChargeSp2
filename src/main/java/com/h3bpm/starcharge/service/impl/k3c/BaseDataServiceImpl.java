package com.h3bpm.starcharge.service.impl.k3c;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.h3bpm.starcharge.bean.k3c.MaterialBase;
import com.h3bpm.starcharge.bean.k3c.MaterialGroup;
import com.h3bpm.starcharge.bean.k3c.StockLoc;
import com.h3bpm.starcharge.common.bean.BaseResult;
import com.h3bpm.starcharge.common.bean.JsonResult;
import com.h3bpm.starcharge.common.uitl.OkHttpUtil;
import com.h3bpm.starcharge.common.uitl.k3c.K3cUtil;
import com.h3bpm.starcharge.service.k3c.BaseDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 基础数据接口
 *
 * @author LLongAgo
 * @date 2019/3/13
 * @since 1.0.0
 */
@Service
public class BaseDataServiceImpl implements BaseDataService {

    @Autowired
    private K3cUtil util;

    @Value("${searchListJson}")
    private String searchListJson;
    @Value("${unitFormId}")
    private String unitFormId;
    @Value("${materialCateGoryFormId}")
    private String materialCateGoryFormId;
    @Value("${stockFormId}")
    private String stockFormId;
    @Value("${tallyTypeFormId}")
    private String tallyTypeFormId;
    @Value("${organizationFormId}")
    private String organizationFormId;
    @Value("${golangApiUrl}")
    private String golangApiUrl;

    /**
     * 计量单位列表
     * @return JsonResult
     * @throws Exception
     */
    @Override
    public JsonResult<List<BaseResult>> getUnit() throws Exception {
        return getBaseList(unitFormId, "FNAME,FNUMBER", "");
    }

    @Override
    public JsonResult<List<BaseResult>> getMaterialCateGory() throws Exception {
        return getBaseList(materialCateGoryFormId, "FNAME,FNUMBER", "");
    }

    @Override
    public JsonResult<List<BaseResult>> getStock(String useOrgId) throws Exception {
        String filterString = "";
        if (null != useOrgId && !"".equals(useOrgId)) {
            filterString = "FUSEORGID = " + useOrgId;
        }
        return getBaseList(stockFormId, "FNAME,FNUMBER", filterString);
    }

    @Override
    public JsonResult<List<BaseResult>> getTallyType() throws Exception {
        return getBaseList(tallyTypeFormId, "FNAME,FNUMBER", "");
    }

    @Override
    public JsonResult<List<BaseResult>> getOrganizationList(String searchCode) throws Exception {
        if (!"".equals(searchCode)) {
            searchCode = "FName like '%" + searchCode + "%'";
        }
        return getBaseList(organizationFormId, "FName, FOrgID", searchCode);
    }

    @Override
    public JsonResult<List<BaseResult>> getOrganizationBySql(String sqlStr) throws Exception {
        return getBaseList(organizationFormId, "FName, FOrgID", sqlStr);
    }

    private JsonResult<List<BaseResult>> getBaseList(String formId, String fieldKeys, String filterString) throws Exception {
        String formatStr = String.format(searchListJson, formId, fieldKeys, filterString);
        String result = util.getList(formatStr);
        List<String[]> resultList = new Gson().fromJson(result, new TypeToken<List<String[]>>(){}.getType());
        List<BaseResult> baseResultList = new ArrayList<>();
        if (null != resultList && resultList.size() > 0) {
            for (String[] list : resultList) {
                BaseResult base = new BaseResult();
                base.setName(list[0]);
                base.setValue(list[1]);
                baseResultList.add(base);
            }
        }
        JsonResult<List<BaseResult>> jsonResult = new JsonResult<>();
        jsonResult.setData(baseResultList);
        return jsonResult;
    }

    /**
     * 物料分组
     * @param name 分组名字
     * @return JsonResult
     */
    @Override
    public JsonResult<List<MaterialBase>> getMaterialGroup(String name) throws Exception {
        Map<String, String> map = new HashMap<>(2);
        map.put("name", name);
        String json = OkHttpUtil.post(golangApiUrl + "baseData/materialGroup", map);
        Gson gson = new Gson();
        MaterialGroup materialGroup = gson.fromJson(json, MaterialGroup.class);
        List<MaterialGroup.DataBean> datas = materialGroup.getData();
        JsonResult<List<MaterialBase>> result = new JsonResult<>();
        List<MaterialBase> list = new ArrayList<>();
        for (MaterialGroup.DataBean data : datas) {
            MaterialBase base = new MaterialBase();
            base.setName(data.getName());
            String code = "";
            if (!"".equals(data.getOne())) {
                code = data.getOne();
            }
            if (!"".equals(data.getTwo())) {
                code = data.getTwo();
            }
            if (!"".equals(data.getThree())) {
                code = data.getThree();
            }
            if (!"".equals(data.getFour())) {
                code = data.getFour();
            }
            if (!"".equals(data.getFive())) {
                code = data.getFive();
            }
            base.setCode(code);
            base.setId(data.getOne() + data.getTwo() + data.getThree() + data.getFour() + data.getFive());
            list.add(base);
        }
        result.setData(list);
        return result;
    }

    /**
     * 仓位
     * @param number 仓库number
     * @return JsonResult
     */
    @Override
    public JsonResult<List<BaseResult>> getStockLoc(String number) throws Exception {
        Map<String, String> map = new HashMap<>(4);
        map.put("number", number);
        String json = OkHttpUtil.post(golangApiUrl + "baseData/stockLoc", map);
        Gson gson = new Gson();
        StockLoc stockLoc = gson.fromJson(json, StockLoc.class);
        List<StockLoc.DataBean> datas = stockLoc.getData();
        JsonResult<List<BaseResult>> result = new JsonResult<>();
        List<BaseResult> list = new ArrayList<>();
        if (null != datas && datas.size() > 0) {
            for (StockLoc.DataBean data : datas) {
                BaseResult baseResult = new BaseResult();
                baseResult.setName(data.getName());
                baseResult.setValue(data.getNumber());
                list.add(baseResult);
            }
        }
        result.setData(list);
        return result;
    }
}