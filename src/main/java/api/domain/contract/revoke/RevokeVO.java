package api.domain.contract.revoke;

public class RevokeVO {

    private String revokeReason;

    public RevokeVO(String revokeReason) {
        this.revokeReason = revokeReason;
    }

    public String getRevokeReason() {
        return revokeReason;
    }

    public void setRevokeReason(String revokeReason) {
        this.revokeReason = revokeReason;
    }

    @Override
    public String toString() {
        return "RevokeVO{" +
                "revokeReason='" + revokeReason + '\'' +
                '}';
    }
}
