package com.h3bpm.starcharge.controller.k3c;

import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.Instance.Data.IInstanceDataItem;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.starcharge.common.bean.RestfulErrorCode;
import com.h3bpm.starcharge.common.bean.RetResponse;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.ErrorUtil;
import com.h3bpm.starcharge.ret.k3c.K3cBase;
import com.h3bpm.starcharge.service.k3c.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.ResourceAccessException;

/**
 * 客户控制器
 *
 * @author LLongAgo
 * @date 2019/4/9
 * @since 1.0.0
 */
@Controller
@RequestMapping("/k3c/customer")
public class CustomerController extends ControllerBase {

    @Autowired
    private CustomerService customerService;

    @Override
    public String getFunctionCode() {
        return null;
    }

    /**
     * 新增客户
     * @param instanceId    instanceId
     * @return RetResult
     */
    @RequestMapping(value = "/save", produces = "application/json;charset=utf-8")
    @ResponseBody
    public RetResult save(@RequestParam String instanceId) {
        try {
            InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            RetResult retResult = customerService.save(instanceData);
            if (!RestfulErrorCode.SUCCESS.getCode().equals(retResult.getCode())) {
                instanceData.getItem("errMsg").setValue(retResult.getMsg());
            } else {
                K3cBase data = (K3cBase) retResult.getData();
                instanceData.getItem("customerCode").setValue(data.getNumber());
            }
            instanceData.Submit();
            return retResult;
        } catch (Exception e) {
            try {
                InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
                if (e instanceof ResourceAccessException) {
                    instanceData.getItem("errMsg").setValue("k3c连接超时，请联系管理员处理！");
                } else {
                    instanceData.getItem("errMsg").setValue(e.getMessage());
                }
                instanceData.Submit();
            } catch (Exception ide) {
                return ErrorUtil.generalException(ide);
            }
            return ErrorUtil.generalException(e);
        }
    }

    /**
     * 修改客户
     * @param instanceId    instanceId
     * @return RetResult
     */
    @RequestMapping(value = "/edit", produces = "application/json;charset=utf-8")
    @ResponseBody
    public RetResult edit(@RequestParam String instanceId) {
        try {
            InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            RetResult retResult = customerService.edit(instanceData);
            if (!RestfulErrorCode.SUCCESS.getCode().equals(retResult.getCode())) {
                instanceData.getItem("errMsg").setValue(retResult.getMsg());
            } else {
                instanceData.getItem("errMsg").setValue("修改成功");
            }
            instanceData.Submit();
            return RetResponse.makeOKRsp();
        } catch (Exception e) {
            try {
                InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
                if (e instanceof ResourceAccessException) {
                    instanceData.getItem("errMsg").setValue("k3c连接超时，请联系管理员处理！");
                } else {
                    instanceData.getItem("errMsg").setValue(e.getMessage());
                }
                instanceData.Submit();
            } catch (Exception ide) {
                return ErrorUtil.generalException(ide);
            }
            return ErrorUtil.generalException(e);
        }
    }

    /**
     * 客户详情
     * @param number    String
     * @return RetResult
     */
    @RequestMapping(value = "/info", produces = "application/json;charset=utf-8")
    @ResponseBody
    public RetResult info(@RequestParam String number) throws Exception {
        return customerService.info(number);
    }
}