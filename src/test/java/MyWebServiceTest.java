import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.encoding.TypeMapping;
import javax.xml.rpc.encoding.TypeMappingRegistry;
import javax.xml.rpc.encoding.XMLType;

import OThinker.H3.Portal.webservices.ActivityInfoService;
import OThinker.H3.Portal.webservices.Entity.WebServiceResult;
import OThinker.H3.Portal.webservices.impl.ActivityInfoServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.h3bpm.base.util.AppUtility;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.ser.BeanDeserializerFactory;
import org.apache.axis.encoding.ser.BeanSerializerFactory;
import org.apache.commons.collections.map.HashedMap;

import OThinker.H3.Portal.webservices.Entity.BPMServiceResult;


public class MyWebServiceTest {

    public static void main(String[] args) throws IOException {
       //startWorkFlow();
        //CancelToken();
        //reStartWorkFlow();
    	//startWorkFlow();
        writeLog();
       // getAllactivity();
    }


    public static void getAllactivity(){
        ActivityInfoService activityInfoService = new ActivityInfoServiceImpl();
        try {
            WebServiceResult webServiceResult = activityInfoService.GetAllActivityInfo("H3","Authine","8e06b575-3f0c-478f-8d90-5bf19b39c0ef");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void writeLog() {
        URL endpoint = null;
        try {
            endpoint = new URL("http://10.9.35.56:21113/Portal/WebServices/ActivityInfoService?wsdl");
            Service service = new Service();
            Call call;
            call = (Call) service.createCall();
            call.setTargetEndpointAddress(endpoint);
            //命名空间及方法名
            call.setOperationName(new QName("http://aozhe.com/", "GetAllActivityInfo"));
            //入参信息 详见该webservice xml文档
            call.addParameter("systemCode", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("secret", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("instanceId", XMLType.SOAP_STRING, ParameterMode.IN);

            //注册返回值    指定的对象
            String link = "http://schemas.xmlsoap.org/soap/encoding/";
            String  nameSpace = "http://aozhe.com/";
            TypeMappingRegistry tmr = service.getTypeMappingRegistry();
            TypeMapping tm = tmr.createTypeMapping();
            tm.register(BPMServiceResult.class,new QName(nameSpace, "bpmServiceResult"),
                    new BeanSerializerFactory(BPMServiceResult.class,new QName(nameSpace,"bpmServiceResult")),
                    new BeanDeserializerFactory(BPMServiceResult.class, new QName(nameSpace,"bpmServiceResult")));
            tmr.register(link,tm);

            //设置返回值
            call.setReturnType(new QName(nameSpace, "bpmServiceResult"));
            //此为 本公司 code和secret 一般在后台管理--单点登陆 添加其他公司 信息
            String systemCode = "cloud";
            String secret = "cloud";

            String instanceId = "8e06b575-3f0c-478f-8d90-5bf19b39c0ef";

            BPMServiceResult returnObj = (BPMServiceResult)call.invoke(new Object[]{systemCode,secret,instanceId});

            System.out.println(returnObj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void cancalInstance() {
        URL endpoint = null;
        Boolean returnObj = false;
        try {
            endpoint = new URL("http://172.16.32.130:12134/Portal/WebServices/BPMService?wsdl");
            Service service = new Service();
            Call call;
            call = (Call) service.createCall();
            call.setTargetEndpointAddress(endpoint);
            //命名空间及方法名
            call.setOperationName(new QName("http://aozhe.com/", "CancelInstance"));
            //入参信息 详见该webservice xml文档
            call.addParameter("systemCode", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("secret", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("instanceId", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("userId", XMLType.SOAP_STRING, ParameterMode.IN);

            //设置返回值
            call.setReturnType(XMLType.SOAP_BOOLEAN);
            //此为 本公司 code和secret 一般在后台管理--单点登陆 添加其他公司 信息
            String systemCode = "H3";
            String secret = "Authine";
            //流程Id
            String instanceId = "4694ebcd-4e2a-4b18-93ca-5e529c893840";
            //提交任务人id
            String userId = "14583";

            returnObj = (Boolean) call.invoke(new Object[]{systemCode,secret,instanceId,userId});

            System.out.println(returnObj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void startWorkFlow() throws MalformedURLException {
    	
    	String paramStr = "{\r\n" + 
    			"	\"creditServiceChargeProfit\": 10,\r\n" + 
    			"	\"dZFinanceCarSalesNo\": 1111111,\r\n" + 
    			"	\"dZFinanceCreditTarget\": 30,\r\n" + 
    			"	\"dZFinancePermeability\": 20,\r\n" + 
    			"	\"effectiveSalesMonTarget\": 12,\r\n" + 
    			"	\"extendNo\": 70,\r\n" + 
    			"	\"financeCarSalesNo\": 10,\r\n" + 
    			"	\"financePermeability\": 70,\r\n" + 
    			"	\"monthsKpi\": 0,\r\n" + 
    			"	\"motorTypePurchaseTarget\": [{\r\n" + 
    			"		\"MonKpi\": 10,\r\n" + 
    			"		\"MonKpis\": 1010,\r\n" + 
    			"		\"MotorSeriesName\": \"string\",\r\n" + 
    			"		\"MotorTypeName\": \"string\"\r\n" + 
    			"	}],\r\n" + 
    			"	\"newCarExtendRate\": 10,\r\n" + 
    			"	\"newCarInsureCommissionProfit\": 4,\r\n" + 
    			"	\"newCarInsureNo\": 1110,\r\n" + 
    			"	\"newCarInsureRate\": 1230,\r\n" + 
    			"	\"newCarSingleCommissionProfit\": 10,\r\n" + 
    			"	\"newPotentialCusMonTarget\": 2,\r\n" + 
    			"	\"newSalesNo\": 50,\r\n" + 
    			"	\"otherSalesIncome\": 123,\r\n" + 
    			"	\"otherSalesProfit\": 124,\r\n" + 
    			"	\"salesDelicateIncome\": 30,\r\n" + 
    			"	\"salesDelicateRate\": 40,\r\n" + 
    			"	\"salesDelicateSingleIncome\": 60,\r\n" + 
    			"	\"salesDelicateSingleProfit\": 50,\r\n" + 
    			"	\"salesExtendProfit\": 10,\r\n" + 
    			"	\"salesNewOrderTarget\": [{\r\n" + 
    			"		\"monKpi\": 10,\r\n" + 
    			"		\"potentialCustomersNo\": 10,\r\n" + 
    			"		\"salesName\": \"5123\"\r\n" + 
    			"	}],\r\n" + 
    			"	\"salesNo\": 50,\r\n" + 
    			"	\"salesOrderPrice\": 60,\r\n" + 
    			"	\"salesOrderRevenue\": 40,\r\n" + 
    			"	\"salesOrderTarget\": [{\r\n" + 
    			"		\"monKpi\": 50,\r\n" + 
    			"		\"potentialCustomersNo\": 60,\r\n" + 
    			"		\"salesName\": \"string\"\r\n" + 
    			"	}],\r\n" + 
    			"	\"sgCreditServiceChargeProfit\": 10,\r\n" + 
    			"	\"singleSalesExtendProfit\": 10\r\n" + 
    			"}";
    	
    	
    	
        Map<String, Object> parmartMap = new HashedMap();
      /*  //构造子表
        List<Map<String, Object>> ZBparamValues = new ArrayList<>();
        Map<String, Object> zbparmartMap = new HashedMap();
        
        zbparmartMap.put("carportName", "汽车城C区");
        zbparmartMap.put("exteriorColorName", "流金棕");
        zbparmartMap.put("interiorColorName", "棕色真皮");
        zbparmartMap.put("vehicleCommonName", "2017款 Q70L 2.5L 手自一体 精英版");
        zbparmartMap.put("vinCode", "JNKCY11E2HM800210");
        ZBparamValues.add(zbparmartMap);
        //构造主表
        parmartMap.put("createdByName", "王维");
        parmartMap.put("orderState", "待确认");
        parmartMap.put("outCount", 1);
        parmartMap.put("outOrdNo", "ZCK115429595971742");
        parmartMap.put("outType", "销售出库");
        parmartMap.put("stockOutDetailDtos", ZBparamValues);

*/
        
        parmartMap.put("circulant", "11418,1141611");//多传阅人
        parmartMap.put("cir", "11418");//单传阅人
        parmartMap.put("orgCir", "057081,1590191111");//多传阅组织
        parmartMap.put("orgSingle", "043027");//单传阅组织
        
        List<Map<String,Object>> list = new ArrayList<>();
        Map<String,Object> childrenTable = new HashMap<>();
        childrenTable.put("cirulant", "11418,11416");//多传阅人
        childrenTable.put("sinCir", "11418");//单传阅人
        childrenTable.put("orgCir", "057081,159019");//多传阅组织
        childrenTable.put("orgSingle", "043027");//单传阅组织
        list.add(childrenTable);
        parmartMap.put("childrenTable", list);
        
        
        ObjectMapper objectMapper = new ObjectMapper();

        URL endpoint = new URL("http://172.17.1.95:12134/Portal/WebServices/BPMService?wsdl");
        Service service = new Service();
        Call call;
        try {
            call = (Call) service.createCall();
            call.setTargetEndpointAddress(endpoint);
            //命名空间及方法名
            call.setOperationName(new QName("http://aozhe.com/", "StartWorkflowNew"));
            //入参信息 详见该webservice xml文档
            call.addParameter("systemCode", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("secret", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("workflowCode", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("userCode", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("finishStart", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("paramValues", XMLType.SOAP_STRING, ParameterMode.IN);

            //注册返回值    指定的对象
            String link = "http://schemas.xmlsoap.org/soap/encoding/";
            String  nameSpace = "http://aozhe.com/";
            TypeMappingRegistry tmr = service.getTypeMappingRegistry();
            TypeMapping tm = tmr.createTypeMapping();
            tm.register(BPMServiceResult.class,new QName(nameSpace, "bpmServiceResult"),
                    new BeanSerializerFactory(BPMServiceResult.class,new QName(nameSpace,"bpmServiceResult")),
                    new BeanDeserializerFactory(BPMServiceResult.class, new QName(nameSpace,"bpmServiceResult")));
            tmr.register(link,tm);

            //设置返回值
            call.setReturnType(new QName(nameSpace, "bpmServiceResult"));
            //此为 本公司 code和secret 一般在后台管理--单点登陆 添加其他公司 信息
            String systemCode = "H3";
            String secret = "Authine";
            //流程模板
            String workflowCode = "TESTFORC";//"OA_QT_NMDMBZSP";
            //发起者
            String userCode = "14454";
            //是否结束发起阶段
            String finishStart = "true";
            //参数
            //String paramValues = paramStr;//objectMapper.writeValueAsString(parmartMap);

            String paramValues = objectMapper.writeValueAsString(parmartMap);
            
            BPMServiceResult returnObj = (BPMServiceResult)call.invoke(new Object[]{systemCode, secret, workflowCode, userCode, finishStart, paramValues});
            System.out.println(returnObj.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Boolean sumbitWorkItem() {
        URL endpoint = null;
        Boolean returnObj = false;
        try {
            endpoint = new URL("http://172.16.32.130:12134/Portal/WebServices/BPMService?wsdl");
            Service service = new Service();
            Call call;
            call = (Call) service.createCall();
            call.setTargetEndpointAddress(endpoint);
            //命名空间及方法名
            call.setOperationName(new QName("http://aozhe.com/", "SubmitWorkItem"));
            //入参信息 详见该webservice xml文档
            call.addParameter("systemCode", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("secret", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("userId", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("workItemId", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("commentText", XMLType.SOAP_STRING, ParameterMode.IN);


            //设置返回值
            call.setReturnType(XMLType.SOAP_BOOLEAN);
            //此为 本公司 code和secret 一般在后台管理--单点登陆 添加其他公司 信息
            String systemCode = "H3";
            String secret = "Authine";
            //提交任务人id
            String userId = "8453e831-41fa-47b6-8e1c-8f754bf7e4cf";
            //待办Id
            String workItemId = "d4742b0c-04e1-4b9e-b8aa-9e36a0e6ee77";
            //审批意见
            String commentText = "";
             returnObj = (Boolean) call.invoke(new Object[]{systemCode, secret, userId, workItemId, commentText});

            System.out.println(returnObj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnObj;
    }


    public static Boolean CancelToken() {
        URL endpoint = null;
        Boolean returnObj = false;
        try {
            endpoint = new URL("http://172.16.32.130:12134/Portal/WebServices/BPMService?wsdl");
            Service service = new Service();
            Call call;
            call = (Call) service.createCall();
            call.setTargetEndpointAddress(endpoint);
            //命名空间及方法名
            call.setOperationName(new QName("http://aozhe.com/", "CancelToken"));
            //入参信息 详见该webservice xml文档
            call.addParameter("systemCode", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("secret", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("instanceId", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("activityCode", XMLType.SOAP_STRING, ParameterMode.IN);


            //设置返回值
            call.setReturnType(XMLType.SOAP_BOOLEAN);
            //此为 本公司 code和secret 一般在后台管理--单点登陆 添加其他公司 信息
            String systemCode = "H3";
            String secret = "Authine";
            //提交任务人id
            String instanceId = "96c1d9ca-6ca8-4404-aa20-b081f87cddc7";
            //待办Id
            String activityCode = "ZTQC_Activity2";

            returnObj = (Boolean) call.invoke(new Object[]{systemCode, secret, instanceId, activityCode});
            System.out.println(returnObj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnObj;
    }



    public static void reStartWorkFlow() throws MalformedURLException {


        Map<String, Object> parmartMap = new HashedMap();
        //构造子表
        List<Map<String, Object>> ZBparamValues = new ArrayList<>();
        Map<String, Object> zbparmartMap = new HashedMap();
        zbparmartMap.put("fitCode", "fitCode23");
        zbparmartMap.put("fitName", "fitName23");
        zbparmartMap.put("specProperties", "specProperties23");
        zbparmartMap.put("model", "model23");
        zbparmartMap.put("saleUnitPrice", "11123");
        zbparmartMap.put("fitCount", "12123");

        ZBparamValues.add(zbparmartMap);

        zbparmartMap.put("fitCode", "fitCode223");
        zbparmartMap.put("fitName", "fitName223");
        zbparmartMap.put("specProperties", "specProperties223");
        zbparmartMap.put("model", "model223");
        zbparmartMap.put("saleUnitPrice", "111223");
        zbparmartMap.put("fitCount", "121223");

        ZBparamValues.add(zbparmartMap);
        //构造主表
        parmartMap.put("orderCode", "orderCode133");
        parmartMap.put("createdOn", "2018-11-23 05:48:02");
        parmartMap.put("createdByName", "createdByName13");
        parmartMap.put("ordState", "ordState133");
        parmartMap.put("orderType", "orderType133");
        parmartMap.put("cstName", "cstName133");
        parmartMap.put("totalAmount", "121233");
        parmartMap.put("orderDetails", ZBparamValues);


        ObjectMapper objectMapper = new ObjectMapper();

        URL endpoint = new URL("http://172.16.32.130:12134/Portal/WebServices/BPMService?wsdl");
        Service service = new Service();
        Call call;
        try {
            call = (Call) service.createCall();
            call.setTargetEndpointAddress(endpoint);
            //命名空间及方法名
            call.setOperationName(new QName("http://aozhe.com/", "ReStartWorkflowNew"));
            //入参信息 详见该webservice xml文档
            call.addParameter("systemCode", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("secret", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("workflowCode", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("userCode", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("instanceId", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("workItemId", XMLType.SOAP_STRING, ParameterMode.IN);
            call.addParameter("paramValues", XMLType.SOAP_STRING, ParameterMode.IN);

            //注册返回值    指定的对象
            String link = "http://schemas.xmlsoap.org/soap/encoding/";
            String  nameSpace = "http://aozhe.com/";
            TypeMappingRegistry tmr = service.getTypeMappingRegistry();
            TypeMapping tm = tmr.createTypeMapping();
            tm.register(BPMServiceResult.class,new QName(nameSpace, "bpmServiceResult"),
                    new BeanSerializerFactory(BPMServiceResult.class,new QName(nameSpace,"bpmServiceResult")),
                    new BeanDeserializerFactory(BPMServiceResult.class, new QName(nameSpace,"bpmServiceResult")));
            tmr.register(link,tm);

            //设置返回值
            call.setReturnType(new QName(nameSpace, "bpmServiceResult"));
            //此为 本公司 code和secret 一般在后台管理--单点登陆 添加其他公司 信息
            String systemCode = "H3";
            String secret = "Authine";
            //流程模板
            String workflowCode = "OA_PJ_PJXSDDSP";
            //发起者
            String userCode = "8525";
            //流程id   mq中有
            String instanceId = "6dee709b-e605-4731-87de-1767a34e29fa";
            //当前任务id  驳回任务mq中有
            String workItemId = "ca57f6cf-8131-45c0-8e68-d55901ad1c3e";
            //参数
            String paramValues = objectMapper.writeValueAsString(parmartMap);

            BPMServiceResult returnObj = (BPMServiceResult)call.invoke(new Object[]{systemCode, secret, workflowCode, userCode, instanceId,workItemId,paramValues});
            System.out.println(returnObj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
