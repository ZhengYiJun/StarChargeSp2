package com.h3bpm.starcharge.controller.k3c;

import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.DataModel.BizObject;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import com.h3bpm.starcharge.common.bean.JsonResult;
import com.h3bpm.starcharge.common.bean.RetResponse;
import com.h3bpm.starcharge.common.bean.RetResult;
import com.h3bpm.starcharge.common.uitl.ErrorUtil;
import com.h3bpm.starcharge.service.k3c.K3cMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 物料控制器
 *
 * @author LLongAgo
 * @date 2019/3/4
 * @since 1.0.0
 */
@Controller
@RequestMapping("/k3c/material")
public class MaterialController extends ControllerBase {

    @Autowired
    private K3cMaterialService k3cMaterialService;

    @Override
    public String getFunctionCode() {
        return null;
    }

    /**
     * 批量保存物料
     *
     * @return JsonResult
     */
    @RequestMapping(value = "/batchSave", produces = "application/json;charset=utf-8")
    @ResponseBody
    public RetResult save(@RequestParam String instanceId) {
        try {
            InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            return k3cMaterialService.save(instanceData);
        } catch (Exception e) {
            return ErrorUtil.generalException(e);
        }
    }

    /**
     * 批量维护物料
     *
     * @param instanceId instanceId
     * @param tableId    子表id
     * @return JsonResult
     */
    @RequestMapping(value = "/batchMaintenance", produces = "application/json;charset=utf-8")
    @ResponseBody
    public RetResult batchMaintenance(@RequestParam String instanceId, @RequestParam String tableId) {
        try {
            InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            return k3cMaterialService.maintenance(instanceData, tableId);
        } catch (Exception e) {
            return ErrorUtil.generalException(e);
        }
    }

    /**
     * 反审核物料
     *
     * @param instanceId instanceId
     * @param tableId    子表id
     * @return JsonResult
     */
    @RequestMapping(value = "/unAudit", produces = "application/json;charset=utf-8")
    @ResponseBody
    public JsonResult unAudit(@RequestParam String instanceId, @RequestParam String tableId) {
        try {
            InstanceData instanceData = new InstanceData(getEngine(), instanceId, User.AdministratorID);
            HashMap<String, Object> valueTable = instanceData.getBizObject().getValueTable();
            BizObject[] zbBizObjects = (BizObject[]) valueTable.get(tableId);
            // 获取id物料id
            StringBuilder sb = new StringBuilder();
            for (BizObject bizObj : zbBizObjects) {
                Map<String, Object> zbValueTable = bizObj.getValueTable();
                sb.append(zbValueTable.get("materialId")).append(",");
            }
            String ids = sb.toString();
            ids = ids.substring(0, ids.length() - 1);
            // 反审核
            return k3cMaterialService.unAuditMaterial(ids);
        } catch (Exception e) {
            return ErrorUtil.packageServerError(e);
        }
    }

    /**
     * 根据code查询物料
     *
     * @param materialCode 物料代码
     * @param useOrgId     使用组织id
     * @return JsonResult
     */
    @RequestMapping(value = "/list", produces = "application/json;charset=utf-8")
    @ResponseBody
    public JsonResult getList(String materialCode, String useOrgId) {
        try {
            return k3cMaterialService.getList(materialCode, useOrgId);
        } catch (Exception e) {
            return ErrorUtil.packageServerError(e);
        }
    }

    /**
     * 根据id查询物料详情
     *
     * @param id 物料id
     * @return JsonResult
     */
    @RequestMapping(value = "/info", produces = "application/json;charset=utf-8")
    @ResponseBody
    public JsonResult getInfo(String id) {
        try {
            return k3cMaterialService.getInfo(id);
        } catch (Exception e) {
            return ErrorUtil.packageServerError(e);
        }
    }
}