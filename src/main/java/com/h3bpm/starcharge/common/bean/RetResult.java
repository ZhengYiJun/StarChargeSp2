package com.h3bpm.starcharge.common.bean;

/**
 * RetResult
 *
 * @author LLongAgo
 * @date 2019/3/22
 * @since 1.0.0
 */
public class RetResult {
    public String code;

    private String msg;

    private Object data;

    public RetResult setCode(RestfulErrorCode retCode) {
        this.code = retCode.getCode();
        return this;
    }

    public String getCode() {
        return code;
    }

    public RetResult setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public RetResult setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Object getData() {
        return data;
    }

    public RetResult setData(Object data) {
        this.data = data;
        return this;
    }
}