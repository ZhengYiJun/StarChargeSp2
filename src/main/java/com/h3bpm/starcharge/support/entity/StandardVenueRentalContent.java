package com.h3bpm.starcharge.support.entity;

import com.h3bpm.starcharge.support.core.BasicContractsField;
import com.h3bpm.starcharge.support.core.annontation.ESealRequestParam;

import java.util.List;
import java.util.Map;

public class StandardVenueRentalContent extends BasicContractsField {

    @ESealRequestParam(eSealField = "所有权", bpmField = "Ownership")
    private String ownership;

    @ESealRequestParam(eSealField = "处分权", bpmField = "ROD")
    private String rOD;

    @ESealRequestParam(eSealField = "使用权", bpmField = "Usufruct")
    private String usufruct;

    @ESealRequestParam(eSealField = "收益权", bpmField = "IncomeRight")
    private String incomeRight;

    @ESealRequestParam(eSealField = "转租权", bpmField = "SubleaseRight")
    private String subleaseRight;

    @ESealRequestParam(eSealField = "租赁物类型", bpmField = "LeaseholdType")
    private String leaseholdType;

    @ESealRequestParam(eSealField = "所有权人", bpmField = "Owner")
    private String owner;

    @ESealRequestParam(eSealField = "坐落地址", bpmField = "Address")
    private String address;

    @ESealRequestParam(eSealField = "面积", bpmField = "EarthArea")
    private String earthArea;

    @ESealRequestParam(eSealField = "租用面积", bpmField = "RentArea")
    private String rentArea;

    @ESealRequestParam(eSealField = "存在权益的限制", bpmField = "EquityFlag")
    private String equityFlag;

    @ESealRequestParam(eSealField = "数量", bpmField = "RentQty")
    private String rentQty;

    @ESealRequestParam(eSealField = "合同开始时间", bpmField = "RentStart")
    private String rentStart;

    @ESealRequestParam(eSealField = "合同结束时间", bpmField = "RentEnd")
    private String rentEnd;

    @ESealRequestParam(eSealField = "合同有效期", bpmField = "RentYear")
    private String rentYear;

    @ESealRequestParam(eSealField = "租金", bpmField = "RentCost")
    private String rentCost;

    @ESealRequestParam(eSealField = "大写", bpmField = "RentCostZHCN")
    private String rentCostZHCN;

    @ESealRequestParam(eSealField = "水费单价", bpmField = "WaterUP")
    private String waterUP;

    @ESealRequestParam(eSealField = "电费单价", bpmField = "ElectronicUP")
    private String electronicUP;

    @ESealRequestParam(eSealField = "物业费单价", bpmField = "PropertyUP")
    private String propertyUP;

    @ESealRequestParam(eSealField = "配电容量", bpmField = "Capacitance")
    private String capacitance;

    @ESealRequestParam(
            table = true,
            tableName = "StandardVenueRentalPile",
            eSealColumnFields = {"产品名称", "规格型号", "数量", "单位"},
            bpmColumnFields = {"pileName", "pileSpec", "pileQty", "pileGunQty"})
    private List<Map<String, String>> standardRentalSalePile;

    @ESealRequestParam(eSealField = "其他", bpmField = "specialAgreement")
    private String specialAgreement;

}
