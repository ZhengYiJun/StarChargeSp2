package com.h3bpm.starcharge.common.uitl.bestsign;

import OThinker.Common.Data.BoolMatchValue;
import OThinker.Common.DotNetToJavaStringHelper;
import OThinker.Common.Organization.Models.User;
import OThinker.H3.Controller.ControllerBase;
import OThinker.H3.Entity.Data.Attachment.Attachment;
import OThinker.H3.Entity.Data.Attachment.AttachmentHeader;
import OThinker.H3.Entity.Instance.Data.InstanceData;
import OThinker.H3.Entity.Instance.InstanceContext;
import api.APIController;
import api.domain.contract.create.*;
import com.h3bpm.base.util.AppUtility;
import com.h3bpm.starcharge.bean.SealApply;
import com.h3bpm.starcharge.common.uitl.BestsignPropertiesUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author zhengyj
 * @date

 */
public class CreateContractParam extends ControllerBase {

    private static final String KEYWORKONE = "盖章处";

    private static final String KEYWORKTWO = "签章处";

    private static final String ACCOUNT = BestsignPropertiesUtil.getProperty("account");


    @Override
    public String getFunctionCode() {
        return null;
    }

    /**
     *
     * @param instanceData
     * @return
     */
    public static CreateContractVO createParam(InstanceData instanceData) {
        CreateContractVO createContractVO = new CreateContractVO();
        try {
            createContractVO = create(instanceData);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return createContractVO;
    }

    public static CreateContractVO create(InstanceData instanceData) {
        CreateContractVO createContractVO = new CreateContractVO();

        try {

            InstanceContext instanceContext = instanceData.getInstanceContext();
            Map<String, Object> paramTables = instanceData.getBizObject().getValueTable();
            List<AttachmentHeader> headers = AppUtility.getEngine().getBizObjectManager().QueryAttachment(
                    instanceData.getSchemaCode(),instanceData.getBizObject().getObjectID(), SealApply.Attachments.name(), BoolMatchValue.True, "");
            //构造文件
            //遍历所有
            List<CreateDocumentVO> documents = new ArrayList<>();
            for (int i = 0 ; i < headers.size(); i++) {
                AttachmentHeader header = headers.get(i);
                Attachment attachment = AppUtility.getEngine().getBizObjectManager().GetAttachment(User.AdministratorID,
                        instanceData.getSchemaCode(),
                        instanceData.getBizObject().getObjectID(),
                        header.getObjectID());
                CreateDocumentVO documentVO = new CreateDocumentVO();
                documentVO.setFileName(header.getFileName());
                documentVO.setContent(APIController.getBase64Content(attachment.getContent()));
                documentVO.setOrder(i);
                documents.add(documentVO);
                //只取第一个
                break;
            }
            createContractVO.setDocuments(documents);

            // 构造基本信息
            createContractVO.setContractDescription(instanceContext.getInstanceName());
            createContractVO.setContractTitle(instanceContext.getInstanceName());
            createContractVO.setSignDeadline(new Date(System.currentTimeMillis()+ 864000000));
            createContractVO.setSignOrdered(false);

            List<CreateReceiverVO> receivers = new ArrayList<>();

            //构造 盖章人one
            CreateReceiverVO receiverVOOne = new CreateReceiverVO();
            receiverVOOne.setUserAccount(ACCOUNT);
            receiverVOOne.setEnterpriseName((String)paramTables.get("OurUnitName"));
            receiverVOOne.setUserType(SignFlowConstants.UserType.ENTERPRISE);
            receiverVOOne.setReceiverType(SignFlowConstants.ReceiverType.SIGNER);
            receiverVOOne.setRouteOrder(1);
            //构造 盖章位置
            CalculatePosition calculatePositionOne = new CalculatePosition(KEYWORKONE,documents);
            receiverVOOne.setLabels(APIController.calculatePositions(calculatePositionOne));
            receivers.add(receiverVOOne);

            //构造 盖章人two
            if (!DotNetToJavaStringHelper.isNullOrEmpty((String)paramTables.get("OurUnitNameTwo"))) {
                CreateReceiverVO receiverVOTwo = new CreateReceiverVO();
                receiverVOTwo.setUserAccount(ACCOUNT);
                receiverVOTwo.setEnterpriseName((String)paramTables.get("OurUnitNameTwo"));
                receiverVOTwo.setUserType(SignFlowConstants.UserType.ENTERPRISE);
                receiverVOTwo.setReceiverType(SignFlowConstants.ReceiverType.SIGNER);
                receiverVOTwo.setRouteOrder(1);
                //构造 盖章位置
                CalculatePosition calculatePositionTwo = new CalculatePosition(KEYWORKTWO,documents);
                receiverVOTwo.setLabels(APIController.calculatePositions(calculatePositionTwo));
                receivers.add(receiverVOTwo);
            }
            createContractVO.setReceivers(receivers);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return createContractVO;
    }


}
