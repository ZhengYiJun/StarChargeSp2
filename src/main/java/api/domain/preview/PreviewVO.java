package api.domain.preview;

import api.domain.template.create.LabelVO;

import java.util.List;

public class PreviewVO {

    private String templateId;

    private List<LabelVO> textLabels;


    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public List<LabelVO> getTextLabels() {
        return textLabels;
    }

    public void setTextLabels(List<LabelVO> textLabels) {
        this.textLabels = textLabels;
    }

    @Override
    public String toString() {
        return "PreviewVO{" +
                "templateId='" + templateId + '\'' +
                ", textLabels=" + textLabels +
                '}';
    }
}
