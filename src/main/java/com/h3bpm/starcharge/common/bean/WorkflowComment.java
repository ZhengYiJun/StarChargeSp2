package com.h3bpm.starcharge.common.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @description workflow 评论对象
 * @author qinyt
 * @date 2019/02/28
 */
@JsonInclude(Include.NON_NULL) 
public class WorkflowComment {
    /**
     * 主键
     */
    private String id;

    /**
     * workItem Id
     */
    private String workItemId;

    /**
     * 表单编码
     */
    private String sheetCode;

    /**
     * 流程实例id
     */
    private String instanceId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论用户的Id
     */
    private String userId;

    /**
     * 评论用户的名字/昵称
     */
    private String userName;

    /**
     * 评论用户的头像
     */
    private String userAvator;
    
    /**
     * 需要发送通知消息的用户列表
     */
    private String notifyUserIds;
    
    /**
     * 是否需要把评论内容通过聊天形式转发给通知列表的用户。
     * 如果需要通知的用户列表为空，则该值无意义。
     */
    private boolean needCC;

    /**
     * 评论时间
     */
    private Date gmtCreate;

    /**
     * 评论更新时间
     */
    private Long gmtModified;

    /**
     * 当前评论上传的附件/图片
     */
    private List<WorkflowCommentAttachment> attachments;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the workItemId
     */
    public String getWorkItemId() {
        return workItemId;
    }

    /**
     * @param workItemId
     *            the workItemId to set
     */
    public void setWorkItemId(String workItemId) {
        this.workItemId = workItemId;
    }

    /**
     * @return the sheetCode
     */
    public String getSheetCode() {
        return sheetCode;
    }

    /**
     * @param sheetCode
     *            the sheetCode to set
     */
    public void setSheetCode(String sheetCode) {
        this.sheetCode = sheetCode;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     *            the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userAvator
     */
    public String getUserAvator() {
        return userAvator;
    }

    /**
     * @param userAvator
     *            the userAvator to set
     */
    public void setUserAvator(String userAvator) {
        this.userAvator = userAvator;
    }

    /**
     * @return the notifyUserIds
     */
    public String getNotifyUserIds() {
        return notifyUserIds;
    }

    /**
     * @param notifyUserIds the notifyUserIds to set
     */
    public void setNotifyUserIds(String notifyUserIds) {
        this.notifyUserIds = notifyUserIds;
    }

    /**
     * @return the needCC
     */
    public boolean isNeedCC() {
        return needCC;
    }

    /**
     * @param needCC the needCC to set
     */
    public void setNeedCC(boolean needCC) {
        this.needCC = needCC;
    }

    /**
     * @return the gmtCreate
     */
    @JsonFormat(pattern = "MM-dd HH:mm",timezone="GMT+8")
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * @return the gmtModified
     */
    public Long getGmtModified() {
        return gmtModified;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public void setGmtModified(Long gmtModified) {
        this.gmtModified = gmtModified;
    }

    /**
     * @return the attachments
     */
    public List<WorkflowCommentAttachment> getAttachments() {
        return attachments;
    }

    /**
     * @param attachments
     *            the attachments to set
     */
    public void setAttachments(List<WorkflowCommentAttachment> attachments) {
        this.attachments = attachments;
    }

}